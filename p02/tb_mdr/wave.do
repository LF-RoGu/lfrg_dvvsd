onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_mdr/itf/clk
add wave -noupdate /tb_mdr/itf/op
add wave -noupdate /tb_mdr/itf/data
add wave -noupdate /tb_mdr/itf/result
add wave -noupdate /tb_mdr/itf/remainder
add wave -noupdate /tb_mdr/itf/load
add wave -noupdate /tb_mdr/itf/start
add wave -noupdate /tb_mdr/itf/load_x
add wave -noupdate /tb_mdr/itf/load_y
add wave -noupdate /tb_mdr/itf/error
add wave -noupdate /tb_mdr/itf/ready
add wave -noupdate /tb_mdr/uut/dut/control/booth_control/current_st
add wave -noupdate /tb_mdr/uut/dut/control/booth_control/nxt_state
add wave -noupdate /tb_mdr/uut/dut/control/booth_control/load
add wave -noupdate /tb_mdr/uut/dut/control/booth_control/loadX
add wave -noupdate /tb_mdr/uut/dut/control/booth_control/loadY
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {29974 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {174298 ps}
