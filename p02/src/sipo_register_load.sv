
module sipo_reg_load
#(
    parameter DW = 10
) 
(
	 input                   clk,											
	 input                   rst,						
    input    logic          dir,
    input    logic          load,
    input    logic          enable,
    output   logic          load_flag,
    input	 logic [DW-1:0] data_in,   //16-bit
    output	 logic [DW-1:0] data_out   //16-bit
);

logic [DW-1:0] dout_fsm_sipo_w;
logic [DW-1:0] dout_sipo_w;
logic [$clog2(DW)-1:0]    count_w;
logic reg_enb_w;
logic ovf_counter_sipo_w;
logic din_siso_w;
logic dout_siso_w;
logic siso_enable_w;
logic counter_enable_w;
logic load_flag_w;
logic [DW-1:0] sipo_data_in_w;

fsm_load_reg fsm_load
(
    .clk(clk),
    .rst(rst),
    .load(load),
    .ovf(ovf_counter_sipo_w),
    .counter_enb(counter_enable_w),
    .reg_enb(reg_enb_w),
    .load_flag(load_flag_w)
);

bin_counter #(
    .DW($clog2(DW)),
    .OVF_VAL(DW)
)   cycles_count
(
    .clk(clk),
    .rst(rst),
    .enb(counter_enable_w),
    .ovf(ovf_counter_sipo_w),
    .count(count_w)
);

sipo_register #(
	.DW(DW)
)   sipo_shift_register
(
	.clk(clk),
    .rst(rst),
    .dir(dir),
	.enable(reg_enb_w || enable),
	.data_in(sipo_data_in_w[0]),
	.data_out(data_out)
);

assign sipo_data_in_w = data_in >> count_w;
assign siso_enable_w = (ovf_counter_sipo_w == 1'b0)? 1'b1:1'b0;
assign load_flag = load_flag_w;

endmodule