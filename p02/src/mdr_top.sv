/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: sequential_multiplier
    Description: sequential_multiplier source file
    Last modification: 13/03/2021
*/

import p02_pkg::*;

module mdr_top
(
	input               clk,
	input               rst,
	input  logic        start,
	input  op_selectr_e op_sel,
	input  data_n_t     data,
	input  logic		load,
	output logic 		loadX,
	output logic		loadY,
	output data_2n_t    result,
	output data_2n_t    remainder,
	output logic 		error,
	output logic        ready
);

/*
 * Wires
 */
data_n_t register_data_a_D, register_data_a_Q, register_data_b_D, register_data_b_Q;
data_n_t register_sqrt_w;

data_2n1_t booth_alu_data_a_w, div_alu_data_a_w, sqrt_alu_data_a_w;data_2n1_t booth_alu_data_b_w, div_alu_data_b_w, sqrt_alu_data_b_w;

data_2n1_t alu_mux_data_a_w, alu_mux_data_b_w;
data_2n1_t alu_res_w; data_2n_t booth_alu_res_w, div_alu_res_w, sqrt_alu_res_w;
	
	
data_n_t alu_op_w;

data_2n1_t booth_mux_register_product, div_mux_register_product, sqrt_mux_register_product;
data_2n1_t booth_mux_register_remainder, div_mux_register_remainder, sqrt_mux_register_remainder;
	
data_2n1_t register_product_D, register_product_Q;
data_2n_t register_remainder_D, register_remainder_Q;

logic enb_register_product_w, enb_register_remainder_w;

logic mux_data_sel;
selectr_e demux_data_sel, op_res_w;
	
op_selectr_e op_sel_w;
logic error_booth, error_div, error_sqrt;

data_n_t comp2_dataA_w;
data_n_t comp2_dataB_w;
logic dataA_sign_w;
logic dataB_sign_w;

data_2n1_t booth_product, div_product, sqrt_product;
data_2n1_t booth_remainder, div_remainder, sqrt_remainder;
/** SQRT*/
data_n_t counter_jx;
logic enb_Rnx_w, enb_Qnx_w, enb_aluOP_w;

data_n_t sqrt_dataA, sqrt_dataB;

data_n_t sqrt_Rnx_D, sqrt_Rnx_Q;
data_n_t sqrt_Qnx_D, sqrt_Qnx_Q;
data_n_t sqrt_Dnx_D, sqrt_Dnx_Q;

data_n_t mux_Rnx_i_a, mux_Rnx_i_b, mux_Rnx_o; selectr_e mux_Rnx_i_sel;
data_n_t mux_Qnx_i_a, mux_Qnx_i_b, mux_Qnx_o; selectr_e mux_Qnx_i_sel;
data_n_t mux_dataC_i_a, mux_dataC_i_b, mux_dataC_o; selectr_e mux_dataC_i_sel;
	
data_n_t sqrt_mux_aluA_i_a, sqrt_mux_aluA_i_b, sqrt_mux_aluA_o; selectr_e sqrt_mux_aluA_i_sel;
data_n_t sqrt_mux_aluB_i_a, sqrt_mux_aluB_i_b, sqrt_mux_aluB_o; selectr_e sqrt_mux_aluB_i_sel;
data_n_t sqrt_mux_aluOP_i_a, sqrt_mux_aluOP_i_b, sqrt_mux_aluOP_o; selectr_e sqrt_mux_aluOP_i_sel;

/** BOOTH*/
data_2n1_t  register_Anx_D, register_Anx_Q;
data_2n1_t  register_Snx_D, register_Snx_Q;
data_2n1_t  register_Pnx_D, register_Pnx_Q;
data_2n1_t  alu_res;

data_2n1_t  mux_Pnx_i_a, mux_Pnx_i_bt, mux_Pnx_i_b, mux_Pnx_o; selectr_e mux_Pnx_sel;
	
logic ovf_w;
logic counter_enb_w;
logic ready_w1, ready_w2;
logic Pnx_enb;
logic Anx_enb;
logic Snx_enb;
	
data_2n1_t booth_mux_alu_dataB;
data_n_t booth_mux_aluOP_dataA, booth_mux_aluOP_dataB, booth_mux_aluOP_o;
selectr_e booth_mux_aluOP_sel;
	
/** DIVIDER*/
data_n_t div_dividend_w, div_divisor_w;
data_2n1_t mux_div_alu_data_a_w, mux_div_alu_data_b_w;
	
alu_op_sel_str div_mux_aluOP_o;

/*
 * Modules
 */
/********************************************************************************************************/
/** SYSTEM **/
/********************************************************************************************************/

assign error_booth = ( ((register_data_a_Q > 'd128) && (register_data_b_Q > 'd128)) && (op_sel == MULTI) ) ? (1'b1):(1'b0);
assign error_div   = ( (register_data_b_Q < 'd1) && (op_sel == DIV) ) ? (1'b1):(1'b0);
assign error_sqrt  = ( (register_data_a_Q[p02_pkg::N-1] == 1'b1) && (op_sel == SQRT) ) ? (1'b1):(1'b0);

assign error = (error_booth) || (error_div) || (error_sqrt);

assign op_sel_w = ( (error_booth) || (error_div) || (error_sqrt) ) ? (ERROR) : (op_sel);

fsm_control
control
(
	.clk(clk),
	.rst(rst),
	.start(start),
	.op_sel(op_sel),
	.ready(ready_w1),
	.count(counter_jx),
	/** BOOTH*/
	.Anx_enb(Anx_enb),
	.Snx_enb(Snx_enb),
	.Pnx_enb(Pnx_enb),
	/** SQRT*/
	.sqrt_enb_Rnx(enb_Rnx_w),
	.sqrt_enb_Qnx(enb_Qnx_w),
	.sqrt_enb_aluOP(enb_aluOP_w)
);
data_selectr
data_selector_data
(
	.clk(clk),
	.rst(rst),
	.enb(load),
	.o_sltd(mux_data_sel)
);

assign demux_data_sel = (1'b1 == mux_data_sel) ? (OP_A ):(OP_B);

pipo_register
#(
	.DW(p02_pkg::N)
)
register_dataA
(
	.clk(clk),
	.rst(rst),
	.enb( (load && (demux_data_sel == OP_A)) ? (1'b1):(1'b0) ),
	.i_data((data)),
	.o_data(register_data_a_Q)
);

comp2bin
#(
	.DW(p02_pkg::N)
)
comp2_dataA
(	
	.i_data(register_data_a_Q),
	.o_data(comp2_dataA_w),
	.sign(dataA_sign_w)
);

pipo_register
#(
	.DW(p02_pkg::N)
)
register_dataB
(
	.clk(clk),
	.rst(rst),
	.enb( (load && (demux_data_sel == OP_B)) ? (1'b1):(1'b0) ),
	.i_data(data),
	.o_data(register_data_b_Q)
);

comp2bin
#(
	.DW(p02_pkg::N)
)
comp2_dataB
(	
	.i_data(register_data_b_Q),
	.o_data(comp2_dataB_w),
	.sign(dataB_sign_w)
);

/** demux_dataA **/
assign register_Anx_D = comp2_dataA_w;
assign register_sqrt_w = comp2_dataA_w;
assign div_dividend_w = comp2_dataA_w;

/** demux_dataB **/
assign register_Snx_D = comp2_dataB_w;
assign div_divisor_w = comp2_dataB_w;
	
/********************************************************************************************************/
/** ALU **/
/********************************************************************************************************/
assign booth_alu_data_a_w = register_product_Q;
assign div_alu_data_a_w = mux_div_alu_data_a_w;
assign sqrt_alu_data_a_w = sqrt_mux_aluA_o;
mux_4_1 
#(
	.DW(2*p02_pkg::N+1)
)	
mux_alu_data_a
(
	.i_a(booth_alu_data_a_w),
	.i_b(div_alu_data_a_w),
	.i_c(sqrt_alu_data_a_w),
	.i_d(),
	.i_sel(selectr_e'(op_sel_w)),
	.o_sltd(alu_mux_data_a_w)
);

assign booth_alu_data_b_w = booth_mux_alu_dataB;
assign div_alu_data_b_w = mux_div_alu_data_b_w;
assign sqrt_alu_data_b_w = sqrt_mux_aluB_o;
mux_4_1 
#(
	.DW(2*p02_pkg::N+1)
)	
mux_alu_data_b
(
	.i_a(booth_alu_data_b_w),
	.i_b(div_alu_data_b_w),
	.i_c(sqrt_alu_data_b_w),
	.i_d(),
	.i_sel(selectr_e'(op_sel_w)),
	.o_sltd(alu_mux_data_b_w)
);

arithmetic_logic_unit 
#(
	.DW(2*p02_pkg::N+1)
)
adder
(
	.A(alu_mux_data_a_w),
	.B(alu_mux_data_b_w),
	.op_sel(alu_op_sel_str'(alu_op_w)),
	.result(alu_res_w)
);

mux_4_1 
#(
	.DW(p02_pkg::N)
)	
mux_alu_op
(
	.i_a(data_n_t'( (ready) ? (NO_OP):(booth_mux_aluOP_o) )), 
	.i_b(data_n_t'(div_mux_aluOP_o)),
	.i_c(data_n_t'(sqrt_mux_aluOP_o)),
	.i_d(data_n_t'(NO_OP)),
	.i_sel(selectr_e'(op_sel_w)),
	.o_sltd((alu_op_w))
);
/********************************************************************************************************/
/** ALU **/
/********************************************************************************************************/


/** demux_alu_res **/
assign booth_alu_res_w = alu_res_w;
assign div_alu_res_w = alu_res_w;
assign sqrt_alu_res_w = alu_res_w;
/********************************************************************************************************/
/** SYSTEM **/
/********************************************************************************************************/

/********************************************************************************************************/
/** BOOTH **/
/********************************************************************************************************/
assign register_Anx_Q = {register_Anx_D, (p02_pkg::N)'('b0)};
assign register_Snx_Q = {register_Anx_D, (p02_pkg::N+1)'('b0)};


assign mux_Pnx_i_a = {((p02_pkg::N)-1)'('b0), register_Snx_D, 1'b0};
assign mux_Pnx_i_b =  {alu_res_w[(2*p02_pkg::N)], alu_res_w[(2*p02_pkg::N):1]};
assign mux_Pnx_sel = (start == 'd1) ? (OP_A):(OP_B);
mux_2_1
#(
	.DW(2*p02_pkg::N+1)
)
booth_mux_Pnx
(
	.i_a(mux_Pnx_i_a),
	.i_b(mux_Pnx_i_b), 
	.i_sel(mux_Pnx_sel),
	.o_sltd(mux_Pnx_o)
);
	
assign register_Pnx_D = mux_Pnx_o;
	
assign booth_mux_aluOP_dataA = (register_product_Q[1:0] == 'd2) ? (data_n_t'(SUBSTRACTION)):(data_n_t'(ADDITION));
assign booth_mux_aluOP_dataB = NO_OP;
assign booth_mux_aluOP_sel = (register_product_Q[1] == register_product_Q[0]) ? (OP_B):(OP_A);
mux_2_1 
booth_mux_aluOP
(
	.i_a(booth_mux_aluOP_dataA),
	.i_b(booth_mux_aluOP_dataB), 
	.i_sel(booth_mux_aluOP_sel), //Assign con un & para checar si son iguales o no
	.o_sltd(booth_mux_aluOP_o)
);
	
mux_2_1
#(
	.DW((2*p02_pkg::N)+2)
)
booth_mux_dataB
(
	.i_a(register_Snx_Q),
	.i_b(register_Anx_Q), 
	.i_sel(booth_mux_aluOP_sel),
	.o_sltd(booth_mux_alu_dataB)
);
/********************************************************************************************************/
/** BOOTH **/
/********************************************************************************************************/

/********************************************************************************************************/
/** DIVISOR **/
/********************************************************************************************************/
seq_divider divider
(
   .clk(clk),
   .rst(rst),
   .start(start),
   .dividend(div_dividend_w),  
   .divisor(div_divisor_w),
   .alu_result(alu_res_w), 
   .alu_a(mux_div_alu_data_a_w),
   .alu_b(mux_div_alu_data_b_w),
   .alu_op(div_mux_aluOP_o),  
   .quotient(div_mux_register_product),
   .remainder(div_mux_register_remainder),
   .ready(ready_w2)
);
/********************************************************************************************************/
/** DIVISOR **/
/********************************************************************************************************/

/********************************************************************************************************/
/** SQRT **/
/********************************************************************************************************/

assign sqrt_Dnx_Q = register_sqrt_w;
	
assign mux_Rnx_i_a = data_n_t'(sqrt_alu_res_w);
assign mux_Rnx_i_b = data_n_t'('b0);
assign mux_Rnx_i_sel = (enb_Rnx_w == 1'b1) ? (OP_A):(OP_B);
//assign mux_Rnx_o = data_n_t'(sqrt_Rnx_D);
mux_2_1 	
sqrt_mux_Rnx
(
	.i_a(mux_Rnx_i_a),
	.i_b(mux_Rnx_i_b),
	.i_sel(mux_Rnx_i_sel),
	.o_sltd(mux_Rnx_o)
);

assign mux_dataC_i_a = data_n_t'((sqrt_Qnx_Q << 'd2) | 'd1);
assign mux_dataC_i_b = data_n_t'((sqrt_Qnx_Q << 'd2) | 'd3);
assign mux_dataC_i_sel = (sqrt_Rnx_Q < 'd127) ? (OP_A):(OP_B);
mux_2_1 
sqrt_mux_dataC
(
	.i_a( mux_dataC_i_a ),
	.i_b( mux_dataC_i_b ),
	.i_sel( mux_dataC_i_sel ),
	.o_sltd( mux_dataC_o )
);

assign sqrt_dataA = (sqrt_Rnx_Q << 'd2);
assign sqrt_dataB = (sqrt_Dnx_Q >> counter_jx) & 'd3;

assign sqrt_mux_aluA_i_a = data_n_t'(sqrt_dataA | sqrt_dataB);
assign sqrt_mux_aluA_i_b = data_n_t'(sqrt_Rnx_Q);
assign sqrt_mux_aluA_i_sel = (ready == 'd0) ? (OP_A):(OP_B);
mux_2_1 
sqrt_mux_alu_dataA
(
	.i_a( sqrt_mux_aluA_i_a ),
	.i_b( sqrt_mux_aluA_i_b ),
	.i_sel( sqrt_mux_aluA_i_sel ),
	.o_sltd( sqrt_mux_aluA_o )
);

assign sqrt_mux_aluB_i_a = data_n_t'(mux_dataC_o);
assign sqrt_mux_aluB_i_b = data_n_t'('b0);
assign sqrt_mux_aluB_i_sel = (ready == 'd0) ? (OP_A):(OP_B);
mux_2_1 
sqrt_mux_alu_dataB
(
	.i_a( sqrt_mux_aluB_i_a ),
	.i_b( sqrt_mux_aluB_i_b ),
	.i_sel( sqrt_mux_aluB_i_sel ),
	.o_sltd( sqrt_mux_aluB_o )
);

assign sqrt_mux_aluOP_i_a = data_n_t'((sqrt_Rnx_Q < 'd127) ? (SUBSTRACTION):(ADDITION));
assign sqrt_mux_aluOP_i_b = data_n_t'(ADDITION);
assign sqrt_mux_aluOP_i_sel = (enb_aluOP_w == 'd1) ? (OP_A):(OP_B);
//assign sqrt_mux_aluOP_o = data_n_t'(alu_op_w);
mux_2_1 
sqrt_mux_alu_op
(
	.i_a( sqrt_mux_aluOP_i_a ),
	.i_b( sqrt_mux_aluOP_i_b ),
	.i_sel( sqrt_mux_aluOP_i_sel ),
	.o_sltd( sqrt_mux_aluOP_o )
);

assign mux_Qnx_i_a = data_n_t'( (counter_jx < 2*(p02_pkg::N)) ? ((sqrt_Qnx_Q << 'd1) | 'd1):('b0) );
assign mux_Qnx_i_b = data_n_t'((sqrt_Qnx_Q << 'd1) | 'd0) ;
assign mux_Qnx_i_sel = (mux_Rnx_o < 'd127) ? (OP_A):(OP_B);
//assign mux_Qnx_o = data_n_t'(sqrt_Qnx_D);
mux_2_1 
sqrt_mux_Qnx
(
	.i_a( mux_Qnx_i_a ),
	.i_b( mux_Qnx_i_b ),
	.i_sel( mux_Qnx_i_sel ),
	.o_sltd( mux_Qnx_o )
);
	
/** Recover signals from general system **/	
assign sqrt_Qnx_Q = register_product_Q;
assign sqrt_Rnx_Q = register_remainder_Q;
/********************************************************************************************************/
/** SQRT **/
/********************************************************************************************************/

/********************************************************************************************************/
/** SYSTEM **/
/********************************************************************************************************/
assign booth_mux_register_product = mux_Pnx_o;
//assign div_mux_register_product = 'b0;
assign sqrt_mux_register_product = mux_Qnx_o;
/** PRODUCT **/
mux_4_1 
#(
	.DW((2*p02_pkg::N)+2)
)
mux_register_product
(
	.i_a(booth_mux_register_product),
	.i_b(div_mux_register_product),
	.i_c(sqrt_mux_register_product),
	.i_d('hFFFF),
	.i_sel(selectr_e'(op_sel_w)),
	.o_sltd(register_product_D)
);

assign enb_register_product_w = (enb_Qnx_w || Pnx_enb) || start;
pipo_register
#(
	.DW((2*p02_pkg::N)+2)
)
register_product
(
	.clk(clk),
	.rst(rst),
	.enb(enb_register_product_w),
	.i_data(register_product_D),
	.o_data(register_product_Q)
);

/** demux_product **/
assign booth_product = register_product_Q;
assign div_product = register_product_Q;
assign sqrt_product = register_product_Q;
	
mux_4_1 
#(
	.DW((2*p02_pkg::N)+1)
)
mux_product
(	
	.i_a(((2*p02_pkg::N)+1)'(mux_Pnx_o[2*(p02_pkg::N):1])),
	//.i_a(((2*p02_pkg::N)+1)'(booth_mux_register_product)),
	.i_b(((2*p02_pkg::N)+1)'(div_mux_register_product)),
	.i_c(((2*p02_pkg::N)+1)'(sqrt_product)),
	.i_d(((2*p02_pkg::N)+1)'('hFFFF)),
	.i_sel(selectr_e'(op_res_w)),
	.o_sltd(result)
);

assign booth_mux_register_remainder = 'b0;
//assign div_mux_register_remainder = 'b0;
assign sqrt_mux_register_remainder = ( ready && (sqrt_Rnx_Q > 'd127) ) ? (sqrt_Rnx_Q + ((sqrt_Qnx_Q >> 1'b1) | 1'b1) ):(mux_Rnx_o);
/** REMAINDER **/
mux_4_1 
#(
	.DW((2*p02_pkg::N)+1)
)
mux_register_remainder
(
	.i_a(((2*p02_pkg::N)+1)'(booth_mux_register_remainder)),
	.i_b(((2*p02_pkg::N)+1)'(div_mux_register_remainder)),
	.i_c(((2*p02_pkg::N)+1)'(sqrt_mux_register_remainder)),
	.i_d(((2*p02_pkg::N)+1)'('hFFFF)),
	.i_sel(selectr_e'(op_sel_w)),
	.o_sltd(register_remainder_D)
);

assign enb_register_remainder_w = (enb_Rnx_w) || start;
pipo_register
#(
	.DW((2*p02_pkg::N)+1)
)
register_remainder
(
	.clk(clk),
	.rst(rst),
	.enb(enb_register_remainder_w),
	.i_data(((2*p02_pkg::N)+1)'(register_remainder_D)),
	.o_data(register_remainder_Q)
);

assign booth_remainder = register_remainder_Q;
assign div_remainder = register_remainder_D;
assign sqrt_remainder = register_remainder_Q;
mux_4_1 
#(
	.DW((2*p02_pkg::N)+1)
)
mux_remainder
(
	.i_a(((2*p02_pkg::N)+1)'(booth_remainder)),
	.i_b(((2*p02_pkg::N)+1)'(div_remainder)),
	.i_c(((2*p02_pkg::N)+1)'(sqrt_remainder)),
	.i_d(((2*p02_pkg::N)+1)'('hFFFF)),
	.i_sel(selectr_e'(op_res_w)),
	.o_sltd(remainder)
);

assign loadX = (mux_data_sel == 1'b1) ? (1'b1) : (1'b0);
assign loadY = (mux_data_sel == 1'b0) ? (1'b1) : (1'b0);
	
assign ready = (op_sel_w == DIV) ? (ready_w2):(ready_w1);
	
assign op_res_w = ( (register_product_Q > 'd65025) || (register_remainder_Q > 'd65025) ) ? (OP_D) : (selectr_e'(op_sel_w));
/********************************************************************************************************/
/** SYSTEM **/
/********************************************************************************************************/

endmodule 