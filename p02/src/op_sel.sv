/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: op_sel
    Description: op_sel source file
    Last modification: 14/02/2021
*/

import p02_pkg::*;
module op_sel
(	
	input						clk,
	input						rst,
	input   logic				enb,
	output  op_selectr_e	   	o_sltd
);

logic [2:0] op_r, op_nxt;
op_selectr_e op_t;
always_comb begin
	op_nxt = op_r + 1'b1;
	if('d3 < op_r )
		op_nxt = 3'b001;
	else
		begin
			case(op_r)
			'd1: begin
				op_t = MULTI;
			end 
			'd2: begin
				op_t = DIV;
			end 
			'd3: begin
				op_t = SQRT;
			end
			default: begin
				op_t = MULTI;
			end 
			endcase 
		end 
end
// Sequential process
always_ff@(posedge clk, negedge rst)begin
    if (!rst)
        op_r <= 'd1;
    else if (enb)
        op_r <= op_nxt;
end

assign o_sltd = op_t;

endmodule
