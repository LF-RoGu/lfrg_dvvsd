// Coder:       Abisai Ramirez Perez
// Date:        January 28th, 2020
// Name:        bin_counter.sv
// Description: This is a binary counter

module dbcr_bin_counter #(
parameter DW=4
)(
input           clk,
input           rst,
input           enb,
output [DW-1:0] count
);

logic [DW-1:0] count_r, cnt_nxt;

// Combinational process
always_comb begin
   cnt_nxt  = count_r + 1'b1 ;
end

// Sequential process
always_ff@(posedge clk, negedge rst)begin: counter
    if (!rst)
        count_r     <=  '0;
    else if (enb)
        count_r     <= cnt_nxt;
end:counter

assign count    =   count_r;

endmodule

