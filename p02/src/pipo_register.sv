/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: pipo_register
    Description: pipo_register source file
    Last modification: 14/02/2021
*/

import p02_pkg::*;
module pipo_register
#(
	parameter DW = p02_pkg::N
)
(
    input            		clk,
    input            		rst,
    input  logic     		enb,
    input  logic[DW-1:0] 	i_data,
    output logic[DW-1:0] 	o_data
);
    
logic[DW-1:0] rgstr_r;
    
always_ff@(posedge clk or negedge rst) begin
    if(!rst) begin
        rgstr_r <= '0;
    end 
    else if(enb) begin
        rgstr_r <= i_data;
    end 
end

assign o_data = rgstr_r;

endmodule 