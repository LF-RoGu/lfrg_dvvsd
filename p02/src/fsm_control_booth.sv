/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: fsm_control
    Description: fsm_control source file
    Last modification: 14/02/2021
*/

import p02_pkg::*;
module fsm_control_booth
(
   input        clk,
   input        rst,
   input  logic start,
   input  logic ovf,
   output logic counter_enb,
   output logic Anx_enb,
   output logic Snx_enb,
   output logic Pnx_enb,
   output logic ready
);

control_st  current_st;	
control_st  nxt_state;

always_comb begin 
   case(current_st)
   IDLE_ST: begin
      if(start == 1'b1)
         nxt_state = START_ST;
      else
         nxt_state = IDLE_ST;          
   end
   START_ST: begin
       nxt_state = BUSY_ST;
   end
   BUSY_ST: begin
      if(ovf == 1'b1)
         nxt_state = IDLE_ST;
      else
         nxt_state = BUSY_ST;    
   end
   endcase
end

always_ff@(posedge clk or negedge rst) begin 
   if (!rst)
      current_st <= IDLE_ST;
   else
      current_st <= nxt_state;
end 

always_comb begin 
   case(current_st)
   IDLE_ST: begin
       ready = 1'b1;
       counter_enb = 1'b0;
       Pnx_enb = 1'b0;
       Anx_enb = 1'b0;
       Snx_enb = 1'b0;
   end
   START_ST: begin
       ready = 1'b0;
       counter_enb = 1'b0;
       Pnx_enb = 1'b0;
       Anx_enb = 1'b0;
       Snx_enb = 1'b0;
   end
   BUSY_ST: begin
       ready = 1'b0;
       counter_enb = 1'b1;
       Pnx_enb = 1'b1;
       Anx_enb = 1'b1;
       Snx_enb = 1'b1;
   end
   endcase
end

	
endmodule 