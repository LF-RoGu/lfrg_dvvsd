/* 
    Author: César Villarreal @cv4497
				Luis Fernando Rodriguez @LF-RoGu
    Title: 
    Description: FSM control
    Last modification: 12/09/2020

    TODO: 
*/

`ifndef FSM_PKG_SV
    `define FSM_PKG_SV    
package fsm_pkg;

// TRUE and FALSE definitions
localparam bit TRUE     =1'b1;
localparam bit FALSE    =1'b0;
localparam DW = 8;

localparam data_size = 4;

//mux n
localparam N_SEL = 4;
localparam  SEL_SA     = 4'b0001;    // parameter
localparam  SEL_PA     = 4'b0010;    // parameter
localparam  SEL_PS     = 4'b0011;    // parameter
localparam  SEL_SS     = 4'b0100;    // parameter

typedef logic [data_size-1:0] data_t;
typedef logic [1:0]           p_cmp_t;


typedef enum logic [2:0]
{
    IDLE, 
    LOAD_X,
    LOAD_Y,
    START, 
    BUSY, 
    READY
} fsm_ctrl_state_e; 

typedef enum logic [3:0]
{
    INIT_FSM,
    START_REG,
    LOAD,
    FINISH_REG
} fsm_load_state_e; 

typedef enum logic [4:0]
{
   START_FSM_DIV,
   INIT_DIV,
   IDLE_DIV,
   REMAINDER_CALC,
   TEST_REMAINDER,
   POSITIVE_REMAINDER,
   NEGATIVE_REMAINDER,
   SHIFT_DIVISOR,
   ITERATION_VER,
   END_DIV
} fsm_divider_state_e; 

typedef enum logic [4:0]
{
   START_FSM_MULT,
   INIT_MULT,
   IDLE_MULT,
   LOAD_HP,
   IDLE_MULT2,
   LOAD_MULT,
   LOAD_HP2,
   IDLE_MULT3,
   TEST_Q0_Q_M1,
   ADD_MCAND,
   SUBSTRACT_MCAND,
   SHIFT_A_Q_QM1,
   ITERATION_VER_MULT,
   END_MULT
} fsm_mult_state_e; 

endpackage 

`endif 