/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: data_pkg
    Description: data_pkg package file
    Last modification: 07/03/2021
*/

 `ifndef DATA_PKG_SV
	`define DATA_PKG_SV
package data_pkg;
	`define MODELSIM_DEF
	/** Parameters*/
	localparam N = 8; 

	// TRUE and FALSE definitions
	localparam bit TRUE     =1'b1;
	localparam bit FALSE    =1'b0;

	/** Data Types*/
	
	/*
	 * UART BD
	 */
	localparam uartTxBR = 'd19_200;
	localparam uartRxBR = 3*uartTxBR;
	localparam uartRefClk = 'd57_600;
	/*
	 * PPC data type
	 */
	typedef logic [N-1:0] req_vector_t;
	/*
	 * TOP MODULE data type
	 */
	typedef logic [N-1:0] data_n_t; 

	/*
	 * ALU data type
	 */

	 typedef enum logic [6:0]
	{
		NO_OP,				//4'b0000
		ADDITION,			//4'b0001
		SUBSTRACTION,		//4'b0010
		AND,				//4'b0011
		OR,					//4'b0100
		COMP2_A,			//4'b0101
		COMP2_B				//4'b0110
	} alu_op_sel_str; 

	localparam N_SEL = 8;
	typedef logic [N-1:0]   dtwidth_t;
	typedef enum logic [N_SEL-1:0]
	{
		OP_A,
		OP_B,
		OP_C,
		OP_D,
		OP_E,
		OP_F,
		OP_G,
		OP_H,
		OP_I
	}selectr_e;
	
	/*
	 * FSM_CONTROL data_type
	 */
	typedef enum logic[2:0]
	{
	    IDLE_ST,
	    START_ST,
	    BUSY_ST
	} control_st;
	
	typedef enum logic [2:0]
	{
		IDLE_BIT,
		START_BIT,
		DATA_BIT,
		PARITY_BIT,
		STOP_BIT
	}uart_st;
	
	typedef enum logic [3:0]
	{
		DATA_0XFE,
		DATA_L,
		DATA_N,
		DATA_CMD,
		DATA_M,
		DATA_SRC,
		DATA_DST,
		DATA_DUMMY,
		DATA_ST,
		DATA_0XEF
	}decoder_st;
	
	typedef enum logic [1:0]
	{
		DT_ST_1,
		DT_ST_2,
		DT_ST_3
	}data_transfer_st;
	
	typedef enum logic [2:0]
	{
		ST_1,
		ST_2,
		ST_3,
		ST_4,
		ST_5
	}delay_st;
	
	/*
	 * COUNTER_OVF data_type
	 */
    localparam MAX_COUNT = N-1;

    typedef logic [N-1:0] count_t; 
	/*
	 * CLK_DIVIDER data_type
	 */
	localparam N_count = 27;  //number of counters
    typedef logic [N_count-1:0] counter_t;
	/*
	 * BIN2BCD data_type
	 */
	localparam PARAM_DW_BCD = 4;
	localparam PARAM_DW_SEGMENT_DISPLAY = 7;
	 
	 /** Data Types*/
	 typedef logic [2*N - 1:0] binary_data_t;
	 typedef logic [PARAM_DW_BCD - 1:0] bcd_data_t;
	 typedef logic [PARAM_DW_SEGMENT_DISPLAY - 1:0] data2display_t;
	/*
	 * DBCR_CNTR data_type
	 */
	 typedef enum logic [1:0]{
		 BAJO = 2'b00,
		 DLY1 = 2'b01,
		 ALTO = 2'b10,
		 DLY2 = 2'b11
	 } fsm_dbcr_state_e;
endpackage
 `endif