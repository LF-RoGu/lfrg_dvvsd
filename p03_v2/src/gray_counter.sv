module gray_counter
import fifo_pkg::*;
(
    input               clk,
    input               rst,
    input  logic        inc,
    input  logic        not_full_or_empty,
    output ptr_t        graynext,
    output addr_t       addr,
    output ptr_t        ptr
);

addr_t  oadder_ibinary_reg_w; //bnext
addr_t  obin2gray_igrayreg_w; //gnext
ptr_t  ograyreg_w;
addr_t obinary_reg_w;
ptr_t  graynext_w;

adder add
(
    .A(obinary_reg_w),
    .B({(W_ADDR-1)'(10'd0),valid_count}),
    .result(oadder_ibinary_reg_w)
);

binary_to_gray bin2gray
(
    .binary(oadder_ibinary_reg_w),
    .gray(obin2gray_igrayreg_w)
);

pipo_register
#(
    .DW(W_ADDR)   //
) binary_reg
(
    .clk(clk),
    .rst(rst),
    .enb(valid_count),
    .i_data(oadder_ibinary_reg_w),
    .o_data(obinary_reg_w)
);

pipo_register 
#(
    .DW(W_ADDR+1) //5
) gray_reg
(
    .clk(clk),
    .rst(rst),
    .enb(valid_count),
    .i_data(graynext_w),
    .o_data(ograyreg_w)
);

/* check if it is a valid increment request */
assign valid_count = not_full_or_empty && inc;
assign addr = obinary_reg_w;
assign ptr = ograyreg_w;
assign graynext_w = {1'b0,obin2gray_igrayreg_w};
assign graynext = graynext_w;

endmodule
