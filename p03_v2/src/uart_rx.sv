
import data_pkg::*;

module uart_rx
(
	input	 clk,
	input	logic rst,
	input logic serial_rx,
	input logic cts,
	
	output logic rts,
	output logic pairty_error,
	output data_n_t data
);

logic bin_cntr_ovf;
	
logic data_cntr_enable;
logic data_cntr_ovf;

data_n_t data_t;
logic stop_sequence;

logic o_clk_rx;
clk_divider
#(
	.FREQUENCY(uartRxBR),
	.REFERENCE_CLOCK(10_000_000)
)
uartRx_clk_divider
(
	.clk_FPGA(clk),
	.rst(rst),
	.clk(o_clk_rx)
);


fsm_cntrl_rx
fsm_cntrl_rx_top
(
	.clk(o_clk_rx),
	.rst(rst),
	.ovf(bin_cntr_ovf),
	.data_in(serial_rx),
	
	.cts(cts),
	.rts(rts),
	
	.stop_sequence(stop_sequence),
	
	.data_cntr_enable(data_cntr_enable)
);

sipo_register_msb
sipo_register_msb_uartRX
(
	.clk(o_clk_rx),
	.rst(rst),
	.enb(data_cntr_ovf),
	.data_in(serial_rx),
	.data_out(data_t)
);

parametric_counter
bin_counter_uartRX
(
	.clk(o_clk_rx),
	.rst(rst),
	.enb(data_cntr_ovf),
	.max_count((stop_sequence)?(0):(10)),
	.count(),
	.ovf(bin_cntr_ovf)
);

parametric_counter
data_counter_uartRx
(
	.clk(o_clk_rx),
	.rst(rst),
	.enb(data_cntr_enable),
	.max_count((stop_sequence)?(0):(3)),
	.count(),
	.ovf(data_cntr_ovf)
);

uart_parity parity_module
(
	.i_data(data_t),
	.parity(pairty_error)
);

pipo_register
uart_data_register
(
	.clk(o_clk_rx),
	.rst(rst),
	.enb(data_cntr_ovf),
	.i_data(data_t),
	.o_data(data)
);

endmodule 