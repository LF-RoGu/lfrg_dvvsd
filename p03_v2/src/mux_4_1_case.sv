// Coder:           DSc Abisai Ramirez Perez
// Date:            June 4th, 2019
// Name:            mux_2_1_case.sv
// Description:     This is a multiplexer using case statement.

module mux_4_1_case
#(
	parameter DW = 1
)
(
	input   logic [DW-1:0]   i_a,
	input   logic [DW-1:0]   i_b,
	input	logic [DW-1:0]	 i_c,
	input	logic [DW-1:0]	 i_d,
	input   logic [1:0]      i_sel,
	output  logic [DW-1:0]   o_sltd
);

//TODO: this code should generate a warning in the simulation, could you correct the code?
// If you cannnot see the warning, simulate the code.

// All combinational logic must be modeled using ternary operator, CASE or IF-ELSE statements.


// Description of a multiplexer two to one using a case statement
always_comb begin
    case(i_sel)
	 2'b00:			 o_sltd = i_a;
	 2'b01:			 o_sltd = i_b;
	 2'b10:			 o_sltd = i_c;
	 2'b11:		     o_sltd = i_d;
    endcase
end

endmodule

