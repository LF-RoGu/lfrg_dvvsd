/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: decoder
    Description: decoder source file
    Last modification: 21/04/2021
*/

import data_pkg::*;

module ififo_port
(
	input logic clk,
	input logic rst,
	
	input logic ififo_data_push,
	input logic ififo_data_pop,
	input data_n_t ififo_idata,
	input logic ififo_dst_push,
	input logic ififo_dst_pop,
	input data_n_t ififo_idst,

	output logic ififo_data_full,
	output logic ififo_data_empty,
	output data_n_t ififo_odata,
	
	output logic ififo_dst_full,
	output logic ififo_dst_empty,
	output data_n_t ififo_odst
);

FIFO
ififo_data
(
	.rclk(clk),
	.wclk(clk),
	.rst(rst),
	.push(ififo_data_push),
	.pop(ififo_data_pop),
	.DataInput(ififo_idata),
	.DataOutput(ififo_odata),
	.full(ififo_data_full),
	.empty(ififo_data_empty)
);

FIFO
ififo_dst
(
	.rclk(clk),
	.wclk(clk),
	.rst(rst),
	.push(ififo_dst_push),
	.pop(ififo_dst_pop),
	.DataInput(ififo_idst),
	.DataOutput(ififo_odst),
	.full(ififo_dst_full),
	.empty(ififo_dst_empty)
);

endmodule 