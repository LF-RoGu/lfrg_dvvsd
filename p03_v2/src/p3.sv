import data_pkg::*, port_pkg::*;

module p3
(
	input bit clk,
	input logic rst,
	input logic transmit,			//button start tx
	
	input  data_n_t data_tx, //parallel data from uart2uart
	
	output logic rts
);

logic cts;
logic serial_tx2rx;
data_n_t data_rx;

logic data_load;
logic dst_load;
	
data_n_t cmd, src_portLenght, src_port;

logic dst_fifo;
data_n_t dst_port;
data_n_t cmd_port;
data_n_t Dn_port;

logic Dn_fifo, cmd_fifo;
logic data_processing;

logic o_clk;
clk_divider
#(
	.FREQUENCY(10_000_000),
	.REFERENCE_CLOCK(50_000_000)
)
uartRx_clk_divider_t
(
	.clk_FPGA(clk),
	.rst(rst),
	.clk(o_clk)
);

uart_tx
uart_tx_PC
(
	.clk(o_clk),
	.rst(rst),
	.data(data_tx),
	
	.transmit(transmit),
	.cts(cts),
	
	.serial_tx(serial_tx2rx)
);

uart_module
uart_module
(
	.clk(clk),
	.rst(rst),
	.serial_tx2rx(serial_tx2rx),
	.cts(cts),
	
	.rts(rts),
	.data(data_rx),
	.data_load(data_load),
	.dst_register(dst_load),
	
	.cmd(cmd),
	.src_portLenght(src_portLenght),
	
	.src_port(src_port),
	.dst_port(dst_port),
	.cmd_port(cmd_port),
	.Dn_port(Dn_port),
	
	.dst_fifo(dst_fifo),
	.Dn_fifo(Dn_fifo),
	.cmd_fifo(cmd_fifo)
);

qsys
qsys_module
(
	.clk(clk),
	.rst(rst),
	.i_data(data_rx),
	.i_cmd(cmd),
	.load(cmd_fifo),
	.iFIFO_data_push(Dn_fifo),
	.num_iports(src_portLenght),
	.src_port(src_port),
	.data_processing(data_processing),
	.o_data()
);

assign data_processing = (cmd == 'h05)?(1'b1):(1'b0);

endmodule 
