/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title:   data transfer
    Description: data transfer source file
    Last modification: May 5th, 2021
*/

import data_pkg::*;

module data_transfer
(
	input bit clk,
	input logic rst,
	input logic fifo_load,
	
	input data_n_t iuart_data,
	input data_n_t iuart_cmd,
	
	output data_n_t ouart_data
);

logic data_selectr;

state_switch
data_selector
(
	.clk(clk),
	.rst(rst),
	.enb(fifo_load),
	.o_sltd(data_selectr)
);

mux_2_1
mux_data_transfer
(
	.i_a(iuart_data),
	.i_b(iuart_cmd),
	.i_sel((data_selectr) ? (OP_B):(OP_A)),
	.o_sltd(ouart_data)
);

endmodule