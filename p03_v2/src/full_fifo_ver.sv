

module full_fifo_ver
import fifo_pkg::*;
(
    input  ptr_t ptr_1,
    input  ptr_t ptr_2,
    output logic        full_flag
);

assign full_flag = (ptr_1 == {~ptr_2[W_ADDR-1:W_ADDR-2],ptr_2[W_ADDR-3:0]});
endmodule