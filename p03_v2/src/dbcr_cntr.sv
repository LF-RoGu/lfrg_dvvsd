module dbcr_cntr #(
parameter DW=10
)(
input           clk,
input           rst_n,
input           Din,
output [DW-1:0] count
	);
	
logic one_shot;
	
dbcr_top dbcr(
    .clk        (clk        ), 
    .rst      (rst_n      ), 
    .Din        (Din        ), 
    .one_shot   (one_shot   )
	);
	
bin_counter #(
    .DW(DW)
    ) cntr (
    .clk   (clk     ),
    .rst   (rst_n   ),
    .enb   (one_shot),
    .count (count   )  );
	
	
	
endmodule
