/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title:   input port
    Description: input port source file
    Last modification: May 5th, 2021
*/

import port_pkg::*;
import fifo_pkg::*;
import data_pkg::*;

module iport
(
    input        clk,
    input        rst,
    input port_data_t i_data,
    input port_data_t i_cmd,
    input port_data_t port_number,
    input logic data_processing,
    input logic port_enable,
    input logic execute_req,
    input logic load, 
    input logic iFIFO_data_push,
    output fsm_port_ctrl_st port_state,
    output logic port_full,
    output logic port_empty,
    output port_data_t o_data,
    output logic arb_ack,
    output logic cmd_ready,
    output logic port_ready,
    output port_data_t dest_port,
    output logic iFIFO_data_pop,
    output port_data_t port_vector
);

fsm_port_ctrl_st port_state_w;
logic port_full_w;
logic port_empty_w;
port_data_t o_data_w;

logic decoder_port_sig_w;
port_data_t dest_port_w;
port_data_t data_length2_w;
port_data_t dest_port2_w;
port_data_t data_length_w;
logic next_data_sig_w;

logic iFIFO_data_push_w;
logic iFIFO_data_pop_w;
port_data_t iFIFO_o_data_w;
logic iFIFO_data_full_w;
logic iFIFO_data_empty_w;

logic iFIFO_cmd_push_w;
logic iFIFO_cmd_pop_w;
port_data_t iFIFO_o_cmd_w;
logic iFIFO_cmd_full_w;
logic iFIFO_cmd_empty_w;

port_data_t o_vect_w;

logic ovf_w;
logic psh_dt_sig_w;
logic pop_sig_w;
logic port_dec_ready_w;
logic arb_ack_w;
port_data_t o_mux_count_w;
logic mux_count_sel_w;
logic cmd_ready_w;
logic port_ready_w;
port_data_t transmit_length_w;
logic count_enb_w;
port_data_t cmd_length_w;
logic mux_sel_w;
logic restore_pointers_w;
port_data_t o_mux_sltd_w;
logic request_active_w;
logic pop_data_sig_w;

FSM_Port_Control fsm_cntrl_port
(
    .clk(clk),
    .rst(rst),
    .port_enable(port_enable),
    .data_processing(data_processing),
    .ovf_delayer(ovf_delayer_w),
    .decode_ack(port_dec_ready_w),
    .load_sig(load && (iFIFO_data_full_w != 1'b1)),
    .ovf(ovf_w),
    .full_flag(port_full),
    .send_sig(execute_req && (iFIFO_data_empty_w != 1'b1)),
    .iFIFO_data_pop(iFIFO_data_pop_w),
    .pop_data_sig(pop_data_sig_w),
    .pop_cmd_sig(iFIFO_cmd_pop_w),
    .empty_data_flag(iFIFO_data_empty_w),
    .empty_cmd_flag(iFIFO_cmd_empty_w),
    .mux_sel(mux_sel_w),
    .psh_cmd_sig(iFIFO_cmd_push_w),
    .port_state(port_state_w),
    .arb_ack(arb_ack_w),
    .count_enb(count_enb_w),
    .decode_sig(decoder_port_sig_w),
    .cmd_ready(cmd_ready_w),
    .port_ready(port_ready_w),
    .request_active(request_active_w),
    .restore_pointers(restore_pointers_w)
);

parametric_counter pop_delayer
(
    .clk(clk),
    .rst(rst),
    .enb(pop_data_sig_w),
    .max_count(6),
    .ovf(ovf_delayer_w), //iFIFO_data_pop_w
    .count()
);

parametric_counter data_counter
(
    .clk(clk),
    .rst(rst),
    .enb(iFIFO_data_push | count_enb_w),
    .max_count(o_mux_sltd_w),
    .ovf(ovf_w),
    .count()
);

mux_2_1 mux
(
    .i_a(data_length_w),
    .i_b(cmd_length_w),
    .i_sel(selectr_e'(mux_sel_w)),
    .o_sltd(o_mux_sltd_w)
);

/* DATA FIFO */
FIFO iFIFO_data
(
    .wclk(clk),
    .rclk(clk),
    .rst(rst),
    .restore_pointers(restore_pointers_w),
    .push(iFIFO_data_push),
    .pop(iFIFO_data_pop_w),
    .DataInput(i_data),  //data_t
    .DataOutput(iFIFO_o_data_w), //data_t
    .full(iFIFO_data_full_w),
    .empty(iFIFO_data_empty_w)
);

/* CMD FIFO */
FIFO iFIFO_cmd
(
    .wclk(clk),
    .rclk(clk),
    .rst(rst),
    .restore_pointers(restore_pointers_w),
    .push(iFIFO_cmd_push_w),
    .pop(iFIFO_cmd_pop_w),
    .DataInput(i_cmd),  
    .DataOutput(iFIFO_o_cmd_w), 
    .full(iFIFO_cmd_full_w),
    .empty(iFIFO_cmd_empty_w)
);

Port_Decoder port_dec1
(
    .clk(clk),
    .rst(rst),
    .decoder_port_sig(decoder_port_sig_w),
    .i_data(i_cmd),
    .dest_port(dest_port_w),
    .data_length(data_length_w)
);

Port_Decoder port_dec2
(
    .clk(clk),
    .rst(rst),
    .decoder_port_sig(decoder_port_sig_w),
    .i_data(iFIFO_o_cmd_w),
    .dest_port(dest_port2_w),
    .data_length(cmd_length_w)
);

vector_gen port_vector_generator
(
    .request_active(request_active_w),
    .dest_port(port_number),
    .o_vect(o_vect_w)
);

assign port_state = port_state_w;
assign port_full = iFIFO_data_full_w;
assign port_empty = iFIFO_data_empty_w;
assign o_data = iFIFO_o_data_w;
assign arb_ack = arb_ack_w;
assign cmd_ready = cmd_ready_w;
assign port_ready = port_ready_w;
assign port_vector = o_vect_w;
assign dest_port = dest_port2_w;
assign iFIFO_data_pop = ovf_delayer_w; 

endmodule
