/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title:   fsm port control
    Description: fsm port control source file
    Last modification: May 5th, 2021
*/

import fifo_pkg::*, port_pkg::*;
module FSM_Port_Control
(
    input        clk,
    input        rst,
    input  logic port_enable,
    input  logic decode_ack,
    input  logic load_sig,
    input  logic ovf,
    input  logic send_sig,
    input  logic full_flag,
    input  logic empty_data_flag,
    input  logic empty_cmd_flag,
    input  logic data_processing,
    input  logic ovf_delayer,
    output logic restore_pointers,
    output logic mux_sel,
    output logic pop_data_sig,
    output logic iFIFO_data_pop,
    output logic psh_cmd_sig,
    output logic arb_ack,
    output logic count_enb,
    output logic decode_sig,
    output logic pop_cmd_sig,
    output logic cmd_ready,
    output logic port_ready,
    output logic request_active,
    output fsm_port_ctrl_st port_state
);

fsm_port_ctrl_st current_st, nxt_state;

always_comb begin
    case(current_st)
    PORT_DISABLED: begin
        if(port_enable == 1'b1)
            nxt_state = PORT_WAIT_CMD;
        else
            nxt_state = PORT_DISABLED;
    end
    PORT_WAIT_CMD: begin
        if((full_flag == 1'b1)| (data_processing == 1'b1 && empty_data_flag == 1'b0))
            nxt_state = PORT_WAIT_ARB;
        else if(send_sig == 1'b1)
            nxt_state = PORT_GET_LEN1;
        else if(load_sig == 1'b1)
            nxt_state = PORT_DECODE;
        else
            nxt_state = PORT_WAIT_CMD;
    end
    PORT_DECODE: begin
            nxt_state = PORT_WAIT_DATA;
    end
    PORT_WAIT_DATA: begin
        nxt_state = PORT_FETCH_DATA;
    end
    PORT_FETCH_DATA: begin
        if(ovf == 1'b1) //or full
            nxt_state = PORT_WAIT_CMD;
        else
            nxt_state = PORT_FETCH_DATA;
    end
    PORT_WAIT_ARB: begin
        if(send_sig == 1'b1)
            nxt_state = PORT_GET_LEN1;
        else if(empty_data_flag == 1'b1)
            nxt_state = PORT_WAIT_CMD;
        else
            nxt_state = PORT_WAIT_ARB;
    end
    PORT_GET_LEN1: begin
        nxt_state = PORT_GET_LEN2;
    end
    PORT_GET_LEN2: begin
        if(empty_cmd_flag == 1'b1)
            nxt_state = RESTORE_POINTERS_ST;
        else
            nxt_state = PORT_SEND_DATA1;
    end
    PORT_SEND_DATA1: begin
        nxt_state = WAIT_FOR_DATA;
    end
    WAIT_FOR_DATA: begin
        if(ovf_delayer == 1'b1)
            nxt_state = PORT_SEND_DATA2;
        else
            nxt_state = WAIT_FOR_DATA;
    end
    PORT_SEND_DATA2: begin
        if(ovf == 1'b1)
            nxt_state = PORT_ARB_ACK;
        else
            nxt_state = PORT_SEND_DATA1;
    end
    PORT_ARB_ACK: begin
        nxt_state = PORT_WAIT_ARB;
    end
    RESTORE_POINTERS_ST: begin
        nxt_state = PORT_WAIT_CMD;
    end
	 default: begin
		  nxt_state = PORT_DISABLED;
	 end
    endcase
end

always_ff@(posedge clk or negedge rst) begin // Circuito Secuenicial en un proceso always.
   if (!rst) 
      current_st  <= PORT_DISABLED;
   else 
      current_st  <= nxt_state;
end

always_comb begin
    case(current_st)
    PORT_DISABLED: begin
        pop_data_sig = 1'b0;
        psh_cmd_sig = 1'b0;
        pop_cmd_sig = 1'b0;
        arb_ack = 1'b0;
        decode_sig = 1'b0;
        cmd_ready = 1'b0;
        port_ready = 1'b0;
        count_enb = 1'b0;
        mux_sel = 1'b0;
        restore_pointers = 1'b0;
        request_active = 1'b0;
        iFIFO_data_pop = 1'b0;
        port_state = PORT_DISABLED;
    end
    PORT_WAIT_CMD: begin
        pop_data_sig = 1'b0;
        psh_cmd_sig = 1'b0;
        pop_cmd_sig = 1'b0;
        arb_ack = 1'b0;
        decode_sig = 1'b0;
        cmd_ready = 1'b0;
        port_ready = 1'b1;
        count_enb = 1'b0;
        mux_sel = 1'b0;
        restore_pointers = 1'b0;
        request_active = 1'b0;
        iFIFO_data_pop = 1'b0;
        port_state = PORT_WAIT_CMD;
    end
    PORT_DECODE: begin
        pop_data_sig = 1'b0;
        psh_cmd_sig = 1'b1;
        pop_cmd_sig = 1'b0;
        decode_sig = 1'b1;
        arb_ack = 1'b0;
        cmd_ready = 1'b0;
        port_ready = 1'b0;
        count_enb = 1'b0;
        mux_sel = 1'b0;
        restore_pointers = 1'b0;
        request_active = 1'b0;
        iFIFO_data_pop = 1'b0;
        port_state = PORT_DECODE;
    end
   PORT_WAIT_DATA: begin
        pop_data_sig = 1'b0;
        psh_cmd_sig = 1'b1;
        pop_cmd_sig = 1'b0;
        decode_sig = 1'b0;
        arb_ack = 1'b0;
        cmd_ready = 1'b0;
        port_ready = 1'b0;
        count_enb = 1'b0;
        mux_sel = 1'b0;
        restore_pointers = 1'b0;
        request_active = 1'b0;
        iFIFO_data_pop = 1'b0;
        port_state = PORT_DECODE;
    end
    PORT_FETCH_DATA: begin
        pop_data_sig = 1'b0;
        psh_cmd_sig = 1'b0;
        pop_cmd_sig = 1'b0;
        arb_ack = 1'b0;
        decode_sig = 1'b0;
        cmd_ready = 1'b0;
        port_ready = 1'b0;
        count_enb = 1'b0;
        mux_sel = 1'b0;
        restore_pointers = 1'b0;
        request_active = 1'b0;
        iFIFO_data_pop = 1'b0;
        port_state = PORT_FETCH_DATA;
    end
    PORT_VER_FULL: begin
        pop_data_sig = 1'b0;
        psh_cmd_sig = 1'b0;
        pop_cmd_sig = 1'b0;
        arb_ack = 1'b0;
        decode_sig = 1'b0;
        cmd_ready = 1'b0;
        port_ready = 1'b0;
        count_enb = 1'b0;
        mux_sel = 1'b0;
        restore_pointers = 1'b0;
        request_active = 1'b0;
        iFIFO_data_pop = 1'b0;
        port_state = PORT_VER_FULL;      
    end
    PORT_WAIT_ARB: begin
        pop_data_sig = 1'b0;
        psh_cmd_sig = 1'b0;
        pop_cmd_sig = 1'b0;
        arb_ack = 1'b0;
        decode_sig = 1'b0;
        cmd_ready = 1'b0;
        port_ready = 1'b1;
        count_enb = 1'b0;
        mux_sel = 1'b0;
        restore_pointers = 1'b0;
        request_active = 1'b1;
        iFIFO_data_pop = 1'b0;
        port_state = PORT_WAIT_ARB;
    end
    PORT_GET_LEN1: begin
        pop_data_sig = 1'b0;
        psh_cmd_sig = 1'b0;
        pop_cmd_sig = 1'b0;
        arb_ack = 1'b0;
        decode_sig = 1'b1;
        cmd_ready = 1'b0;
        port_ready = 1'b0;
        count_enb = 1'b0;
        mux_sel = 1'b1;
        restore_pointers = 1'b0;
        request_active = 1'b1;
        iFIFO_data_pop = 1'b0;
        port_state = PORT_GET_LEN1;       
    end
    PORT_GET_LEN2: begin
        pop_data_sig = 1'b0;
        psh_cmd_sig = 1'b0;
        pop_cmd_sig = 1'b1;
        arb_ack = 1'b0;
        decode_sig = 1'b0;
        cmd_ready = 1'b0;
        port_ready = 1'b0;
        count_enb = 1'b0;
        mux_sel = 1'b1;
        restore_pointers = 1'b0;
        request_active = 1'b1;
        iFIFO_data_pop = 1'b0;
        port_state = PORT_GET_LEN2;       
    end
    PORT_SEND_DATA1: begin
        pop_data_sig = 1'b0;
        psh_cmd_sig = 1'b0;
        pop_cmd_sig = 1'b0;
        arb_ack = 1'b0;
        decode_sig = 1'b0;
        cmd_ready = 1'b0;
        port_ready = 1'b0;
        count_enb = 1'b0;
        mux_sel = 1'b1;
        restore_pointers = 1'b0;
        request_active = 1'b1;
        iFIFO_data_pop = 1'b1;
        port_state = PORT_SEND_DATA1;
    end
    WAIT_FOR_DATA: begin
        pop_data_sig = 1'b1;
        psh_cmd_sig = 1'b0;
        pop_cmd_sig = 1'b0;
        arb_ack = 1'b0;
        decode_sig = 1'b0;
        cmd_ready = 1'b0;
        port_ready = 1'b0;
        count_enb = 1'b0;
        mux_sel = 1'b1;
        restore_pointers = 1'b0;
        request_active = 1'b1;
        iFIFO_data_pop = 1'b0;
        port_state = WAIT_FOR_DATA;
    end
    PORT_SEND_DATA2: begin
        pop_data_sig = 1'b0;
        psh_cmd_sig = 1'b0;
        pop_cmd_sig = 1'b0;
        arb_ack = 1'b0;
        decode_sig = 1'b0;
        cmd_ready = 1'b0;
        port_ready = 1'b0;
        count_enb = 1'b1;
        mux_sel = 1'b1;
        restore_pointers = 1'b0;
        request_active = 1'b1;
        iFIFO_data_pop = 1'b0;
        port_state = PORT_SEND_DATA2;
    end
    PORT_ARB_ACK: begin
        pop_data_sig = 1'b0;
        psh_cmd_sig = 1'b0;
        pop_cmd_sig = 1'b0;
        arb_ack = 1'b1;
        decode_sig = 1'b0;
        cmd_ready = 1'b0;
        port_ready = 1'b0;
        count_enb = 1'b0;
        mux_sel = 1'b1;
        restore_pointers = 1'b0;
        request_active = 1'b0;
        iFIFO_data_pop = 1'b0;
        port_state = PORT_ARB_ACK;
    end
    RESTORE_POINTERS_ST: begin
        pop_data_sig = 1'b0;
        psh_cmd_sig = 1'b0;
        pop_cmd_sig = 1'b0;
        arb_ack = 1'b0;
        decode_sig = 1'b0;
        cmd_ready = 1'b0;
        port_ready = 1'b0;
        count_enb = 1'b0;
        mux_sel = 1'b1;
        restore_pointers = 1'b1;
        request_active = 1'b0;
        iFIFO_data_pop = 1'b0;
        port_state = RESTORE_POINTERS_ST;
    end
    endcase
end



endmodule