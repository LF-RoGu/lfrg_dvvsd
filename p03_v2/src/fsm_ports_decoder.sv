/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: uartTx_fsm
    Description: uartTx_fsm source file
    Last modification: 07/03/2021
*/
import data_pkg::*;

module fsm_ports_decoder
(
	input bit clk,
	input bit rst,
	input logic enb,
	input data_n_t uart_data,
	
	output decoder_st decoder_state,
	output data_n_t Dn_ports
);

decoder_st current_st, nxt_state;

always_comb begin
   case(current_st)
	   DATA_0XFE: begin
		   if((uart_data == data_n_t'('hFE)) && (enb)) begin
			   nxt_state = DATA_L;
		   end 
		   else begin
			   nxt_state = DATA_0XFE;
		   end 
	   end 
	   DATA_L: begin
		   if(uart_data == data_n_t'('d3)) begin
			   nxt_state = DATA_CMD;
		   end 
		   else begin
			   nxt_state = DATA_L;
		   end 
	   end 
	   DATA_CMD: begin
		   if(uart_data == data_n_t'('h01)) begin
			   nxt_state = DATA_M;
		   end 
		   else begin
			   nxt_state = DATA_CMD;
		   end 
	   end 
	   DATA_M: begin
		   if((uart_data >= data_n_t'('d1)) && (uart_data <= data_n_t'('d8))) begin
			   nxt_state = DATA_0XEF;
		   end 
		   else begin
			   nxt_state = DATA_M;
		   end 
	   end 
	   DATA_0XEF: begin
		   if(uart_data == data_n_t'('hEF)) begin
			   nxt_state = DATA_0XFE;
		   end 
		   else begin
			   nxt_state = DATA_0XEF;
		   end 
	   end 
	endcase
end

always_ff@(posedge clk or negedge rst) begin // Circuito Secuenicial en un proceso always.
   if (!rst) 
      current_st  <= DATA_0XFE;
   else 
      current_st  <= nxt_state;
end

always_comb begin
   case(current_st)
	   DATA_0XFE: begin
		   decoder_state = current_st;
		   Dn_ports = 1'b0;
	   end 
	   DATA_L: begin
		   decoder_state = current_st;
		   Dn_ports = 1'b0;
	   end 
	   DATA_CMD: begin
		   decoder_state = current_st;
		   Dn_ports = 1'b0;
	   end 
	   DATA_M: begin
		   decoder_state = current_st;
		   Dn_ports = uart_data;
	   end 
	   DATA_0XEF: begin
		   decoder_state = current_st;
		   Dn_ports = 1'b0;
	   end 
	endcase
end

endmodule 