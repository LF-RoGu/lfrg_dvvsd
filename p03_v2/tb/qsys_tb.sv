
`timescale 1ns / 1ps
module qsys_tb();


localparam PERIOD = 2;

/*************/
import port_pkg::*;
import fifo_pkg::*;
import data_pkg::*;

/* module ports */
logic clk;
logic rst;
port_data_t i_data;
port_data_t i_cmd;
port_data_t o_data;
port_data_t num_iports;
port_data_t src_port;
logic load;
logic iFIFO_data_push;
logic data_processing;

qsys qsys_mod
(
    .clk(clk),
    .rst(rst),
    .i_data(i_data),
    .i_cmd(i_cmd),
    .load(load),
    .iFIFO_data_push(iFIFO_data_push),
    .num_iports(num_iports),
    .src_port(src_port),
    .data_processing(data_processing),
    .o_data(o_data)
);

localparam array_size = 16;
localparam DW_G= 8;
logic [3:0] src;
logic [3:0] dest;
logic [3:0] length;
logic [array_size-1:0][DW_G-1:0] data_0;
logic [array_size-1:0][DW_G-1:0] data_1;
logic [array_size-1:0][DW_G-1:0] data_2;
logic [array_size-1:0][DW_G-1:0] data_3;
logic [array_size-1:0][DW_G-1:0] data_4;
logic [array_size-1:0][DW_G-1:0] data_5;
logic [array_size-1:0][DW_G-1:0] data_6;
int j;
initial begin
    reset();
    #PERIOD data_0 = {"h", "e", "l", "l","o"};
    #PERIOD data_1 = {"w", "o", "r", "l","d"};
    #PERIOD data_2 = {"g","u","y","s"};
    #PERIOD data_3 = {"t","h","i","s"};
    #PERIOD data_4 = {"i", "s"};
    #PERIOD data_5 = {"a"};
    #PERIOD data_6 = {"t","e","s","t"};

    /* Enable ports */
    command_0x01(8);
    /* load data to dest port */
    #PERIOD src = 4'd0;
    #PERIOD dest = 4'd0;
    command_0x04(src, dest, getDataArrayLength(data_0), data_0);

    #PERIOD src = 4'd1;
    #PERIOD dest = 4'd0;
    command_0x04(src, dest, getDataArrayLength(data_1), data_1);

    #PERIOD src = 4'd3;
    #PERIOD dest = 4'd1;
    command_0x04(src, dest, getDataArrayLength(data_2), data_2);

    #PERIOD src = 4'd4;
    #PERIOD dest = 4'd2;
    command_0x04(src, dest, getDataArrayLength(data_3), data_3);

    #PERIOD src = 4'd5;
    #PERIOD dest = 4'd3;
    command_0x04(src, dest, getDataArrayLength(data_4), data_4);

    #PERIOD src = 4'd5;
    #PERIOD dest = 4'd4;
    command_0x04(src, dest, getDataArrayLength(data_5), data_5);

    #PERIOD src = 4'd5;
    #PERIOD dest = 4'd5;
    command_0x04(src, dest, getDataArrayLength(data_6), data_6);

    #PERIOD src = 4'd6;
    #PERIOD dest = 4'd6;
    command_0x04(src, dest, getDataArrayLength(data_0), data_0);

    #PERIOD src = 4'd6;
    #PERIOD dest = 4'd6;
    command_0x04(src, dest, getDataArrayLength(data_1), data_1);

    #PERIOD src = 4'd7;
    #PERIOD dest = 4'd7;
    command_0x04(src, dest, getDataArrayLength(data_0), data_0);

    #PERIOD src = 4'd8;
    #PERIOD dest = 4'd8;
    command_0x04(src, dest, getDataArrayLength(data_1), data_1);
    /* enable proccesing */
    command_0x05();
   
end

always begin
    #(PERIOD/2) clk<= ~clk;
end

function [7:0] getDataArrayLength(logic [array_size-1:0][DW_G-1:0] data);
    logic [7:0] count;
    int k;
    count = 8'd0;
    for(k=0; k < array_size; k++)
    begin
        if(data[k] != 8'd0) begin
            count = count + 8'd1;
        end
    end
    return count;
endfunction

task command_0x01(input logic [3:0] n_ports);
    #PERIOD num_iports = n_ports;
endtask

//brief: load data to a dest port
task command_0x04(input logic [3:0] src, input logic [3:0] dest, input logic [3:0] length, input logic [array_size-1:0][DW_G-1:0] data);
    int i;
    /* set_dest_and_length */
    #PERIOD src_port = src;
    #PERIOD i_cmd = {dest,length};
    load_data();

    for(i=length-1; i >= 0; i--) begin
        #PERIOD i_data = data[i];
        #PERIOD iFIFO_data_push = 1'b1;
        #PERIOD iFIFO_data_push= 1'b0;
    end
endtask

//brief: enable data proccesing
task command_0x05();
    data_processing = 1'b1;
endtask


task load_data();
    #PERIOD load= 1'b1;
    #PERIOD load = 1'b0;
endtask



task reset();
	clk = 1'b0;
	rst = 1'b1;
    i_data = 8'd0;
    i_cmd  = 8'd0;
    num_iports = 8'd0;
    iFIFO_data_push = 1'b0;
    load = 1'b0;
    src_port = 8'd0;
    data_processing = 1'b0;

	#PERIOD rst = 1'b1;
	#PERIOD rst = 1'b0;
	#PERIOD rst = 1'b1;
endtask

endmodule
