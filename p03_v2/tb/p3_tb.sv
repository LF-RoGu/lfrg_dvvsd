
`timescale 1ns / 1ps
module p3_tb();

localparam PERIOD = 2;

/*************/
import data_pkg::*, port_pkg::*, fifo_pkg::*;

localparam array_size = 16;
localparam DW_G= 8;

/* module ports */
logic clk;
logic rst;
logic transmit;
data_n_t data_uart;
logic rts;

p3 p3_top
(
    .clk(clk),
    .rst(rst),
    .transmit(transmit),
    
    .data_tx(data_uart),
    
    .rts(rts)
);

logic [array_size-1:0][DW_G-1:0] data_0;
logic [array_size-1:0][DW_G-1:0] data_1;
logic [array_size-1:0][DW_G-1:0] data_2;
logic [array_size-1:0][DW_G-1:0] data_3;
logic [array_size-1:0][DW_G-1:0] data_4;
logic [array_size-1:0][DW_G-1:0] data_5;
logic [array_size-1:0][DW_G-1:0] data_6;

initial begin
    reset();
	
    #PERIOD data_0 = {"h", "e", "l", "l","o"};
    #PERIOD data_1 = {"w", "o", "r", "l","d"};
    #PERIOD data_2 = {"g","u","y","s"};
    #PERIOD data_3 = {"t","h","i","s"};
    #PERIOD data_4 = {"i", "s"};
    #PERIOD data_5 = {"a"};
    #PERIOD data_6 = {"t","e","s","t"};
	
    set_ports('h8);
	data2src_port('h0, 'h0, getDataArrayLength(data_0), data_0);
	data2src_port('h1, 'h0, getDataArrayLength(data_1), data_1);
	data2src_port('h3, 'h1, getDataArrayLength(data_2), data_2);
	data2src_port('h4, 'h2, getDataArrayLength(data_3), data_3);
	data2src_port('h5, 'h3, getDataArrayLength(data_4), data_4);
	data2src_port('h6, 'h6, getDataArrayLength(data_5), data_5);
	data2src_port('h6, 'h6, getDataArrayLength(data_6), data_6);
	
	set_procesing();
end

always begin
    #(PERIOD/2) clk<= ~clk;
end

task set_ports(data_n_t n_ports);
	data2send('hFE);
	delay(150);
	data2send('h03);
	delay(150);
	data2send('h01);
	delay(150);
	data2send('h08);
	delay(150);
	data2send('hEF);
	delay(150);
endtask 

task data2src_port(	input logic [3:0] src_ports, 
					input logic [3:0] dst_ports, 
					input logic [3:0] Dn_lenght , 
					input logic [array_size-1:0][DW_G-1:0] Dn);
	integer i;
	
	data2send('hFE);
	delay(150);
	data2send(Dn_lenght + 4);
	delay(150);
	data2send('h04);
	delay(150);
	data2send(src_ports); /* SRC PORT */
	delay(150);
	data2send(dst_ports); /* DST PORT */
	delay(150);
	for(i = Dn_lenght; i >= 0; i--) begin
		data2send(Dn[i]);
		delay(150);
	end 
	data2send('hEF);
	delay(150);
endtask 

function [7:0] getDataArrayLength(logic [array_size-1:0][DW_G-1:0] data);
    logic [7:0] count;
    int k;
    count = 8'd0;
    for(k=0; k < array_size; k++)
    begin
        if(data[k] != 8'd0) begin
            count = count + 8'd1;
        end
    end
    return count;
endfunction

task set_procesing();
	data2send('hFE);
	delay(150);
	data2send('h02);
	delay(150);
	data2send('h05);
	delay(150);
	data2send('hEF);
	delay(150);
endtask 

task set_transmit();
	data2send('hFE);
	delay(150);
	data2send('h02);
	delay(150);
	data2send('h06);
	delay(150);
	data2send('hEF);
	delay(150);
endtask 

task data2send(data_n_t data);
	#PERIOD data_uart = data;
	#PERIOD transmit = 1'b0;
	#PERIOD transmit = 1'b1; 
	@(posedge rts)
	#PERIOD transmit = 1'b0;
endtask

task reset();
	clk = 1'b0;
	rst = 1'b1;
    transmit = 'd0;
    data_uart  = 'd0;

	#PERIOD rst = 1'b1;
	#PERIOD rst = 1'b0;
	#PERIOD rst = 1'b1;
endtask

//delay
task delay(int dly = 1);
    repeat(dly)
        @(posedge clk);
endtask

endmodule
