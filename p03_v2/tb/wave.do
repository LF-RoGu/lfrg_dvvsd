onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {UART PC}
add wave -noupdate /p3_tb/p3_top/uartRx_clk_divider_t/clk_FPGA
add wave -noupdate /p3_tb/p3_top/uartRx_clk_divider_t/clk
add wave -noupdate /p3_tb/p3_top/uart_tx_PC/uartTx_clk_divider/clk_FPGA
add wave -noupdate /p3_tb/p3_top/uart_tx_PC/uartTx_clk_divider/clk
add wave -noupdate -radix hexadecimal /p3_tb/p3_top/data_tx
add wave -noupdate /p3_tb/p3_top/rts
add wave -noupdate /p3_tb/p3_top/transmit
add wave -noupdate -expand -group {uart tx} -group piso /p3_tb/p3_top/uart_tx_PC/piso_register_lsb_uartTX/clk
add wave -noupdate -expand -group {uart tx} -group piso /p3_tb/p3_top/uart_tx_PC/piso_register_lsb_uartTX/rst
add wave -noupdate -expand -group {uart tx} -group piso /p3_tb/p3_top/uart_tx_PC/piso_register_lsb_uartTX/enb
add wave -noupdate -expand -group {uart tx} -group piso /p3_tb/p3_top/uart_tx_PC/piso_register_lsb_uartTX/l_s
add wave -noupdate -expand -group {uart tx} -group piso -radix unsigned /p3_tb/p3_top/uart_tx_PC/piso_register_lsb_uartTX/i_data
add wave -noupdate -expand -group {uart tx} -group piso /p3_tb/p3_top/uart_tx_PC/piso_register_lsb_uartTX/o_data
add wave -noupdate -expand -group {uart tx} -expand -group {fsm tx} /p3_tb/p3_top/uart_tx_PC/uart_tx_fsm/transmit
add wave -noupdate -expand -group {uart tx} -expand -group {fsm tx} /p3_tb/p3_top/uart_tx_PC/uart_tx_fsm/ovf
add wave -noupdate -expand -group {uart tx} -expand -group {fsm tx} /p3_tb/p3_top/uart_tx_PC/uart_tx_fsm/l_s
add wave -noupdate -expand -group {uart tx} -expand -group {fsm tx} /p3_tb/p3_top/uart_tx_PC/uart_tx_fsm/cntr_enb
add wave -noupdate -expand -group {uart tx} -expand -group {fsm tx} /p3_tb/p3_top/uart_tx_PC/uart_tx_fsm/reg_enb
add wave -noupdate -expand -group {uart tx} -expand -group {fsm tx} /p3_tb/p3_top/uart_tx_PC/uart_tx_fsm/mux_sel
add wave -noupdate -expand -group {uart tx} -expand -group {fsm tx} /p3_tb/p3_top/uart_tx_PC/uart_tx_fsm/cts
add wave -noupdate -expand -group {uart tx} -expand -group {fsm tx} /p3_tb/p3_top/uart_tx_PC/uart_tx_fsm/tx_active
add wave -noupdate -expand -group {uart tx} -expand -group {fsm tx} /p3_tb/p3_top/uart_tx_PC/uart_tx_fsm/current_st
add wave -noupdate -expand -group {uart tx} -expand -group {fsm tx} /p3_tb/p3_top/uart_tx_PC/uart_tx_fsm/nxt_state
add wave -noupdate -expand -group {uart tx} -expand -group {fsm tx} /p3_tb/p3_top/uart_tx_PC/uart_tx_fsm/parity_bit
add wave -noupdate -divider P3
add wave -noupdate -divider P3
add wave -noupdate -divider P3
add wave -noupdate -divider {UART MODULE}
add wave -noupdate -divider {UART MODULE}
add wave -noupdate -radix hexadecimal /p3_tb/p3_top/data_tx
add wave -noupdate -radix ascii /p3_tb/p3_top/uart_module/data
add wave -noupdate -group {uart rx} /p3_tb/p3_top/uart_module/uart_rx_top/uartRx_clk_divider/clk_FPGA
add wave -noupdate -group {uart rx} /p3_tb/p3_top/uart_module/uart_rx_top/uartRx_clk_divider/clk
add wave -noupdate -group {uart rx} -group {fsm rx} /p3_tb/p3_top/uart_module/uart_rx_top/fsm_cntrl_rx_top/clk
add wave -noupdate -group {uart rx} -group {fsm rx} /p3_tb/p3_top/uart_module/uart_rx_top/fsm_cntrl_rx_top/rst
add wave -noupdate -group {uart rx} -group {fsm rx} /p3_tb/p3_top/uart_module/uart_rx_top/fsm_cntrl_rx_top/ovf
add wave -noupdate -group {uart rx} -group {fsm rx} /p3_tb/p3_top/uart_module/uart_rx_top/fsm_cntrl_rx_top/data_in
add wave -noupdate -group {uart rx} -group {fsm rx} /p3_tb/p3_top/uart_module/uart_rx_top/fsm_cntrl_rx_top/cts
add wave -noupdate -group {uart rx} -group {fsm rx} /p3_tb/p3_top/uart_module/uart_rx_top/fsm_cntrl_rx_top/rts
add wave -noupdate -group {uart rx} -group {fsm rx} /p3_tb/p3_top/uart_module/uart_rx_top/fsm_cntrl_rx_top/stop_sequence
add wave -noupdate -group {uart rx} -group {fsm rx} /p3_tb/p3_top/uart_module/uart_rx_top/fsm_cntrl_rx_top/data_cntr_enable
add wave -noupdate -group {uart rx} -group {fsm rx} /p3_tb/p3_top/uart_module/uart_rx_top/fsm_cntrl_rx_top/current_st
add wave -noupdate -group {uart rx} -group {fsm rx} /p3_tb/p3_top/uart_module/uart_rx_top/fsm_cntrl_rx_top/nxt_state
add wave -noupdate -group {uart rx} -group {fsm rx} /p3_tb/p3_top/uart_module/uart_rx_top/fsm_cntrl_rx_top/rts_t
add wave -noupdate -group {uart rx} -group sipo /p3_tb/p3_top/uart_module/uart_rx_top/sipo_register_msb_uartRX/clk
add wave -noupdate -group {uart rx} -group sipo /p3_tb/p3_top/uart_module/uart_rx_top/sipo_register_msb_uartRX/rst
add wave -noupdate -group {uart rx} -group sipo /p3_tb/p3_top/uart_module/uart_rx_top/sipo_register_msb_uartRX/enb
add wave -noupdate -group {uart rx} -group sipo /p3_tb/p3_top/uart_module/uart_rx_top/sipo_register_msb_uartRX/data_in
add wave -noupdate -group {uart rx} -group sipo -radix hexadecimal /p3_tb/p3_top/uart_module/uart_rx_top/sipo_register_msb_uartRX/data_out
add wave -noupdate -group {uart rx} -group {bin counter} /p3_tb/p3_top/uart_module/uart_rx_top/bin_counter_uartRX/enb
add wave -noupdate -group {uart rx} -group {bin counter} /p3_tb/p3_top/uart_module/uart_rx_top/bin_counter_uartRX/max_count
add wave -noupdate -group {uart rx} -group {bin counter} /p3_tb/p3_top/uart_module/uart_rx_top/bin_counter_uartRX/ovf
add wave -noupdate -group {uart rx} -group {bin counter} -radix decimal /p3_tb/p3_top/uart_module/uart_rx_top/bin_counter_uartRX/count
add wave -noupdate -group {uart rx} -group {data counter} /p3_tb/p3_top/uart_module/uart_rx_top/data_counter_uartRx/enb
add wave -noupdate -group {uart rx} -group {data counter} /p3_tb/p3_top/uart_module/uart_rx_top/data_counter_uartRx/max_count
add wave -noupdate -group {uart rx} -group {data counter} /p3_tb/p3_top/uart_module/uart_rx_top/data_counter_uartRx/ovf
add wave -noupdate -group {uart rx} -group {data counter} -radix decimal /p3_tb/p3_top/uart_module/uart_rx_top/data_counter_uartRx/count
add wave -noupdate -group {uart rx} -group {rx register} /p3_tb/p3_top/uart_module/uart_rx_top/uart_data_register/enb
add wave -noupdate -group {uart rx} -group {rx register} /p3_tb/p3_top/uart_module/uart_rx_top/uart_data_register/i_data
add wave -noupdate -group {uart rx} -group {rx register} -radix ascii /p3_tb/p3_top/uart_module/uart_rx_top/uart_data_register/o_data
add wave -noupdate -expand -group decoder -group {rts detector} /p3_tb/p3_top/uart_module/seq_decode/rts_detector/enb
add wave -noupdate -expand -group decoder -group {rts detector} /p3_tb/p3_top/uart_module/seq_decode/rts_detector/i_data
add wave -noupdate -expand -group decoder -group {rts detector} /p3_tb/p3_top/uart_module/seq_decode/rts_detector/o_data
add wave -noupdate -expand -group decoder -group {sequence detector} /p3_tb/p3_top/uart_module/seq_decode/sequence_detector/enb
add wave -noupdate -expand -group decoder -group {sequence detector} /p3_tb/p3_top/uart_module/seq_decode/sequence_detector/i_data
add wave -noupdate -expand -group decoder -group {sequence detector} /p3_tb/p3_top/uart_module/seq_decode/sequence_detector/o_data
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/decoder_data_ovf
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/valid_data
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_0xfe
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_l
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_cmd_01
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_cmd_02
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_cmd_03
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_cmd_04
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_cmd_05
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_cmd_06
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_m
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_n
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_src
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_dst
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_oxef
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/data_load
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/cmd_register
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/lenght_register
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/src_portLenght_register
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/src_register
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/dst_register
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/data_counter_enb
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/dst_fifo
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/Dn_fifo
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/current_st
add wave -noupdate -expand -group decoder -expand -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/nxt_state
add wave -noupdate -expand -group decoder -expand -group {cmd detector} /p3_tb/p3_top/uart_module/seq_decode/cmd_detector/enb
add wave -noupdate -expand -group decoder -expand -group {cmd detector} -radix decimal /p3_tb/p3_top/uart_module/seq_decode/cmd_detector/i_data
add wave -noupdate -expand -group decoder -expand -group {cmd detector} -radix decimal /p3_tb/p3_top/uart_module/seq_decode/cmd_detector/o_data
add wave -noupdate -expand -group decoder -expand -group length_detector /p3_tb/p3_top/uart_module/seq_decode/lenght_detector/enb
add wave -noupdate -expand -group decoder -expand -group length_detector -radix decimal /p3_tb/p3_top/uart_module/seq_decode/lenght_detector/i_data
add wave -noupdate -expand -group decoder -expand -group length_detector -radix decimal /p3_tb/p3_top/uart_module/seq_decode/lenght_detector/o_data
add wave -noupdate -expand -group decoder -group portLenght_detector /p3_tb/p3_top/uart_module/seq_decode/portLenght_detector/enb
add wave -noupdate -expand -group decoder -group portLenght_detector /p3_tb/p3_top/uart_module/seq_decode/portLenght_detector/i_data
add wave -noupdate -expand -group decoder -group portLenght_detector /p3_tb/p3_top/uart_module/seq_decode/portLenght_detector/o_data
add wave -noupdate -expand -group decoder -group src_detector /p3_tb/p3_top/uart_module/seq_decode/src_detector/enb
add wave -noupdate -expand -group decoder -group src_detector -radix decimal /p3_tb/p3_top/uart_module/seq_decode/src_detector/i_data
add wave -noupdate -expand -group decoder -group src_detector -radix decimal /p3_tb/p3_top/uart_module/seq_decode/src_detector/o_data
add wave -noupdate -expand -group decoder -group dst_detector /p3_tb/p3_top/uart_module/seq_decode/dst_detector/enb
add wave -noupdate -expand -group decoder -group dst_detector -radix decimal /p3_tb/p3_top/uart_module/seq_decode/dst_detector/i_data
add wave -noupdate -expand -group decoder -group dst_detector -radix decimal /p3_tb/p3_top/uart_module/seq_decode/dst_detector/o_data
add wave -noupdate -expand -group decoder -group cmd_vector /p3_tb/p3_top/uart_module/seq_decode/cmd_vector/enb
add wave -noupdate -expand -group decoder -group cmd_vector /p3_tb/p3_top/uart_module/seq_decode/cmd_vector/i_data
add wave -noupdate -expand -group decoder -group cmd_vector /p3_tb/p3_top/uart_module/seq_decode/cmd_vector/o_data
add wave -noupdate -expand -group decoder -expand -group Dn_vector /p3_tb/p3_top/uart_module/seq_decode/Dn_vector/enb
add wave -noupdate -expand -group decoder -expand -group Dn_vector -radix ascii /p3_tb/p3_top/uart_module/seq_decode/Dn_vector/i_data
add wave -noupdate -expand -group decoder -expand -group Dn_vector -radix ascii /p3_tb/p3_top/uart_module/seq_decode/Dn_vector/o_data
add wave -noupdate -divider QSYS
add wave -noupdate -divider QSYS
add wave -noupdate -divider iFIFO
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {893973000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 471
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {4181825550 ps}
