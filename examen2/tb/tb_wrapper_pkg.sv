`ifndef TB_WRAPPER_PKG_SV
    `define TB_WRAPPER_PKG_SV
package tb_wrapper_pkg ;

	localparam PERIOD = 4;
	
    localparam TRUE   = 1'b1;
    localparam FALSE  = 1'b0;
	
    typedef logic [8-1:0]        data_t;
	typedef logic [8-1:0] 		  req_vector_t;

endpackage
`endif