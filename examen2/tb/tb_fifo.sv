`timescale 1ns / 1ps
`include "tester_fifo.svh"

module fifo_wrapper(
// Clock signal
input bit clk,
// reset signal
input bit rst_n,
fifo_if.fifo fifo_itf
);

FIFO dut(
    .clk         ( fifo_itf.clk          ) 
   ,.rst		 ( rst_n 				)
   ,.data_input  ( fifo_itf.data_input  )               
   ,.push        ( fifo_itf.push    )         
   ,.pop         ( fifo_itf.pop     )        
   ,.data_out    ( fifo_itf.data_out )             
   ,.full        ( fifo_itf.full    )         
   ,.empty       ( fifo_itf.empty   ) 
);

endmodule

module tb_fifo ();
//You should include your tester around here.
import tb_wrapper_pkg::*;


bit clk;
bit rst_n;

tester_fifo t;

fifo_if fifo_itf (
	.clk(clk)
	);

//Instance of the DUT
fifo_wrapper dut(
.clk        (  clk            )
,.rst_n     (  rst_n          )
,.fifo_itf  (  fifo_itf.fifo  )
);

initial begin
	t = new(fifo_itf);
	t.init();
   clk         = '0;
   rst_n       = 'd1;
	#(2*PERIOD)
   		# PERIOD rst_n   = 'd0;
	#(2*PERIOD)
   		# PERIOD rst_n   = 'd1;
   // Call your task/tasks for injecting data and push
   fork
	   t.inject_data();
   join
   $stop();
end

initial begin
   // Call your task/tasks for pop data and review output data
end


always begin
   #(PERIOD/2) clk =!clk;
end

endmodule
