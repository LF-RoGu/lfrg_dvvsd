interface fifo_if(
input clk
);
import tb_wrapper_pkg::*;

data_t data_input, data_out;
logic pop, push;
logic full, empty;

modport tstr (
	output data_input,
	output push,
	output pop,
	input clk,
	
	input data_out,
	input full,
	input empty
);

modport  fifo(
   	input data_input,
   	input push,
   	input pop,
   	input clk,
   	
   	output data_out,
   	output full,
   	output empty
);
endinterface
