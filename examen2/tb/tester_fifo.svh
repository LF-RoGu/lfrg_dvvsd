class tester_fifo; 

localparam DW        = 16;
localparam PERIOD    = 4; // Minimum value 4
localparam TRUE      = 1'b1;
localparam FALSE     = 1'b0;

typedef logic [DW-1:0]       data_t;

data_t  q_a[$];
data_t  q_b[$];
	
data_t index;

integer seed = 4;

virtual fifo_if itf;
	
function new(virtual fifo_if.tstr t);
	itf = t;
endfunction

// Code your tasks here

task automatic init();
	itf.data_input = FALSE;
	itf.push = FALSE;
	itf.pop = FALSE;
endtask

task automatic inject_data();
	
	for(index = 0; index < 16; index++) begin
		if(!itf.full) begin
			itf.data_input = $random(seed);
			push_data();
		end
	end 
	if(itf.full) begin
		for(index = 0; index < 16; index++) begin
			if(!itf.empty) begin
				pop_data();
			end 	
		end 
	end 
endtask 

task push_data();
	itf.push = 1'b0;
	itf.push = 1'b1;
	itf.push = 1'b0;
endtask 

task pop_data();
	itf.pop = 1'b0;
	itf.pop = 1'b1;
	itf.pop = 1'b0;
endtask

endclass
