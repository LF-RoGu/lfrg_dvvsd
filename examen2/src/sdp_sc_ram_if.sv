//Coder:          Abisai Ramirez Perez
//Date:           03/31/2019
//Name:           sp_dc_ ram_if.sv
//Description:    This is the interface of a dual-port dual-clock random access memory. 

`ifndef SDP_SC_RAM_IF_SV
    `define SDP_SC_RAM_IF_SV

interface sdp_sc_ram_if ();
import sdp_sc_ram_pkg::*;

// Write enable signal
logic       we        ;   // Write enable
data_t      data      ;   // data to be stored
data_t      rd_data   ;   // read data from memory
addr_t      wr_addr   ;   // Read write address
addr_t      rd_addr   ;   // Read write address

// Memory modport
modport mem (
input   we,
input   data,
input   wr_addr,
output  rd_data,
input   rd_addr
);

//Client modport
modport cln (
output  we,
output  data,
output  wr_addr,
input   rd_data,
output  rd_addr
);

endinterface
`endif

