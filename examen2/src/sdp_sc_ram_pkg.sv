// Coder:           DSc Abisai Ramirez Perez
// Date:            31 April 2019
// Name:            sdp_sc_ram_pkg.sv
// Description:     This is the package of single-port RAM
`ifndef SDP_SC_RAM__PKG_SV
    `define SDP_SC_RAM_PKG_SV
package sdp_sc_ram_pkg ;

    localparam  W_DATA      = 8;
    localparam  W_ADDR      = 3;
    localparam  W_DEPTH     = 2**W_ADDR;

    typedef logic [W_DATA-1:0]        data_t;
    typedef logic [W_ADDR-1:0]        addr_t;
	typedef logic [W_ADDR:0]		  waddr_t;

endpackage
`endif
