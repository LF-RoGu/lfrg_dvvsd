// Coder:       Fernanda & Omar
// Date:        November 2020
// Name:        compare.sv
// Description: This is module compares the pointers for the full and empty flag
////full =(rdaddr[MSB] !=wraddr[MSB]) && ( rdaddr[MSB-1:0] == wraddr[MSB-1:0] )	

module compare
import sdp_sc_ram_pkg::*;		
(
	input addr_t r_ptr,
	input waddr_t w_ptr,
	
	output bit full,
	output bit empty
);	
bit temp_full,temp_empty;
always_comb begin
	temp_full = (r_ptr[W_ADDR-1] != w_ptr[W_ADDR])&&(r_ptr[W_ADDR-1:0] == w_ptr[W_ADDR-1:0]);
	temp_empty = (r_ptr == w_ptr);
end	
assign full = temp_full;
assign empty = temp_empty;

endmodule	