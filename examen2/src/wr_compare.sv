// Coder:       Fernanda & Omar
// Date:        November 2020
// Name:        wr_compare.sv
// Description: This is compare for the write


`ifndef WR_COMPARATOR_SV
    `define WR_COMPARATOR_SV

module wr_comparator
#(
	parameter DW = 4	
)	
(
	input logic [DW-1:0] comp_in,
	output logic [DW-1:0] comp_out
	);
logic [DW-1:0]temp;	
always_comb begin
	temp = comp_in[DW-1];
end
assign comp_out = temp;

endmodule 
`endif