module FIFO
import sdp_sc_ram_pkg::*;	
(
//inputs for the FIFO
input data_t data_input,
input bit push,
input bit pop,
input bit clk,
input bit rst,

//output in the FIFO
output data_t data_out,
output bit full,
output bit empty
	);
	
addr_t read_address;
waddr_t write_address;	
data_t data_ram;	

wr_ptr
#(.DW(W_ADDR+1))
write_pointer
(
	.clk(clk),
	.rst(rst),
	.address(write_address),
	.en(push)
	);
wr_ptr
#(.DW(W_ADDR))
read_pointer
(
	.clk(clk),
	.rst(rst),
	.address(read_address),
	.en(pop)
	);

sdp_sc_ram
ram_memory
(
	.clk(clk),
	.we(push),
	.data(data_input),
	.rd_data(data_ram),
	.rd_addr(read_address),
	.wr_addr(write_address)
	);

register_enable
#(.DW(W_DATA))
out_flip_flop
(
	.clk(clk),
	.rst(rst),
	.enable(pop),
	.D_input(data_ram),
	.Q_output(data_out)
);

compare
full_and_empty
(
	.r_ptr(read_address),
	.w_ptr(write_address),
	.empty(empty),
	.full(full)
	);


endmodule

