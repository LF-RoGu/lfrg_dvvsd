`ifndef REGISTER_ENABLE_SV
    `define REGISTER_ENABLE_SV

module register_enable
#(
//parameter
parameter 	DW = 4
)
(
	input bit clk,
	input bit rst,
	input logic enable,

	input logic  [DW-1:0]  D_input,
	output logic [DW-1:0]  Q_output

	);
	
logic [DW-1:0] Data_reg;		
	
always_ff @(posedge clk, negedge rst) begin: register_enb
if(!rst)
	Data_reg <= 'd0;
else
	if(enable)
		Data_reg <= D_input;		
end: register_enb
assign Q_output = Data_reg;

endmodule
	`endif