/*
 * CODER: Luis Fernando Rodriguez Gtz
 * DATE: 25/Jan/2021
 * PROJECT: basic_counter
 */
 
 /*
  * FIXME[LUIS]: agregar guardas de proteccion 
  */
 
 /** Imported files*/
 import basic_counter_pkg::*;
 
 /** Module to set:
  * 	Input
  * 	Output*/
 module basic_counter
	 (
		 /** Input*/
		 input logic clk,
		 input logic rst,
		 input logic enable,
		 /** Output*/
		 output data_t count
		 );
 /** Auxiliar variables*/
 data_t count_aux, count_next;
	 
 /** Bloque combinacional
  * Operaciones a realizar, cambiar
  * Cada que haya un cambio en una variable de entrada del sistema, se hace
  * un acceso al always_comb*/
 always_comb
 begin
	 /** = es bloqueante*/
	 count_next = count_aux + 'd1;
 end 
 /** Bloque secuencial
  * Valores a guardar o hacer el paso*/
 always_ff@(posedge clk, negedge rst)
 begin
	 if(!rst)
		 begin
			 /** <= no bloqueante*/
			 count_aux <= '0;
		 end
	 else if(enable)
		 begin
			 count_aux <= count_next;
		 end 
 end 
	 
 assign count = count_aux;
 endmodule
