/*
 * CODER: Luis Fernando Rodriguez Gtz
 * DATE: 25/Jan/2021
 * PROJECT: basic_counter_pkg
 */

`ifndef BASIC_COUNTER_PKG_SV
	`define BASIC_COUNTER_PKG_SV
package basic_counter_pkg;
	localparam DW = 4;
	typedef logic [DW-1:0] data_t;
endpackage
`endif