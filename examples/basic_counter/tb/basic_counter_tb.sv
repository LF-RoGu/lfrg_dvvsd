/*
 * CODER: Luis Fernando Rodriguez Gtz
 * DATE: 25/Jan/2021
 * PROJECT: basic_counter_tb
 */
 /** scale*/
 `timescale 1ns / 1ps
 /** Init module for tb*/
 module basic_counter_tb();
	 
 import basic_counter_pkg::*;
	 
 /** LocalParam*/
 localparam PERIOD = 2;
 
 /** Signal Declaration*/	 
 bit clk;
 bit rst;
 logic enable;
 data_t count;
 /** Instace of unit under test*/
 basic_counter uut_dut
 (
	 /** INPUT*/
	 .clk( clk ),
	 .rst( rst ),
	 .enable( enable ),
	 /** OUTPUT*/
	 .count( count )
	 );
 /** hilos de ejecucion, podemos tener hasta #n hilos de ejecucion*/
 initial begin
	 clk 	= '0;
	 rst 	= '0; 
	 #(PERIOD) rst = ~rst;
	 enable = 'b1;
	 repeat(32)
	 	#(PERIOD) enable = ~enable;
 end 
 
 /** */
 always begin
	 /** Cada segundo ciclo de tiempo, cambia el estado de bit clk*/
	 #(PERIOD/2) clk = ~clk;
 end 
	 
 endmodule 