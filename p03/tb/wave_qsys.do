onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /qsys_tb/clk
add wave -noupdate /qsys_tb/rst
add wave -noupdate -radix ascii /qsys_tb/i_data
add wave -noupdate -radix unsigned /qsys_tb/i_cmd
add wave -noupdate -radix unsigned /qsys_tb/o_data
add wave -noupdate -radix unsigned /qsys_tb/num_iports
add wave -noupdate -radix unsigned /qsys_tb/src_port
add wave -noupdate /qsys_tb/load
add wave -noupdate /qsys_tb/iFIFO_data_push
add wave -noupdate /qsys_tb/data_processing
add wave -noupdate /qsys_tb/dest
add wave -noupdate /qsys_tb/length
add wave -noupdate -group {Arbeiter 0} -label ack {/qsys_tb/qsys_mod/arbeiters[0]/arbeiter/ack}
add wave -noupdate -group {Arbeiter 0} -label request_vector {/qsys_tb/qsys_mod/arbeiters[0]/arbeiter/request_vector}
add wave -noupdate -group {Arbeiter 0} -label valid {/qsys_tb/qsys_mod/arbeiters[0]/arbeiter/valid}
add wave -noupdate -group {Arbeiter 0} -label grant {/qsys_tb/qsys_mod/arbeiters[0]/arbeiter/grant}
add wave -noupdate -group {arb ack mux 0} {/qsys_tb/qsys_mod/arb_ack_mux[0]/arb_ack_mux/i_a}
add wave -noupdate -group {arb ack mux 0} {/qsys_tb/qsys_mod/arb_ack_mux[0]/arb_ack_mux/i_b}
add wave -noupdate -group {arb ack mux 0} {/qsys_tb/qsys_mod/arb_ack_mux[0]/arb_ack_mux/i_c}
add wave -noupdate -group {arb ack mux 0} {/qsys_tb/qsys_mod/arb_ack_mux[0]/arb_ack_mux/i_d}
add wave -noupdate -group {arb ack mux 0} {/qsys_tb/qsys_mod/arb_ack_mux[0]/arb_ack_mux/i_e}
add wave -noupdate -group {arb ack mux 0} {/qsys_tb/qsys_mod/arb_ack_mux[0]/arb_ack_mux/i_f}
add wave -noupdate -group {arb ack mux 0} {/qsys_tb/qsys_mod/arb_ack_mux[0]/arb_ack_mux/i_g}
add wave -noupdate -group {arb ack mux 0} {/qsys_tb/qsys_mod/arb_ack_mux[0]/arb_ack_mux/i_h}
add wave -noupdate -group {arb ack mux 0} {/qsys_tb/qsys_mod/arb_ack_mux[0]/arb_ack_mux/o_sltd}
add wave -noupdate -group iport0 -label i_data -radix ascii {/qsys_tb/qsys_mod/iports[0]/iport/i_data}
add wave -noupdate -group iport0 -label i_cmd -radix unsigned {/qsys_tb/qsys_mod/iports[0]/iport/i_cmd}
add wave -noupdate -group iport0 -label port_number {/qsys_tb/qsys_mod/iports[0]/iport/port_number}
add wave -noupdate -group iport0 -label port_enable {/qsys_tb/qsys_mod/iports[0]/iport/port_enable}
add wave -noupdate -group iport0 -label execute_req {/qsys_tb/qsys_mod/iports[0]/iport/execute_req}
add wave -noupdate -group iport0 -label load {/qsys_tb/qsys_mod/iports[0]/iport/load}
add wave -noupdate -group iport0 -label iFIFO_data_push {/qsys_tb/qsys_mod/iports[0]/iport/iFIFO_data_push}
add wave -noupdate -group iport0 -label port_state {/qsys_tb/qsys_mod/iports[0]/iport/port_state}
add wave -noupdate -group iport0 -label port_full {/qsys_tb/qsys_mod/iports[0]/iport/port_full}
add wave -noupdate -group iport0 -label port_empty {/qsys_tb/qsys_mod/iports[0]/iport/port_empty}
add wave -noupdate -group iport0 -label o_data -radix ascii {/qsys_tb/qsys_mod/iports[0]/iport/o_data}
add wave -noupdate -group iport0 -label cmd_ready {/qsys_tb/qsys_mod/iports[0]/iport/cmd_ready}
add wave -noupdate -group iport0 -label port_ready {/qsys_tb/qsys_mod/iports[0]/iport/port_ready}
add wave -noupdate -group iport0 -label arb_ack {/qsys_tb/qsys_mod/iports[0]/iport/arb_ack}
add wave -noupdate -group iport0 -label dest_port -radix unsigned {/qsys_tb/qsys_mod/iports[0]/iport/dest_port}
add wave -noupdate -group iport0 -label port_vector -radix binary {/qsys_tb/qsys_mod/iports[0]/iport/port_vector}
add wave -noupdate -group iport0 {/qsys_tb/qsys_mod/iports[0]/iport/iFIFO_data_pop}
add wave -noupdate -group iport0 -radix ascii {/qsys_tb/qsys_mod/iports[0]/iport/iFIFO_data/ram/ram}
add wave -noupdate -group iport0 {/qsys_tb/qsys_mod/iports[0]/iport/iFIFO_cmd/ram/ram}
add wave -noupdate -group iport0 -radix unsigned {/qsys_tb/qsys_mod/iports[0]/iport/port_dec2/data_length}
add wave -noupdate -group iport0 {/qsys_tb/qsys_mod/iports[0]/iport/iFIFO_data_push_w}
add wave -noupdate -group iport0 {/qsys_tb/qsys_mod/iports[0]/iport/iFIFO_data_pop_w}
add wave -noupdate -group iport0 {/qsys_tb/qsys_mod/iports[0]/iport/iFIFO_cmd_push_w}
add wave -noupdate -group iport0 {/qsys_tb/qsys_mod/iports[0]/iport/iFIFO_cmd_pop_w}
add wave -noupdate -group iport1 -label i_data {/qsys_tb/qsys_mod/iports[1]/iport/i_data}
add wave -noupdate -group iport1 -label i_cmd {/qsys_tb/qsys_mod/iports[1]/iport/i_cmd}
add wave -noupdate -group iport1 -label port_number {/qsys_tb/qsys_mod/iports[1]/iport/port_number}
add wave -noupdate -group iport1 -label port_enable {/qsys_tb/qsys_mod/iports[1]/iport/port_enable}
add wave -noupdate -group iport1 -label execute_req {/qsys_tb/qsys_mod/iports[1]/iport/execute_req}
add wave -noupdate -group iport1 -label load {/qsys_tb/qsys_mod/iports[1]/iport/load}
add wave -noupdate -group iport1 -label iFIFO_data_push {/qsys_tb/qsys_mod/iports[1]/iport/iFIFO_data_push}
add wave -noupdate -group iport1 -label port_state {/qsys_tb/qsys_mod/iports[1]/iport/port_state}
add wave -noupdate -group iport1 -label port_full {/qsys_tb/qsys_mod/iports[1]/iport/port_full}
add wave -noupdate -group iport1 -label port_empty {/qsys_tb/qsys_mod/iports[1]/iport/port_empty}
add wave -noupdate -group iport1 -label o_data {/qsys_tb/qsys_mod/iports[1]/iport/o_data}
add wave -noupdate -group iport1 -label cmd_ready {/qsys_tb/qsys_mod/iports[1]/iport/cmd_ready}
add wave -noupdate -group iport1 -label port_ready {/qsys_tb/qsys_mod/iports[1]/iport/port_ready}
add wave -noupdate -group iport1 -label arb_ack {/qsys_tb/qsys_mod/iports[1]/iport/arb_ack}
add wave -noupdate -group iport1 -label dest_port {/qsys_tb/qsys_mod/iports[1]/iport/dest_port}
add wave -noupdate -group iport1 -label port_vector {/qsys_tb/qsys_mod/iports[1]/iport/port_vector}
add wave -noupdate -group iport2 -label i_data {/qsys_tb/qsys_mod/iports[2]/iport/i_data}
add wave -noupdate -group iport2 -label i_cmd {/qsys_tb/qsys_mod/iports[2]/iport/i_cmd}
add wave -noupdate -group iport2 -label port_number {/qsys_tb/qsys_mod/iports[2]/iport/port_number}
add wave -noupdate -group iport2 -label port_enable {/qsys_tb/qsys_mod/iports[2]/iport/port_enable}
add wave -noupdate -group iport2 -label execute_req {/qsys_tb/qsys_mod/iports[2]/iport/execute_req}
add wave -noupdate -group iport2 -label load {/qsys_tb/qsys_mod/iports[2]/iport/load}
add wave -noupdate -group iport2 -label iFIFO_data_push {/qsys_tb/qsys_mod/iports[2]/iport/iFIFO_data_push}
add wave -noupdate -group iport2 -label port_state {/qsys_tb/qsys_mod/iports[2]/iport/port_state}
add wave -noupdate -group iport2 -label port_full {/qsys_tb/qsys_mod/iports[2]/iport/port_full}
add wave -noupdate -group iport2 -label port_empty {/qsys_tb/qsys_mod/iports[2]/iport/port_empty}
add wave -noupdate -group iport2 -label o_data {/qsys_tb/qsys_mod/iports[2]/iport/o_data}
add wave -noupdate -group iport2 -label cmd_ready {/qsys_tb/qsys_mod/iports[2]/iport/cmd_ready}
add wave -noupdate -group iport2 -label port_ready {/qsys_tb/qsys_mod/iports[2]/iport/port_ready}
add wave -noupdate -group iport2 -label dest_port {/qsys_tb/qsys_mod/iports[2]/iport/dest_port}
add wave -noupdate -group iport2 -label port_vector {/qsys_tb/qsys_mod/iports[2]/iport/port_vector}
add wave -noupdate -group iport3 -label i_data -radix ascii {/qsys_tb/qsys_mod/iports[3]/iport/i_data}
add wave -noupdate -group iport3 -label i_cmd -radix unsigned {/qsys_tb/qsys_mod/iports[3]/iport/i_cmd}
add wave -noupdate -group iport3 -label port_number {/qsys_tb/qsys_mod/iports[3]/iport/port_number}
add wave -noupdate -group iport3 -label port_enable {/qsys_tb/qsys_mod/iports[3]/iport/port_enable}
add wave -noupdate -group iport3 -label execute_req {/qsys_tb/qsys_mod/iports[3]/iport/execute_req}
add wave -noupdate -group iport3 -label load {/qsys_tb/qsys_mod/iports[3]/iport/load}
add wave -noupdate -group iport3 -label iFIFO_data_push {/qsys_tb/qsys_mod/iports[3]/iport/iFIFO_data_push}
add wave -noupdate -group iport3 -label port_state {/qsys_tb/qsys_mod/iports[3]/iport/port_state}
add wave -noupdate -group iport3 -label port_full {/qsys_tb/qsys_mod/iports[3]/iport/port_full}
add wave -noupdate -group iport3 -label port_empty {/qsys_tb/qsys_mod/iports[3]/iport/port_empty}
add wave -noupdate -group iport3 -label o_data -radix ascii {/qsys_tb/qsys_mod/iports[3]/iport/o_data}
add wave -noupdate -group iport3 -label cmd_ready {/qsys_tb/qsys_mod/iports[3]/iport/cmd_ready}
add wave -noupdate -group iport3 -label port_ready {/qsys_tb/qsys_mod/iports[3]/iport/port_ready}
add wave -noupdate -group iport3 -label dest_port {/qsys_tb/qsys_mod/iports[3]/iport/dest_port}
add wave -noupdate -group iport3 -label port_vector {/qsys_tb/qsys_mod/iports[3]/iport/port_vector}
add wave -noupdate -group iport4 -label i_data -radix ascii {/qsys_tb/qsys_mod/iports[4]/iport/i_data}
add wave -noupdate -group iport4 -label i_cmd -radix unsigned {/qsys_tb/qsys_mod/iports[4]/iport/i_cmd}
add wave -noupdate -group iport4 -label port_number {/qsys_tb/qsys_mod/iports[4]/iport/port_number}
add wave -noupdate -group iport4 -label port_enable {/qsys_tb/qsys_mod/iports[4]/iport/port_enable}
add wave -noupdate -group iport4 -label execute_req {/qsys_tb/qsys_mod/iports[4]/iport/execute_req}
add wave -noupdate -group iport4 -label load {/qsys_tb/qsys_mod/iports[4]/iport/load}
add wave -noupdate -group iport4 -label iFIFO_data_push {/qsys_tb/qsys_mod/iports[4]/iport/iFIFO_data_push}
add wave -noupdate -group iport4 -label port_state {/qsys_tb/qsys_mod/iports[4]/iport/port_state}
add wave -noupdate -group iport4 -label port_full {/qsys_tb/qsys_mod/iports[4]/iport/port_full}
add wave -noupdate -group iport4 -label port_empty {/qsys_tb/qsys_mod/iports[4]/iport/port_empty}
add wave -noupdate -group iport4 -label o_data -radix ascii {/qsys_tb/qsys_mod/iports[4]/iport/o_data}
add wave -noupdate -group iport4 -label cmd_ready {/qsys_tb/qsys_mod/iports[4]/iport/cmd_ready}
add wave -noupdate -group iport4 -label port_ready {/qsys_tb/qsys_mod/iports[4]/iport/port_ready}
add wave -noupdate -group iport4 -label dest_port {/qsys_tb/qsys_mod/iports[4]/iport/dest_port}
add wave -noupdate -group iport4 -label port_vector {/qsys_tb/qsys_mod/iports[4]/iport/port_vector}
add wave -noupdate -group iport5 -label i_data {/qsys_tb/qsys_mod/iports[5]/iport/i_data}
add wave -noupdate -group iport5 -label i_cmd {/qsys_tb/qsys_mod/iports[5]/iport/i_cmd}
add wave -noupdate -group iport5 -label port_number {/qsys_tb/qsys_mod/iports[5]/iport/port_number}
add wave -noupdate -group iport5 -label port_enable {/qsys_tb/qsys_mod/iports[5]/iport/port_enable}
add wave -noupdate -group iport5 -label execute_req {/qsys_tb/qsys_mod/iports[5]/iport/execute_req}
add wave -noupdate -group iport5 -label load {/qsys_tb/qsys_mod/iports[5]/iport/load}
add wave -noupdate -group iport5 -label iFIFO_data_push {/qsys_tb/qsys_mod/iports[5]/iport/iFIFO_data_push}
add wave -noupdate -group iport5 -label port_state {/qsys_tb/qsys_mod/iports[5]/iport/port_state}
add wave -noupdate -group iport5 -label port_full {/qsys_tb/qsys_mod/iports[5]/iport/port_full}
add wave -noupdate -group iport5 -label port_empty {/qsys_tb/qsys_mod/iports[5]/iport/port_empty}
add wave -noupdate -group iport5 -label o_data {/qsys_tb/qsys_mod/iports[5]/iport/o_data}
add wave -noupdate -group iport5 -label cmd_ready {/qsys_tb/qsys_mod/iports[5]/iport/cmd_ready}
add wave -noupdate -group iport5 -label port_ready {/qsys_tb/qsys_mod/iports[5]/iport/port_ready}
add wave -noupdate -group iport5 -label dest_port {/qsys_tb/qsys_mod/iports[5]/iport/dest_port}
add wave -noupdate -group iport5 -label port_vector {/qsys_tb/qsys_mod/iports[5]/iport/port_vector}
add wave -noupdate -group iport6 -label i_data {/qsys_tb/qsys_mod/iports[6]/iport/i_data}
add wave -noupdate -group iport6 -label i_cmd {/qsys_tb/qsys_mod/iports[6]/iport/i_cmd}
add wave -noupdate -group iport6 -label port_number {/qsys_tb/qsys_mod/iports[6]/iport/port_number}
add wave -noupdate -group iport6 -label port_enable {/qsys_tb/qsys_mod/iports[6]/iport/port_enable}
add wave -noupdate -group iport6 -label execute_req {/qsys_tb/qsys_mod/iports[6]/iport/execute_req}
add wave -noupdate -group iport6 -label load {/qsys_tb/qsys_mod/iports[6]/iport/load}
add wave -noupdate -group iport6 -label iFIFO_data_push {/qsys_tb/qsys_mod/iports[6]/iport/iFIFO_data_push}
add wave -noupdate -group iport6 -label port_state {/qsys_tb/qsys_mod/iports[6]/iport/port_state}
add wave -noupdate -group iport6 -label port_full {/qsys_tb/qsys_mod/iports[6]/iport/port_full}
add wave -noupdate -group iport6 -label port_empty {/qsys_tb/qsys_mod/iports[6]/iport/port_empty}
add wave -noupdate -group iport6 -label o_data {/qsys_tb/qsys_mod/iports[6]/iport/o_data}
add wave -noupdate -group iport6 -label cmd_ready {/qsys_tb/qsys_mod/iports[6]/iport/cmd_ready}
add wave -noupdate -group iport6 -label port_ready {/qsys_tb/qsys_mod/iports[6]/iport/port_ready}
add wave -noupdate -group iport6 -label dest_port {/qsys_tb/qsys_mod/iports[6]/iport/dest_port}
add wave -noupdate -group iport6 -label port_vector {/qsys_tb/qsys_mod/iports[6]/iport/port_vector}
add wave -noupdate -group iport7 -label i_data {/qsys_tb/qsys_mod/iports[7]/iport/i_data}
add wave -noupdate -group iport7 -label i_cmd {/qsys_tb/qsys_mod/iports[7]/iport/i_cmd}
add wave -noupdate -group iport7 -label port_number {/qsys_tb/qsys_mod/iports[7]/iport/port_number}
add wave -noupdate -group iport7 -label port_enable {/qsys_tb/qsys_mod/iports[7]/iport/port_enable}
add wave -noupdate -group iport7 -label execute_req {/qsys_tb/qsys_mod/iports[7]/iport/execute_req}
add wave -noupdate -group iport7 -label load {/qsys_tb/qsys_mod/iports[7]/iport/load}
add wave -noupdate -group iport7 -label iFIFO_data_push {/qsys_tb/qsys_mod/iports[7]/iport/iFIFO_data_push}
add wave -noupdate -group iport7 -label port_state {/qsys_tb/qsys_mod/iports[7]/iport/port_state}
add wave -noupdate -group iport7 -label port_full {/qsys_tb/qsys_mod/iports[7]/iport/port_full}
add wave -noupdate -group iport7 -label port_empty {/qsys_tb/qsys_mod/iports[7]/iport/port_empty}
add wave -noupdate -group iport7 -label o_data {/qsys_tb/qsys_mod/iports[7]/iport/o_data}
add wave -noupdate -group iport7 -label cmd_ready {/qsys_tb/qsys_mod/iports[7]/iport/cmd_ready}
add wave -noupdate -group iport7 -label port_ready {/qsys_tb/qsys_mod/iports[7]/iport/port_ready}
add wave -noupdate -group iport7 -label dest_port {/qsys_tb/qsys_mod/iports[7]/iport/dest_port}
add wave -noupdate -group iport7 -label port_vector {/qsys_tb/qsys_mod/iports[7]/iport/port_vector}
add wave -noupdate -group demux_req_vector_iport0 -label req_vector_iport {/qsys_tb/qsys_mod/demux_req_vector_iport[0]/req_vector_iport/i_data}
add wave -noupdate -group demux_req_vector_iport0 -label i_sel {/qsys_tb/qsys_mod/demux_req_vector_iport[0]/req_vector_iport/i_sel}
add wave -noupdate -group demux_req_vector_iport0 -label o_data_a {/qsys_tb/qsys_mod/demux_req_vector_iport[0]/req_vector_iport/o_data_a}
add wave -noupdate -group demux_req_vector_iport0 -label o_data_b {/qsys_tb/qsys_mod/demux_req_vector_iport[0]/req_vector_iport/o_data_b}
add wave -noupdate -group demux_req_vector_iport0 -label o_data_c {/qsys_tb/qsys_mod/demux_req_vector_iport[0]/req_vector_iport/o_data_c}
add wave -noupdate -group demux_req_vector_iport0 -label o_data_d {/qsys_tb/qsys_mod/demux_req_vector_iport[0]/req_vector_iport/o_data_d}
add wave -noupdate -group demux_req_vector_iport0 -label o_data_e {/qsys_tb/qsys_mod/demux_req_vector_iport[0]/req_vector_iport/o_data_e}
add wave -noupdate -group demux_req_vector_iport0 -label o_data_f {/qsys_tb/qsys_mod/demux_req_vector_iport[0]/req_vector_iport/o_data_f}
add wave -noupdate -group demux_req_vector_iport0 -label o_data_g {/qsys_tb/qsys_mod/demux_req_vector_iport[0]/req_vector_iport/o_data_g}
add wave -noupdate -group demux_req_vector_iport0 -label o_data_h {/qsys_tb/qsys_mod/demux_req_vector_iport[0]/req_vector_iport/o_data_h}
add wave -noupdate -group demux_req_vector_iport1 -label req_vector_iport {/qsys_tb/qsys_mod/demux_req_vector_iport[1]/req_vector_iport/i_data}
add wave -noupdate -group demux_req_vector_iport1 -label i_sel {/qsys_tb/qsys_mod/demux_req_vector_iport[1]/req_vector_iport/i_sel}
add wave -noupdate -group demux_req_vector_iport1 -label o_data_a {/qsys_tb/qsys_mod/demux_req_vector_iport[1]/req_vector_iport/o_data_a}
add wave -noupdate -group demux_req_vector_iport1 -label o_data_b {/qsys_tb/qsys_mod/demux_req_vector_iport[1]/req_vector_iport/o_data_b}
add wave -noupdate -group demux_req_vector_iport1 -label o_data_c {/qsys_tb/qsys_mod/demux_req_vector_iport[1]/req_vector_iport/o_data_c}
add wave -noupdate -group demux_req_vector_iport1 -label o_data_d {/qsys_tb/qsys_mod/demux_req_vector_iport[1]/req_vector_iport/o_data_d}
add wave -noupdate -group demux_req_vector_iport1 -label o_data_e {/qsys_tb/qsys_mod/demux_req_vector_iport[1]/req_vector_iport/o_data_e}
add wave -noupdate -group demux_req_vector_iport1 -label o_data_f {/qsys_tb/qsys_mod/demux_req_vector_iport[1]/req_vector_iport/o_data_f}
add wave -noupdate -group demux_req_vector_iport1 -label o_data_g {/qsys_tb/qsys_mod/demux_req_vector_iport[1]/req_vector_iport/o_data_g}
add wave -noupdate -group demux_req_vector_iport1 -label o_data_h {/qsys_tb/qsys_mod/demux_req_vector_iport[1]/req_vector_iport/o_data_h}
add wave -noupdate -group demux_req_vector_iport2 -label req_vector_iport {/qsys_tb/qsys_mod/demux_req_vector_iport[2]/req_vector_iport/i_data}
add wave -noupdate -group demux_req_vector_iport2 -label i_sel {/qsys_tb/qsys_mod/demux_req_vector_iport[2]/req_vector_iport/i_sel}
add wave -noupdate -group demux_req_vector_iport2 -label o_data_a {/qsys_tb/qsys_mod/demux_req_vector_iport[2]/req_vector_iport/o_data_a}
add wave -noupdate -group demux_req_vector_iport2 -label o_data_b {/qsys_tb/qsys_mod/demux_req_vector_iport[2]/req_vector_iport/o_data_b}
add wave -noupdate -group demux_req_vector_iport2 -label o_data_c {/qsys_tb/qsys_mod/demux_req_vector_iport[2]/req_vector_iport/o_data_c}
add wave -noupdate -group demux_req_vector_iport2 -label o_data_d {/qsys_tb/qsys_mod/demux_req_vector_iport[2]/req_vector_iport/o_data_d}
add wave -noupdate -group demux_req_vector_iport2 -label o_data_e {/qsys_tb/qsys_mod/demux_req_vector_iport[2]/req_vector_iport/o_data_e}
add wave -noupdate -group demux_req_vector_iport2 -label o_data_f {/qsys_tb/qsys_mod/demux_req_vector_iport[2]/req_vector_iport/o_data_f}
add wave -noupdate -group demux_req_vector_iport2 -label o_data_g {/qsys_tb/qsys_mod/demux_req_vector_iport[2]/req_vector_iport/o_data_g}
add wave -noupdate -group demux_req_vector_iport2 -label o_data_h {/qsys_tb/qsys_mod/demux_req_vector_iport[2]/req_vector_iport/o_data_h}
add wave -noupdate -group demux_req_vector_iport3 -label req_vector_iport {/qsys_tb/qsys_mod/demux_req_vector_iport[3]/req_vector_iport/i_data}
add wave -noupdate -group demux_req_vector_iport3 -label i_sel {/qsys_tb/qsys_mod/demux_req_vector_iport[3]/req_vector_iport/i_sel}
add wave -noupdate -group demux_req_vector_iport3 -label o_data_a {/qsys_tb/qsys_mod/demux_req_vector_iport[3]/req_vector_iport/o_data_a}
add wave -noupdate -group demux_req_vector_iport3 -label o_data_b {/qsys_tb/qsys_mod/demux_req_vector_iport[3]/req_vector_iport/o_data_b}
add wave -noupdate -group demux_req_vector_iport3 -label o_data_c {/qsys_tb/qsys_mod/demux_req_vector_iport[3]/req_vector_iport/o_data_c}
add wave -noupdate -group demux_req_vector_iport3 -label o_data_d {/qsys_tb/qsys_mod/demux_req_vector_iport[3]/req_vector_iport/o_data_d}
add wave -noupdate -group demux_req_vector_iport3 -label o_data_e {/qsys_tb/qsys_mod/demux_req_vector_iport[3]/req_vector_iport/o_data_e}
add wave -noupdate -group demux_req_vector_iport3 -label o_data_f {/qsys_tb/qsys_mod/demux_req_vector_iport[3]/req_vector_iport/o_data_f}
add wave -noupdate -group demux_req_vector_iport3 -label o_data_g {/qsys_tb/qsys_mod/demux_req_vector_iport[3]/req_vector_iport/o_data_g}
add wave -noupdate -group demux_req_vector_iport3 -label o_data_h {/qsys_tb/qsys_mod/demux_req_vector_iport[3]/req_vector_iport/o_data_h}
add wave -noupdate -group demux_req_vector_iport4 -label req_vector_iport {/qsys_tb/qsys_mod/demux_req_vector_iport[4]/req_vector_iport/i_data}
add wave -noupdate -group demux_req_vector_iport4 -label i_sel {/qsys_tb/qsys_mod/demux_req_vector_iport[4]/req_vector_iport/i_sel}
add wave -noupdate -group demux_req_vector_iport4 -label o_data_a {/qsys_tb/qsys_mod/demux_req_vector_iport[4]/req_vector_iport/o_data_a}
add wave -noupdate -group demux_req_vector_iport4 -label o_data_b {/qsys_tb/qsys_mod/demux_req_vector_iport[4]/req_vector_iport/o_data_b}
add wave -noupdate -group demux_req_vector_iport4 -label o_data_c {/qsys_tb/qsys_mod/demux_req_vector_iport[4]/req_vector_iport/o_data_c}
add wave -noupdate -group demux_req_vector_iport4 -label o_data_d {/qsys_tb/qsys_mod/demux_req_vector_iport[4]/req_vector_iport/o_data_d}
add wave -noupdate -group demux_req_vector_iport4 -label o_data_e {/qsys_tb/qsys_mod/demux_req_vector_iport[4]/req_vector_iport/o_data_e}
add wave -noupdate -group demux_req_vector_iport4 -label o_data_f {/qsys_tb/qsys_mod/demux_req_vector_iport[4]/req_vector_iport/o_data_f}
add wave -noupdate -group demux_req_vector_iport4 -label o_data_g {/qsys_tb/qsys_mod/demux_req_vector_iport[4]/req_vector_iport/o_data_g}
add wave -noupdate -group demux_req_vector_iport4 -label o_data_h {/qsys_tb/qsys_mod/demux_req_vector_iport[4]/req_vector_iport/o_data_h}
add wave -noupdate -group demux_req_vector_iport5 -label req_vector_iport {/qsys_tb/qsys_mod/demux_req_vector_iport[5]/req_vector_iport/i_data}
add wave -noupdate -group demux_req_vector_iport5 -label i_sel {/qsys_tb/qsys_mod/demux_req_vector_iport[5]/req_vector_iport/i_sel}
add wave -noupdate -group demux_req_vector_iport5 -label o_data_a {/qsys_tb/qsys_mod/demux_req_vector_iport[5]/req_vector_iport/o_data_a}
add wave -noupdate -group demux_req_vector_iport5 -label o_data_b {/qsys_tb/qsys_mod/demux_req_vector_iport[5]/req_vector_iport/o_data_b}
add wave -noupdate -group demux_req_vector_iport5 -label o_data_c {/qsys_tb/qsys_mod/demux_req_vector_iport[5]/req_vector_iport/o_data_c}
add wave -noupdate -group demux_req_vector_iport5 -label o_data_d {/qsys_tb/qsys_mod/demux_req_vector_iport[5]/req_vector_iport/o_data_d}
add wave -noupdate -group demux_req_vector_iport5 -label o_data_e {/qsys_tb/qsys_mod/demux_req_vector_iport[5]/req_vector_iport/o_data_e}
add wave -noupdate -group demux_req_vector_iport5 -label o_data_f {/qsys_tb/qsys_mod/demux_req_vector_iport[5]/req_vector_iport/o_data_f}
add wave -noupdate -group demux_req_vector_iport5 -label o_data_g {/qsys_tb/qsys_mod/demux_req_vector_iport[5]/req_vector_iport/o_data_g}
add wave -noupdate -group demux_req_vector_iport5 -label o_data_h {/qsys_tb/qsys_mod/demux_req_vector_iport[5]/req_vector_iport/o_data_h}
add wave -noupdate -group demux_req_vector_iport6 -label req_vector_iport {/qsys_tb/qsys_mod/demux_req_vector_iport[6]/req_vector_iport/i_data}
add wave -noupdate -group demux_req_vector_iport6 -label i_sel {/qsys_tb/qsys_mod/demux_req_vector_iport[6]/req_vector_iport/i_sel}
add wave -noupdate -group demux_req_vector_iport6 -label o_data_a {/qsys_tb/qsys_mod/demux_req_vector_iport[6]/req_vector_iport/o_data_a}
add wave -noupdate -group demux_req_vector_iport6 -label o_data_b {/qsys_tb/qsys_mod/demux_req_vector_iport[6]/req_vector_iport/o_data_b}
add wave -noupdate -group demux_req_vector_iport6 -label o_data_c {/qsys_tb/qsys_mod/demux_req_vector_iport[6]/req_vector_iport/o_data_c}
add wave -noupdate -group demux_req_vector_iport6 -label o_data_d {/qsys_tb/qsys_mod/demux_req_vector_iport[6]/req_vector_iport/o_data_d}
add wave -noupdate -group demux_req_vector_iport6 -label o_data_e {/qsys_tb/qsys_mod/demux_req_vector_iport[6]/req_vector_iport/o_data_e}
add wave -noupdate -group demux_req_vector_iport6 -label o_data_f {/qsys_tb/qsys_mod/demux_req_vector_iport[6]/req_vector_iport/o_data_f}
add wave -noupdate -group demux_req_vector_iport6 -label o_data_g {/qsys_tb/qsys_mod/demux_req_vector_iport[6]/req_vector_iport/o_data_g}
add wave -noupdate -group demux_req_vector_iport6 -label o_data_h {/qsys_tb/qsys_mod/demux_req_vector_iport[6]/req_vector_iport/o_data_h}
add wave -noupdate -group demux_req_vector_iport7 -label req_vector_iport {/qsys_tb/qsys_mod/demux_req_vector_iport[7]/req_vector_iport/i_data}
add wave -noupdate -group demux_req_vector_iport7 -label i_sel {/qsys_tb/qsys_mod/demux_req_vector_iport[7]/req_vector_iport/i_sel}
add wave -noupdate -group demux_req_vector_iport7 -label o_data_a {/qsys_tb/qsys_mod/demux_req_vector_iport[7]/req_vector_iport/o_data_a}
add wave -noupdate -group demux_req_vector_iport7 -label o_data_b {/qsys_tb/qsys_mod/demux_req_vector_iport[7]/req_vector_iport/o_data_b}
add wave -noupdate -group demux_req_vector_iport7 -label o_data_c {/qsys_tb/qsys_mod/demux_req_vector_iport[7]/req_vector_iport/o_data_c}
add wave -noupdate -group demux_req_vector_iport7 -label o_data_d {/qsys_tb/qsys_mod/demux_req_vector_iport[7]/req_vector_iport/o_data_d}
add wave -noupdate -group demux_req_vector_iport7 -label o_data_e {/qsys_tb/qsys_mod/demux_req_vector_iport[7]/req_vector_iport/o_data_e}
add wave -noupdate -group demux_req_vector_iport7 -label o_data_f {/qsys_tb/qsys_mod/demux_req_vector_iport[7]/req_vector_iport/o_data_f}
add wave -noupdate -group demux_req_vector_iport7 -label o_data_g {/qsys_tb/qsys_mod/demux_req_vector_iport[7]/req_vector_iport/o_data_g}
add wave -noupdate -group demux_req_vector_iport7 -label o_data_h {/qsys_tb/qsys_mod/demux_req_vector_iport[7]/req_vector_iport/o_data_h}
add wave -noupdate -group OR_CONCAT0 -label i_a {/qsys_tb/qsys_mod/or_request_concat[0]/or_BW_8_1/i_a}
add wave -noupdate -group OR_CONCAT0 -label i_b {/qsys_tb/qsys_mod/or_request_concat[0]/or_BW_8_1/i_b}
add wave -noupdate -group OR_CONCAT0 -label i_c {/qsys_tb/qsys_mod/or_request_concat[0]/or_BW_8_1/i_c}
add wave -noupdate -group OR_CONCAT0 -label i_d {/qsys_tb/qsys_mod/or_request_concat[0]/or_BW_8_1/i_d}
add wave -noupdate -group OR_CONCAT0 -label i_e {/qsys_tb/qsys_mod/or_request_concat[0]/or_BW_8_1/i_e}
add wave -noupdate -group OR_CONCAT0 -label i_f {/qsys_tb/qsys_mod/or_request_concat[0]/or_BW_8_1/i_f}
add wave -noupdate -group OR_CONCAT0 -label i_g {/qsys_tb/qsys_mod/or_request_concat[0]/or_BW_8_1/i_g}
add wave -noupdate -group OR_CONCAT0 -label i_h {/qsys_tb/qsys_mod/or_request_concat[0]/or_BW_8_1/i_h}
add wave -noupdate -group OR_CONCAT0 -label o_sltd {/qsys_tb/qsys_mod/or_request_concat[0]/or_BW_8_1/o_sltd}
add wave -noupdate -group OR_CONCAT1 -label i_a {/qsys_tb/qsys_mod/or_request_concat[1]/or_BW_8_1/i_a}
add wave -noupdate -group OR_CONCAT1 -label i_b {/qsys_tb/qsys_mod/or_request_concat[1]/or_BW_8_1/i_b}
add wave -noupdate -group OR_CONCAT1 -label i_c {/qsys_tb/qsys_mod/or_request_concat[1]/or_BW_8_1/i_c}
add wave -noupdate -group OR_CONCAT1 -label i_d {/qsys_tb/qsys_mod/or_request_concat[1]/or_BW_8_1/i_d}
add wave -noupdate -group OR_CONCAT1 -label i_e {/qsys_tb/qsys_mod/or_request_concat[1]/or_BW_8_1/i_e}
add wave -noupdate -group OR_CONCAT1 -label i_f {/qsys_tb/qsys_mod/or_request_concat[1]/or_BW_8_1/i_f}
add wave -noupdate -group OR_CONCAT1 -label i_g {/qsys_tb/qsys_mod/or_request_concat[1]/or_BW_8_1/i_g}
add wave -noupdate -group OR_CONCAT1 -label i_h {/qsys_tb/qsys_mod/or_request_concat[1]/or_BW_8_1/i_h}
add wave -noupdate -group OR_CONCAT1 -label o_sltd {/qsys_tb/qsys_mod/or_request_concat[1]/or_BW_8_1/o_sltd}
add wave -noupdate -group OR_CONCAT2 -label i_a {/qsys_tb/qsys_mod/or_request_concat[2]/or_BW_8_1/i_a}
add wave -noupdate -group OR_CONCAT2 -label i_b {/qsys_tb/qsys_mod/or_request_concat[2]/or_BW_8_1/i_b}
add wave -noupdate -group OR_CONCAT2 -label i_c {/qsys_tb/qsys_mod/or_request_concat[2]/or_BW_8_1/i_c}
add wave -noupdate -group OR_CONCAT2 -label i_d {/qsys_tb/qsys_mod/or_request_concat[2]/or_BW_8_1/i_d}
add wave -noupdate -group OR_CONCAT2 -label i_e {/qsys_tb/qsys_mod/or_request_concat[2]/or_BW_8_1/i_e}
add wave -noupdate -group OR_CONCAT2 -label i_f {/qsys_tb/qsys_mod/or_request_concat[2]/or_BW_8_1/i_f}
add wave -noupdate -group OR_CONCAT2 -label i_g {/qsys_tb/qsys_mod/or_request_concat[2]/or_BW_8_1/i_g}
add wave -noupdate -group OR_CONCAT2 -label i_h {/qsys_tb/qsys_mod/or_request_concat[2]/or_BW_8_1/i_h}
add wave -noupdate -group OR_CONCAT2 -label o_sltd {/qsys_tb/qsys_mod/or_request_concat[2]/or_BW_8_1/o_sltd}
add wave -noupdate -group OR_CONCAT3 -label i_a {/qsys_tb/qsys_mod/or_request_concat[3]/or_BW_8_1/i_a}
add wave -noupdate -group OR_CONCAT3 -label i_b {/qsys_tb/qsys_mod/or_request_concat[3]/or_BW_8_1/i_b}
add wave -noupdate -group OR_CONCAT3 -label i_c {/qsys_tb/qsys_mod/or_request_concat[3]/or_BW_8_1/i_c}
add wave -noupdate -group OR_CONCAT3 -label i_d {/qsys_tb/qsys_mod/or_request_concat[3]/or_BW_8_1/i_d}
add wave -noupdate -group OR_CONCAT3 -label i_e {/qsys_tb/qsys_mod/or_request_concat[3]/or_BW_8_1/i_e}
add wave -noupdate -group OR_CONCAT3 -label i_f {/qsys_tb/qsys_mod/or_request_concat[3]/or_BW_8_1/i_f}
add wave -noupdate -group OR_CONCAT3 -label i_g {/qsys_tb/qsys_mod/or_request_concat[3]/or_BW_8_1/i_g}
add wave -noupdate -group OR_CONCAT3 -label i_h {/qsys_tb/qsys_mod/or_request_concat[3]/or_BW_8_1/i_h}
add wave -noupdate -group OR_CONCAT3 -label o_sltd {/qsys_tb/qsys_mod/or_request_concat[3]/or_BW_8_1/o_sltd}
add wave -noupdate -group OR_CONCAT4 -label i_a {/qsys_tb/qsys_mod/or_request_concat[4]/or_BW_8_1/i_a}
add wave -noupdate -group OR_CONCAT4 -label i_b {/qsys_tb/qsys_mod/or_request_concat[4]/or_BW_8_1/i_b}
add wave -noupdate -group OR_CONCAT4 -label i_c {/qsys_tb/qsys_mod/or_request_concat[4]/or_BW_8_1/i_c}
add wave -noupdate -group OR_CONCAT4 -label i_d {/qsys_tb/qsys_mod/or_request_concat[4]/or_BW_8_1/i_d}
add wave -noupdate -group OR_CONCAT4 -label i_e {/qsys_tb/qsys_mod/or_request_concat[4]/or_BW_8_1/i_e}
add wave -noupdate -group OR_CONCAT4 -label i_f {/qsys_tb/qsys_mod/or_request_concat[4]/or_BW_8_1/i_f}
add wave -noupdate -group OR_CONCAT4 -label i_g {/qsys_tb/qsys_mod/or_request_concat[4]/or_BW_8_1/i_g}
add wave -noupdate -group OR_CONCAT4 -label i_h {/qsys_tb/qsys_mod/or_request_concat[4]/or_BW_8_1/i_h}
add wave -noupdate -group OR_CONCAT4 -label o_sltd {/qsys_tb/qsys_mod/or_request_concat[4]/or_BW_8_1/o_sltd}
add wave -noupdate -group OR_CONCAT5 -label i_a {/qsys_tb/qsys_mod/or_request_concat[5]/or_BW_8_1/i_a}
add wave -noupdate -group OR_CONCAT5 -label i_b {/qsys_tb/qsys_mod/or_request_concat[5]/or_BW_8_1/i_b}
add wave -noupdate -group OR_CONCAT5 -label i_c {/qsys_tb/qsys_mod/or_request_concat[5]/or_BW_8_1/i_c}
add wave -noupdate -group OR_CONCAT5 -label i_d {/qsys_tb/qsys_mod/or_request_concat[5]/or_BW_8_1/i_d}
add wave -noupdate -group OR_CONCAT5 -label i_e {/qsys_tb/qsys_mod/or_request_concat[5]/or_BW_8_1/i_e}
add wave -noupdate -group OR_CONCAT5 -label i_f {/qsys_tb/qsys_mod/or_request_concat[5]/or_BW_8_1/i_f}
add wave -noupdate -group OR_CONCAT5 -label i_g {/qsys_tb/qsys_mod/or_request_concat[5]/or_BW_8_1/i_g}
add wave -noupdate -group OR_CONCAT5 -label i_h {/qsys_tb/qsys_mod/or_request_concat[5]/or_BW_8_1/i_h}
add wave -noupdate -group OR_CONCAT5 -label o_sltd {/qsys_tb/qsys_mod/or_request_concat[5]/or_BW_8_1/o_sltd}
add wave -noupdate -group OR_CONCAT6 -label i_a {/qsys_tb/qsys_mod/or_request_concat[6]/or_BW_8_1/i_a}
add wave -noupdate -group OR_CONCAT6 -label i_b {/qsys_tb/qsys_mod/or_request_concat[6]/or_BW_8_1/i_b}
add wave -noupdate -group OR_CONCAT6 -label i_c {/qsys_tb/qsys_mod/or_request_concat[6]/or_BW_8_1/i_c}
add wave -noupdate -group OR_CONCAT6 -label i_d {/qsys_tb/qsys_mod/or_request_concat[6]/or_BW_8_1/i_d}
add wave -noupdate -group OR_CONCAT6 -label i_e {/qsys_tb/qsys_mod/or_request_concat[6]/or_BW_8_1/i_e}
add wave -noupdate -group OR_CONCAT6 -label i_f {/qsys_tb/qsys_mod/or_request_concat[6]/or_BW_8_1/i_f}
add wave -noupdate -group OR_CONCAT6 -label i_g {/qsys_tb/qsys_mod/or_request_concat[6]/or_BW_8_1/i_g}
add wave -noupdate -group OR_CONCAT6 -label i_h {/qsys_tb/qsys_mod/or_request_concat[6]/or_BW_8_1/i_h}
add wave -noupdate -group OR_CONCAT6 -label o_sltd {/qsys_tb/qsys_mod/or_request_concat[6]/or_BW_8_1/o_sltd}
add wave -noupdate -group OR_CONCAT7 -label i_a {/qsys_tb/qsys_mod/or_request_concat[7]/or_BW_8_1/i_a}
add wave -noupdate -group OR_CONCAT7 -label i_b {/qsys_tb/qsys_mod/or_request_concat[7]/or_BW_8_1/i_b}
add wave -noupdate -group OR_CONCAT7 -label i_c {/qsys_tb/qsys_mod/or_request_concat[7]/or_BW_8_1/i_c}
add wave -noupdate -group OR_CONCAT7 -label i_d {/qsys_tb/qsys_mod/or_request_concat[7]/or_BW_8_1/i_d}
add wave -noupdate -group OR_CONCAT7 -label i_e {/qsys_tb/qsys_mod/or_request_concat[7]/or_BW_8_1/i_e}
add wave -noupdate -group OR_CONCAT7 -label i_f {/qsys_tb/qsys_mod/or_request_concat[7]/or_BW_8_1/i_f}
add wave -noupdate -group OR_CONCAT7 -label i_g {/qsys_tb/qsys_mod/or_request_concat[7]/or_BW_8_1/i_g}
add wave -noupdate -group OR_CONCAT7 -label i_h {/qsys_tb/qsys_mod/or_request_concat[7]/or_BW_8_1/i_h}
add wave -noupdate -group OR_CONCAT7 -label o_sltd {/qsys_tb/qsys_mod/or_request_concat[7]/or_BW_8_1/o_sltd}
add wave -noupdate -group MUX_DATA_WCRR0 -label i_a -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[0]/mux_wcrr_8_1/i_a}
add wave -noupdate -group MUX_DATA_WCRR0 -label i_b -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[0]/mux_wcrr_8_1/i_b}
add wave -noupdate -group MUX_DATA_WCRR0 -label i_c -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[0]/mux_wcrr_8_1/i_c}
add wave -noupdate -group MUX_DATA_WCRR0 -label i_d -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[0]/mux_wcrr_8_1/i_d}
add wave -noupdate -group MUX_DATA_WCRR0 -label i_e -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[0]/mux_wcrr_8_1/i_e}
add wave -noupdate -group MUX_DATA_WCRR0 -label i_f -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[0]/mux_wcrr_8_1/i_f}
add wave -noupdate -group MUX_DATA_WCRR0 -label i_g -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[0]/mux_wcrr_8_1/i_g}
add wave -noupdate -group MUX_DATA_WCRR0 -label i_h -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[0]/mux_wcrr_8_1/i_h}
add wave -noupdate -group MUX_DATA_WCRR0 -label i_sel -radix decimal {/qsys_tb/qsys_mod/mux_wcrr_8_1[0]/mux_wcrr_8_1/i_sel}
add wave -noupdate -group MUX_DATA_WCRR0 -label o_sltd -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[0]/mux_wcrr_8_1/o_sltd}
add wave -noupdate -group MUX_DATA_WCRR1 -label i_a -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[1]/mux_wcrr_8_1/i_a}
add wave -noupdate -group MUX_DATA_WCRR1 -label i_b -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[1]/mux_wcrr_8_1/i_b}
add wave -noupdate -group MUX_DATA_WCRR1 -label i_c -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[1]/mux_wcrr_8_1/i_c}
add wave -noupdate -group MUX_DATA_WCRR1 -label i_d -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[1]/mux_wcrr_8_1/i_d}
add wave -noupdate -group MUX_DATA_WCRR1 -label i_e -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[1]/mux_wcrr_8_1/i_e}
add wave -noupdate -group MUX_DATA_WCRR1 -label i_f -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[1]/mux_wcrr_8_1/i_f}
add wave -noupdate -group MUX_DATA_WCRR1 -label i_g -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[1]/mux_wcrr_8_1/i_g}
add wave -noupdate -group MUX_DATA_WCRR1 -label i_h -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[1]/mux_wcrr_8_1/i_h}
add wave -noupdate -group MUX_DATA_WCRR1 -label i_sel -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[1]/mux_wcrr_8_1/i_sel}
add wave -noupdate -group MUX_DATA_WCRR1 -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[1]/mux_wcrr_8_1/o_sltd}
add wave -noupdate -group MUX_DATA_WCRR2 -label i_a -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[2]/mux_wcrr_8_1/i_a}
add wave -noupdate -group MUX_DATA_WCRR2 -label i_b -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[2]/mux_wcrr_8_1/i_b}
add wave -noupdate -group MUX_DATA_WCRR2 -label i_c -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[2]/mux_wcrr_8_1/i_c}
add wave -noupdate -group MUX_DATA_WCRR2 -label i_d -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[2]/mux_wcrr_8_1/i_d}
add wave -noupdate -group MUX_DATA_WCRR2 -label i_e -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[2]/mux_wcrr_8_1/i_e}
add wave -noupdate -group MUX_DATA_WCRR2 -label i_f -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[2]/mux_wcrr_8_1/i_f}
add wave -noupdate -group MUX_DATA_WCRR2 -label i_g -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[2]/mux_wcrr_8_1/i_g}
add wave -noupdate -group MUX_DATA_WCRR2 -label i_h -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[2]/mux_wcrr_8_1/i_h}
add wave -noupdate -group MUX_DATA_WCRR2 -label i_sel {/qsys_tb/qsys_mod/mux_wcrr_8_1[2]/mux_wcrr_8_1/i_sel}
add wave -noupdate -group MUX_DATA_WCRR2 -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[2]/mux_wcrr_8_1/o_sltd}
add wave -noupdate -group MUX_DATA_WCRR3 -label i_a -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[3]/mux_wcrr_8_1/i_a}
add wave -noupdate -group MUX_DATA_WCRR3 -label i_b -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[3]/mux_wcrr_8_1/i_b}
add wave -noupdate -group MUX_DATA_WCRR3 -label i_c -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[3]/mux_wcrr_8_1/i_c}
add wave -noupdate -group MUX_DATA_WCRR3 -label i_d -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[3]/mux_wcrr_8_1/i_d}
add wave -noupdate -group MUX_DATA_WCRR3 -label i_e -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[3]/mux_wcrr_8_1/i_e}
add wave -noupdate -group MUX_DATA_WCRR3 -label i_f -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[3]/mux_wcrr_8_1/i_f}
add wave -noupdate -group MUX_DATA_WCRR3 -label i_g -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[3]/mux_wcrr_8_1/i_g}
add wave -noupdate -group MUX_DATA_WCRR3 -label i_h -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[3]/mux_wcrr_8_1/i_h}
add wave -noupdate -group MUX_DATA_WCRR3 -label i_sel {/qsys_tb/qsys_mod/mux_wcrr_8_1[3]/mux_wcrr_8_1/i_sel}
add wave -noupdate -group MUX_DATA_WCRR3 -radix ascii {/qsys_tb/qsys_mod/mux_wcrr_8_1[3]/mux_wcrr_8_1/o_sltd}
add wave -noupdate -group MUX_DATA_WCRR4 -label i_a {/qsys_tb/qsys_mod/mux_wcrr_8_1[4]/mux_wcrr_8_1/i_a}
add wave -noupdate -group MUX_DATA_WCRR4 -label i_b {/qsys_tb/qsys_mod/mux_wcrr_8_1[4]/mux_wcrr_8_1/i_b}
add wave -noupdate -group MUX_DATA_WCRR4 -label i_c {/qsys_tb/qsys_mod/mux_wcrr_8_1[4]/mux_wcrr_8_1/i_c}
add wave -noupdate -group MUX_DATA_WCRR4 -label i_d {/qsys_tb/qsys_mod/mux_wcrr_8_1[4]/mux_wcrr_8_1/i_d}
add wave -noupdate -group MUX_DATA_WCRR4 -label i_e {/qsys_tb/qsys_mod/mux_wcrr_8_1[4]/mux_wcrr_8_1/i_e}
add wave -noupdate -group MUX_DATA_WCRR4 -label i_f {/qsys_tb/qsys_mod/mux_wcrr_8_1[4]/mux_wcrr_8_1/i_f}
add wave -noupdate -group MUX_DATA_WCRR4 -label i_g {/qsys_tb/qsys_mod/mux_wcrr_8_1[4]/mux_wcrr_8_1/i_g}
add wave -noupdate -group MUX_DATA_WCRR4 -label i_h {/qsys_tb/qsys_mod/mux_wcrr_8_1[4]/mux_wcrr_8_1/i_h}
add wave -noupdate -group MUX_DATA_WCRR4 -label i_sel {/qsys_tb/qsys_mod/mux_wcrr_8_1[4]/mux_wcrr_8_1/i_sel}
add wave -noupdate -group MUX_DATA_WCRR4 {/qsys_tb/qsys_mod/mux_wcrr_8_1[4]/mux_wcrr_8_1/o_sltd}
add wave -noupdate -group MUX_DATA_WCRR5 -label i_a {/qsys_tb/qsys_mod/mux_wcrr_8_1[5]/mux_wcrr_8_1/i_a}
add wave -noupdate -group MUX_DATA_WCRR5 -label i_b {/qsys_tb/qsys_mod/mux_wcrr_8_1[5]/mux_wcrr_8_1/i_b}
add wave -noupdate -group MUX_DATA_WCRR5 -label i_c {/qsys_tb/qsys_mod/mux_wcrr_8_1[5]/mux_wcrr_8_1/i_c}
add wave -noupdate -group MUX_DATA_WCRR5 -label i_d {/qsys_tb/qsys_mod/mux_wcrr_8_1[5]/mux_wcrr_8_1/i_d}
add wave -noupdate -group MUX_DATA_WCRR5 -label i_e {/qsys_tb/qsys_mod/mux_wcrr_8_1[5]/mux_wcrr_8_1/i_e}
add wave -noupdate -group MUX_DATA_WCRR5 -label i_f {/qsys_tb/qsys_mod/mux_wcrr_8_1[5]/mux_wcrr_8_1/i_f}
add wave -noupdate -group MUX_DATA_WCRR5 -label i_g {/qsys_tb/qsys_mod/mux_wcrr_8_1[5]/mux_wcrr_8_1/i_g}
add wave -noupdate -group MUX_DATA_WCRR5 -label i_h {/qsys_tb/qsys_mod/mux_wcrr_8_1[5]/mux_wcrr_8_1/i_h}
add wave -noupdate -group MUX_DATA_WCRR5 -label i_sel {/qsys_tb/qsys_mod/mux_wcrr_8_1[5]/mux_wcrr_8_1/i_sel}
add wave -noupdate -group MUX_DATA_WCRR5 {/qsys_tb/qsys_mod/mux_wcrr_8_1[5]/mux_wcrr_8_1/o_sltd}
add wave -noupdate -group MUX_DATA_WCRR6 -label i_a {/qsys_tb/qsys_mod/mux_wcrr_8_1[6]/mux_wcrr_8_1/i_a}
add wave -noupdate -group MUX_DATA_WCRR6 -label i_b {/qsys_tb/qsys_mod/mux_wcrr_8_1[6]/mux_wcrr_8_1/i_b}
add wave -noupdate -group MUX_DATA_WCRR6 -label i_c {/qsys_tb/qsys_mod/mux_wcrr_8_1[6]/mux_wcrr_8_1/i_c}
add wave -noupdate -group MUX_DATA_WCRR6 -label i_d {/qsys_tb/qsys_mod/mux_wcrr_8_1[6]/mux_wcrr_8_1/i_d}
add wave -noupdate -group MUX_DATA_WCRR6 -label i_e {/qsys_tb/qsys_mod/mux_wcrr_8_1[6]/mux_wcrr_8_1/i_e}
add wave -noupdate -group MUX_DATA_WCRR6 -label i_f {/qsys_tb/qsys_mod/mux_wcrr_8_1[6]/mux_wcrr_8_1/i_f}
add wave -noupdate -group MUX_DATA_WCRR6 -label i_g {/qsys_tb/qsys_mod/mux_wcrr_8_1[6]/mux_wcrr_8_1/i_g}
add wave -noupdate -group MUX_DATA_WCRR6 -label i_h {/qsys_tb/qsys_mod/mux_wcrr_8_1[6]/mux_wcrr_8_1/i_h}
add wave -noupdate -group MUX_DATA_WCRR6 -label i_sel {/qsys_tb/qsys_mod/mux_wcrr_8_1[6]/mux_wcrr_8_1/i_sel}
add wave -noupdate -group MUX_DATA_WCRR6 {/qsys_tb/qsys_mod/mux_wcrr_8_1[6]/mux_wcrr_8_1/o_sltd}
add wave -noupdate -group oFIFO0 -label push {/qsys_tb/qsys_mod/oFIFOS[0]/oFIFO/push}
add wave -noupdate -group oFIFO0 -label pop {/qsys_tb/qsys_mod/oFIFOS[0]/oFIFO/pop}
add wave -noupdate -group oFIFO0 -label restore_pointers {/qsys_tb/qsys_mod/oFIFOS[0]/oFIFO/restore_pointers}
add wave -noupdate -group oFIFO0 -label DataInput -radix ascii -radixshowbase 0 {/qsys_tb/qsys_mod/oFIFOS[0]/oFIFO/DataInput}
add wave -noupdate -group oFIFO0 -label DataOutput {/qsys_tb/qsys_mod/oFIFOS[0]/oFIFO/DataOutput}
add wave -noupdate -group oFIFO0 -label full {/qsys_tb/qsys_mod/oFIFOS[0]/oFIFO/full}
add wave -noupdate -group oFIFO0 -label empty {/qsys_tb/qsys_mod/oFIFOS[0]/oFIFO/empty}
add wave -noupdate -group oFIFO0 -label ram -radix ascii -radixshowbase 0 {/qsys_tb/qsys_mod/oFIFOS[0]/oFIFO/ram/ram}
add wave -noupdate -group oFIFO1 -label push {/qsys_tb/qsys_mod/oFIFOS[1]/oFIFO/push}
add wave -noupdate -group oFIFO1 -label pop {/qsys_tb/qsys_mod/oFIFOS[1]/oFIFO/pop}
add wave -noupdate -group oFIFO1 -label restore_pointers {/qsys_tb/qsys_mod/oFIFOS[1]/oFIFO/restore_pointers}
add wave -noupdate -group oFIFO1 -label DataInput -radix ascii -radixshowbase 0 {/qsys_tb/qsys_mod/oFIFOS[1]/oFIFO/DataInput}
add wave -noupdate -group oFIFO1 -label DataOutput {/qsys_tb/qsys_mod/oFIFOS[1]/oFIFO/DataOutput}
add wave -noupdate -group oFIFO1 -label full {/qsys_tb/qsys_mod/oFIFOS[1]/oFIFO/full}
add wave -noupdate -group oFIFO1 -label empty {/qsys_tb/qsys_mod/oFIFOS[1]/oFIFO/empty}
add wave -noupdate -group oFIFO1 -label ram -radix ascii -radixshowbase 0 {/qsys_tb/qsys_mod/oFIFOS[1]/oFIFO/ram/ram}
add wave -noupdate -group oFIFO2 -label push {/qsys_tb/qsys_mod/oFIFOS[2]/oFIFO/push}
add wave -noupdate -group oFIFO2 -label pop {/qsys_tb/qsys_mod/oFIFOS[2]/oFIFO/pop}
add wave -noupdate -group oFIFO2 -label restore_pointers {/qsys_tb/qsys_mod/oFIFOS[2]/oFIFO/restore_pointers}
add wave -noupdate -group oFIFO2 -label DataInput -radix ascii -radixshowbase 0 {/qsys_tb/qsys_mod/oFIFOS[2]/oFIFO/DataInput}
add wave -noupdate -group oFIFO2 -label DataOutput {/qsys_tb/qsys_mod/oFIFOS[2]/oFIFO/DataOutput}
add wave -noupdate -group oFIFO2 -label full {/qsys_tb/qsys_mod/oFIFOS[2]/oFIFO/full}
add wave -noupdate -group oFIFO2 -label empty {/qsys_tb/qsys_mod/oFIFOS[2]/oFIFO/empty}
add wave -noupdate -group oFIFO2 -label ram -radix ascii -radixshowbase 0 {/qsys_tb/qsys_mod/oFIFOS[2]/oFIFO/ram/ram}
add wave -noupdate -group oFIFO3 -label push {/qsys_tb/qsys_mod/oFIFOS[3]/oFIFO/push}
add wave -noupdate -group oFIFO3 -label pop {/qsys_tb/qsys_mod/oFIFOS[3]/oFIFO/pop}
add wave -noupdate -group oFIFO3 -label restore_pointers {/qsys_tb/qsys_mod/oFIFOS[3]/oFIFO/restore_pointers}
add wave -noupdate -group oFIFO3 -label DataInput -radix ascii -radixshowbase 0 {/qsys_tb/qsys_mod/oFIFOS[3]/oFIFO/DataInput}
add wave -noupdate -group oFIFO3 -label DataOutput {/qsys_tb/qsys_mod/oFIFOS[3]/oFIFO/DataOutput}
add wave -noupdate -group oFIFO3 -label full {/qsys_tb/qsys_mod/oFIFOS[3]/oFIFO/full}
add wave -noupdate -group oFIFO3 -label empty {/qsys_tb/qsys_mod/oFIFOS[3]/oFIFO/empty}
add wave -noupdate -group oFIFO3 -label ram -radix ascii -radixshowbase 0 {/qsys_tb/qsys_mod/oFIFOS[3]/oFIFO/ram/ram}
add wave -noupdate -group oFIFO4 -label push {/qsys_tb/qsys_mod/oFIFOS[4]/oFIFO/push}
add wave -noupdate -group oFIFO4 -label pop {/qsys_tb/qsys_mod/oFIFOS[4]/oFIFO/pop}
add wave -noupdate -group oFIFO4 -label restore_pointers {/qsys_tb/qsys_mod/oFIFOS[4]/oFIFO/restore_pointers}
add wave -noupdate -group oFIFO4 -label DataInput -radix ascii -radixshowbase 0 {/qsys_tb/qsys_mod/oFIFOS[4]/oFIFO/DataInput}
add wave -noupdate -group oFIFO4 -label DataOutput {/qsys_tb/qsys_mod/oFIFOS[4]/oFIFO/DataOutput}
add wave -noupdate -group oFIFO4 -label full {/qsys_tb/qsys_mod/oFIFOS[4]/oFIFO/full}
add wave -noupdate -group oFIFO4 -label empty {/qsys_tb/qsys_mod/oFIFOS[4]/oFIFO/empty}
add wave -noupdate -group oFIFO4 -label ram -radix ascii -radixshowbase 0 {/qsys_tb/qsys_mod/oFIFOS[4]/oFIFO/ram/ram}
add wave -noupdate -group oFIFO5 -label push {/qsys_tb/qsys_mod/oFIFOS[5]/oFIFO/push}
add wave -noupdate -group oFIFO5 -label pop {/qsys_tb/qsys_mod/oFIFOS[5]/oFIFO/pop}
add wave -noupdate -group oFIFO5 -label restore_pointers {/qsys_tb/qsys_mod/oFIFOS[5]/oFIFO/restore_pointers}
add wave -noupdate -group oFIFO5 -label DataInput -radix ascii -radixshowbase 0 {/qsys_tb/qsys_mod/oFIFOS[5]/oFIFO/DataInput}
add wave -noupdate -group oFIFO5 -label DataOutput {/qsys_tb/qsys_mod/oFIFOS[5]/oFIFO/DataOutput}
add wave -noupdate -group oFIFO5 -label full {/qsys_tb/qsys_mod/oFIFOS[5]/oFIFO/full}
add wave -noupdate -group oFIFO5 -label empty {/qsys_tb/qsys_mod/oFIFOS[5]/oFIFO/empty}
add wave -noupdate -group oFIFO5 -label ram -radix ascii -radixshowbase 0 {/qsys_tb/qsys_mod/oFIFOS[5]/oFIFO/ram/ram}
add wave -noupdate -group oFIFO6 -label push {/qsys_tb/qsys_mod/oFIFOS[6]/oFIFO/push}
add wave -noupdate -group oFIFO6 -label pop {/qsys_tb/qsys_mod/oFIFOS[6]/oFIFO/pop}
add wave -noupdate -group oFIFO6 -label restore_pointers {/qsys_tb/qsys_mod/oFIFOS[6]/oFIFO/restore_pointers}
add wave -noupdate -group oFIFO6 -label DataInput -radix ascii -radixshowbase 0 {/qsys_tb/qsys_mod/oFIFOS[6]/oFIFO/DataInput}
add wave -noupdate -group oFIFO6 -label DataOutput {/qsys_tb/qsys_mod/oFIFOS[6]/oFIFO/DataOutput}
add wave -noupdate -group oFIFO6 -label full {/qsys_tb/qsys_mod/oFIFOS[6]/oFIFO/full}
add wave -noupdate -group oFIFO6 -label empty {/qsys_tb/qsys_mod/oFIFOS[6]/oFIFO/empty}
add wave -noupdate -group oFIFO6 -label ram -radix ascii -radixshowbase 0 {/qsys_tb/qsys_mod/oFIFOS[6]/oFIFO/ram/ram}
add wave -noupdate -group oFIFO7 -label push {/qsys_tb/qsys_mod/oFIFOS[7]/oFIFO/push}
add wave -noupdate -group oFIFO7 -label pop {/qsys_tb/qsys_mod/oFIFOS[7]/oFIFO/pop}
add wave -noupdate -group oFIFO7 -label restore_pointers {/qsys_tb/qsys_mod/oFIFOS[7]/oFIFO/restore_pointers}
add wave -noupdate -group oFIFO7 -label DataInput -radix ascii -radixshowbase 0 {/qsys_tb/qsys_mod/oFIFOS[7]/oFIFO/DataInput}
add wave -noupdate -group oFIFO7 -label DataOutput {/qsys_tb/qsys_mod/oFIFOS[7]/oFIFO/DataOutput}
add wave -noupdate -group oFIFO7 -label full {/qsys_tb/qsys_mod/oFIFOS[7]/oFIFO/full}
add wave -noupdate -group oFIFO7 -label empty {/qsys_tb/qsys_mod/oFIFOS[7]/oFIFO/empty}
add wave -noupdate -group oFIFO7 -label ram -radix ascii -radixshowbase 0 {/qsys_tb/qsys_mod/oFIFOS[7]/oFIFO/ram/ram}
add wave -noupdate -group MUX_DATA_WCRR7 -label i_a {/qsys_tb/qsys_mod/mux_wcrr_8_1[7]/mux_wcrr_8_1/i_a}
add wave -noupdate -group MUX_DATA_WCRR7 -label i_b {/qsys_tb/qsys_mod/mux_wcrr_8_1[7]/mux_wcrr_8_1/i_b}
add wave -noupdate -group MUX_DATA_WCRR7 -label i_c {/qsys_tb/qsys_mod/mux_wcrr_8_1[7]/mux_wcrr_8_1/i_c}
add wave -noupdate -group MUX_DATA_WCRR7 -label i_d {/qsys_tb/qsys_mod/mux_wcrr_8_1[7]/mux_wcrr_8_1/i_d}
add wave -noupdate -group MUX_DATA_WCRR7 -label i_e {/qsys_tb/qsys_mod/mux_wcrr_8_1[7]/mux_wcrr_8_1/i_e}
add wave -noupdate -group MUX_DATA_WCRR7 -label i_f {/qsys_tb/qsys_mod/mux_wcrr_8_1[7]/mux_wcrr_8_1/i_f}
add wave -noupdate -group MUX_DATA_WCRR7 -label i_g {/qsys_tb/qsys_mod/mux_wcrr_8_1[7]/mux_wcrr_8_1/i_g}
add wave -noupdate -group MUX_DATA_WCRR7 -label i_h {/qsys_tb/qsys_mod/mux_wcrr_8_1[7]/mux_wcrr_8_1/i_h}
add wave -noupdate -group MUX_DATA_WCRR7 -label i_sel {/qsys_tb/qsys_mod/mux_wcrr_8_1[7]/mux_wcrr_8_1/i_sel}
add wave -noupdate -group MUX_DATA_WCRR7 {/qsys_tb/qsys_mod/mux_wcrr_8_1[7]/mux_wcrr_8_1/o_sltd}
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {629 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 471
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {712 ps}
