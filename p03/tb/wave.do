onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {UART PC}
add wave -noupdate -expand -group {uart tx} /p3_tb/p3_top/uart_tx_PC/clk
add wave -noupdate -expand -group {uart tx} /p3_tb/p3_top/uart_tx_PC/rst
add wave -noupdate -expand -group {uart tx} -radix ascii /p3_tb/p3_top/uart_tx_PC/data
add wave -noupdate -expand -group {uart tx} /p3_tb/p3_top/uart_tx_PC/transmit
add wave -noupdate -expand -group {uart tx} /p3_tb/p3_top/uart_tx_PC/cts
add wave -noupdate -expand -group {uart tx} /p3_tb/p3_top/uart_tx_PC/serial_tx
add wave -noupdate -divider P3
add wave -noupdate -divider P3
add wave -noupdate -divider P3
add wave -noupdate -divider {UART MODULE}
add wave -noupdate -divider {UART MODULE}
add wave -noupdate /p3_tb/p3_top/uart_module/uart_rx_top/uartRx_clk_divider/clk_FPGA
add wave -noupdate /p3_tb/p3_top/uart_module/uart_rx_top/uartRx_clk_divider/clk
add wave -noupdate -radix ascii /p3_tb/p3_top/uart_module/data
add wave -noupdate -group {uart rx} /p3_tb/p3_top/uart_module/uart_rx_top/clk
add wave -noupdate -group {uart rx} /p3_tb/p3_top/uart_module/uart_rx_top/rst
add wave -noupdate -group {uart rx} /p3_tb/p3_top/uart_module/uart_rx_top/serial_rx
add wave -noupdate -group {uart rx} /p3_tb/p3_top/uart_module/uart_rx_top/cts
add wave -noupdate -group {uart rx} /p3_tb/p3_top/uart_module/uart_rx_top/rts
add wave -noupdate -group {uart rx} /p3_tb/p3_top/uart_module/uart_rx_top/pairty_error
add wave -noupdate -group {uart rx} -radix ascii /p3_tb/p3_top/uart_module/uart_rx_top/data
add wave -noupdate -group decoder -radix ascii /p3_tb/p3_top/uart_module/seq_decode/uart_data
add wave -noupdate -group decoder /p3_tb/p3_top/uart_module/seq_decode/data_load
add wave -noupdate -group decoder /p3_tb/p3_top/uart_module/seq_decode/dst_register
add wave -noupdate -group decoder -radix hexadecimal /p3_tb/p3_top/uart_module/seq_decode/cmd
add wave -noupdate -group decoder -radix decimal /p3_tb/p3_top/uart_module/seq_decode/src_portLenght
add wave -noupdate -group decoder -radix decimal /p3_tb/p3_top/uart_module/seq_decode/src_port
add wave -noupdate -group decoder -radix decimal /p3_tb/p3_top/uart_module/seq_decode/dst_port
add wave -noupdate -group decoder -radix binary /p3_tb/p3_top/uart_module/seq_decode/cmd_port
add wave -noupdate -group decoder -radix ascii /p3_tb/p3_top/uart_module/seq_decode/Dn_port
add wave -noupdate -group decoder /p3_tb/p3_top/uart_module/seq_decode/dst_fifo
add wave -noupdate -group decoder /p3_tb/p3_top/uart_module/seq_decode/Dn_fifo
add wave -noupdate -group decoder /p3_tb/p3_top/uart_module/seq_decode/cmd_fifo
add wave -noupdate {/p3_tb/p3_top/qsys_module/oFIFOS[0]/oFIFO/wclk}
add wave -noupdate {/p3_tb/p3_top/qsys_module/oFIFOS[0]/oFIFO/rclk}
add wave -noupdate {/p3_tb/p3_top/qsys_module/oFIFOS[0]/oFIFO/rst}
add wave -noupdate {/p3_tb/p3_top/qsys_module/oFIFOS[0]/oFIFO/push}
add wave -noupdate {/p3_tb/p3_top/qsys_module/oFIFOS[0]/oFIFO/pop}
add wave -noupdate {/p3_tb/p3_top/qsys_module/oFIFOS[0]/oFIFO/restore_pointers}
add wave -noupdate {/p3_tb/p3_top/qsys_module/oFIFOS[0]/oFIFO/DataInput}
add wave -noupdate {/p3_tb/p3_top/qsys_module/oFIFOS[0]/oFIFO/DataOutput}
add wave -noupdate {/p3_tb/p3_top/qsys_module/oFIFOS[0]/oFIFO/full}
add wave -noupdate {/p3_tb/p3_top/qsys_module/oFIFOS[0]/oFIFO/empty}
add wave -noupdate {/p3_tb/p3_top/qsys_module/oFIFOS[0]/oFIFO/full_flag}
add wave -noupdate {/p3_tb/p3_top/qsys_module/oFIFOS[0]/oFIFO/ram/clk_a}
add wave -noupdate {/p3_tb/p3_top/qsys_module/oFIFOS[0]/oFIFO/ram/clk_b}
add wave -noupdate {/p3_tb/p3_top/qsys_module/oFIFOS[0]/oFIFO/ram/addr_logic}
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {6673000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 471
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {1429649 ps} {20853105 ps}
