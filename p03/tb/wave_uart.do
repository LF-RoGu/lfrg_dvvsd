onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /uart_tb/clk
add wave -noupdate /uart_tb/rst
add wave -noupdate /uart_tb/transmit
add wave -noupdate -radix hexadecimal /uart_tb/data_rx
add wave -noupdate -divider {UART TX}
add wave -noupdate /uart_tb/uut/uart_tx_top/cts
add wave -noupdate -group uart_tx_bin_counter /uart_tb/uut/uart_tx_top/bin_counter_uartTX/clk
add wave -noupdate -group uart_tx_bin_counter /uart_tb/uut/uart_tx_top/bin_counter_uartTX/rst
add wave -noupdate -group uart_tx_bin_counter /uart_tb/uut/uart_tx_top/bin_counter_uartTX/enb
add wave -noupdate -group uart_tx_bin_counter /uart_tb/uut/uart_tx_top/bin_counter_uartTX/ovf
add wave -noupdate -group uart_tx_bin_counter /uart_tb/uut/uart_tx_top/bin_counter_uartTX/count
add wave -noupdate -group uart_tx_bin_counter /uart_tb/uut/uart_tx_top/bin_counter_uartTX/count_r
add wave -noupdate -group uart_tx_bin_counter /uart_tb/uut/uart_tx_top/bin_counter_uartTX/count_nxt
add wave -noupdate -group uart_tx_bin_counter /uart_tb/uut/uart_tx_top/bin_counter_uartTX/ovf_st
add wave -noupdate -group uart_tx_fsm_cntrl /uart_tb/uut/uart_tx_top/uart_tx_fsm/clk
add wave -noupdate -group uart_tx_fsm_cntrl /uart_tb/uut/uart_tx_top/uart_tx_fsm/rst
add wave -noupdate -group uart_tx_fsm_cntrl /uart_tb/uut/uart_tx_top/uart_tx_fsm/transmit
add wave -noupdate -group uart_tx_fsm_cntrl /uart_tb/uut/uart_tx_top/uart_tx_fsm/cts
add wave -noupdate -group uart_tx_fsm_cntrl /uart_tb/uut/uart_tx_top/uart_tx_fsm/ovf
add wave -noupdate -group uart_tx_fsm_cntrl /uart_tb/uut/uart_tx_top/uart_tx_fsm/l_s
add wave -noupdate -group uart_tx_fsm_cntrl /uart_tb/uut/uart_tx_top/uart_tx_fsm/cntr_enb
add wave -noupdate -group uart_tx_fsm_cntrl /uart_tb/uut/uart_tx_top/uart_tx_fsm/reg_enb
add wave -noupdate -group uart_tx_fsm_cntrl /uart_tb/uut/uart_tx_top/uart_tx_fsm/mux_sel
add wave -noupdate -group uart_tx_fsm_cntrl /uart_tb/uut/uart_tx_top/uart_tx_fsm/tx_active
add wave -noupdate -group uart_tx_fsm_cntrl /uart_tb/uut/uart_tx_top/uart_tx_fsm/current_st
add wave -noupdate -group uart_tx_fsm_cntrl /uart_tb/uut/uart_tx_top/uart_tx_fsm/nxt_state
add wave -noupdate -group uart_tx_piso_reg /uart_tb/uut/uart_tx_top/piso_register_lsb_uartTX/clk
add wave -noupdate -group uart_tx_piso_reg /uart_tb/uut/uart_tx_top/piso_register_lsb_uartTX/rst
add wave -noupdate -group uart_tx_piso_reg /uart_tb/uut/uart_tx_top/piso_register_lsb_uartTX/enb
add wave -noupdate -group uart_tx_piso_reg /uart_tb/uut/uart_tx_top/piso_register_lsb_uartTX/l_s
add wave -noupdate -group uart_tx_piso_reg /uart_tb/uut/uart_tx_top/piso_register_lsb_uartTX/i_data
add wave -noupdate -group uart_tx_piso_reg /uart_tb/uut/uart_tx_top/piso_register_lsb_uartTX/o_data
add wave -noupdate -group uart_tx_piso_reg /uart_tb/uut/uart_tx_top/piso_register_lsb_uartTX/rgstr_r
add wave -noupdate -group uart_tx_piso_reg /uart_tb/uut/uart_tx_top/piso_register_lsb_uartTX/rgstr_next
add wave -noupdate -divider {UART RX}
add wave -noupdate /uart_tb/uut/uart_rx_top/cts
add wave -noupdate /uart_tb/uut/uart_rx_top/rts
add wave -noupdate -group uart_rx_bin_counter /uart_tb/uut/uart_rx_top/bin_counter_uartRX/clk
add wave -noupdate -group uart_rx_bin_counter /uart_tb/uut/uart_rx_top/bin_counter_uartRX/rst
add wave -noupdate -group uart_rx_bin_counter /uart_tb/uut/uart_rx_top/bin_counter_uartRX/enb
add wave -noupdate -group uart_rx_bin_counter /uart_tb/uut/uart_rx_top/bin_counter_uartRX/rst_counter
add wave -noupdate -group uart_rx_bin_counter /uart_tb/uut/uart_rx_top/bin_counter_uartRX/ovf
add wave -noupdate -group uart_rx_bin_counter /uart_tb/uut/uart_rx_top/bin_counter_uartRX/count
add wave -noupdate -group uart_rx_bin_counter /uart_tb/uut/uart_rx_top/bin_counter_uartRX/ovf_st
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/clk
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/rst
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/ovf
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/data_in
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/start_cntr_ovf
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/data_cntr_ovf
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/cts
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/rts
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/enable
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/bin_cntr_enb
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/start_cntr_enable
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/data_cntr_enable
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/current_st
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/nxt_state
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/rts_t
add wave -noupdate -group uart_rx_sipo_reg /uart_tb/uut/uart_rx_top/sipo_register_msb_uartRX/clk
add wave -noupdate -group uart_rx_sipo_reg /uart_tb/uut/uart_rx_top/sipo_register_msb_uartRX/rst
add wave -noupdate -group uart_rx_sipo_reg /uart_tb/uut/uart_rx_top/sipo_register_msb_uartRX/enb
add wave -noupdate -group uart_rx_sipo_reg /uart_tb/uut/uart_rx_top/sipo_register_msb_uartRX/data_in
add wave -noupdate -group uart_rx_sipo_reg -radix hexadecimal /uart_tb/uut/uart_rx_top/sipo_register_msb_uartRX/data_out
add wave -noupdate -group uart_rx_start_counter /uart_tb/uut/uart_rx_top/start_counter_uartRx/clk
add wave -noupdate -group uart_rx_start_counter /uart_tb/uut/uart_rx_top/start_counter_uartRx/rst
add wave -noupdate -group uart_rx_start_counter /uart_tb/uut/uart_rx_top/start_counter_uartRx/enb
add wave -noupdate -group uart_rx_start_counter /uart_tb/uut/uart_rx_top/start_counter_uartRx/rst_counter
add wave -noupdate -group uart_rx_start_counter /uart_tb/uut/uart_rx_top/start_counter_uartRx/ovf
add wave -noupdate -group uart_rx_start_counter /uart_tb/uut/uart_rx_top/start_counter_uartRx/count
add wave -noupdate -group uart_rx_start_counter /uart_tb/uut/uart_rx_top/start_counter_uartRx/ovf_st
add wave -noupdate -group uart_rx_data_counter /uart_tb/uut/uart_rx_top/data_counter_uartRx/clk
add wave -noupdate -group uart_rx_data_counter /uart_tb/uut/uart_rx_top/data_counter_uartRx/rst
add wave -noupdate -group uart_rx_data_counter /uart_tb/uut/uart_rx_top/data_counter_uartRx/enb
add wave -noupdate -group uart_rx_data_counter /uart_tb/uut/uart_rx_top/data_counter_uartRx/rst_counter
add wave -noupdate -group uart_rx_data_counter /uart_tb/uut/uart_rx_top/data_counter_uartRx/ovf
add wave -noupdate -group uart_rx_data_counter /uart_tb/uut/uart_rx_top/data_counter_uartRx/count
add wave -noupdate -radix hexadecimal /uart_tb/uut/uart_rx_top/sipo_register_msb_uartRX/data_out
add wave -noupdate -divider DECODER
add wave -noupdate /uart_tb/uut/seq_decode/decoder/clk
add wave -noupdate /uart_tb/uut/seq_decode/decoder/current_st
add wave -noupdate /uart_tb/uut/seq_decode/dst_fifo
add wave -noupdate /uart_tb/uut/seq_decode/Dn_fifo
add wave -noupdate /uart_tb/uut/seq_decode/src_port
add wave -noupdate /uart_tb/uut/seq_decode/cmd_port
add wave -noupdate /uart_tb/uut/seq_decode/valid_data_w
add wave -noupdate -group rts_detector /uart_tb/uut/seq_decode/rts_detector/clk
add wave -noupdate -group rts_detector /uart_tb/uut/seq_decode/rts_detector/rst
add wave -noupdate -group rts_detector /uart_tb/uut/seq_decode/rts_detector/enb
add wave -noupdate -group rts_detector -radix decimal /uart_tb/uut/seq_decode/rts_detector/i_data
add wave -noupdate -group rts_detector -radix decimal /uart_tb/uut/seq_decode/rts_detector/o_data
add wave -noupdate -group {sequence detector} /uart_tb/uut/seq_decode/sequence_detector/clk
add wave -noupdate -group {sequence detector} /uart_tb/uut/seq_decode/sequence_detector/rst
add wave -noupdate -group {sequence detector} /uart_tb/uut/seq_decode/sequence_detector/enb
add wave -noupdate -group {sequence detector} -radix hexadecimal /uart_tb/uut/seq_decode/sequence_detector/i_data
add wave -noupdate -group {sequence detector} -radix hexadecimal /uart_tb/uut/seq_decode/sequence_detector/o_data
add wave -noupdate -group {decoder data counter} /uart_tb/uut/seq_decode/decoder_data_counter/clk
add wave -noupdate -group {decoder data counter} /uart_tb/uut/seq_decode/decoder_data_counter/rst
add wave -noupdate -group {decoder data counter} /uart_tb/uut/seq_decode/decoder_data_counter/enb
add wave -noupdate -group {decoder data counter} -radix decimal /uart_tb/uut/seq_decode/decoder_data_counter/max_count
add wave -noupdate -group {decoder data counter} /uart_tb/uut/seq_decode/decoder_data_counter/ovf
add wave -noupdate -group {decoder data counter} -radix decimal /uart_tb/uut/seq_decode/decoder_data_counter/count
add wave -noupdate -group {decoder data counter} -radix decimal /uart_tb/uut/seq_decode/decoder_data_counter/count_r
add wave -noupdate -group {decoder data counter} -radix decimal /uart_tb/uut/seq_decode/decoder_data_counter/count_nxt
add wave -noupdate -group fsm_decoder /uart_tb/uut/seq_decode/decoder/clk
add wave -noupdate -group fsm_decoder /uart_tb/uut/seq_decode/decoder/rst
add wave -noupdate -group fsm_decoder /uart_tb/uut/seq_decode/decoder/src_portLenght_register
add wave -noupdate -group fsm_decoder /uart_tb/uut/seq_decode/decoder/Dn_fifo
add wave -noupdate -group fsm_decoder /uart_tb/uut/seq_decode/decoder/current_st
add wave -noupdate -group fsm_decoder /uart_tb/uut/seq_decode/decoder/nxt_state
add wave -noupdate -group cmd_detector_register /uart_tb/uut/seq_decode/cmd_detector/enb
add wave -noupdate -group cmd_detector_register -radix hexadecimal /uart_tb/uut/seq_decode/cmd_detector/i_data
add wave -noupdate -group cmd_detector_register -radix hexadecimal /uart_tb/uut/seq_decode/cmd_detector/o_data
add wave -noupdate -group cmd_detector /uart_tb/uut/seq_decode/cmd_detector/enb
add wave -noupdate -group cmd_detector -radix hexadecimal /uart_tb/uut/seq_decode/cmd_detector/i_data
add wave -noupdate -group cmd_detector -radix hexadecimal /uart_tb/uut/seq_decode/cmd_detector/o_data
add wave -noupdate -group l_detector /uart_tb/uut/seq_decode/lenght_detector/enb
add wave -noupdate -group l_detector -radix hexadecimal /uart_tb/uut/seq_decode/lenght_detector/i_data
add wave -noupdate -group l_detector -radix decimal /uart_tb/uut/seq_decode/lenght_detector/o_data
add wave -noupdate -group portLenght_detector /uart_tb/uut/seq_decode/portLenght_detector/enb
add wave -noupdate -group portLenght_detector -radix hexadecimal /uart_tb/uut/seq_decode/portLenght_detector/i_data
add wave -noupdate -group portLenght_detector -radix hexadecimal /uart_tb/uut/seq_decode/portLenght_detector/o_data
add wave -noupdate -group src_detector /uart_tb/uut/seq_decode/src_detector/enb
add wave -noupdate -group src_detector -radix hexadecimal /uart_tb/uut/seq_decode/src_detector/i_data
add wave -noupdate -group src_detector -radix hexadecimal /uart_tb/uut/seq_decode/src_detector/o_data
add wave -noupdate -group dst_detector /uart_tb/uut/seq_decode/dst_detector/enb
add wave -noupdate -group dst_detector -radix hexadecimal /uart_tb/uut/seq_decode/dst_detector/i_data
add wave -noupdate -group dst_detector -radix decimal /uart_tb/uut/seq_decode/dst_detector/o_data
add wave -noupdate -group cmd_vector /uart_tb/uut/seq_decode/cmd_vector/clk
add wave -noupdate -group cmd_vector /uart_tb/uut/seq_decode/cmd_vector/rst
add wave -noupdate -group cmd_vector /uart_tb/uut/seq_decode/cmd_vector/enb
add wave -noupdate -group cmd_vector /uart_tb/uut/seq_decode/cmd_vector/i_data
add wave -noupdate -group cmd_vector -radix decimal /uart_tb/uut/seq_decode/cmd_vector/o_data
add wave -noupdate -group {Dn vector} /uart_tb/uut/seq_decode/Dn_vector/clk
add wave -noupdate -group {Dn vector} /uart_tb/uut/seq_decode/Dn_vector/rst
add wave -noupdate -group {Dn vector} /uart_tb/uut/seq_decode/Dn_vector/enb
add wave -noupdate -group {Dn vector} -radix hexadecimal /uart_tb/uut/seq_decode/Dn_vector/i_data
add wave -noupdate -group {Dn vector} -radix decimal /uart_tb/uut/seq_decode/Dn_vector/o_data
add wave -noupdate -divider PORT
add wave -noupdate /uart_tb/uut/seq_decode/decoder/current_st
add wave -noupdate -radix hexadecimal /uart_tb/uut/seq_decode/src_detector/o_data
add wave -noupdate -radix decimal /uart_tb/uut/seq_decode/dst_detector/o_data
add wave -noupdate -radix decimal /uart_tb/uut/seq_decode/cmd_vector/o_data
add wave -noupdate -radix decimal /uart_tb/uut/seq_decode/Dn_vector/o_data
add wave -noupdate -group {push cmd} /uart_tb/uut/demux_cmd_push/i_data
add wave -noupdate -group {push cmd} /uart_tb/uut/demux_cmd_push/i_sel
add wave -noupdate -group {push cmd} /uart_tb/uut/demux_cmd_push/o_data_a
add wave -noupdate -group {push cmd} /uart_tb/uut/demux_cmd_push/o_data_b
add wave -noupdate -group {push cmd} /uart_tb/uut/demux_cmd_push/o_data_c
add wave -noupdate -group {push cmd} /uart_tb/uut/demux_cmd_push/o_data_d
add wave -noupdate -group {push cmd} /uart_tb/uut/demux_cmd_push/o_data_e
add wave -noupdate -group {push cmd} /uart_tb/uut/demux_cmd_push/o_data_f
add wave -noupdate -group {push cmd} /uart_tb/uut/demux_cmd_push/o_data_g
add wave -noupdate -group {push cmd} /uart_tb/uut/demux_cmd_push/o_data_h
add wave -noupdate -group {push fifo} /uart_tb/uut/demux_data_push/i_data
add wave -noupdate -group {push fifo} /uart_tb/uut/demux_data_push/i_sel
add wave -noupdate -group {push fifo} /uart_tb/uut/demux_data_push/o_data_a
add wave -noupdate -group {push fifo} /uart_tb/uut/demux_data_push/o_data_b
add wave -noupdate -group {push fifo} /uart_tb/uut/demux_data_push/o_data_c
add wave -noupdate -group {push fifo} /uart_tb/uut/demux_data_push/o_data_d
add wave -noupdate -group {push fifo} /uart_tb/uut/demux_data_push/o_data_e
add wave -noupdate -group {push fifo} /uart_tb/uut/demux_data_push/o_data_f
add wave -noupdate -group {push fifo} /uart_tb/uut/demux_data_push/o_data_g
add wave -noupdate -group {push fifo} /uart_tb/uut/demux_data_push/o_data_h
add wave -noupdate -group {cmd vector counter} /uart_tb/uut/seq_decode/cmd_vector_counter/clk
add wave -noupdate -group {cmd vector counter} /uart_tb/uut/seq_decode/cmd_vector_counter/rst
add wave -noupdate -group {cmd vector counter} /uart_tb/uut/seq_decode/cmd_vector_counter/enb
add wave -noupdate -group {cmd vector counter} /uart_tb/uut/seq_decode/cmd_vector_counter/max_count
add wave -noupdate -group {cmd vector counter} /uart_tb/uut/seq_decode/cmd_vector_counter/ovf
add wave -noupdate -group {cmd vector counter} /uart_tb/uut/seq_decode/cmd_vector_counter/count
add wave -noupdate -group {cmd vector counter} /uart_tb/uut/seq_decode/cmd_vector_counter/ovf_st
add wave -noupdate -group {data selector} /uart_tb/uut/data_selector/clk
add wave -noupdate -group {data selector} /uart_tb/uut/data_selector/rst
add wave -noupdate -group {data selector} /uart_tb/uut/data_selector/fifo_load
add wave -noupdate -group {data selector} -radix decimal /uart_tb/uut/data_selector/iuart_data
add wave -noupdate -group {data selector} -radix decimal /uart_tb/uut/data_selector/iuart_cmd
add wave -noupdate -group {data selector} -radix decimal /uart_tb/uut/data_selector/ouart_data
add wave -noupdate -group {data selector} /uart_tb/uut/data_selector/data_selectr
add wave -noupdate -group {data selector} -expand -group {mux data selector} -radix decimal /uart_tb/uut/data_selector/mux_data_transfer/i_a
add wave -noupdate -group {data selector} -expand -group {mux data selector} -radix decimal /uart_tb/uut/data_selector/mux_data_transfer/i_b
add wave -noupdate -group {data selector} -expand -group {mux data selector} /uart_tb/uut/data_selector/mux_data_transfer/i_sel
add wave -noupdate -group {data selector} -expand -group {mux data selector} -radix decimal /uart_tb/uut/data_selector/mux_data_transfer/o_sltd
add wave -noupdate -group iport0 -group {oneshot2dec fifo0} /uart_tb/uut/oneshot_convert_ofifo0/i_oneshot
add wave -noupdate -group iport0 -group {oneshot2dec fifo0} /uart_tb/uut/oneshot_convert_ofifo0/o_dec
add wave -noupdate -group iport0 /uart_tb/uut/iport0/clk
add wave -noupdate -group iport0 /uart_tb/uut/iport0/rst
add wave -noupdate -group iport0 /uart_tb/uut/iport0/i_data
add wave -noupdate -group iport0 -radix decimal /uart_tb/uut/iport0/i_cmd
add wave -noupdate -group iport0 /uart_tb/uut/iport0/port_enable
add wave -noupdate -group iport0 /uart_tb/uut/iport0/execute_req
add wave -noupdate -group iport0 /uart_tb/uut/iport0/load
add wave -noupdate -group iport0 /uart_tb/uut/iport0/iFIFO_data_push
add wave -noupdate -group iport0 /uart_tb/uut/iport0/port_state
add wave -noupdate -group iport0 /uart_tb/uut/iport0/port_full
add wave -noupdate -group iport0 /uart_tb/uut/iport0/port_empty
add wave -noupdate -group iport0 -radix decimal /uart_tb/uut/iport0/o_data
add wave -noupdate -group iport0 -radix decimal /uart_tb/uut/iport0/o_cmd
add wave -noupdate -group iport0 -group {ififo0 cmd} /uart_tb/uut/iport0/iFIFO_cmd/wclk
add wave -noupdate -group iport0 -group {ififo0 cmd} /uart_tb/uut/iport0/iFIFO_cmd/rclk
add wave -noupdate -group iport0 -group {ififo0 cmd} /uart_tb/uut/iport0/iFIFO_cmd/rst
add wave -noupdate -group iport0 -group {ififo0 cmd} /uart_tb/uut/iport0/iFIFO_cmd/push
add wave -noupdate -group iport0 -group {ififo0 cmd} /uart_tb/uut/iport0/iFIFO_cmd/pop
add wave -noupdate -group iport0 -group {ififo0 cmd} -radix decimal /uart_tb/uut/iport0/iFIFO_cmd/DataInput
add wave -noupdate -group iport0 -group {ififo0 cmd} -radix decimal /uart_tb/uut/iport0/iFIFO_cmd/DataOutput
add wave -noupdate -group iport0 -group {ififo0 cmd} /uart_tb/uut/iport0/iFIFO_cmd/full
add wave -noupdate -group iport0 -group {ififo0 cmd} /uart_tb/uut/iport0/iFIFO_cmd/empty
add wave -noupdate -group iport0 -group {ififo0 cmd} -group wpointer -radix decimal /uart_tb/uut/iport0/iFIFO_cmd/wptr_full_and_inc/waddr
add wave -noupdate -group iport0 -group {ififo0 cmd} -group wpointer -radix decimal /uart_tb/uut/iport0/iFIFO_cmd/wptr_full_and_inc/wptr
add wave -noupdate -group iport0 -group {ififo0 cmd} -group {memory interfaze} /uart_tb/uut/iport0/iFIFO_cmd/mem_if/we_a
add wave -noupdate -group iport0 -group {ififo0 cmd} -group {memory interfaze} -radix decimal /uart_tb/uut/iport0/iFIFO_cmd/mem_if/data_a
add wave -noupdate -group iport0 -group {ififo0 cmd} -group {memory interfaze} -radix decimal /uart_tb/uut/iport0/iFIFO_cmd/mem_if/rd_data_a
add wave -noupdate -group iport0 -group {ififo0 cmd} -group {memory interfaze} -radix decimal /uart_tb/uut/iport0/iFIFO_cmd/mem_if/wr_addr_a
add wave -noupdate -group iport0 -group {ififo0 cmd} -group {memory interfaze} -radix decimal /uart_tb/uut/iport0/iFIFO_cmd/mem_if/rd_addr_b
add wave -noupdate -group iport0 -group {ififo0 data} /uart_tb/uut/iport0/iFIFO_data/wclk
add wave -noupdate -group iport0 -group {ififo0 data} /uart_tb/uut/iport0/iFIFO_data/rclk
add wave -noupdate -group iport0 -group {ififo0 data} /uart_tb/uut/iport0/iFIFO_data/rst
add wave -noupdate -group iport0 -group {ififo0 data} /uart_tb/uut/iport0/iFIFO_data/push
add wave -noupdate -group iport0 -group {ififo0 data} /uart_tb/uut/iport0/iFIFO_data/pop
add wave -noupdate -group iport0 -group {ififo0 data} -radix decimal /uart_tb/uut/iport0/iFIFO_data/DataInput
add wave -noupdate -group iport0 -group {ififo0 data} -radix decimal /uart_tb/uut/iport0/iFIFO_data/DataOutput
add wave -noupdate -group iport0 -group {ififo0 data} /uart_tb/uut/iport0/iFIFO_data/full
add wave -noupdate -group iport0 -group {ififo0 data} /uart_tb/uut/iport0/iFIFO_data/empty
add wave -noupdate -group iport0 -group {ififo0 data} -expand -group wpointer -radix decimal /uart_tb/uut/iport0/iFIFO_data/wptr_full_and_inc/waddr
add wave -noupdate -group iport0 -group {ififo0 data} -expand -group wpointer -radix decimal /uart_tb/uut/iport0/iFIFO_data/wptr_full_and_inc/wptr
add wave -noupdate -group iport0 -group {ififo0 data} -expand -group {memory interfaze} /uart_tb/uut/iport0/iFIFO_data/mem_if/we_a
add wave -noupdate -group iport0 -group {ififo0 data} -expand -group {memory interfaze} -radix decimal /uart_tb/uut/iport0/iFIFO_data/mem_if/data_a
add wave -noupdate -group iport0 -group {ififo0 data} -expand -group {memory interfaze} -radix decimal /uart_tb/uut/iport0/iFIFO_data/mem_if/rd_data_a
add wave -noupdate -group iport0 -group {ififo0 data} -expand -group {memory interfaze} -radix decimal /uart_tb/uut/iport0/iFIFO_data/mem_if/wr_addr_a
add wave -noupdate -group iport0 -group {ififo0 data} -expand -group {memory interfaze} -radix decimal /uart_tb/uut/iport0/iFIFO_data/mem_if/rd_addr_b
add wave -noupdate -group iport0 -group {counter iport0} /uart_tb/uut/iport0/data_counter/clk
add wave -noupdate -group iport0 -group {counter iport0} /uart_tb/uut/iport0/data_counter/rst
add wave -noupdate -group iport0 -group {counter iport0} /uart_tb/uut/iport0/data_counter/enb
add wave -noupdate -group iport0 -group {counter iport0} -radix decimal /uart_tb/uut/iport0/data_counter/max_count
add wave -noupdate -group iport0 -group {counter iport0} /uart_tb/uut/iport0/data_counter/ovf
add wave -noupdate -group iport0 -group {counter iport0} /uart_tb/uut/iport0/data_counter/count
add wave -noupdate -group iport1 -group {oneshot2dec fifo1} /uart_tb/uut/oneshot_convert_ofifo1/i_oneshot
add wave -noupdate -group iport1 -group {oneshot2dec fifo1} /uart_tb/uut/oneshot_convert_ofifo1/o_dec
add wave -noupdate -group iport1 /uart_tb/uut/iport1/clk
add wave -noupdate -group iport1 /uart_tb/uut/iport1/rst
add wave -noupdate -group iport1 /uart_tb/uut/iport1/i_data
add wave -noupdate -group iport1 /uart_tb/uut/iport1/i_cmd
add wave -noupdate -group iport1 /uart_tb/uut/iport1/port_enable
add wave -noupdate -group iport1 /uart_tb/uut/iport1/execute_req
add wave -noupdate -group iport1 /uart_tb/uut/iport1/load
add wave -noupdate -group iport1 /uart_tb/uut/iport1/iFIFO_data_push
add wave -noupdate -group iport1 /uart_tb/uut/iport1/port_state
add wave -noupdate -group iport1 /uart_tb/uut/iport1/port_full
add wave -noupdate -group iport1 /uart_tb/uut/iport1/port_empty
add wave -noupdate -group iport1 /uart_tb/uut/iport1/o_data
add wave -noupdate -group iport1 /uart_tb/uut/iport1/o_cmd
add wave -noupdate -group iport1 -group {ififo1 cmd} /uart_tb/uut/iport1/iFIFO_cmd/wclk
add wave -noupdate -group iport1 -group {ififo1 cmd} /uart_tb/uut/iport1/iFIFO_cmd/rclk
add wave -noupdate -group iport1 -group {ififo1 cmd} /uart_tb/uut/iport1/iFIFO_cmd/rst
add wave -noupdate -group iport1 -group {ififo1 cmd} /uart_tb/uut/iport1/iFIFO_cmd/push
add wave -noupdate -group iport1 -group {ififo1 cmd} /uart_tb/uut/iport1/iFIFO_cmd/pop
add wave -noupdate -group iport1 -group {ififo1 cmd} /uart_tb/uut/iport1/iFIFO_cmd/DataInput
add wave -noupdate -group iport1 -group {ififo1 cmd} /uart_tb/uut/iport1/iFIFO_cmd/DataOutput
add wave -noupdate -group iport1 -group {ififo1 cmd} /uart_tb/uut/iport1/iFIFO_cmd/full
add wave -noupdate -group iport1 -group {ififo1 cmd} /uart_tb/uut/iport1/iFIFO_cmd/empty
add wave -noupdate -group iport1 -group {ififo1 cmd} /uart_tb/uut/iport1/iFIFO_cmd/full_flag
add wave -noupdate -group iport1 -group {ififo1 cmd} /uart_tb/uut/iport1/iFIFO_cmd/empty_flag
add wave -noupdate -group iport1 -group {ififo1 cmd} -expand -group {memory interfaze} /uart_tb/uut/iport1/iFIFO_cmd/mem_if/we_a
add wave -noupdate -group iport1 -group {ififo1 cmd} -expand -group {memory interfaze} /uart_tb/uut/iport1/iFIFO_cmd/mem_if/data_a
add wave -noupdate -group iport1 -group {ififo1 cmd} -expand -group {memory interfaze} /uart_tb/uut/iport1/iFIFO_cmd/mem_if/rd_data_a
add wave -noupdate -group iport1 -group {ififo1 cmd} -expand -group {memory interfaze} /uart_tb/uut/iport1/iFIFO_cmd/mem_if/wr_addr_a
add wave -noupdate -group iport1 -group {ififo1 cmd} -expand -group {memory interfaze} /uart_tb/uut/iport1/iFIFO_cmd/mem_if/rd_addr_b
add wave -noupdate -group iport1 -group {ififo1 cmd} -expand -group wpointer /uart_tb/uut/iport1/iFIFO_cmd/wptr_full_and_inc/waddr
add wave -noupdate -group iport1 -group {ififo1 cmd} -expand -group wpointer /uart_tb/uut/iport1/iFIFO_cmd/wptr_full_and_inc/wptr
add wave -noupdate -group iport1 -group {ififo1 data} /uart_tb/uut/iport1/iFIFO_data/wclk
add wave -noupdate -group iport1 -group {ififo1 data} /uart_tb/uut/iport1/iFIFO_data/rclk
add wave -noupdate -group iport1 -group {ififo1 data} /uart_tb/uut/iport1/iFIFO_data/rst
add wave -noupdate -group iport1 -group {ififo1 data} /uart_tb/uut/iport1/iFIFO_data/push
add wave -noupdate -group iport1 -group {ififo1 data} /uart_tb/uut/iport1/iFIFO_data/pop
add wave -noupdate -group iport1 -group {ififo1 data} /uart_tb/uut/iport1/iFIFO_data/DataInput
add wave -noupdate -group iport1 -group {ififo1 data} /uart_tb/uut/iport1/iFIFO_data/DataOutput
add wave -noupdate -group iport1 -group {ififo1 data} /uart_tb/uut/iport1/iFIFO_data/full
add wave -noupdate -group iport1 -group {ififo1 data} /uart_tb/uut/iport1/iFIFO_data/empty
add wave -noupdate -group iport1 -group {ififo1 data} /uart_tb/uut/iport1/iFIFO_data/full_flag
add wave -noupdate -group iport1 -group {ififo1 data} /uart_tb/uut/iport1/iFIFO_data/empty_flag
add wave -noupdate -group iport2 -group {oneshot2dec fifo2} /uart_tb/uut/oneshot_convert_ofifo2/i_oneshot
add wave -noupdate -group iport2 -group {oneshot2dec fifo2} /uart_tb/uut/oneshot_convert_ofifo2/o_dec
add wave -noupdate -group iport2 /uart_tb/uut/iport2/clk
add wave -noupdate -group iport2 /uart_tb/uut/iport2/rst
add wave -noupdate -group iport2 /uart_tb/uut/iport2/i_data
add wave -noupdate -group iport2 /uart_tb/uut/iport2/i_cmd
add wave -noupdate -group iport2 /uart_tb/uut/iport2/port_enable
add wave -noupdate -group iport2 /uart_tb/uut/iport2/execute_req
add wave -noupdate -group iport2 /uart_tb/uut/iport2/load
add wave -noupdate -group iport2 /uart_tb/uut/iport2/iFIFO_data_push
add wave -noupdate -group iport2 /uart_tb/uut/iport2/port_state
add wave -noupdate -group iport2 /uart_tb/uut/iport2/port_full
add wave -noupdate -group iport2 /uart_tb/uut/iport2/port_empty
add wave -noupdate -group iport2 /uart_tb/uut/iport2/o_data
add wave -noupdate -group iport2 /uart_tb/uut/iport2/o_cmd
add wave -noupdate -group iport2 -group {ififo2 cmd} /uart_tb/uut/iport2/iFIFO_cmd/wptr_full_and_inc/waddr
add wave -noupdate -group iport2 -group {ififo2 cmd} /uart_tb/uut/iport2/iFIFO_cmd/wptr_full_and_inc/wptr
add wave -noupdate -group iport2 -group {ififo2 cmd} /uart_tb/uut/iport2/iFIFO_cmd/wclk
add wave -noupdate -group iport2 -group {ififo2 cmd} /uart_tb/uut/iport2/iFIFO_cmd/rclk
add wave -noupdate -group iport2 -group {ififo2 cmd} /uart_tb/uut/iport2/iFIFO_cmd/rst
add wave -noupdate -group iport2 -group {ififo2 cmd} /uart_tb/uut/iport2/iFIFO_cmd/push
add wave -noupdate -group iport2 -group {ififo2 cmd} /uart_tb/uut/iport2/iFIFO_cmd/pop
add wave -noupdate -group iport2 -group {ififo2 cmd} -radix decimal /uart_tb/uut/iport2/iFIFO_cmd/DataInput
add wave -noupdate -group iport2 -group {ififo2 cmd} -radix decimal /uart_tb/uut/iport2/iFIFO_cmd/DataOutput
add wave -noupdate -group iport2 -group {ififo2 cmd} /uart_tb/uut/iport2/iFIFO_cmd/full
add wave -noupdate -group iport2 -group {ififo2 cmd} /uart_tb/uut/iport2/iFIFO_cmd/empty
add wave -noupdate -group iport2 -group {ififo2 cmd} /uart_tb/uut/iport2/iFIFO_cmd/full_flag
add wave -noupdate -group iport2 -group {ififo2 cmd} /uart_tb/uut/iport2/iFIFO_cmd/empty_flag
add wave -noupdate -group iport2 -group {ififo2 cmd} -expand -group {memory if} /uart_tb/uut/iport2/iFIFO_cmd/mem_if/we_a
add wave -noupdate -group iport2 -group {ififo2 cmd} -expand -group {memory if} -radix decimal /uart_tb/uut/iport2/iFIFO_cmd/mem_if/data_a
add wave -noupdate -group iport2 -group {ififo2 cmd} -expand -group {memory if} -radix decimal /uart_tb/uut/iport2/iFIFO_cmd/mem_if/rd_data_a
add wave -noupdate -group iport2 -group {ififo2 cmd} -expand -group {memory if} -radix decimal /uart_tb/uut/iport2/iFIFO_cmd/mem_if/wr_addr_a
add wave -noupdate -group iport2 -group {ififo2 cmd} -expand -group {memory if} -radix decimal /uart_tb/uut/iport2/iFIFO_cmd/mem_if/rd_addr_b
add wave -noupdate -group iport2 -group {ififo2 cmd} -expand -group wpointer -radix decimal /uart_tb/uut/iport2/iFIFO_cmd/wptr_full_and_inc/waddr
add wave -noupdate -group iport2 -group {ififo2 cmd} -expand -group wpointer -radix decimal /uart_tb/uut/iport2/iFIFO_cmd/wptr_full_and_inc/wptr
add wave -noupdate -group iport2 -group {ififo2 data} /uart_tb/uut/iport2/iFIFO_data/wclk
add wave -noupdate -group iport2 -group {ififo2 data} /uart_tb/uut/iport2/iFIFO_data/rclk
add wave -noupdate -group iport2 -group {ififo2 data} /uart_tb/uut/iport2/iFIFO_data/rst
add wave -noupdate -group iport2 -group {ififo2 data} /uart_tb/uut/iport2/iFIFO_data/push
add wave -noupdate -group iport2 -group {ififo2 data} /uart_tb/uut/iport2/iFIFO_data/pop
add wave -noupdate -group iport2 -group {ififo2 data} /uart_tb/uut/iport2/iFIFO_data/DataInput
add wave -noupdate -group iport2 -group {ififo2 data} /uart_tb/uut/iport2/iFIFO_data/DataOutput
add wave -noupdate -group iport2 -group {ififo2 data} /uart_tb/uut/iport2/iFIFO_data/full
add wave -noupdate -group iport2 -group {ififo2 data} /uart_tb/uut/iport2/iFIFO_data/empty
add wave -noupdate -group iport2 -group {ififo2 data} /uart_tb/uut/iport2/iFIFO_data/full_flag
add wave -noupdate -group iport2 -group {ififo2 data} /uart_tb/uut/iport2/iFIFO_data/empty_flag
add wave -noupdate -group iport2 -group {ififo2 data} -expand -group {memory if} /uart_tb/uut/iport2/iFIFO_data/mem_if/we_a
add wave -noupdate -group iport2 -group {ififo2 data} -expand -group {memory if} -radix decimal /uart_tb/uut/iport2/iFIFO_data/mem_if/data_a
add wave -noupdate -group iport2 -group {ififo2 data} -expand -group {memory if} -radix decimal /uart_tb/uut/iport2/iFIFO_data/mem_if/rd_data_a
add wave -noupdate -group iport2 -group {ififo2 data} -expand -group {memory if} -radix decimal /uart_tb/uut/iport2/iFIFO_data/mem_if/wr_addr_a
add wave -noupdate -group iport2 -group {ififo2 data} -expand -group {memory if} -radix decimal /uart_tb/uut/iport2/iFIFO_data/mem_if/rd_addr_b
add wave -noupdate -group iport2 -group {ififo2 data} -expand -group wpointer -radix decimal /uart_tb/uut/iport2/iFIFO_data/wptr_full_and_inc/waddr
add wave -noupdate -group iport2 -group {ififo2 data} -expand -group wpointer -radix decimal /uart_tb/uut/iport2/iFIFO_data/wptr_full_and_inc/wptr
add wave -noupdate -group iport3 -group {oneshot2dec fifo3} /uart_tb/uut/oneshot_convert_ofifo3/i_oneshot
add wave -noupdate -group iport3 -group {oneshot2dec fifo3} /uart_tb/uut/oneshot_convert_ofifo3/o_dec
add wave -noupdate -group iport3 /uart_tb/uut/iport3/clk
add wave -noupdate -group iport3 /uart_tb/uut/iport3/rst
add wave -noupdate -group iport3 /uart_tb/uut/iport3/i_data
add wave -noupdate -group iport3 /uart_tb/uut/iport3/i_cmd
add wave -noupdate -group iport3 /uart_tb/uut/iport3/port_enable
add wave -noupdate -group iport3 /uart_tb/uut/iport3/execute_req
add wave -noupdate -group iport3 /uart_tb/uut/iport3/load
add wave -noupdate -group iport3 /uart_tb/uut/iport3/iFIFO_data_push
add wave -noupdate -group iport3 /uart_tb/uut/iport3/port_state
add wave -noupdate -group iport3 /uart_tb/uut/iport3/port_full
add wave -noupdate -group iport3 /uart_tb/uut/iport3/port_empty
add wave -noupdate -group iport3 /uart_tb/uut/iport3/o_data
add wave -noupdate -group iport3 /uart_tb/uut/iport3/o_cmd
add wave -noupdate -group iport3 -group {ififo3 cmd} /uart_tb/uut/iport3/iFIFO_cmd/wclk
add wave -noupdate -group iport3 -group {ififo3 cmd} /uart_tb/uut/iport3/iFIFO_cmd/rclk
add wave -noupdate -group iport3 -group {ififo3 cmd} /uart_tb/uut/iport3/iFIFO_cmd/rst
add wave -noupdate -group iport3 -group {ififo3 cmd} /uart_tb/uut/iport3/iFIFO_cmd/push
add wave -noupdate -group iport3 -group {ififo3 cmd} /uart_tb/uut/iport3/iFIFO_cmd/pop
add wave -noupdate -group iport3 -group {ififo3 cmd} /uart_tb/uut/iport3/iFIFO_cmd/DataInput
add wave -noupdate -group iport3 -group {ififo3 cmd} /uart_tb/uut/iport3/iFIFO_cmd/DataOutput
add wave -noupdate -group iport3 -group {ififo3 cmd} /uart_tb/uut/iport3/iFIFO_cmd/full
add wave -noupdate -group iport3 -group {ififo3 cmd} /uart_tb/uut/iport3/iFIFO_cmd/empty
add wave -noupdate -group iport3 -group {ififo3 cmd} /uart_tb/uut/iport3/iFIFO_cmd/full_flag
add wave -noupdate -group iport3 -group {ififo3 cmd} /uart_tb/uut/iport3/iFIFO_cmd/empty_flag
add wave -noupdate -group iport3 -group {ififo3 data} /uart_tb/uut/iport3/iFIFO_data/wclk
add wave -noupdate -group iport3 -group {ififo3 data} /uart_tb/uut/iport3/iFIFO_data/rclk
add wave -noupdate -group iport3 -group {ififo3 data} /uart_tb/uut/iport3/iFIFO_data/rst
add wave -noupdate -group iport3 -group {ififo3 data} /uart_tb/uut/iport3/iFIFO_data/push
add wave -noupdate -group iport3 -group {ififo3 data} /uart_tb/uut/iport3/iFIFO_data/pop
add wave -noupdate -group iport3 -group {ififo3 data} /uart_tb/uut/iport3/iFIFO_data/DataInput
add wave -noupdate -group iport3 -group {ififo3 data} /uart_tb/uut/iport3/iFIFO_data/DataOutput
add wave -noupdate -group iport3 -group {ififo3 data} /uart_tb/uut/iport3/iFIFO_data/full
add wave -noupdate -group iport3 -group {ififo3 data} /uart_tb/uut/iport3/iFIFO_data/empty
add wave -noupdate -group iport3 -group {ififo3 data} /uart_tb/uut/iport3/iFIFO_data/full_flag
add wave -noupdate -group iport3 -group {ififo3 data} /uart_tb/uut/iport3/iFIFO_data/empty_flag
add wave -noupdate -group iport4 -group {oneshot2dec fifo4} /uart_tb/uut/oneshot_convert_ofifo4/i_oneshot
add wave -noupdate -group iport4 -group {oneshot2dec fifo4} /uart_tb/uut/oneshot_convert_ofifo4/o_dec
add wave -noupdate -group iport4 /uart_tb/uut/iport4/clk
add wave -noupdate -group iport4 /uart_tb/uut/iport4/rst
add wave -noupdate -group iport4 /uart_tb/uut/iport4/i_data
add wave -noupdate -group iport4 /uart_tb/uut/iport4/i_cmd
add wave -noupdate -group iport4 /uart_tb/uut/iport4/port_enable
add wave -noupdate -group iport4 /uart_tb/uut/iport4/execute_req
add wave -noupdate -group iport4 /uart_tb/uut/iport4/load
add wave -noupdate -group iport4 /uart_tb/uut/iport4/iFIFO_data_push
add wave -noupdate -group iport4 /uart_tb/uut/iport4/port_state
add wave -noupdate -group iport4 /uart_tb/uut/iport4/port_full
add wave -noupdate -group iport4 /uart_tb/uut/iport4/port_empty
add wave -noupdate -group iport4 /uart_tb/uut/iport4/o_data
add wave -noupdate -group iport4 /uart_tb/uut/iport4/o_cmd
add wave -noupdate -group iport4 -group {ififo4 cmd} /uart_tb/uut/iport4/iFIFO_cmd/wclk
add wave -noupdate -group iport4 -group {ififo4 cmd} /uart_tb/uut/iport4/iFIFO_cmd/rclk
add wave -noupdate -group iport4 -group {ififo4 cmd} /uart_tb/uut/iport4/iFIFO_cmd/rst
add wave -noupdate -group iport4 -group {ififo4 cmd} /uart_tb/uut/iport4/iFIFO_cmd/push
add wave -noupdate -group iport4 -group {ififo4 cmd} /uart_tb/uut/iport4/iFIFO_cmd/pop
add wave -noupdate -group iport4 -group {ififo4 cmd} -radix binary /uart_tb/uut/iport4/iFIFO_cmd/DataInput
add wave -noupdate -group iport4 -group {ififo4 cmd} -radix binary /uart_tb/uut/iport4/iFIFO_cmd/DataOutput
add wave -noupdate -group iport4 -group {ififo4 cmd} /uart_tb/uut/iport4/iFIFO_cmd/full
add wave -noupdate -group iport4 -group {ififo4 cmd} /uart_tb/uut/iport4/iFIFO_cmd/empty
add wave -noupdate -group iport4 -group {ififo4 cmd} /uart_tb/uut/iport4/iFIFO_cmd/full_flag
add wave -noupdate -group iport4 -group {ififo4 cmd} /uart_tb/uut/iport4/iFIFO_cmd/empty_flag
add wave -noupdate -group iport4 -group {ififo4 data} /uart_tb/uut/iport4/iFIFO_data/wclk
add wave -noupdate -group iport4 -group {ififo4 data} /uart_tb/uut/iport4/iFIFO_data/rclk
add wave -noupdate -group iport4 -group {ififo4 data} /uart_tb/uut/iport4/iFIFO_data/rst
add wave -noupdate -group iport4 -group {ififo4 data} /uart_tb/uut/iport4/iFIFO_data/push
add wave -noupdate -group iport4 -group {ififo4 data} /uart_tb/uut/iport4/iFIFO_data/pop
add wave -noupdate -group iport4 -group {ififo4 data} /uart_tb/uut/iport4/iFIFO_data/DataInput
add wave -noupdate -group iport4 -group {ififo4 data} /uart_tb/uut/iport4/iFIFO_data/DataOutput
add wave -noupdate -group iport4 -group {ififo4 data} /uart_tb/uut/iport4/iFIFO_data/full
add wave -noupdate -group iport4 -group {ififo4 data} /uart_tb/uut/iport4/iFIFO_data/empty
add wave -noupdate -group iport4 -group {ififo4 data} /uart_tb/uut/iport4/iFIFO_data/full_flag
add wave -noupdate -group iport5 -group {oneshot2dec fifo5} /uart_tb/uut/oneshot_convert_ofifo5/i_oneshot
add wave -noupdate -group iport5 -group {oneshot2dec fifo5} /uart_tb/uut/oneshot_convert_ofifo5/o_dec
add wave -noupdate -group iport5 /uart_tb/uut/iport5/clk
add wave -noupdate -group iport5 /uart_tb/uut/iport5/rst
add wave -noupdate -group iport5 /uart_tb/uut/iport5/i_data
add wave -noupdate -group iport5 /uart_tb/uut/iport5/i_cmd
add wave -noupdate -group iport5 /uart_tb/uut/iport5/port_enable
add wave -noupdate -group iport5 /uart_tb/uut/iport5/execute_req
add wave -noupdate -group iport5 /uart_tb/uut/iport5/load
add wave -noupdate -group iport5 /uart_tb/uut/iport5/iFIFO_data_push
add wave -noupdate -group iport5 /uart_tb/uut/iport5/port_state
add wave -noupdate -group iport5 /uart_tb/uut/iport5/port_full
add wave -noupdate -group iport5 /uart_tb/uut/iport5/port_empty
add wave -noupdate -group iport5 /uart_tb/uut/iport5/o_data
add wave -noupdate -group iport5 /uart_tb/uut/iport5/o_cmd
add wave -noupdate -group iport5 -group {ififo5 cmd} /uart_tb/uut/iport5/iFIFO_cmd/wclk
add wave -noupdate -group iport5 -group {ififo5 cmd} /uart_tb/uut/iport5/iFIFO_cmd/rclk
add wave -noupdate -group iport5 -group {ififo5 cmd} /uart_tb/uut/iport5/iFIFO_cmd/rst
add wave -noupdate -group iport5 -group {ififo5 cmd} /uart_tb/uut/iport5/iFIFO_cmd/push
add wave -noupdate -group iport5 -group {ififo5 cmd} /uart_tb/uut/iport5/iFIFO_cmd/pop
add wave -noupdate -group iport5 -group {ififo5 cmd} /uart_tb/uut/iport5/iFIFO_cmd/DataInput
add wave -noupdate -group iport5 -group {ififo5 cmd} /uart_tb/uut/iport5/iFIFO_cmd/DataOutput
add wave -noupdate -group iport5 -group {ififo5 cmd} /uart_tb/uut/iport5/iFIFO_cmd/full
add wave -noupdate -group iport5 -group {ififo5 cmd} /uart_tb/uut/iport5/iFIFO_cmd/empty
add wave -noupdate -group iport5 -group {ififo5 cmd} /uart_tb/uut/iport5/iFIFO_cmd/full_flag
add wave -noupdate -group iport5 -group {ififo5 cmd} /uart_tb/uut/iport5/iFIFO_cmd/empty_flag
add wave -noupdate -group iport5 -group {ififo5 data} /uart_tb/uut/iport5/iFIFO_data/wclk
add wave -noupdate -group iport5 -group {ififo5 data} /uart_tb/uut/iport5/iFIFO_data/rclk
add wave -noupdate -group iport5 -group {ififo5 data} /uart_tb/uut/iport5/iFIFO_data/rst
add wave -noupdate -group iport5 -group {ififo5 data} /uart_tb/uut/iport5/iFIFO_data/push
add wave -noupdate -group iport5 -group {ififo5 data} /uart_tb/uut/iport5/iFIFO_data/pop
add wave -noupdate -group iport5 -group {ififo5 data} /uart_tb/uut/iport5/iFIFO_data/DataInput
add wave -noupdate -group iport5 -group {ififo5 data} /uart_tb/uut/iport5/iFIFO_data/DataOutput
add wave -noupdate -group iport5 -group {ififo5 data} /uart_tb/uut/iport5/iFIFO_data/full
add wave -noupdate -group iport5 -group {ififo5 data} /uart_tb/uut/iport5/iFIFO_data/empty
add wave -noupdate -group iport5 -group {ififo5 data} /uart_tb/uut/iport5/iFIFO_data/full_flag
add wave -noupdate -group iport5 -group {ififo5 data} /uart_tb/uut/iport5/iFIFO_data/empty_flag
add wave -noupdate -group iport6 -group {oneshot2dec fifo6} /uart_tb/uut/oneshot_convert_ofifo6/i_oneshot
add wave -noupdate -group iport6 -group {oneshot2dec fifo6} /uart_tb/uut/oneshot_convert_ofifo6/o_dec
add wave -noupdate -group iport6 /uart_tb/uut/iport6/clk
add wave -noupdate -group iport6 /uart_tb/uut/iport6/rst
add wave -noupdate -group iport6 /uart_tb/uut/iport6/i_data
add wave -noupdate -group iport6 /uart_tb/uut/iport6/i_cmd
add wave -noupdate -group iport6 /uart_tb/uut/iport6/port_enable
add wave -noupdate -group iport6 /uart_tb/uut/iport6/execute_req
add wave -noupdate -group iport6 /uart_tb/uut/iport6/load
add wave -noupdate -group iport6 /uart_tb/uut/iport6/iFIFO_data_push
add wave -noupdate -group iport6 /uart_tb/uut/iport6/port_state
add wave -noupdate -group iport6 /uart_tb/uut/iport6/port_full
add wave -noupdate -group iport6 /uart_tb/uut/iport6/port_empty
add wave -noupdate -group iport6 /uart_tb/uut/iport6/o_data
add wave -noupdate -group iport6 /uart_tb/uut/iport6/o_cmd
add wave -noupdate -group iport6 -group {ififo6 cmd} /uart_tb/uut/iport6/iFIFO_cmd/wclk
add wave -noupdate -group iport6 -group {ififo6 cmd} /uart_tb/uut/iport6/iFIFO_cmd/rclk
add wave -noupdate -group iport6 -group {ififo6 cmd} /uart_tb/uut/iport6/iFIFO_cmd/rst
add wave -noupdate -group iport6 -group {ififo6 cmd} /uart_tb/uut/iport6/iFIFO_cmd/push
add wave -noupdate -group iport6 -group {ififo6 cmd} /uart_tb/uut/iport6/iFIFO_cmd/pop
add wave -noupdate -group iport6 -group {ififo6 cmd} /uart_tb/uut/iport6/iFIFO_cmd/DataInput
add wave -noupdate -group iport6 -group {ififo6 cmd} /uart_tb/uut/iport6/iFIFO_cmd/DataOutput
add wave -noupdate -group iport6 -group {ififo6 cmd} /uart_tb/uut/iport6/iFIFO_cmd/full
add wave -noupdate -group iport6 -group {ififo6 cmd} /uart_tb/uut/iport6/iFIFO_cmd/empty
add wave -noupdate -group iport6 -group {ififo6 cmd} /uart_tb/uut/iport6/iFIFO_cmd/full_flag
add wave -noupdate -group iport6 -group {ififo6 cmd} /uart_tb/uut/iport6/iFIFO_cmd/empty_flag
add wave -noupdate -group iport6 -group {ififo6 data} /uart_tb/uut/iport6/iFIFO_data/wclk
add wave -noupdate -group iport6 -group {ififo6 data} /uart_tb/uut/iport6/iFIFO_data/rclk
add wave -noupdate -group iport6 -group {ififo6 data} /uart_tb/uut/iport6/iFIFO_data/rst
add wave -noupdate -group iport6 -group {ififo6 data} /uart_tb/uut/iport6/iFIFO_data/push
add wave -noupdate -group iport6 -group {ififo6 data} /uart_tb/uut/iport6/iFIFO_data/pop
add wave -noupdate -group iport6 -group {ififo6 data} /uart_tb/uut/iport6/iFIFO_data/DataInput
add wave -noupdate -group iport6 -group {ififo6 data} /uart_tb/uut/iport6/iFIFO_data/DataOutput
add wave -noupdate -group iport6 -group {ififo6 data} /uart_tb/uut/iport6/iFIFO_data/full
add wave -noupdate -group iport6 -group {ififo6 data} /uart_tb/uut/iport6/iFIFO_data/empty
add wave -noupdate -group iport6 -group {ififo6 data} /uart_tb/uut/iport6/iFIFO_data/full_flag
add wave -noupdate -group iport6 -group {ififo6 data} /uart_tb/uut/iport6/iFIFO_data/empty_flag
add wave -noupdate -group iport7 -group {oneshot2dec fifo7} /uart_tb/uut/oneshot_convert_ofifo7/i_oneshot
add wave -noupdate -group iport7 -group {oneshot2dec fifo7} /uart_tb/uut/oneshot_convert_ofifo7/o_dec
add wave -noupdate -group iport7 /uart_tb/uut/iport7/clk
add wave -noupdate -group iport7 /uart_tb/uut/iport7/rst
add wave -noupdate -group iport7 /uart_tb/uut/iport7/i_data
add wave -noupdate -group iport7 /uart_tb/uut/iport7/i_cmd
add wave -noupdate -group iport7 /uart_tb/uut/iport7/port_enable
add wave -noupdate -group iport7 /uart_tb/uut/iport7/execute_req
add wave -noupdate -group iport7 /uart_tb/uut/iport7/load
add wave -noupdate -group iport7 /uart_tb/uut/iport7/iFIFO_data_push
add wave -noupdate -group iport7 /uart_tb/uut/iport7/port_state
add wave -noupdate -group iport7 /uart_tb/uut/iport7/port_full
add wave -noupdate -group iport7 /uart_tb/uut/iport7/port_empty
add wave -noupdate -group iport7 /uart_tb/uut/iport7/o_data
add wave -noupdate -group iport7 /uart_tb/uut/iport7/o_cmd
add wave -noupdate -group iport7 -group {ififo7 cmd} /uart_tb/uut/iport7/iFIFO_cmd/wclk
add wave -noupdate -group iport7 -group {ififo7 cmd} /uart_tb/uut/iport7/iFIFO_cmd/rclk
add wave -noupdate -group iport7 -group {ififo7 cmd} /uart_tb/uut/iport7/iFIFO_cmd/rst
add wave -noupdate -group iport7 -group {ififo7 cmd} /uart_tb/uut/iport7/iFIFO_cmd/push
add wave -noupdate -group iport7 -group {ififo7 cmd} /uart_tb/uut/iport7/iFIFO_cmd/pop
add wave -noupdate -group iport7 -group {ififo7 cmd} /uart_tb/uut/iport7/iFIFO_cmd/DataInput
add wave -noupdate -group iport7 -group {ififo7 cmd} /uart_tb/uut/iport7/iFIFO_cmd/DataOutput
add wave -noupdate -group iport7 -group {ififo7 cmd} /uart_tb/uut/iport7/iFIFO_cmd/full
add wave -noupdate -group iport7 -group {ififo7 cmd} /uart_tb/uut/iport7/iFIFO_cmd/empty
add wave -noupdate -group iport7 -group {ififo7 cmd} /uart_tb/uut/iport7/iFIFO_cmd/full_flag
add wave -noupdate -group iport7 -group {ififo7 cmd} /uart_tb/uut/iport7/iFIFO_cmd/empty_flag
add wave -noupdate -group iport7 -group {ififo7 data} /uart_tb/uut/iport7/iFIFO_data/wclk
add wave -noupdate -group iport7 -group {ififo7 data} /uart_tb/uut/iport7/iFIFO_data/rclk
add wave -noupdate -group iport7 -group {ififo7 data} /uart_tb/uut/iport7/iFIFO_data/rst
add wave -noupdate -group iport7 -group {ififo7 data} /uart_tb/uut/iport7/iFIFO_data/push
add wave -noupdate -group iport7 -group {ififo7 data} /uart_tb/uut/iport7/iFIFO_data/pop
add wave -noupdate -group iport7 -group {ififo7 data} /uart_tb/uut/iport7/iFIFO_data/DataInput
add wave -noupdate -group iport7 -group {ififo7 data} /uart_tb/uut/iport7/iFIFO_data/DataOutput
add wave -noupdate -group iport7 -group {ififo7 data} /uart_tb/uut/iport7/iFIFO_data/full
add wave -noupdate -group iport7 -group {ififo7 data} /uart_tb/uut/iport7/iFIFO_data/empty
add wave -noupdate -group iport7 -group {ififo7 data} /uart_tb/uut/iport7/iFIFO_data/full_flag
add wave -noupdate -group iport7 -group {ififo7 data} /uart_tb/uut/iport7/iFIFO_data/empty_flag
add wave -noupdate -divider WCRR
add wave -noupdate -radix binary /uart_tb/uut/iport0/o_cmd
add wave -noupdate -radix binary /uart_tb/uut/iport1/o_cmd
add wave -noupdate -radix binary /uart_tb/uut/iport2/o_cmd
add wave -noupdate -radix binary /uart_tb/uut/iport3/o_cmd
add wave -noupdate -radix binary /uart_tb/uut/iport5/o_cmd
add wave -noupdate -radix binary /uart_tb/uut/iport6/o_cmd
add wave -noupdate -radix binary /uart_tb/uut/iport7/o_cmd
add wave -noupdate -radix binary /uart_tb/uut/iport4/o_cmd
add wave -noupdate -radix decimal /uart_tb/uut/rvg0
add wave -noupdate -radix decimal /uart_tb/uut/rvg1
add wave -noupdate -radix decimal /uart_tb/uut/rvg2
add wave -noupdate -radix decimal /uart_tb/uut/rvg3
add wave -noupdate -radix decimal /uart_tb/uut/rvg4
add wave -noupdate -radix decimal /uart_tb/uut/rvg5
add wave -noupdate -radix decimal /uart_tb/uut/rvg6
add wave -noupdate -radix decimal /uart_tb/uut/rvg7
add wave -noupdate -radix hexadecimal /uart_tb/uut/seq_decode/src_detector/o_data
add wave -noupdate -radix hexadecimal /uart_tb/uut/seq_decode/dst_detector/o_data
add wave -noupdate /uart_tb/uut/seq_decode/cmd_port
add wave -noupdate -group {demux wcrr0} /uart_tb/uut/demux_rvg_port0/i_data
add wave -noupdate -group {demux wcrr0} -radix binary /uart_tb/uut/demux_rvg_port0/i_sel
add wave -noupdate -group {demux wcrr0} /uart_tb/uut/demux_rvg_port0/o_data_a
add wave -noupdate -group {demux wcrr0} /uart_tb/uut/demux_rvg_port0/o_data_b
add wave -noupdate -group {demux wcrr0} /uart_tb/uut/demux_rvg_port0/o_data_c
add wave -noupdate -group {demux wcrr0} /uart_tb/uut/demux_rvg_port0/o_data_d
add wave -noupdate -group {demux wcrr0} /uart_tb/uut/demux_rvg_port0/o_data_e
add wave -noupdate -group {demux wcrr0} /uart_tb/uut/demux_rvg_port0/o_data_f
add wave -noupdate -group {demux wcrr0} /uart_tb/uut/demux_rvg_port0/o_data_g
add wave -noupdate -group {demux wcrr0} /uart_tb/uut/demux_rvg_port0/o_data_h
add wave -noupdate -group wcrr_fifo0 /uart_tb/uut/wcrr_fifo0/clk
add wave -noupdate -group wcrr_fifo0 /uart_tb/uut/wcrr_fifo0/rst
add wave -noupdate -group wcrr_fifo0 /uart_tb/uut/wcrr_fifo0/ack
add wave -noupdate -group wcrr_fifo0 /uart_tb/uut/wcrr_fifo0/request_vector
add wave -noupdate -group wcrr_fifo0 /uart_tb/uut/wcrr_fifo0/valid
add wave -noupdate -group wcrr_fifo0 /uart_tb/uut/wcrr_fifo0/grant
add wave -noupdate -group {or wcrr0} /uart_tb/uut/or_rvg0/i_a
add wave -noupdate -group {or wcrr0} /uart_tb/uut/or_rvg0/i_b
add wave -noupdate -group {or wcrr0} /uart_tb/uut/or_rvg0/i_c
add wave -noupdate -group {or wcrr0} /uart_tb/uut/or_rvg0/i_d
add wave -noupdate -group {or wcrr0} /uart_tb/uut/or_rvg0/i_e
add wave -noupdate -group {or wcrr0} /uart_tb/uut/or_rvg0/i_f
add wave -noupdate -group {or wcrr0} /uart_tb/uut/or_rvg0/i_g
add wave -noupdate -group {or wcrr0} /uart_tb/uut/or_rvg0/i_h
add wave -noupdate -group {or wcrr0} /uart_tb/uut/or_rvg0/o_sltd
add wave -noupdate -group {demux wcrr1} /uart_tb/uut/demux_rvg_port1/i_data
add wave -noupdate -group {demux wcrr1} /uart_tb/uut/demux_rvg_port1/i_sel
add wave -noupdate -group {demux wcrr1} /uart_tb/uut/demux_rvg_port1/o_data_a
add wave -noupdate -group {demux wcrr1} /uart_tb/uut/demux_rvg_port1/o_data_b
add wave -noupdate -group {demux wcrr1} /uart_tb/uut/demux_rvg_port1/o_data_c
add wave -noupdate -group {demux wcrr1} /uart_tb/uut/demux_rvg_port1/o_data_d
add wave -noupdate -group {demux wcrr1} /uart_tb/uut/demux_rvg_port1/o_data_e
add wave -noupdate -group {demux wcrr1} /uart_tb/uut/demux_rvg_port1/o_data_f
add wave -noupdate -group {demux wcrr1} /uart_tb/uut/demux_rvg_port1/o_data_g
add wave -noupdate -group {demux wcrr1} /uart_tb/uut/demux_rvg_port1/o_data_h
add wave -noupdate -group wcrr_fifo1 /uart_tb/uut/wcrr_fifo1/clk
add wave -noupdate -group wcrr_fifo1 /uart_tb/uut/wcrr_fifo1/rst
add wave -noupdate -group wcrr_fifo1 /uart_tb/uut/wcrr_fifo1/ack
add wave -noupdate -group wcrr_fifo1 /uart_tb/uut/wcrr_fifo1/request_vector
add wave -noupdate -group wcrr_fifo1 /uart_tb/uut/wcrr_fifo1/valid
add wave -noupdate -group wcrr_fifo1 /uart_tb/uut/wcrr_fifo1/grant
add wave -noupdate -group {demux wcrr2} /uart_tb/uut/demux_rvg_port2/i_data
add wave -noupdate -group {demux wcrr2} /uart_tb/uut/demux_rvg_port2/i_sel
add wave -noupdate -group {demux wcrr2} /uart_tb/uut/demux_rvg_port2/o_data_a
add wave -noupdate -group {demux wcrr2} /uart_tb/uut/demux_rvg_port2/o_data_b
add wave -noupdate -group {demux wcrr2} /uart_tb/uut/demux_rvg_port2/o_data_c
add wave -noupdate -group {demux wcrr2} /uart_tb/uut/demux_rvg_port2/o_data_d
add wave -noupdate -group {demux wcrr2} /uart_tb/uut/demux_rvg_port2/o_data_e
add wave -noupdate -group {demux wcrr2} /uart_tb/uut/demux_rvg_port2/o_data_f
add wave -noupdate -group {demux wcrr2} /uart_tb/uut/demux_rvg_port2/o_data_g
add wave -noupdate -group {demux wcrr2} /uart_tb/uut/demux_rvg_port2/o_data_h
add wave -noupdate -group wcrr_fifo2 /uart_tb/uut/wcrr_fifo2/clk
add wave -noupdate -group wcrr_fifo2 /uart_tb/uut/wcrr_fifo2/rst
add wave -noupdate -group wcrr_fifo2 /uart_tb/uut/wcrr_fifo2/ack
add wave -noupdate -group wcrr_fifo2 /uart_tb/uut/wcrr_fifo2/request_vector
add wave -noupdate -group wcrr_fifo2 /uart_tb/uut/wcrr_fifo2/valid
add wave -noupdate -group wcrr_fifo2 /uart_tb/uut/wcrr_fifo2/grant
add wave -noupdate -group {or wcrr2} /uart_tb/uut/or_rvg2/i_a
add wave -noupdate -group {or wcrr2} /uart_tb/uut/or_rvg2/i_b
add wave -noupdate -group {or wcrr2} /uart_tb/uut/or_rvg2/i_c
add wave -noupdate -group {or wcrr2} /uart_tb/uut/or_rvg2/i_d
add wave -noupdate -group {or wcrr2} /uart_tb/uut/or_rvg2/i_e
add wave -noupdate -group {or wcrr2} /uart_tb/uut/or_rvg2/i_f
add wave -noupdate -group {or wcrr2} /uart_tb/uut/or_rvg2/i_g
add wave -noupdate -group {or wcrr2} /uart_tb/uut/or_rvg2/i_h
add wave -noupdate -group {or wcrr2} /uart_tb/uut/or_rvg2/o_sltd
add wave -noupdate -group wcrr_fifo3 /uart_tb/uut/wcrr_fifo3/clk
add wave -noupdate -group wcrr_fifo3 /uart_tb/uut/wcrr_fifo3/rst
add wave -noupdate -group wcrr_fifo3 /uart_tb/uut/wcrr_fifo3/ack
add wave -noupdate -group wcrr_fifo3 /uart_tb/uut/wcrr_fifo3/request_vector
add wave -noupdate -group wcrr_fifo3 /uart_tb/uut/wcrr_fifo3/valid
add wave -noupdate -group wcrr_fifo3 /uart_tb/uut/wcrr_fifo3/grant
add wave -noupdate -group wcrr_fifo4 /uart_tb/uut/wcrr_fifo4/clk
add wave -noupdate -group wcrr_fifo4 /uart_tb/uut/wcrr_fifo4/rst
add wave -noupdate -group wcrr_fifo4 /uart_tb/uut/wcrr_fifo4/ack
add wave -noupdate -group wcrr_fifo4 /uart_tb/uut/wcrr_fifo4/request_vector
add wave -noupdate -group wcrr_fifo4 /uart_tb/uut/wcrr_fifo4/valid
add wave -noupdate -group wcrr_fifo4 /uart_tb/uut/wcrr_fifo4/grant
add wave -noupdate -group wcrr_fifo5 /uart_tb/uut/wcrr_fifo5/clk
add wave -noupdate -group wcrr_fifo5 /uart_tb/uut/wcrr_fifo5/rst
add wave -noupdate -group wcrr_fifo5 /uart_tb/uut/wcrr_fifo5/ack
add wave -noupdate -group wcrr_fifo5 /uart_tb/uut/wcrr_fifo5/request_vector
add wave -noupdate -group wcrr_fifo5 /uart_tb/uut/wcrr_fifo5/valid
add wave -noupdate -group wcrr_fifo5 /uart_tb/uut/wcrr_fifo5/grant
add wave -noupdate -group wcrr_fifo6 /uart_tb/uut/wcrr_fifo6/clk
add wave -noupdate -group wcrr_fifo6 /uart_tb/uut/wcrr_fifo6/rst
add wave -noupdate -group wcrr_fifo6 /uart_tb/uut/wcrr_fifo6/ack
add wave -noupdate -group wcrr_fifo6 /uart_tb/uut/wcrr_fifo6/request_vector
add wave -noupdate -group wcrr_fifo6 /uart_tb/uut/wcrr_fifo6/valid
add wave -noupdate -group wcrr_fifo6 /uart_tb/uut/wcrr_fifo6/grant
add wave -noupdate -group wcrr_fifo7 /uart_tb/uut/wcrr_fifo7/clk
add wave -noupdate -group wcrr_fifo7 /uart_tb/uut/wcrr_fifo7/rst
add wave -noupdate -group wcrr_fifo7 /uart_tb/uut/wcrr_fifo7/ack
add wave -noupdate -group wcrr_fifo7 /uart_tb/uut/wcrr_fifo7/request_vector
add wave -noupdate -group wcrr_fifo7 /uart_tb/uut/wcrr_fifo7/valid
add wave -noupdate -group wcrr_fifo7 /uart_tb/uut/wcrr_fifo7/grant
add wave -noupdate -divider ofifos
add wave -noupdate -radix decimal /uart_tb/uut/iport0/o_cmd
add wave -noupdate -radix decimal /uart_tb/uut/iport1/o_cmd
add wave -noupdate -radix decimal /uart_tb/uut/iport2/o_cmd
add wave -noupdate -radix decimal /uart_tb/uut/iport3/o_cmd
add wave -noupdate -radix decimal /uart_tb/uut/iport4/o_cmd
add wave -noupdate -radix decimal /uart_tb/uut/iport5/o_cmd
add wave -noupdate -radix decimal /uart_tb/uut/iport6/o_cmd
add wave -noupdate -radix decimal /uart_tb/uut/iport7/o_cmd
add wave -noupdate /uart_tb/uut/seq_decode/cmd_port
add wave -noupdate -radix hexadecimal /uart_tb/uut/seq_decode/dst_detector/o_data
add wave -noupdate /uart_tb/uut/wcrr_fifo0/grant
add wave -noupdate /uart_tb/uut/wcrr_fifo1/grant
add wave -noupdate /uart_tb/uut/wcrr_fifo2/grant
add wave -noupdate /uart_tb/uut/wcrr_fifo3/grant
add wave -noupdate /uart_tb/uut/wcrr_fifo4/grant
add wave -noupdate /uart_tb/uut/wcrr_fifo5/grant
add wave -noupdate /uart_tb/uut/wcrr_fifo6/grant
add wave -noupdate /uart_tb/uut/wcrr_fifo7/grant
add wave -noupdate -group ofifo0 -group {convert ofifo0} /uart_tb/uut/oneshot_convert_ofifo0/i_oneshot
add wave -noupdate -group ofifo0 -group {convert ofifo0} /uart_tb/uut/oneshot_convert_ofifo0/o_dec
add wave -noupdate -group ofifo0 -group {mux ofifo0} -radix decimal /uart_tb/uut/mux_dst_ofifo0/i_a
add wave -noupdate -group ofifo0 -group {mux ofifo0} -radix decimal /uart_tb/uut/mux_dst_ofifo0/i_b
add wave -noupdate -group ofifo0 -group {mux ofifo0} -radix decimal /uart_tb/uut/mux_dst_ofifo0/i_c
add wave -noupdate -group ofifo0 -group {mux ofifo0} -radix decimal /uart_tb/uut/mux_dst_ofifo0/i_d
add wave -noupdate -group ofifo0 -group {mux ofifo0} -radix decimal /uart_tb/uut/mux_dst_ofifo0/i_e
add wave -noupdate -group ofifo0 -group {mux ofifo0} -radix decimal /uart_tb/uut/mux_dst_ofifo0/i_f
add wave -noupdate -group ofifo0 -group {mux ofifo0} -radix decimal /uart_tb/uut/mux_dst_ofifo0/i_g
add wave -noupdate -group ofifo0 -group {mux ofifo0} -radix decimal /uart_tb/uut/mux_dst_ofifo0/i_h
add wave -noupdate -group ofifo0 -group {mux ofifo0} /uart_tb/uut/mux_dst_ofifo0/i_sel
add wave -noupdate -group ofifo0 -group {mux ofifo0} -radix decimal /uart_tb/uut/mux_dst_ofifo0/o_sltd
add wave -noupdate -group ofifo0 /uart_tb/uut/oFIFO0/wclk
add wave -noupdate -group ofifo0 /uart_tb/uut/oFIFO0/rclk
add wave -noupdate -group ofifo0 /uart_tb/uut/oFIFO0/rst
add wave -noupdate -group ofifo0 /uart_tb/uut/oFIFO0/push
add wave -noupdate -group ofifo0 /uart_tb/uut/oFIFO0/pop
add wave -noupdate -group ofifo0 -radix decimal /uart_tb/uut/oFIFO0/DataInput
add wave -noupdate -group ofifo0 /uart_tb/uut/oFIFO0/DataOutput
add wave -noupdate -group ofifo0 /uart_tb/uut/oFIFO0/full
add wave -noupdate -group ofifo0 /uart_tb/uut/oFIFO0/empty
add wave -noupdate -group ofifo1 -group {conver ofifo1} /uart_tb/uut/oneshot_convert_ofifo1/i_oneshot
add wave -noupdate -group ofifo1 -group {conver ofifo1} /uart_tb/uut/oneshot_convert_ofifo1/o_dec
add wave -noupdate -group ofifo1 -group {mux ofifo 1} /uart_tb/uut/mux_dst_ofifo1/i_a
add wave -noupdate -group ofifo1 -group {mux ofifo 1} /uart_tb/uut/mux_dst_ofifo1/i_b
add wave -noupdate -group ofifo1 -group {mux ofifo 1} /uart_tb/uut/mux_dst_ofifo1/i_c
add wave -noupdate -group ofifo1 -group {mux ofifo 1} /uart_tb/uut/mux_dst_ofifo1/i_d
add wave -noupdate -group ofifo1 -group {mux ofifo 1} /uart_tb/uut/mux_dst_ofifo1/i_e
add wave -noupdate -group ofifo1 -group {mux ofifo 1} /uart_tb/uut/mux_dst_ofifo1/i_f
add wave -noupdate -group ofifo1 -group {mux ofifo 1} /uart_tb/uut/mux_dst_ofifo1/i_g
add wave -noupdate -group ofifo1 -group {mux ofifo 1} /uart_tb/uut/mux_dst_ofifo1/i_h
add wave -noupdate -group ofifo1 -group {mux ofifo 1} /uart_tb/uut/mux_dst_ofifo1/i_sel
add wave -noupdate -group ofifo1 -group {mux ofifo 1} /uart_tb/uut/mux_dst_ofifo1/o_sltd
add wave -noupdate -group ofifo1 /uart_tb/uut/oFIFO1/wclk
add wave -noupdate -group ofifo1 /uart_tb/uut/oFIFO1/rclk
add wave -noupdate -group ofifo1 /uart_tb/uut/oFIFO1/rst
add wave -noupdate -group ofifo1 /uart_tb/uut/oFIFO1/push
add wave -noupdate -group ofifo1 /uart_tb/uut/oFIFO1/pop
add wave -noupdate -group ofifo1 /uart_tb/uut/oFIFO1/DataInput
add wave -noupdate -group ofifo1 /uart_tb/uut/oFIFO1/DataOutput
add wave -noupdate -group ofifo1 /uart_tb/uut/oFIFO1/full
add wave -noupdate -group ofifo1 /uart_tb/uut/oFIFO1/empty
add wave -noupdate -expand -group ofifo2 -group {convert ofifo2} /uart_tb/uut/oneshot_convert_ofifo2/i_oneshot
add wave -noupdate -expand -group ofifo2 -group {convert ofifo2} -radix decimal /uart_tb/uut/oneshot_convert_ofifo2/o_dec
add wave -noupdate -expand -group ofifo2 -expand -group {mux ofifo2} -radix decimal /uart_tb/uut/mux_dst_ofifo2/i_a
add wave -noupdate -expand -group ofifo2 -expand -group {mux ofifo2} -radix decimal /uart_tb/uut/mux_dst_ofifo2/i_b
add wave -noupdate -expand -group ofifo2 -expand -group {mux ofifo2} -radix decimal /uart_tb/uut/mux_dst_ofifo2/i_c
add wave -noupdate -expand -group ofifo2 -expand -group {mux ofifo2} -radix decimal /uart_tb/uut/mux_dst_ofifo2/i_d
add wave -noupdate -expand -group ofifo2 -expand -group {mux ofifo2} -radix decimal /uart_tb/uut/mux_dst_ofifo2/i_e
add wave -noupdate -expand -group ofifo2 -expand -group {mux ofifo2} -radix decimal /uart_tb/uut/mux_dst_ofifo2/i_f
add wave -noupdate -expand -group ofifo2 -expand -group {mux ofifo2} -radix decimal /uart_tb/uut/mux_dst_ofifo2/i_g
add wave -noupdate -expand -group ofifo2 -expand -group {mux ofifo2} -radix decimal /uart_tb/uut/mux_dst_ofifo2/i_h
add wave -noupdate -expand -group ofifo2 -expand -group {mux ofifo2} /uart_tb/uut/mux_dst_ofifo2/i_sel
add wave -noupdate -expand -group ofifo2 -expand -group {mux ofifo2} -radix decimal /uart_tb/uut/mux_dst_ofifo2/o_sltd
add wave -noupdate -expand -group ofifo2 /uart_tb/uut/oFIFO2/wclk
add wave -noupdate -expand -group ofifo2 /uart_tb/uut/oFIFO2/rclk
add wave -noupdate -expand -group ofifo2 /uart_tb/uut/oFIFO2/rst
add wave -noupdate -expand -group ofifo2 /uart_tb/uut/oFIFO2/push
add wave -noupdate -expand -group ofifo2 /uart_tb/uut/oFIFO2/pop
add wave -noupdate -expand -group ofifo2 -radix decimal /uart_tb/uut/oFIFO2/DataInput
add wave -noupdate -expand -group ofifo2 /uart_tb/uut/oFIFO2/DataOutput
add wave -noupdate -expand -group ofifo2 /uart_tb/uut/oFIFO2/full
add wave -noupdate -expand -group ofifo2 /uart_tb/uut/oFIFO2/empty
add wave -noupdate -group ofifo3 -group {convert ofifo3} /uart_tb/uut/oneshot_convert_ofifo3/i_oneshot
add wave -noupdate -group ofifo3 -group {convert ofifo3} /uart_tb/uut/oneshot_convert_ofifo3/o_dec
add wave -noupdate -group ofifo3 -group {mux ofifo3} /uart_tb/uut/mux_dst_ofifo3/i_a
add wave -noupdate -group ofifo3 -group {mux ofifo3} /uart_tb/uut/mux_dst_ofifo3/i_b
add wave -noupdate -group ofifo3 -group {mux ofifo3} /uart_tb/uut/mux_dst_ofifo3/i_c
add wave -noupdate -group ofifo3 -group {mux ofifo3} /uart_tb/uut/mux_dst_ofifo3/i_d
add wave -noupdate -group ofifo3 -group {mux ofifo3} /uart_tb/uut/mux_dst_ofifo3/i_e
add wave -noupdate -group ofifo3 -group {mux ofifo3} /uart_tb/uut/mux_dst_ofifo3/i_f
add wave -noupdate -group ofifo3 -group {mux ofifo3} /uart_tb/uut/mux_dst_ofifo3/i_g
add wave -noupdate -group ofifo3 -group {mux ofifo3} /uart_tb/uut/mux_dst_ofifo3/i_h
add wave -noupdate -group ofifo3 -group {mux ofifo3} /uart_tb/uut/mux_dst_ofifo3/i_sel
add wave -noupdate -group ofifo3 -group {mux ofifo3} /uart_tb/uut/mux_dst_ofifo3/o_sltd
add wave -noupdate -group ofifo3 /uart_tb/uut/oFIFO3/wclk
add wave -noupdate -group ofifo3 /uart_tb/uut/oFIFO3/rclk
add wave -noupdate -group ofifo3 /uart_tb/uut/oFIFO3/rst
add wave -noupdate -group ofifo3 /uart_tb/uut/oFIFO3/push
add wave -noupdate -group ofifo3 /uart_tb/uut/oFIFO3/pop
add wave -noupdate -group ofifo3 /uart_tb/uut/oFIFO3/DataInput
add wave -noupdate -group ofifo3 /uart_tb/uut/oFIFO3/DataOutput
add wave -noupdate -group ofifo3 /uart_tb/uut/oFIFO3/full
add wave -noupdate -group ofifo3 /uart_tb/uut/oFIFO3/empty
add wave -noupdate -group ofifo4 -group {convert ofifo4} -radix decimal /uart_tb/uut/oneshot_convert_ofifo4/i_oneshot
add wave -noupdate -group ofifo4 -group {convert ofifo4} -radix decimal /uart_tb/uut/oneshot_convert_ofifo4/o_dec
add wave -noupdate -group ofifo4 -expand -group {mux ofifo4} -radix decimal /uart_tb/uut/mux_dst_ofifo4/i_a
add wave -noupdate -group ofifo4 -expand -group {mux ofifo4} -radix decimal /uart_tb/uut/mux_dst_ofifo4/i_b
add wave -noupdate -group ofifo4 -expand -group {mux ofifo4} -radix decimal /uart_tb/uut/mux_dst_ofifo4/i_c
add wave -noupdate -group ofifo4 -expand -group {mux ofifo4} -radix decimal /uart_tb/uut/mux_dst_ofifo4/i_d
add wave -noupdate -group ofifo4 -expand -group {mux ofifo4} -radix decimal /uart_tb/uut/mux_dst_ofifo4/i_e
add wave -noupdate -group ofifo4 -expand -group {mux ofifo4} -radix decimal /uart_tb/uut/mux_dst_ofifo4/i_f
add wave -noupdate -group ofifo4 -expand -group {mux ofifo4} -radix decimal /uart_tb/uut/mux_dst_ofifo4/i_g
add wave -noupdate -group ofifo4 -expand -group {mux ofifo4} -radix decimal /uart_tb/uut/mux_dst_ofifo4/i_h
add wave -noupdate -group ofifo4 -expand -group {mux ofifo4} /uart_tb/uut/mux_dst_ofifo4/i_sel
add wave -noupdate -group ofifo4 -expand -group {mux ofifo4} -radix decimal /uart_tb/uut/mux_dst_ofifo4/o_sltd
add wave -noupdate -group ofifo4 /uart_tb/uut/oFIFO4/wclk
add wave -noupdate -group ofifo4 /uart_tb/uut/oFIFO4/rclk
add wave -noupdate -group ofifo4 /uart_tb/uut/oFIFO4/rst
add wave -noupdate -group ofifo4 /uart_tb/uut/oFIFO4/push
add wave -noupdate -group ofifo4 /uart_tb/uut/oFIFO4/pop
add wave -noupdate -group ofifo4 /uart_tb/uut/oFIFO4/DataInput
add wave -noupdate -group ofifo4 /uart_tb/uut/oFIFO4/DataOutput
add wave -noupdate -group ofifo4 /uart_tb/uut/oFIFO4/full
add wave -noupdate -group ofifo4 /uart_tb/uut/oFIFO4/empty
add wave -noupdate -group ofifo5 -group {convert ofifo5} /uart_tb/uut/oneshot_convert_ofifo5/i_oneshot
add wave -noupdate -group ofifo5 -group {convert ofifo5} /uart_tb/uut/oneshot_convert_ofifo5/o_dec
add wave -noupdate -group ofifo5 -group {mux ofifo5} /uart_tb/uut/mux_dst_ofifo5/i_a
add wave -noupdate -group ofifo5 -group {mux ofifo5} /uart_tb/uut/mux_dst_ofifo5/i_b
add wave -noupdate -group ofifo5 -group {mux ofifo5} /uart_tb/uut/mux_dst_ofifo5/i_c
add wave -noupdate -group ofifo5 -group {mux ofifo5} /uart_tb/uut/mux_dst_ofifo5/i_d
add wave -noupdate -group ofifo5 -group {mux ofifo5} /uart_tb/uut/mux_dst_ofifo5/i_e
add wave -noupdate -group ofifo5 -group {mux ofifo5} /uart_tb/uut/mux_dst_ofifo5/i_f
add wave -noupdate -group ofifo5 -group {mux ofifo5} /uart_tb/uut/mux_dst_ofifo5/i_g
add wave -noupdate -group ofifo5 -group {mux ofifo5} /uart_tb/uut/mux_dst_ofifo5/i_h
add wave -noupdate -group ofifo5 -group {mux ofifo5} /uart_tb/uut/mux_dst_ofifo5/i_sel
add wave -noupdate -group ofifo5 -group {mux ofifo5} /uart_tb/uut/mux_dst_ofifo5/o_sltd
add wave -noupdate -group ofifo5 /uart_tb/uut/oFIFO5/wclk
add wave -noupdate -group ofifo5 /uart_tb/uut/oFIFO5/rclk
add wave -noupdate -group ofifo5 /uart_tb/uut/oFIFO5/rst
add wave -noupdate -group ofifo5 /uart_tb/uut/oFIFO5/push
add wave -noupdate -group ofifo5 /uart_tb/uut/oFIFO5/pop
add wave -noupdate -group ofifo5 /uart_tb/uut/oFIFO5/DataInput
add wave -noupdate -group ofifo5 /uart_tb/uut/oFIFO5/DataOutput
add wave -noupdate -group ofifo5 /uart_tb/uut/oFIFO5/full
add wave -noupdate -group ofifo5 /uart_tb/uut/oFIFO5/empty
add wave -noupdate -group ofifo6 /uart_tb/uut/oFIFO6/wclk
add wave -noupdate -group ofifo6 /uart_tb/uut/oFIFO6/rclk
add wave -noupdate -group ofifo6 /uart_tb/uut/oFIFO6/rst
add wave -noupdate -group ofifo6 /uart_tb/uut/oFIFO6/push
add wave -noupdate -group ofifo6 /uart_tb/uut/oFIFO6/pop
add wave -noupdate -group ofifo6 /uart_tb/uut/oFIFO6/DataInput
add wave -noupdate -group ofifo6 /uart_tb/uut/oFIFO6/DataOutput
add wave -noupdate -group ofifo6 /uart_tb/uut/oFIFO6/full
add wave -noupdate -group ofifo6 /uart_tb/uut/oFIFO6/empty
add wave -noupdate -group ofifo6 -group {convert ofifo6} /uart_tb/uut/oneshot_convert_ofifo6/i_oneshot
add wave -noupdate -group ofifo6 -group {convert ofifo6} /uart_tb/uut/oneshot_convert_ofifo6/o_dec
add wave -noupdate -group ofifo6 -group {mux ofifo6} /uart_tb/uut/mux_dst_ofifo6/i_a
add wave -noupdate -group ofifo6 -group {mux ofifo6} /uart_tb/uut/mux_dst_ofifo6/i_b
add wave -noupdate -group ofifo6 -group {mux ofifo6} /uart_tb/uut/mux_dst_ofifo6/i_c
add wave -noupdate -group ofifo6 -group {mux ofifo6} /uart_tb/uut/mux_dst_ofifo6/i_d
add wave -noupdate -group ofifo6 -group {mux ofifo6} /uart_tb/uut/mux_dst_ofifo6/i_e
add wave -noupdate -group ofifo6 -group {mux ofifo6} /uart_tb/uut/mux_dst_ofifo6/i_f
add wave -noupdate -group ofifo6 -group {mux ofifo6} /uart_tb/uut/mux_dst_ofifo6/i_g
add wave -noupdate -group ofifo6 -group {mux ofifo6} /uart_tb/uut/mux_dst_ofifo6/i_h
add wave -noupdate -group ofifo6 -group {mux ofifo6} /uart_tb/uut/mux_dst_ofifo6/i_sel
add wave -noupdate -group ofifo6 -group {mux ofifo6} /uart_tb/uut/mux_dst_ofifo6/o_sltd
add wave -noupdate -group ofifo7 /uart_tb/uut/oFIFO7/wclk
add wave -noupdate -group ofifo7 /uart_tb/uut/oFIFO7/rclk
add wave -noupdate -group ofifo7 /uart_tb/uut/oFIFO7/rst
add wave -noupdate -group ofifo7 /uart_tb/uut/oFIFO7/push
add wave -noupdate -group ofifo7 /uart_tb/uut/oFIFO7/pop
add wave -noupdate -group ofifo7 /uart_tb/uut/oFIFO7/DataInput
add wave -noupdate -group ofifo7 /uart_tb/uut/oFIFO7/DataOutput
add wave -noupdate -group ofifo7 /uart_tb/uut/oFIFO7/full
add wave -noupdate -group ofifo7 /uart_tb/uut/oFIFO7/empty
add wave -noupdate -group ofifo7 -group {convert ofifo7} /uart_tb/uut/oneshot_convert_ofifo7/i_oneshot
add wave -noupdate -group ofifo7 -group {convert ofifo7} /uart_tb/uut/oneshot_convert_ofifo7/o_dec
add wave -noupdate -group ofifo7 -expand -group {mux ofifo7} /uart_tb/uut/mux_dst_ofifo7/i_a
add wave -noupdate -group ofifo7 -expand -group {mux ofifo7} /uart_tb/uut/mux_dst_ofifo7/i_b
add wave -noupdate -group ofifo7 -expand -group {mux ofifo7} /uart_tb/uut/mux_dst_ofifo7/i_c
add wave -noupdate -group ofifo7 -expand -group {mux ofifo7} /uart_tb/uut/mux_dst_ofifo7/i_d
add wave -noupdate -group ofifo7 -expand -group {mux ofifo7} /uart_tb/uut/mux_dst_ofifo7/i_e
add wave -noupdate -group ofifo7 -expand -group {mux ofifo7} /uart_tb/uut/mux_dst_ofifo7/i_f
add wave -noupdate -group ofifo7 -expand -group {mux ofifo7} /uart_tb/uut/mux_dst_ofifo7/i_g
add wave -noupdate -group ofifo7 -expand -group {mux ofifo7} /uart_tb/uut/mux_dst_ofifo7/i_h
add wave -noupdate -group ofifo7 -expand -group {mux ofifo7} /uart_tb/uut/mux_dst_ofifo7/i_sel
add wave -noupdate -group ofifo7 -expand -group {mux ofifo7} /uart_tb/uut/mux_dst_ofifo7/o_sltd
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {13602565 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 409
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {24456600 ps}
