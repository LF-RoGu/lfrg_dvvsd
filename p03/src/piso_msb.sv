module piso_msb #(
parameter DW = 4
) (
input                  clk,    // Clock
input                  rst,    // asynchronous reset low active 
input  logic           enb,    // Enable
input  logic           l_s,    // load or shift
input  logic [DW-1:0]  inp,    // data input
output logic           out     // Serial output
);

logic [DW-1:0] rgstr_r;
logic [DW-1:0] rgstr_nxt;

// Combinational module
always_comb begin
    if (l_s)
        rgstr_nxt  = inp;
    else 
        rgstr_nxt  = {rgstr_r[DW-2:0], rgstr_r[DW-1]};
end

always_ff@(posedge clk or negedge rst) begin: rgstr_label
    if(!rst)
        rgstr_r     <= '0;
    else if (enb) begin
        rgstr_r     <= rgstr_nxt;
    end
end:rgstr_label

assign out  = rgstr_r[DW-1];    // MSB bit is the first to leave
//TODO: try to design a piso register, where the LSB bit leave the register first and then the LSB bit+1.
endmodule
