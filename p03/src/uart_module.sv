/* 
    Author: César Villarreal @cv4497
			Luis Fernando Rodriguez @LF-RoGu
    Title: adder.sv
    Description: adder module for gray counter
    Last modification: 13/11/2020
*/
import data_pkg::*;
module uart_module
(
	input bit clk,
	input logic rst,
	input logic serial_tx2rx,
	input logic cts,
	output logic rts,
	output data_n_t data,
	output logic data_load,
	output logic dst_register,
	output data_n_t cmd,
	output data_n_t src_portLenght,
	output data_n_t src_port,
	output data_n_t dst_port,
	output data_n_t cmd_port,
	output data_n_t Dn_port,
	output logic dst_fifo,
	output logic Dn_fifo,
	output logic cmd_fifo
);

uart_rx
uart_rx_top
(
	.clk(clk),
	.rst(rst),
	.serial_rx(serial_tx2rx),
	.cts(cts),
	.rts(rts),
	.pairty_error(),
	.data(data)
);

decoder
seq_decode
(
	.clk(clk),
	.rst(rst),
	.rts(rts),
	.uart_data(data),
	.data_load(data_load),
	.dst_register(dst_register),
	.cmd(cmd),
	.src_portLenght(src_portLenght),
	.src_port(src_port),
	.dst_port(dst_port),
	.cmd_port(cmd_port),
	.Dn_port(Dn_port),
	.dst_fifo(dst_fifo),
	.Dn_fifo(Dn_fifo),
	.cmd_fifo(cmd_fifo)
);

endmodule
