/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title:   input port
    Description: input port source file
    Last modification: May 5th, 2021
*/

import port_pkg::*;
import fifo_pkg::*;
import data_pkg::*;

module oport
(
    input        clk,
    input        rst,
    input port_data_t i_data,
    input logic iFIFO_data_push,
    input logic iFIFO_data_pop,
    
    input logic valid_data,
    
    output logic port_full,
    output logic port_empty,
   
    output data_t o_data
);

logic iFIFO_data_full_w;
logic iFIFO_data_empty_w;
	
logic iFIFO_data_push_w;

/* DATA FIFO */
FIFO iFIFO_data
(
    .wclk(clk),
    .rclk(clk),
    .rst(rst),
    .restore_pointers(),
    .push(iFIFO_data_push_w && valid_data),
    .pop(iFIFO_data_pop),
    .DataInput(i_data),  //data_t
    .DataOutput(o_data), //data_t
    .full(iFIFO_data_full_w),
    .empty(iFIFO_data_empty_w)
);

FSM_delay_Control
delay_control_data
(
	.clk(clk),
	.rst(rst),
	.pop_cmd(iFIFO_data_push),
	.enb_reg_pop(iFIFO_data_push_w)
);


assign port_full = iFIFO_data_full_w;
assign port_empty = iFIFO_data_empty_w;

endmodule
