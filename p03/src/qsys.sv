/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title:   QSYS
    Description: QSYS source file
    Last modification: May 9th, 2021
*/

import port_pkg::*;
import fifo_pkg::*;
import data_pkg::*;

module qsys
(
    input clk,
    input rst,
    input  port_data_t i_data,
    input  port_data_t i_cmd,
    input  logic load,
    input  logic iFIFO_data_push,
    input  port_data_t num_iports,
    input  port_data_t src_port,
    input  logic data_processing,
    output port_data_t o_data
);

logic [7:0] port_enable_w;
logic [7:0] execute_req_w;
logic [7:0] load_w;
logic [7:0] enb_count_w;
logic [7:0] iFIFO_data_push_w;
/*outs */
fsm_port_ctrl_st port_state_w [7:0];
logic [7:0] port_full_w;
logic [7:0] port_empty_w;
port_data_t [7:0] o_data_w;
logic [7:0] arb_ack_w;
logic [7:0] cmd_ready_w;
logic [7:0] port_ready_w;
port_data_t port_vector_w [7:0];
port_data_t dest_port_w [7:0];
port_data_t data_length_w [7:0];

port_data_t o_demux_or_req [7:0][7:0];
port_data_t o_or_req_vect [7:0];
//arbeiters
req_vector_t request_vector_w[7:0];
logic valid_w[7:0];
req_vector_t grant_w[7:0];

port_data_t o_mux_data_w [7:0];
port_data_t oFIFO_o_data_w [7:0];
port_data_t o_mux_odata_w;

logic wcrr_execute_data_w[7:0];
logic o_wcrr_execute[7:0];

port_data_t i_cmd_w[7:0];
port_data_t i_data_w[7:0];

port_data_t o_req_vect [7:0];
logic ififo_ack_w [7:0];

logic o_or_arb_ack_w[7:0];

port_data_t o_data_mux_sel_w[7:0];

logic oFIFO_data_pop_w[7:0];
logic oFIFO_data_push_w[7:0];
logic oFIFO_data_full_w[7:0];
logic oFIFO_data_empty_w[7:0];
logic oFIFO_restore_pointers_w[7:0];
logic iFIFO_data_pop_w[7:0];
logic vector_consumed_w[7:0];
req_vector_t one_hot_w[7:0];

demux_1_8 src_port_data
(
    .i_data(i_data),
    .i_sel(selectr_e'(src_port)),
    .o_data_a(i_data_w[0]),
    .o_data_b(i_data_w[1]),
    .o_data_c(i_data_w[2]),
    .o_data_d(i_data_w[3]),
    .o_data_e(i_data_w[4]),
    .o_data_f(i_data_w[5]),
    .o_data_g(i_data_w[6]),
    .o_data_h(i_data_w[7])
);

demux_1_8 src_port_cmd
(
    .i_data(i_cmd),
    .i_sel(selectr_e'(src_port)),
    .o_data_a(i_cmd_w[0]),
    .o_data_b(i_cmd_w[1]),
    .o_data_c(i_cmd_w[2]),
    .o_data_d(i_cmd_w[3]),
    .o_data_e(i_cmd_w[4]),
    .o_data_f(i_cmd_w[5]),
    .o_data_g(i_cmd_w[6]),
    .o_data_h(i_cmd_w[7])
);

demux_1_8 src_port_load
(
    .i_data(load),
    .i_sel(selectr_e'(src_port)),
    .o_data_a(load_w[0]),
    .o_data_b(load_w[1]),
    .o_data_c(load_w[2]),
    .o_data_d(load_w[3]),
    .o_data_e(load_w[4]),
    .o_data_f(load_w[5]),
    .o_data_g(load_w[6]),
    .o_data_h(load_w[7])
);

demux_1_8 src_port_fifo_push
(
    .i_data(iFIFO_data_push),
    .i_sel(selectr_e'(src_port)),
    .o_data_a(iFIFO_data_push_w[0]),
    .o_data_b(iFIFO_data_push_w[1]),
    .o_data_c(iFIFO_data_push_w[2]),
    .o_data_d(iFIFO_data_push_w[3]),
    .o_data_e(iFIFO_data_push_w[4]),
    .o_data_f(iFIFO_data_push_w[5]),
    .o_data_g(iFIFO_data_push_w[6]),
    .o_data_h(iFIFO_data_push_w[7])
);

/* module ports */
genvar i,j;
generate
    for(i=0; i < 8; i++) begin: iports
    iport iport
    (
        /*ins*/
        .clk(clk),
        .rst(rst),
        .i_cmd(i_cmd_w[i]),
        .i_data(i_data_w[i]),
        .port_number(i),
        .data_processing(data_processing),
        .port_enable(port_enable_w[i]),
        .execute_req(execute_req_w[i]),
        .load(load_w[i]),
        .iFIFO_data_push(iFIFO_data_push_w[i]),
        /*outs */
        .port_state(port_state_w[i]),
        .port_full(port_full_w[i]),
        .port_empty(port_empty_w[i]),
        .o_data(o_data_w[i]),
        .arb_ack(ififo_ack_w[i]),
        .cmd_ready(cmd_ready_w[i]),
        .port_ready(port_ready_w[i]),
        .dest_port(dest_port_w[i]),
        .iFIFO_data_pop(iFIFO_data_pop_w[i]),
        .port_vector(port_vector_w[i])
    );

    assign port_enable_w[i] = (num_iports > i)? 1'b1:1'b0;
    end
endgenerate

/* module ports */
generate
    for(i=0; i < 8; i++) begin: demux_req_vector_iport
        demux_1_8 req_vector_iport
        (
            .i_data(port_vector_w[i]),
            .i_sel(selectr_e'(dest_port_w[i])),
            .o_data_a(o_demux_or_req[i][0]),
            .o_data_b(o_demux_or_req[i][1]),
            .o_data_c(o_demux_or_req[i][2]),
            .o_data_d(o_demux_or_req[i][3]),
            .o_data_e(o_demux_or_req[i][4]),
            .o_data_f(o_demux_or_req[i][5]),
            .o_data_g(o_demux_or_req[i][6]),
            .o_data_h(o_demux_or_req[i][7])
        );
    end
endgenerate


/* module ports */
generate
    for(i=0; i < 8; i++) begin: or_request_concat
        or_8_1 or_BW_8_1
        (
            .i_a(o_demux_or_req[0][i]),
            .i_b(o_demux_or_req[1][i]),
            .i_c(o_demux_or_req[2][i]),
            .i_d(o_demux_or_req[3][i]),
            .i_e(o_demux_or_req[4][i]),
            .i_f(o_demux_or_req[5][i]),
            .i_g(o_demux_or_req[6][i]),
            .i_h(o_demux_or_req[7][i]),
            .o_sltd(o_or_req_vect[i])
        );

        assign o_req_vect[i] = (data_processing == 1'b1) ? o_or_req_vect[i] : 8'd0;
    end
endgenerate

generate
    for(i=0; i < 8; i++) begin: arbeiters
    wcrr arbeiter
    (
        .clk(clk),
        .rst(rst),
        .ack(o_or_arb_ack_w[i]),
        .request_vector(o_req_vect[i]),
        .valid(valid_w[i]),
        .grant(grant_w[i])
    );
    end
endgenerate

/* module ports */
generate
    for(i=0; i < 8; i++) begin: arb_ack_mux
        mux_8_1
        #(
            .DW(1)
        )   arb_ack_mux
        (
            .i_a(ififo_ack_w[0]),
            .i_b(ififo_ack_w[1]),
            .i_c(ififo_ack_w[2]),
            .i_d(ififo_ack_w[3]),
            .i_e(ififo_ack_w[4]),
            .i_f(ififo_ack_w[5]),
            .i_g(ififo_ack_w[6]),
            .i_h(ififo_ack_w[7]),
            .i_sel(selectr_e'(o_data_mux_sel_w[i])),
            .o_sltd(o_or_arb_ack_w[i])
        );
    end
endgenerate

/* module ports */
generate
    for(i=0; i < 8; i++) begin: WCRR_EXECUTE_REQ
        or_8_1 or_BW_8_1
        (
            .i_a(grant_w[0][i]),
            .i_b(grant_w[1][i]),
            .i_c(grant_w[2][i]),
            .i_d(grant_w[3][i]),
            .i_e(grant_w[4][i]),
            .i_f(grant_w[5][i]),
            .i_g(grant_w[6][i]),
            .i_h(grant_w[7][i]),
            .o_sltd(o_wcrr_execute[i])
        );
    end
endgenerate

generate
    for(i=0; i < 8; i++) begin: one_hots
        oneshot2dec one_hot
        (
            .i_oneshot(grant_w[i]),
            .o_dec(one_hot_w[i])
        );

        assign o_data_mux_sel_w[i] = (grant_w[i] != 8'd0) ? one_hot_w[i] : 8'hFF;
        assign execute_req_w[i] = o_wcrr_execute[i]; 
    end
endgenerate

/* module ports TO CHECK*/ 
generate
    for(i=0; i < 8; i++) begin: mux_wcrr_8_1
        mux_8_1 mux_wcrr_8_1
        (
            .i_a(o_data_w[0]),
            .i_b(o_data_w[1]),
            .i_c(o_data_w[2]),
            .i_d(o_data_w[3]),
            .i_e(o_data_w[4]),
            .i_f(o_data_w[5]),
            .i_g(o_data_w[6]),
            .i_h(o_data_w[7]),
            .i_sel(selectr_e'(o_data_mux_sel_w[i])),
            .o_sltd(o_mux_data_w[i])
        );
    end
endgenerate

/* module ports */
generate
    for(i=0; i < 8; i++) begin: oFIFO_pops
        mux_8_1 oFIFO_pop_or
        (
            .i_a(iFIFO_data_pop_w[0]),
            .i_b(iFIFO_data_pop_w[1]),
            .i_c(iFIFO_data_pop_w[2]),
            .i_d(iFIFO_data_pop_w[3]),
            .i_e(iFIFO_data_pop_w[4]),
            .i_f(iFIFO_data_pop_w[5]),
            .i_g(iFIFO_data_pop_w[6]),
            .i_h(iFIFO_data_pop_w[7]),
            .i_sel(selectr_e'(o_data_mux_sel_w[i])),
            .o_sltd(oFIFO_data_push_w[i])
        );
    end
endgenerate


generate
    for(i=0; i < 8; i++) begin: oFIFOS
    FIFO oFIFO
    (
        .wclk(clk),
        .rclk(clk),
        .rst(rst),
        .restore_pointers(oFIFO_restore_pointers_w[i]),
        .push(oFIFO_data_push_w[i]),
        .pop(oFIFO_data_pop_w[i]),
        .DataInput(o_mux_data_w[i]),  //data_t
        .DataOutput(oFIFO_o_data_w[i]), //data_t
        .full(oFIFO_data_full_w[i]),
        .empty(oFIFO_data_empty_w[i])
    );
    end
endgenerate

mux_8_1 o_mux_fifo
(
    .i_a(oFIFO_o_data_w[0]),
    .i_b(oFIFO_o_data_w[1]),
    .i_c(oFIFO_o_data_w[2]),
    .i_d(oFIFO_o_data_w[3]),
    .i_e(oFIFO_o_data_w[4]),
    .i_f(oFIFO_o_data_w[5]),
    .i_g(oFIFO_o_data_w[6]),
    .i_h(oFIFO_o_data_w[7]),
    .i_sel(), //selectr_e
    .o_sltd(o_mux_odata_w)
);

assign o_data = o_mux_odata_w;

endmodule