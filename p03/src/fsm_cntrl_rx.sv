module fsm_cntrl_rx
import data_pkg::*;
(
	input bit clk,
	input logic rst,
	input logic ovf,
	input logic data_in,
	
	input logic start_cntr_ovf,
	input logic data_cntr_ovf,
	
	output logic rts,
	
	output logic enable,
	output logic bin_cntr_enb,
	output logic start_cntr_enable,
	output logic data_cntr_enable
);

uart_st current_st, nxt_state;

// Circuito combinacional entrada, define siguiente estado
always_comb begin
   case(current_st)
		IDLE_BIT: begin
			if(!data_in) begin
				nxt_state <= DATA_BIT;
			end
			else begin
				nxt_state <= IDLE_BIT;
			end
		end
		DATA_BIT: begin
			if(ovf) begin
				nxt_state <= PARITY_BIT;
			end
			else begin
				nxt_state <= DATA_BIT;
			end
		end
		PARITY_BIT: begin
			nxt_state <= STOP_BIT;
		end
		STOP_BIT: begin
			nxt_state <= IDLE_BIT;
		end
	endcase
end

//asignaciones por default y siguiente
always_ff@(posedge clk or negedge rst) begin // Circuito Secuenicial en un proceso always.
   if (!rst) 
      current_st  <= IDLE_BIT;
   else 
      current_st  <= nxt_state;
end

always_comb begin
   case(current_st)
		IDLE_BIT: begin
			enable 			= 1'b0;
			bin_cntr_enb 	= (!data_in)?(1'b1):(1'b0);
			rts				= (!data_in)?(1'b1):(1'b0);
			start_cntr_enable = (!data_in)?(1'b1):(1'b0);
			data_cntr_enable = 1'b0;
		end
		DATA_BIT: begin
			enable 			= (data_cntr_ovf)?(1'b1):(1'b0);
			bin_cntr_enb 	= 1'b1;
			rts				= 1'b1;
			start_cntr_enable = 1'b0;
			data_cntr_enable = 1'b1;
		end
		PARITY_BIT: begin
			enable 			= 1'b0;
			bin_cntr_enb 	= 1'b0;
			rts				= 1'b1;
			start_cntr_enable = 1'b0;
			data_cntr_enable = 1'b0;
		end
		STOP_BIT: begin
			enable 			= 1'b0;
			bin_cntr_enb 	= 1'b0;
			rts				= 1'b1;
			start_cntr_enable = 1'b0;
			data_cntr_enable = 1'b0;
		end
	endcase
end

endmodule 