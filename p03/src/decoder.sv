/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: decoder
    Description: decoder source file
    Last modification: 21/04/2021
*/

import data_pkg::*;

module decoder
(
	input logic clk,
	input logic rst,
	
	input logic rts,
	input data_n_t uart_data,

	output logic data_load,

	output logic dst_register,

	output data_n_t cmd,
	output data_n_t src_portLenght,
	output data_n_t src_port,
	output data_n_t dst_port,
	output data_n_t cmd_port,
	output data_n_t Dn_port,
	
	output logic dst_fifo,
	output logic Dn_fifo,
	
	output logic cmd_fifo
);

logic cmd_register;
data_n_t icmd, ocmd;
	
logic lenght_register;
data_n_t ilenght, olenght;
	
logic src_portLenght_register;
data_n_t isrc_portLenght, osrc_portLenght;
	
logic valid_data_w;
logic uart_0xfe_w;
logic uart_l_w;
logic uart_cmd_01_w, uart_cmd_02_w, uart_cmd_03_w, uart_cmd_04_w, uart_cmd_05_w, uart_cmd_06_w;
logic uart_m_w;
logic uart_n_w;
logic uart_src_w, uart_dst_w;
logic uart_oxef_w;

logic scr_register;
data_n_t isrc, osrc;
data_n_t idst, odst;

logic Dn_register;
data_n_t iDn, oDn;

data_n_t idata_rx, odata_rx;
data_n_t irts_rx, orts_rx;
	
logic rts_counter_enb, rts_counter_ovf;
	
data_n_t dst_vector, length_vector;

assign irts_rx = rts;
pipo_register rts_detector
(
	.clk(clk),
	.rst(rst),
	.enb(1'b1),
	.i_data(irts_rx),
	.o_data(orts_rx)
);

assign idata_rx = uart_data;
pipo_register sequence_detector
(
	.clk(clk),
	.rst(rst),
	.enb(rts_counter_enb),
	.i_data(idata_rx),
	.o_data(odata_rx)
);

assign rts_counter_enb = (orts_rx == 'd0) && (irts_rx == 'd1);
assign valid_data_w = rts_counter_enb;
assign uart_0xfe_w = (uart_data == data_n_t'('hFE)) ? (1'b1):(1'b0);
assign uart_l_w = (uart_data > data_n_t'('d0)) && (uart_data < data_n_t'('d20));

assign uart_cmd_01_w = uart_data == data_n_t'('d1);
assign uart_cmd_02_w = uart_data == data_n_t'('d2);
assign uart_cmd_03_w = uart_data == data_n_t'('d3);
assign uart_cmd_04_w = uart_data == data_n_t'('d4);
assign uart_cmd_05_w = uart_data == data_n_t'('d5);
assign uart_cmd_06_w = uart_data == data_n_t'('d6);
	
assign uart_m_w = (uart_data > data_n_t'('d0)) && (uart_data < data_n_t'('d9));
assign uart_n_w = (uart_data > data_n_t'('d0)) && (uart_data < data_n_t'('d9));

assign uart_src_w = (uart_data > data_n_t'('d0)) && (uart_data < data_n_t'('d9));
assign uart_dst_w = (uart_data > data_n_t'('d0)) && (uart_data < data_n_t'('d9));

assign uart_oxef_w = uart_data == data_n_t'('hEF);

logic decoder_counter_enb, decoder_counter_ovf;
data_n_t decoder_counter_data;

parametric_counter
decoder_data_counter
(
	.clk(rts),
	.rst(rst),
	.enb((decoder_counter_enb)||( decoder_counter_data == (olenght - 'd4) )),
	.max_count(olenght - 'd3),
	.count(decoder_counter_data),
	.ovf(decoder_counter_ovf)
);

fsm_decoder
decoder
(
	.clk(clk),
	.rst(rst),
	
	.decoder_data_ovf(decoder_counter_ovf),
	
	.valid_data(valid_data_w),
	.uart_0xfe(uart_0xfe_w),
	.uart_l(uart_l_w),
	
	.uart_cmd_01(uart_cmd_01_w),
	.uart_cmd_02(uart_cmd_02_w),
	.uart_cmd_03(uart_cmd_03_w),
	.uart_cmd_04(uart_cmd_04_w),
	.uart_cmd_05(uart_cmd_05_w),
	.uart_cmd_06(uart_cmd_06_w),
	
	.uart_m(uart_m_w),
	.uart_n(uart_n_w),
	
	.uart_src(uart_src_w),
	.uart_dst(uart_dst_w),
	
	.uart_oxef(uart_oxef_w),
	/** */
	.data_load(data_load),
	
	.cmd_register(cmd_register),
	
	.lenght_register(lenght_register),
	
	.src_portLenght_register(src_portLenght_register),
	
	.src_register(scr_register),
	.dst_register(dst_register),
	
	.data_counter_enb(decoder_counter_enb),
	
	.dst_fifo(dst_fifo),
	.Dn_fifo(Dn_register)
);

assign icmd = uart_data;
pipo_register cmd_detector
(
	.clk(clk),
	.rst(rst),
	.enb(cmd_register),
	.i_data(icmd),
	.o_data(ocmd)
);

assign ilenght = uart_data;
pipo_register lenght_detector
(
	.clk(clk),
	.rst(rst),
	.enb(lenght_register),
	.i_data(ilenght),
	.o_data(olenght)
);

assign isrc_portLenght = uart_data;
pipo_register portLenght_detector
(
	.clk(clk),
	.rst(rst),
	.enb(src_portLenght_register),
	.i_data(isrc_portLenght),
	.o_data(osrc_portLenght)
);
	
assign isrc = uart_data;
pipo_register src_detector
(
	.clk(clk),
	.rst(rst),
	.enb(scr_register),
	.i_data(isrc),
	.o_data(osrc)
);

assign idst = uart_data;
pipo_register dst_detector
(
	.clk(clk),
	.rst(rst),
	.enb(dst_register),
	.i_data(idst),
	.o_data(odst)
);

assign cmd = ocmd;
assign src_portLenght = osrc_portLenght;
assign src_port = osrc;
assign dst_port = odst;
	
pipo_register cmd_vector
(
	.clk(clk),
	.rst(rst),
	.enb(dst_register),
	.i_data({idst[3:0],(olenght[3:0] - 4'd4)}),
	.o_data(cmd_port)
);

assign iDn = uart_data;
pipo_register Dn_vector
(
	.clk(clk),
	.rst(rst),
	.enb(Dn_register),
	.i_data(iDn),
	.o_data(Dn_port)
);

parametric_counter
cmd_vector_counter
(
	.clk(clk),
	.rst(rst),
	.enb(dst_register || (cmd_fifo)),
	.max_count(2),
	.count(),
	.ovf(cmd_fifo)
);
	
assign Dn_fifo = Dn_register;	
endmodule 