/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: mux_8_1
    Description: mux_8_1 source file
    Last modification: 14/02/2021
*/

import data_pkg::*;
module or_8_1
#(
	parameter DW = data_pkg::N
)
(	
	input   logic [DW-1:0]   i_a,
	input   logic [DW-1:0]   i_b,
	input	logic [DW-1:0]	 i_c,
	input	logic [DW-1:0]	 i_d,
	input	logic [DW-1:0]	 i_e,
	input	logic [DW-1:0]	 i_f,
	input	logic [DW-1:0]	 i_g,
	input	logic [DW-1:0]	 i_h,
	output  logic [DW-1:0]   o_sltd
);

assign o_sltd = i_a | i_b | i_c | i_d | i_e | i_f | i_g | i_h;

endmodule
