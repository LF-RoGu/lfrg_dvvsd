/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: uart_tx
    Description: uart_tx package file
    Last modification: 07/03/2021
*/

import data_pkg::*;

module uart_tx
(
	input  clk,
	input  rst,
	input logic [7:0] data,
	input logic transmit,
	output logic cts,
	output logic serial_tx
);	
	
logic reg_enb_w, cntr_enb_w, ovf_w,tx_active_w,ls_w;
logic [1:0] mux_sel_w;
logic q_piso_register_w;
logic parity_w;
logic tx_mux_w;
	
logic o_clk_tx;
clk_divider
#(
	.FREQUENCY(uartTxBR),
	.REFERENCE_CLOCK(uartRefClk)
)
uartTx_clk_divider
(
	.clk_FPGA(clk),
	.rst(rst),
	.clk(o_clk_tx)
);

uart_parity parity_module
(
	.i_data(data),
	.parity(parity_w)
);

fsm_uart_tx uart_tx_fsm
(
	.clk(o_clk_tx),
	.rst(rst),
	.transmit(transmit),
	.ovf(ovf_w),
	.cntr_enb(cntr_enb_w),
	.reg_enb(reg_enb_w),
	.l_s(ls_w),
	.mux_sel(mux_sel_w),
	.cts(cts),
	.tx_active(tx_active_w)
);

piso_register_lsb	
piso_register_lsb_uartTX
(
	.clk(o_clk_tx),
	.rst(rst),
	.enb(reg_enb_w),
	.l_s(ls_w), //load or shift
	.i_data(data),
	.o_data(q_piso_register_w)
);

mux_4_1_case 
mux_4_1_case_uartTX
(
	.i_a(1'b0),			 //START
	.i_b(q_piso_register_w), //DATA
	.i_c(parity_w),      //PARITY
	.i_d(1'b1),		     //STOP
	.i_sel(mux_sel_w),	
	.o_sltd(tx_mux_w)
);

counter_ovf	
bin_counter_uartTX
(
	.clk(o_clk_tx),
	.rst(rst),
	.enb(cntr_enb_w),
	.rst_counter(),
	.count(),
	.ovf(ovf_w)
);

assign serial_tx = (tx_active_w == 1'b1) ? tx_mux_w : 1'b1;

endmodule