/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: fsm_decoder
    Description: fsm_decoder source file
    Last modification: 07/03/2021
*/
import data_pkg::*;

module fsm_decoder
(
	input bit clk,
	input logic rst,
	
	input logic decoder_data_ovf,
	
	input logic valid_data,
	input logic uart_0xfe,
	input logic uart_l,
	
	input logic uart_cmd_01,
	input logic uart_cmd_02,
	input logic uart_cmd_03,
	input logic uart_cmd_04,
	input logic uart_cmd_05,
	input logic uart_cmd_06,
	
	input logic uart_m,
	input logic uart_n,
	
	input logic uart_src,
	input logic uart_dst,
	
	input logic uart_oxef,
	
	output logic data_load,
	
	output logic cmd_register, //Habilita registro para guardar puerto
	
	output logic lenght_register,
	
	output logic src_portLenght_register, //Habilita registro para guardar puerto
	
	output logic src_register, //Habilita registro para guardar puerto
	output logic dst_register, //Habilita registro para guardar puerto
	
	output logic data_counter_enb,
	
	output logic dst_fifo,
	output logic Dn_fifo //Habilita fifo para guardar datos
);

decoder_st current_st, nxt_state;
data_n_t src_port_t;
logic Dn_ovf;

data_n_t idata_rx, odata_rx;
data_n_t iDn_rx, oDn_rx;

always_comb begin
   case(current_st)
	   DATA_0XFE: begin
		   if(valid_data) begin
			   if(uart_0xfe)
				   nxt_state = DATA_L;
		   end 
		   
		   else begin
			   nxt_state = DATA_0XFE;
		   end 
	   end 
	   DATA_L: begin
		   if(valid_data) begin
			   if(uart_l)
				  nxt_state = DATA_CMD; 
		   end 
		   else begin
			   nxt_state = DATA_L;
		   end 
	   end 
	   DATA_CMD: begin
		   if(valid_data) begin
			   if(uart_cmd_01)
				   nxt_state = DATA_M;
			   else if(uart_cmd_02)
				   nxt_state = DATA_0XEF;
			   else if(uart_cmd_03)
				   nxt_state = DATA_N;
			   else if(uart_cmd_04)
				   nxt_state = DATA_SRC;
			   else if(uart_cmd_05)
				   nxt_state = DATA_0XEF;
			   else if(uart_cmd_06)
				   nxt_state = DATA_0XEF;
		   end 
		   else begin
			   nxt_state = DATA_CMD;
		   end 
	   end 
	   DATA_M: begin
		   if(valid_data) begin
			   if(uart_m)
				   nxt_state = DATA_0XEF;
		   end 
		   else begin
			   nxt_state = DATA_M;
		   end 
	   end 
	   DATA_N: begin
		   if(valid_data) begin
			   if(uart_n)
				   nxt_state = DATA_0XEF;
		   end 
		   else begin
			   nxt_state = DATA_M;
		   end 
	   end 
	   DATA_SRC: begin
		   if(valid_data) begin
			   if(uart_src)
				   nxt_state = DATA_DST;
		   end 
		   else begin
			   nxt_state = DATA_SRC;
		   end 
	   end
	   DATA_DST: begin
		   if(valid_data) begin
			   if(uart_dst)
				   nxt_state = DATA_ST;
		   end 
		   else begin
			   nxt_state = DATA_DST;
		   end 
	   end
	   DATA_ST: begin
		   if(valid_data) begin
			   if(decoder_data_ovf)
				   nxt_state = DATA_0XEF;
		   end 
		   else begin
			   nxt_state = DATA_ST;
		   end 
	   end
	   DATA_0XEF: begin
		   nxt_state = DATA_0XFE;
	   end 
	endcase
end

always_ff@(posedge clk or negedge rst) begin // Circuito Secuenicial en un proceso always.
   if (!rst) 
      current_st  <= DATA_0XFE;
   else
      current_st  <= nxt_state;
end

always_comb begin
   case(current_st)
	   DATA_0XFE: begin
		   data_load = 1'b0;
		   
		   cmd_register = 1'b0;
		   
		   lenght_register = 1'b0;
		   
		   src_portLenght_register = 1'b0;
		   
		   src_register = 1'b0;
		   dst_register = 'b0;
		   
		   data_counter_enb = 1'b0;
		   
		   dst_fifo = 1'b0;

		   Dn_fifo = 1'b0;
	   end 
	   DATA_L: begin
		   data_load = 1'b0;
		   
		   cmd_register = 1'b0;
		   
		   lenght_register = (valid_data) ? (1'b1):(1'b0);
		   
		   src_portLenght_register = 1'b0;
		   
		   src_register = 1'b0;
		   dst_register = 'b0;
		   
		   data_counter_enb = 1'b0;
		   
		   dst_fifo = 1'b0;

		   Dn_fifo = 1'b0;
	   end
	   DATA_CMD: begin
		   data_load = 1'b0;
		   
		   cmd_register = (valid_data) ? (1'b1):(1'b0);
		   
		   lenght_register = 1'b0;
		   
		   src_portLenght_register = 1'b0;
		   
		   src_register = 1'b0;
		   dst_register = 'b0;
		   
		   data_counter_enb = 1'b0;
		   
		   dst_fifo = 1'b0;

		   Dn_fifo = 1'b0;
	   end  
	   DATA_M: begin
		   data_load = 1'b0;
		   
		   cmd_register = 1'b0;
		   
		   lenght_register = 1'b0;

		   src_portLenght_register = (valid_data) ? (1'b1):(1'b0);
		   
		   src_register = 1'b0;
		   dst_register = 'b0;
		   
		   data_counter_enb = 1'b0;
		   
		   dst_fifo = 1'b0;

		   Dn_fifo = 1'b0;
	   end 
	   DATA_N: begin
		   data_load = 1'b0;
		   
		   cmd_register = 1'b0;
		   
		   lenght_register = 1'b0;
		   
		   src_portLenght_register = 1'b0;
		   
		   src_register = 1'b0;
		   dst_register = 'b0;
		   
		   data_counter_enb = 1'b0;
		   
		   dst_fifo = 1'b0;

		   Dn_fifo = 1'b0;
	   end 
	   DATA_SRC: begin
		   data_load = 1'b0;
		   
		   cmd_register = 1'b0;
		   
		   lenght_register = 1'b0;
		   
		   src_portLenght_register = 1'b0;
		   
		   src_register = (valid_data) ? (1'b1):(1'b0);
		   dst_register = 'b0;
		   
		   data_counter_enb = 1'b0;
		   
		   dst_fifo = 1'b0;

		   Dn_fifo = 1'b0;
	   end 
	   DATA_DST: begin
		   data_load = 1'b1;
		   
		   cmd_register = 1'b0;
		   
		   lenght_register = 1'b0;
		   
		   src_portLenght_register = 1'b0;
		   
		   src_register = 1'b0;
		   dst_register = (valid_data) ? (1'b1):(1'b0);
		   
		   data_counter_enb = 1'b0;
		   
		   dst_fifo = 1'b0;

		   Dn_fifo = 1'b0;
	   end 
	   DATA_ST: begin
		   data_load = 1'b0;
		   
		   cmd_register = 1'b0;
		   
		   lenght_register = 1'b0;
		   
		   src_portLenght_register = 1'b0;
		   
		   src_register = 1'b0;
		   dst_register = 'b0;
		   
		   data_counter_enb = 1'b1;
		   
		   dst_fifo = (valid_data) ? (1'b1):(1'b0);

		   Dn_fifo = (valid_data) ? (1'b1):(1'b0);
	   end 
	   DATA_0XEF: begin
		   data_load = 1'b0;
		   
		   cmd_register = 1'b0;
		   
		   lenght_register = 1'b0;
		   
		   src_portLenght_register = 1'b0;
		   
		   src_register = 1'b0;
		   dst_register = 'b0;
		   
		   data_counter_enb = 1'b0;
		   
		   dst_fifo = 1'b0;

		   Dn_fifo = 1'b0;
	   end 
	endcase
end

endmodule 