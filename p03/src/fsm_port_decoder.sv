/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title:   fsm port control
    Description: fsm port control source file
    Last modification: May 5th, 2021
*/

import port_pkg::*;
module FSM_Port_Decoder
(
    input        clk,
    input        rst,
    input  logic start,
    output logic load_dest,
    output logic load_count,
    output logic next_data_sig,
    output logic ready
);

fsm_decoder_st current_st, nxt_state;

always_comb begin
   case(current_st)
        IDLE1_PORT_DEC_ST: begin
            if(start)
                nxt_state = DEST_PORT_DEC_ST;
            else 
                nxt_state = IDLE1_PORT_DEC_ST;
        end
        DEST_PORT_DEC_ST: begin
            nxt_state = IDLE2_PORT_DEC_ST;
        end
        IDLE2_PORT_DEC_ST: begin
            nxt_state = DLEN_PORT_DEC_ST;
        end
        DLEN_PORT_DEC_ST: begin
            nxt_state = IDLE1_PORT_DEC_ST;
        end
   endcase
end

always_ff@(posedge clk or negedge rst) begin // Circuito Secuenicial en un proceso always.
   if (!rst) 
      current_st  <= IDLE1_PORT_DEC_ST;
   else 
      current_st  <= nxt_state;
end

always_comb begin
   case(current_st)
        IDLE1_PORT_DEC_ST: begin
            load_dest = 1'b0;
            load_count = 1'b0;
            next_data_sig = 1'b0;
            ready = 1'b0;
        end
        DEST_PORT_DEC_ST: begin
            load_dest = 1'b1;
            load_count = 1'b0;
            next_data_sig = 1'b1;
            ready = 1'b0;
        end
        IDLE2_PORT_DEC_ST: begin
            load_dest = 1'b0;
            load_count = 1'b0;
            next_data_sig = 1'b0;
            ready = 1'b0;
        end
        DLEN_PORT_DEC_ST: begin
            load_dest = 1'b0;
            load_count = 1'b1;
            next_data_sig = 1'b1;
            ready = 1'b1;
        end
   endcase
end

endmodule