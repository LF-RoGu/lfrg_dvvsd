import data_pkg::*, port_pkg::*;

module uart
(
	input bit clk,
	input logic rst,
	input logic transmit,			//button start tx
	
	input  data_n_t data_tx //parallel data from uart2uart
	
);

logic serial_tx2rx;
	
logic cts;
logic rts;
logic data_load;
logic dst_load;
data_n_t data_rx;
	
data_n_t cmd, src_portLenght, src_port;

logic dst_fifo;
data_n_t dst_port;
data_n_t cmd_port;
data_n_t Dn_port;
	
data_n_t data_selector_port;

logic Dn_fifo, cmd_fifo;
	
logic sequence_ovf_fifo0, sequence_ovf_fifo1, sequence_ovf_fifo2,sequence_ovf_fifo3,sequence_ovf_fifo4,sequence_ovf_fifo5,sequence_ovf_fifo6,sequence_ovf_fifo7;
logic push_fifo0,push_fifo1,push_fifo2,push_fifo3,push_fifo4,push_fifo5,push_fifo6,push_fifo7;
logic cmd_fifo0,cmd_fifo1,cmd_fifo2,cmd_fifo3,cmd_fifo4,cmd_fifo5,cmd_fifo6,cmd_fifo7;

data_n_t dst_port_fifo0,dst_port_fifo1,dst_port_fifo2,dst_port_fifo3,dst_port_fifo4,dst_port_fifo5,dst_port_fifo6,dst_port_fifo7;

data_n_t rvg0,rvg1,rvg2,rvg3,rvg4,rvg5,rvg6,rvg7;
req_vector_t orvg0,orvg1,orvg2,orvg3,orvg4,orvg5,orvg6,orvg7;
	
req_vector_t port0_wcrr0, port0_wcrr1, port0_wcrr2, port0_wcrr3, port0_wcrr4, port0_wcrr5, port0_wcrr6, port0_wcrr7;
req_vector_t port1_wcrr0, port1_wcrr1, port1_wcrr2, port1_wcrr3, port1_wcrr4, port1_wcrr5, port1_wcrr6, port1_wcrr7;
req_vector_t port2_wcrr0, port2_wcrr1, port2_wcrr2, port2_wcrr3, port2_wcrr4, port2_wcrr5, port2_wcrr6, port2_wcrr7;
req_vector_t port3_wcrr0, port3_wcrr1, port3_wcrr2, port3_wcrr3, port3_wcrr4, port3_wcrr5, port3_wcrr6, port3_wcrr7;
req_vector_t port4_wcrr0, port4_wcrr1, port4_wcrr2, port4_wcrr3, port4_wcrr4, port4_wcrr5, port4_wcrr6, port4_wcrr7;
req_vector_t port5_wcrr0, port5_wcrr1, port5_wcrr2, port5_wcrr3, port5_wcrr4, port5_wcrr5, port5_wcrr6, port5_wcrr7;
req_vector_t port6_wcrr0, port6_wcrr1, port6_wcrr2, port6_wcrr3, port6_wcrr4, port6_wcrr5, port6_wcrr6, port6_wcrr7;
req_vector_t port7_wcrr0, port7_wcrr1, port7_wcrr2, port7_wcrr3, port7_wcrr4, port7_wcrr5, port7_wcrr6, port7_wcrr7;	

req_vector_t oport_wcrr0, oport_wcrr1, oport_wcrr2, oport_wcrr3, oport_wcrr4, oport_wcrr5, oport_wcrr6, oport_wcrr7;

data_n_t ofifo0,ofifo1,ofifo2,ofifo3,ofifo4,ofifo5,ofifo6,ofifo7;
data_n_t i_ofifo0,i_ofifo1,i_ofifo2,i_ofifo3,i_ofifo4,i_ofifo5,i_ofifo6,i_ofifo7;
data_n_t o_ofifo0,o_ofifo1,o_ofifo2,o_ofifo3,o_ofifo4,o_ofifo5,o_ofifo6,o_ofifo7;

data_n_t grant_fifo0, grant_fifo1, grant_fifo2, grant_fifo3, grant_fifo4, grant_fifo5, grant_fifo6, grant_fifo7;
data_n_t conv_fifo0, conv_fifo1, conv_fifo2, conv_fifo3, conv_fifo4, conv_fifo5, conv_fifo6, conv_fifo7;

uart_tx
uart_tx_top
(
	.clk(clk),
	.rst(rst),
	.data(data_tx),
	
	.transmit(transmit),
	.cts(cts),
	
	.serial_tx(serial_tx2rx)
);

uart_rx
uart_rx_top
(
	.clk(clk),
	.rst(rst),
	
	.serial_rx(serial_tx2rx),

	.cts(cts),
	
	.rts(rts),
	
	.pairty_error(),
	.data(data_rx)
);

decoder
seq_decode
(
	.clk(clk),
	.rst(rst),
	
	.rts(rts),
	.uart_data(data_rx),
	
	.data_load(data_load),
	
	.dst_register(dst_load),
	
	.cmd(cmd),
	.src_portLenght(src_portLenght),
	
	.src_port(src_port),
	.dst_port(dst_port),
	.cmd_port(cmd_port),
	.Dn_port(Dn_port),
	
	.dst_fifo(dst_fifo),
	.Dn_fifo(Dn_fifo),
	.cmd_fifo(cmd_fifo)
);

demux_1_8
#(
	.DW(1)
)
demux_data_push
(
	.i_data(dst_fifo && Dn_fifo),
	.i_sel( selectr_e'(src_port - 'd1) ),
	.o_data_a(push_fifo0),
	.o_data_b(push_fifo1),
	.o_data_c(push_fifo2),
	.o_data_d(push_fifo3),
	.o_data_e(push_fifo4),
	.o_data_f(push_fifo5),
	.o_data_g(push_fifo6),
	.o_data_h(push_fifo7)
);	
	
demux_1_8
#(
	.DW(1)
)
demux_cmd_push
(
	.i_data(cmd_fifo),
	.i_sel( selectr_e'(src_port - 'd1) ),
	.o_data_a(cmd_fifo0),
	.o_data_b(cmd_fifo1),
	.o_data_c(cmd_fifo2),
	.o_data_d(cmd_fifo3),
	.o_data_e(cmd_fifo4),
	.o_data_f(cmd_fifo5),
	.o_data_g(cmd_fifo6),
	.o_data_h(cmd_fifo7)
);

data_transfer
data_selector
(
	.clk(rts),
	.rst(rst),
	.fifo_load(data_load && cmd_fifo0),
	.iuart_data(Dn_port),
	.iuart_cmd(cmd_port),
	.ouart_data(data_selector_port)
);

/*------------------------------------*/
iport
iport0
(
	.clk(clk),
	.rst(rst),
	.i_data(data_rx),
	.i_cmd(cmd_port),
	.port_number('d0),
	.port_enable( (src_portLenght > 'd0) ? (1'b1):(1'b0) ),
	.execute_req(),
	.load( (src_portLenght > 'd0) ? (cmd_fifo0):(1'b0) ),
	.enb_count(),
	.iFIFO_data_push( (src_portLenght > 'd0) ? (push_fifo0):(1'b0) ),
	.port_state(),
	.port_full(),
	.port_empty(),
	.o_data(ofifo0),
	.o_cmd(dst_port_fifo0),
	.arb_ack(),
	.cmd_ready(),
	.port_ready(),
	.dest_port(rvg0),
	.data_length(),
	.port_vector(orvg0)
);

/*------------------------------------*/
iport
iport1
(
	.clk(clk),
	.rst(rst),
	.i_data(data_rx),
	.i_cmd(cmd_port),
	.port_number('d1),
	.port_enable( (src_portLenght > 'd1) ? (1'b1):(1'b0) ),
	.execute_req(),
	.load( (src_portLenght > 'd1) ? (cmd_fifo1):(1'b0) ),
	.enb_count(),
	.iFIFO_data_push( (src_portLenght > 'd1) ? (push_fifo1):(1'b0) ),
	.port_state(),
	.port_full(),
	.port_empty(),
	.o_data(ofifo1),
	.o_cmd(dst_port_fifo1),
	.arb_ack(),
	.cmd_ready(),
	.port_ready(),
	.dest_port(rvg1),
	.data_length(),
	.port_vector(orvg1)
);

/*------------------------------------*/
iport
iport2
(
	.clk(clk),
	.rst(rst),
	.i_data(data_rx),
	.i_cmd(cmd_port),
	.port_number('d2),
	.port_enable( (src_portLenght > 'd2) ? (1'b1):(1'b0) ),
	.execute_req(),
	.load( (src_portLenght > 'd2) ? (cmd_fifo2):(1'b0) ),
	.enb_count(),
	.iFIFO_data_push( (src_portLenght > 'd2) ? (push_fifo2):(1'b0) ),
	.port_state(),
	.port_full(),
	.port_empty(),
	.o_data(ofifo2),
	.o_cmd(dst_port_fifo2),
	.arb_ack(),
	.cmd_ready(),
	.port_ready(),
	.dest_port(rvg2),
	.data_length(),
	.port_vector(orvg2)
);

/*------------------------------------*/
iport
iport3
(
	.clk(clk),
	.rst(rst),
	.i_data(data_rx),
	.i_cmd(cmd_port),
	.port_number('d3),
	.port_enable( (src_portLenght > 'd3) ? (1'b1):(1'b0) ),
	.execute_req(),
	.load( (src_portLenght > 'd3) ? (cmd_fifo3):(1'b0) ),
	.enb_count(),
	.iFIFO_data_push( (src_portLenght > 'd3) ? (push_fifo3):(1'b0) ),
	.port_state(),
	.port_full(),
	.port_empty(),
	.o_data(ofifo3),
	.o_cmd(dst_port_fifo3),
	.arb_ack(),
	.cmd_ready(),
	.port_ready(),
	.dest_port(rvg3),
	.data_length(),
	.port_vector(orvg3)
);

/*------------------------------------*/
iport
iport4
(
	.clk(clk),
	.rst(rst),
	.i_data(data_rx),
	.i_cmd(cmd_port),
	.port_number('d4),
	.port_enable( (src_portLenght > 'd4) ? (1'b1):(1'b0) ),
	.execute_req(),
	.load( (src_portLenght > 'd4) ? (cmd_fifo4):(1'b0) ),
	.enb_count(),
	.iFIFO_data_push( (src_portLenght > 'd4) ? (push_fifo4):(1'b0) ),
	.port_state(),
	.port_full(),
	.port_empty(),
	.o_data(ofifo4),
	.o_cmd(dst_port_fifo4),
	.arb_ack(),
	.cmd_ready(),
	.port_ready(),
	.dest_port(rvg4),
	.data_length(),
	.port_vector(orvg4)
);
	
/*------------------------------------*/
iport
iport5
(
	.clk(clk),
	.rst(rst),
	.i_data(data_rx),
	.i_cmd(cmd_port),
	.port_number('d5),
	.port_enable( (src_portLenght > 'd5) ? (1'b1):(1'b0) ),
	.execute_req(),
	.load( (src_portLenght > 'd5) ? (cmd_fifo5):(1'b0) ),
	.enb_count(),
	.iFIFO_data_push( (src_portLenght > 'd5) ? (push_fifo5):(1'b0) ),
	.port_state(),
	.port_full(),
	.port_empty(),
	.o_data(ofifo5),
	.o_cmd(dst_port_fifo5),
	.arb_ack(),
	.cmd_ready(),
	.port_ready(),
	.dest_port(rvg5),
	.data_length(),
	.port_vector(orvg5)
);	

/*------------------------------------*/
iport
iport6
(
	.clk(clk),
	.rst(rst),
	.i_data(data_rx),
	.i_cmd(cmd_port),
	.port_number('d6),
	.port_enable( (src_portLenght > 'd6) ? (1'b1):(1'b0) ),
	.execute_req(),
	.load( (src_portLenght > 'd6) ? (cmd_fifo6):(1'b0) ),
	.enb_count(),
	.iFIFO_data_push( (src_portLenght > 'd6) ? (push_fifo6):(1'b0) ),
	.port_state(),
	.port_full(),
	.port_empty(),
	.o_data(ofifo6),
	.o_cmd(dst_port_fifo6),
	.arb_ack(),
	.cmd_ready(),
	.port_ready(),
	.dest_port(rvg6),
	.data_length(),
	.port_vector(orvg6)
);

/*------------------------------------*/
iport
iport7
(
	.clk(clk),
	.rst(rst),
	.i_data(data_rx),
	.i_cmd(cmd_port),
	.port_number('d7),
	.port_enable( (src_portLenght > 'd7) ? (1'b1):(1'b0) ),
	.execute_req(),
	.load( (src_portLenght > 'd7) ? (cmd_fifo7):(1'b0) ),
	.enb_count(),
	.iFIFO_data_push( (src_portLenght > 'd7) ? (push_fifo7):(1'b0) ),
	.port_state(),
	.port_full(),
	.port_empty(),
	.o_data(ofifo7),
	.o_cmd(dst_port_fifo7),
	.arb_ack(),
	.cmd_ready(),
	.port_ready(),
	.dest_port(rvg7),
	.data_length(),
	.port_vector(orvg7)
);

demux_1_8
demux_rvg_port0
(
	.i_data(orvg0),
	.i_sel( selectr_e'(rvg0 - 'd1) ),
	.o_data_a(port0_wcrr0),
	.o_data_b(port0_wcrr1),
	.o_data_c(port0_wcrr2),
	.o_data_d(port0_wcrr3),
	.o_data_e(port0_wcrr4),
	.o_data_f(port0_wcrr5),
	.o_data_g(port0_wcrr6),
	.o_data_h(port0_wcrr7)
);

demux_1_8
demux_rvg_port1
(
	.i_data(orvg1),
	.i_sel( selectr_e'(rvg1) ),
	.o_data_a(port1_wcrr0),
	.o_data_b(port1_wcrr1),
	.o_data_c(port1_wcrr2),
	.o_data_d(port1_wcrr3),
	.o_data_e(port1_wcrr4),
	.o_data_f(port1_wcrr5),
	.o_data_g(port1_wcrr6),
	.o_data_h(port1_wcrr7)
);

demux_1_8
demux_rvg_port2
(
	.i_data(orvg2),
	.i_sel( selectr_e'(rvg2) ),
	.o_data_a(port2_wcrr0),
	.o_data_b(port2_wcrr1),
	.o_data_c(port2_wcrr2),
	.o_data_d(port2_wcrr3),
	.o_data_e(port2_wcrr4),
	.o_data_f(port2_wcrr5),
	.o_data_g(port2_wcrr6),
	.o_data_h(port2_wcrr7)
);

demux_1_8
demux_rvg_port3
(
	.i_data(orvg3),
	.i_sel( selectr_e'(rvg3) ),
	.o_data_a(port3_wcrr0),
	.o_data_b(port3_wcrr1),
	.o_data_c(port3_wcrr2),
	.o_data_d(port3_wcrr3),
	.o_data_e(port3_wcrr4),
	.o_data_f(port3_wcrr5),
	.o_data_g(port3_wcrr6),
	.o_data_h(port3_wcrr7)
);

demux_1_8
demux_rvg_port4
(
	.i_data(orvg4),
	.i_sel( selectr_e'(rvg4) ),
	.o_data_a(port4_wcrr0),
	.o_data_b(port4_wcrr1),
	.o_data_c(port4_wcrr2),
	.o_data_d(port4_wcrr3),
	.o_data_e(port4_wcrr4),
	.o_data_f(port4_wcrr5),
	.o_data_g(port4_wcrr6),
	.o_data_h(port4_wcrr7)
);

demux_1_8
demux_rvg_port5
(
	.i_data(orvg5),
	.i_sel( selectr_e'(rvg5) ),
	.o_data_a(port5_wcrr0),
	.o_data_b(port5_wcrr1),
	.o_data_c(port5_wcrr2),
	.o_data_d(port5_wcrr3),
	.o_data_e(port5_wcrr4),
	.o_data_f(port5_wcrr5),
	.o_data_g(port5_wcrr6),
	.o_data_h(port5_wcrr7)
);

demux_1_8
demux_rvg_port6
(
	.i_data(orvg6),
	.i_sel( selectr_e'(rvg6) ),
	.o_data_a(port6_wcrr0),
	.o_data_b(port6_wcrr1),
	.o_data_c(port6_wcrr2),
	.o_data_d(port6_wcrr3),
	.o_data_e(port6_wcrr4),
	.o_data_f(port6_wcrr5),
	.o_data_g(port6_wcrr6),
	.o_data_h(port6_wcrr7)
);


demux_1_8
demux_rvg_port7
(
	.i_data(orvg7),
	.i_sel( selectr_e'(rvg7 - 'd1) ),
	.o_data_a(port7_wcrr0),
	.o_data_b(port7_wcrr1),
	.o_data_c(port7_wcrr2),
	.o_data_d(port7_wcrr3),
	.o_data_e(port7_wcrr4),
	.o_data_f(port7_wcrr5),
	.o_data_g(port7_wcrr6),
	.o_data_h(port7_wcrr7)
);


or_8_1
or_rvg0
(
	.i_a(port0_wcrr0),	
	.i_b(port1_wcrr0),
	.i_c(port2_wcrr0),
	.i_d(port3_wcrr0),
	.i_e(port4_wcrr0),
	.i_f(port5_wcrr0),
	.i_g(port6_wcrr0),
	.i_h(port7_wcrr0),
	.o_sltd(oport_wcrr0)
);

or_8_1
or_rvg1
(
	.i_a(port0_wcrr1),	
	.i_b(port1_wcrr1),
	.i_c(port2_wcrr1),
	.i_d(port3_wcrr1),
	.i_e(port4_wcrr1),
	.i_f(port5_wcrr1),
	.i_g(port6_wcrr1),
	.i_h(port7_wcrr1),
	.o_sltd(oport_wcrr1)
);

or_8_1
or_rvg2
(
	.i_a(port0_wcrr2),	
	.i_b(port1_wcrr2),
	.i_c(port2_wcrr2),
	.i_d(port3_wcrr2),
	.i_e(port4_wcrr2),
	.i_f(port5_wcrr2),
	.i_g(port6_wcrr2),
	.i_h(port7_wcrr2),
	.o_sltd(oport_wcrr2)
);

or_8_1
or_rvg3
(
	.i_a(port0_wcrr3),	
	.i_b(port1_wcrr3),
	.i_c(port2_wcrr3),
	.i_d(port3_wcrr3),
	.i_e(port4_wcrr3),
	.i_f(port5_wcrr3),
	.i_g(port6_wcrr3),
	.i_h(port7_wcrr3),
	.o_sltd(oport_wcrr3)
);

or_8_1
or_rvg4
(
	.i_a(port0_wcrr4),	
	.i_b(port1_wcrr4),
	.i_c(port2_wcrr4),
	.i_d(port3_wcrr4),
	.i_e(port4_wcrr4),
	.i_f(port5_wcrr4),
	.i_g(port6_wcrr4),
	.i_h(port7_wcrr4),
	.o_sltd(oport_wcrr4)
);

or_8_1
or_rvg5
(
	.i_a(port0_wcrr5),	
	.i_b(port1_wcrr5),
	.i_c(port2_wcrr5),
	.i_d(port3_wcrr5),
	.i_e(port4_wcrr5),
	.i_f(port5_wcrr5),
	.i_g(port6_wcrr5),
	.i_h(port7_wcrr5),
	.o_sltd(oport_wcrr5)
);

or_8_1
or_rvg6
(
	.i_a(port0_wcrr6),	
	.i_b(port1_wcrr6),
	.i_c(port2_wcrr6),
	.i_d(port3_wcrr6),
	.i_e(port4_wcrr6),
	.i_f(port5_wcrr6),
	.i_g(port6_wcrr6),
	.i_h(port7_wcrr6),
	.o_sltd(oport_wcrr6)
);

or_8_1
or_rvg7
(
	.i_a(port0_wcrr7),	
	.i_b(port1_wcrr7),
	.i_c(port2_wcrr7),
	.i_d(port3_wcrr7),
	.i_e(port4_wcrr7),
	.i_f(port5_wcrr7),
	.i_g(port6_wcrr7),
	.i_h(port7_wcrr7),
	.o_sltd(oport_wcrr7)
);

wcrr
wcrr_fifo0
(
	.clk(clk),
	.rst(rst),
	.ack(),
	.request_vector( oport_wcrr0 ),
	.valid(),
	.grant(grant_fifo0)
);
wcrr
wcrr_fifo1
(
	.clk(clk),
	.rst(rst),
	.ack(),
	.request_vector( oport_wcrr1 ),
	.valid(),
	.grant(grant_fifo1)
);
wcrr
wcrr_fifo2
(
	.clk(clk),
	.rst(rst),
	.ack(),
	.request_vector( oport_wcrr2 ),
	.valid(),
	.grant(grant_fifo2)
);
wcrr
wcrr_fifo3
(
	.clk(clk),
	.rst(rst),
	.ack(),
	.request_vector( oport_wcrr3 ),
	.valid(),
	.grant(grant_fifo3)
);
wcrr
wcrr_fifo4
(
	.clk(clk),
	.rst(rst),
	.ack(),
	.request_vector( oport_wcrr4 ),
	.valid(),
	.grant(grant_fifo4)
);
wcrr
wcrr_fifo5
(
	.clk(clk),
	.rst(rst),
	.ack(),
	.request_vector( oport_wcrr5 ),
	.valid(),
	.grant(grant_fifo5)
);
wcrr
wcrr_fifo6
(
	.clk(clk),
	.rst(rst),
	.ack(),
	.request_vector( oport_wcrr6 ),
	.valid(),
	.grant(grant_fifo6)
);
wcrr
wcrr_fifo7
(
	.clk(clk),
	.rst(rst),
	.ack(),
	.request_vector( oport_wcrr7 ),
	.valid(),
	.grant(grant_fifo7)
);

oneshot2dec
oneshot_convert_ofifo0
(
	.i_oneshot(grant_fifo0),
	.o_dec(conv_fifo0)
);

mux_8_1
mux_dst_ofifo0
(
	.i_a(ofifo0),
	.i_b(ofifo1),
	.i_c(ofifo2),
	.i_d(ofifo3),
	.i_e(ofifo4),
	.i_f(ofifo5),
	.i_g(ofifo6),
	.i_h(ofifo7),
	.i_sel(selectr_e'(conv_fifo0)),
	.o_sltd(i_ofifo0)
);
FIFO 
oFIFO0
(
    .wclk(clk),
    .rclk(clk),
    .rst(rst),
    .push(),
    .pop(),
    .DataInput(i_ofifo0),  //data_t
    .DataOutput(o_ofifo0), //data_t
    .full(),
    .empty()
);
	
oneshot2dec
oneshot_convert_ofifo1
(
	.i_oneshot(grant_fifo1),
	.o_dec(conv_fifo1)
);

mux_8_1
mux_dst_ofifo1
(
	.i_a(ofifo0),
	.i_b(ofifo1),
	.i_c(ofifo2),
	.i_d(ofifo3),
	.i_e(ofifo4),
	.i_f(ofifo5),
	.i_g(ofifo6),
	.i_h(ofifo7),
	.i_sel(selectr_e'(conv_fifo1)),
	.o_sltd(i_ofifo1)
);	
FIFO 
oFIFO1
(
    .wclk(clk),
    .rclk(clk),
    .rst(rst),
    .push(),
    .pop(),
    .DataInput(i_ofifo1),  //data_t
    .DataOutput(o_ofifo1), //data_t
    .full(),
    .empty()
);

oneshot2dec
oneshot_convert_ofifo2
(
	.i_oneshot(grant_fifo2),
	.o_dec(conv_fifo2)
);

mux_8_1
mux_dst_ofifo2
(
	.i_a(ofifo0),
	.i_b(ofifo1),
	.i_c(ofifo2),
	.i_d(ofifo3),
	.i_e(ofifo4),
	.i_f(ofifo5),
	.i_g(ofifo6),
	.i_h(ofifo7),
	.i_sel(selectr_e'(conv_fifo2)),
	.o_sltd(i_ofifo2)
);
FIFO 
oFIFO2
(
    .wclk(clk),
    .rclk(clk),
    .rst(rst),
    .push(),
    .pop(),
    .DataInput(i_ofifo2),  //data_t
    .DataOutput(o_ofifo2), //data_t
    .full(),
    .empty()
);

oneshot2dec
oneshot_convert_ofifo3
(
	.i_oneshot(grant_fifo3),
	.o_dec(conv_fifo3)
);

mux_8_1
mux_dst_ofifo3
(
	.i_a(ofifo0),
	.i_b(ofifo1),
	.i_c(ofifo2),
	.i_d(ofifo3),
	.i_e(ofifo4),
	.i_f(ofifo5),
	.i_g(ofifo6),
	.i_h(ofifo7),
	.i_sel(selectr_e'(conv_fifo3)),
	.o_sltd(i_ofifo3)
);
FIFO 
oFIFO3
(
    .wclk(clk),
    .rclk(clk),
    .rst(rst),
    .push(),
    .pop(),
    .DataInput(i_ofifo3),  //data_t
    .DataOutput(o_ofifo3), //data_t
    .full(),
    .empty()
);

oneshot2dec
oneshot_convert_ofifo4
(
	.i_oneshot(grant_fifo4),
	.o_dec(conv_fifo4)
);

mux_8_1
mux_dst_ofifo4
(
	.i_a(ofifo0),
	.i_b(ofifo1),
	.i_c(ofifo2),
	.i_d(ofifo3),
	.i_e(ofifo4),
	.i_f(ofifo5),
	.i_g(ofifo6),
	.i_h(ofifo7),
	.i_sel(selectr_e'(conv_fifo4)),
	.o_sltd(i_ofifo4)
);
FIFO 
oFIFO4
(
    .wclk(clk),
    .rclk(clk),
    .rst(rst),
    .push(),
    .pop(),
    .DataInput(i_ofifo4),  //data_t
    .DataOutput(o_ofifo4), //data_t
    .full(),
    .empty()
);

oneshot2dec
oneshot_convert_ofifo5
(
	.i_oneshot(grant_fifo5),
	.o_dec(conv_fifo5)
);

mux_8_1
mux_dst_ofifo5
(
	.i_a(ofifo0),
	.i_b(ofifo1),
	.i_c(ofifo2),
	.i_d(ofifo3),
	.i_e(ofifo4),
	.i_f(ofifo5),
	.i_g(ofifo6),
	.i_h(ofifo7),
	.i_sel(selectr_e'(conv_fifo5)),
	.o_sltd(i_ofifo5)
);
FIFO 
oFIFO5
(
    .wclk(clk),
    .rclk(clk),
    .rst(rst),
    .push(),
    .pop(),
    .DataInput(i_ofifo5),  //data_t
    .DataOutput(o_ofifo5), //data_t
    .full(),
    .empty()
);

oneshot2dec
oneshot_convert_ofifo6
(
	.i_oneshot(grant_fifo6),
	.o_dec(conv_fifo6)
);

mux_8_1
mux_dst_ofifo6
(
	.i_a(ofifo0),
	.i_b(ofifo1),
	.i_c(ofifo2),
	.i_d(ofifo3),
	.i_e(ofifo4),
	.i_f(ofifo5),
	.i_g(ofifo6),
	.i_h(ofifo7),
	.i_sel(selectr_e'(conv_fifo6)),
	.o_sltd(i_ofifo6)
);
FIFO 
oFIFO6
(
    .wclk(clk),
    .rclk(clk),
    .rst(rst),
    .push(),
    .pop(),
    .DataInput(i_ofifo6),  //data_t
    .DataOutput(o_ofifo6), //data_t
    .full(),
    .empty()
);

oneshot2dec
oneshot_convert_ofifo7
(
	.i_oneshot(grant_fifo7),
	.o_dec(conv_fifo7)
);

mux_8_1
mux_dst_ofifo7
(
	.i_a(ofifo0),
	.i_b(ofifo1),
	.i_c(ofifo2),
	.i_d(ofifo3),
	.i_e(ofifo4),
	.i_f(ofifo5),
	.i_g(ofifo6),
	.i_h(ofifo7),
	.i_sel(selectr_e'(conv_fifo7)),
	.o_sltd(i_ofifo7)
);
FIFO 
oFIFO7
(
    .wclk(clk),
    .rclk(clk),
    .rst(rst),
    .push(),
    .pop(),
    .DataInput(i_ofifo7),  //data_t
    .DataOutput(o_ofifo7), //data_t
    .full(),
    .empty()
);

endmodule 
