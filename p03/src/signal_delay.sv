

module signal_delay
(
    input clk,
    input rst,
    input signal,
    output delayed_signal
);

logic ovf_w;
logic counter_enb_w;
logic delayed_signal_w;

parametric_counter data_counter_pop
(
    .clk(clk),
    .rst(rst),
    .enb(counter_enb_w),
    .max_count(8'd6),
    .ovf(ovf_w),
    .count()
);


fsm_signal_delay fsm_delay
(
    .clk(clk),
    .rst(rst),
    .start(signal),
    .counter_enb(counter_enb_w),
    .ovf(ovf_w),
    .delayed_signal(delayed_signal_w)
);

// signal_delay sig_delay
// (
//     .clk(clk),
//     .rst(rst),
//     .signal(),
//     .delayed_signal()
// );

assign delayed_signal = delayed_signal_w;

endmodule