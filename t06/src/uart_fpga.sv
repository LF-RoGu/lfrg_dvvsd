import data_pkg::*;

module uart
(
	input bit clk,
	input logic rst,
	input logic transmit,			//button start tx
	
	input  data_n_t data_tx, //parallel data from uart2uart
	output data_n_t data_rx, //parallel data from uart2uart
	
	output logic octs,
	
	output rx_interrupt,
	output parity_error
);

logic serial_tx2rx;
	
logic cts;
logic parity_bit;

logic outclk;
logic locked_pll;
pll_10M
syspll
(
	.refclk(clk),
	.rst(!rst),
	.outclk_0(outclk),
	.locked(locked_pll)
);

uart_tx
uart_tx_top
(
	.clk(outclk),
	.rst(rst),
	.data(data_tx),
	
	.transmit(transmit),
	.cts(cts),
	
	.serial_tx(serial_tx2rx)
);

uart_rx
uart_rx_top
(
	.clk(outclk),
	.rst(rst),
	
	.serial_rx(serial_tx2rx),

	.cts(cts),
	
	.rts(rx_interrupt),
	
	.pairty_error(parity_error),
	.data(data_rx)
);

assign octs = cts;

endmodule 
