// Coder:       Abisai Ramirez Perez
// Date:        January 28th, 2020
// Name:        bin_counter.sv
// Description: This is a binary counter

module bin_counter #(
    parameter DW=4,
    parameter OVF_VAL=4
)(
    input           clk,
    input           rst,
    input  logic    enb,
    input  logic	rst_count,
    output logic	ovf,
    output [DW-1:0] count
);

logic [DW-1:0] count_r, cnt_nxt;
logic ovf_t;

// Combinational process
always_comb begin
    if(count_r == OVF_VAL-1) begin
		ovf_t = 1'b1;
        cnt_nxt = '0;
    end
    else begin
        ovf_t = 1'b0;
        cnt_nxt  = count_r + 1'b1 ;
    end
end

// Sequential process
always_ff@(posedge clk, negedge rst)begin: counter
    if (!rst)
        count_r     <=  '0;
    else if (enb)
        count_r     <= (rst_count)?('0):(cnt_nxt);

end:counter

assign ovf    =   ovf_t;
assign count = count_r; 

endmodule

