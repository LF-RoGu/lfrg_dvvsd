onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /uart_tb/uut/uartRx_clk_divider_t/clk_FPGA
add wave -noupdate /uart_tb/uut/uartRx_clk_divider_t/clk
add wave -noupdate -divider {UART TX}
add wave -noupdate -label clk_FPGA /uart_tb/uut/uart_tx_top/uartTx_clk_divider/clk_FPGA
add wave -noupdate -label clk /uart_tb/uut/uart_tx_top/uartTx_clk_divider/clk
add wave -noupdate -label data -radix unsigned /uart_tb/uut/uart_tx_top/data
add wave -noupdate -label data /uart_tb/uut/uart_tx_top/data
add wave -noupdate -label transmit /uart_tb/uut/uart_tx_top/transmit
add wave -noupdate -label cts /uart_tb/uut/uart_tx_top/cts
add wave -noupdate -label serial_tx /uart_tb/uut/uart_tx_top/serial_tx
add wave -noupdate -group {fsm tx} /uart_tb/uut/uart_tx_top/uart_tx_fsm/transmit
add wave -noupdate -group {fsm tx} /uart_tb/uut/uart_tx_top/uart_tx_fsm/ovf
add wave -noupdate -group {fsm tx} /uart_tb/uut/uart_tx_top/uart_tx_fsm/l_s
add wave -noupdate -group {fsm tx} /uart_tb/uut/uart_tx_top/uart_tx_fsm/cntr_enb
add wave -noupdate -group {fsm tx} /uart_tb/uut/uart_tx_top/uart_tx_fsm/reg_enb
add wave -noupdate -group {fsm tx} /uart_tb/uut/uart_tx_top/uart_tx_fsm/mux_sel
add wave -noupdate -group {fsm tx} /uart_tb/uut/uart_tx_top/uart_tx_fsm/cts
add wave -noupdate -group {fsm tx} /uart_tb/uut/uart_tx_top/uart_tx_fsm/tx_active
add wave -noupdate -group {fsm tx} /uart_tb/uut/uart_tx_top/uart_tx_fsm/current_st
add wave -noupdate -group {fsm tx} /uart_tb/uut/uart_tx_top/uart_tx_fsm/nxt_state
add wave -noupdate -group piso /uart_tb/uut/uart_tx_top/piso_register_lsb_uartTX/enb
add wave -noupdate -group piso /uart_tb/uut/uart_tx_top/piso_register_lsb_uartTX/l_s
add wave -noupdate -group piso /uart_tb/uut/uart_tx_top/piso_register_lsb_uartTX/i_data
add wave -noupdate -group piso /uart_tb/uut/uart_tx_top/piso_register_lsb_uartTX/o_data
add wave -noupdate -group piso /uart_tb/uut/uart_tx_top/piso_register_lsb_uartTX/rgstr_r
add wave -noupdate -group piso /uart_tb/uut/uart_tx_top/piso_register_lsb_uartTX/rgstr_next
add wave -noupdate -group counter -label enb /uart_tb/uut/uart_tx_top/bin_counter_uartTX/enb
add wave -noupdate -group counter -label count /uart_tb/uut/uart_tx_top/bin_counter_uartTX/count
add wave -noupdate -group counter -label ovf /uart_tb/uut/uart_tx_top/bin_counter_uartTX/ovf
add wave -noupdate -divider {UART RX}
add wave -noupdate -label clk_FPGA /uart_tb/uut/uart_rx_top/uartRx_clk_divider/clk_FPGA
add wave -noupdate -label clk /uart_tb/uut/uart_rx_top/uartRx_clk_divider/clk
add wave -noupdate -expand -group {fsm rx} /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/clk
add wave -noupdate -expand -group {fsm rx} /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/rst
add wave -noupdate -expand -group {fsm rx} /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/ovf
add wave -noupdate -expand -group {fsm rx} /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/data_in
add wave -noupdate -expand -group {fsm rx} /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/rts
add wave -noupdate -expand -group {fsm rx} /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/data_cntr_enable
add wave -noupdate -expand -group {fsm rx} /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/current_st
add wave -noupdate -expand -group {fsm rx} /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/nxt_state
add wave -noupdate -expand -group sipo /uart_tb/uut/uart_rx_top/sipo_register_msb_uartRX/enb
add wave -noupdate -expand -group sipo /uart_tb/uut/uart_rx_top/sipo_register_msb_uartRX/data_in
add wave -noupdate -expand -group sipo /uart_tb/uut/uart_rx_top/sipo_register_msb_uartRX/data_out
add wave -noupdate -expand -group {data counter} /uart_tb/uut/uart_rx_top/bin_counter_uartRX/clk
add wave -noupdate -expand -group {data counter} /uart_tb/uut/uart_rx_top/bin_counter_uartRX/rst
add wave -noupdate -expand -group {data counter} /uart_tb/uut/uart_rx_top/bin_counter_uartRX/enb
add wave -noupdate -expand -group {data counter} /uart_tb/uut/uart_rx_top/bin_counter_uartRX/max_count
add wave -noupdate -expand -group {data counter} /uart_tb/uut/uart_rx_top/bin_counter_uartRX/ovf
add wave -noupdate -expand -group {data counter} /uart_tb/uut/uart_rx_top/bin_counter_uartRX/count
add wave -noupdate -expand -group {data counter} /uart_tb/uut/uart_rx_top/bin_counter_uartRX/count_r
add wave -noupdate -expand -group {data counter} /uart_tb/uut/uart_rx_top/bin_counter_uartRX/count_nxt
add wave -noupdate -expand -group {data counter} /uart_tb/uut/uart_rx_top/bin_counter_uartRX/ovf_st
add wave -noupdate -expand -group {count data} /uart_tb/uut/uart_rx_top/data_counter_uartRx/clk
add wave -noupdate -expand -group {count data} /uart_tb/uut/uart_rx_top/data_counter_uartRx/rst
add wave -noupdate -expand -group {count data} /uart_tb/uut/uart_rx_top/data_counter_uartRx/enb
add wave -noupdate -expand -group {count data} -radix decimal /uart_tb/uut/uart_rx_top/data_counter_uartRx/max_count
add wave -noupdate -expand -group {count data} /uart_tb/uut/uart_rx_top/data_counter_uartRx/ovf
add wave -noupdate -expand -group {count data} -radix decimal /uart_tb/uut/uart_rx_top/data_counter_uartRx/count
add wave -noupdate -expand -group {count data} -radix decimal /uart_tb/uut/uart_rx_top/data_counter_uartRx/count_r
add wave -noupdate -expand -group {count data} -radix decimal /uart_tb/uut/uart_rx_top/data_counter_uartRx/count_nxt
add wave -noupdate -expand -group {count data} /uart_tb/uut/uart_rx_top/data_counter_uartRx/ovf_st
add wave -noupdate -expand -group odata /uart_tb/uut/uart_rx_top/uart_data_register/enb
add wave -noupdate -expand -group odata /uart_tb/uut/uart_rx_top/uart_data_register/i_data
add wave -noupdate -expand -group odata -radix unsigned /uart_tb/uut/uart_rx_top/uart_data_register/o_data
add wave -noupdate -expand -group odata /uart_tb/uut/uart_rx_top/uart_data_register/o_data
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {41519187 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 434
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {197415750 ps}
