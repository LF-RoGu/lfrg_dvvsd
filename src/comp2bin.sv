/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: bin2comp
    Description: bin2comp source file
    Last modification: 13/02/2021
*/

import p02_pkg::*;

module comp2bin
#(
	parameter DW = p02_pkg::N
)
(	
	/** INPUT*/
	input   logic[DW-1:0]   	i_data,
	/** OUTPUT*/
	output  logic[DW-1:0]   	o_data,
	output  logic               sign
);

always_comb begin
	//NEGATIVE NUMBER
	if(i_data[DW-1] == 1'b1) begin
		o_data = (~i_data + 1'b1);
		sign = 1'b1;
	end
	//POSITIVE NUMBER
	else begin
		o_data = i_data;
		sign = 1'b0;
	end
end

endmodule
