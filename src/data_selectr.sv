/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: op_sel
    Description: op_sel source file
    Last modification: 14/02/2021
*/

import p02_pkg::*;
module data_selectr
(	
	input						clk,
	input						rst,
	input   logic				enb,
	output  logic			   	o_sltd
);

logic state_r, state_nxt;

always_comb begin
	state_nxt = ~state_r;
end
// Sequential process
always_ff@(posedge clk, negedge rst)begin
    if (!rst)
        state_r <= 1'b1;
    else if (enb)
        state_r <= state_nxt;
end

assign o_sltd = state_r;

endmodule
