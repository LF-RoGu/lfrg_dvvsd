/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: booth_multiplier_pkg
    Description: booth_multiplier_pkg package file
    Last modification: 14/02/2021
*/

 `ifndef P02_PKG_SV
	`define P02_PKG_SV
package p02_pkg;
	//`define MODELSIM_DEF
	/** Parameters*/
	localparam N = 8; 

	// TRUE and FALSE definitions
	localparam bit TRUE     =1'b1;
	localparam bit FALSE    =1'b0;

	/** Data Types*/
	/*
	 * OP data type
	 */
	typedef enum logic [1:0]
	{
		MULTI,
		DIV,
		SQRT,
		ERROR
	}op_selectr_e;
	/*
	 * TOP MODULE data type
	 */
	typedef logic [N-1:0] data_n_t; 
	typedef logic [(2*N)-1:0] data_2n_t; 
	typedef logic [(2*N)+1:0] data_2n1_t; 

	/*
	 * ALU data type
	 */

	 typedef enum logic [6:0]
	{
		NO_OP,				//4'b0000
		ADDITION,			//4'b0001
		SUBSTRACTION,		//4'b0010
		AND,				//4'b0011
		OR,					//4'b0100
		COMP2_A,			//4'b0101
		COMP2_B				//4'b0110
	} alu_op_sel_str; 

	typedef logic [N-1:0] alu_dt; //alu_data_type

	localparam N_SEL = 4;
	typedef logic [N-1:0]   dtwidth_t;
	typedef enum logic [N_SEL-1:0]
	{
		OP_A,
		OP_B,
		OP_C,
		OP_D
	}selectr_e;
	
	/*
	 * FSM_CONTROL data_type
	 */
	typedef enum logic[2:0]
	{
	    IDLE_ST,
	    LOADX_ST,
	    LOADY_ST,
	    START_ST,
	    BUSY_ST
	} control_st;
	
	/*
	 * COUNTER_OVF data_type
	 */
    localparam MAX_COUNT = N-1;

    typedef logic [N-1:0] count_t; 
	/*
	 * CLK_DIVIDER data_type
	 */
	localparam N_count = 27;  //number of counters
    typedef logic [N_count-1:0] counter_t;
	/*
	 * BIN2BCD data_type
	 */
	localparam PARAM_DW_BCD = 4;
	localparam PARAM_DW_SEGMENT_DISPLAY = 7;
	 
	 /** Data Types*/
	 typedef logic [2*N - 1:0] binary_data_t;
	 typedef logic [PARAM_DW_BCD - 1:0] bcd_data_t;
	 typedef logic [PARAM_DW_SEGMENT_DISPLAY - 1:0] data2display_t;
	/*
	 * DBCR_CNTR data_type
	 */
	 typedef enum logic [1:0]{
		 BAJO = 2'b00,
		 DLY1 = 2'b01,
		 ALTO = 2'b10,
		 DLY2 = 2'b11
	 } fsm_dbcr_state_e;
endpackage
 `endif