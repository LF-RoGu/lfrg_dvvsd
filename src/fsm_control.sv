/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: fsm_control
    Description: fsm_control source file
    Last modification: 20/02/2021
*/

import p02_pkg::*;
module fsm_control
(
   input        		clk,
   input        		rst,
   input  logic 		start,
   input  logic			load,
   input  op_selectr_e	op_sel,
   output logic 		ready,
   output count_t 		count,
   
   output logic 		loadX,
   output logic 		loadY,
   
   /** BOOTH*/
   output logic      	Anx_enb,
   output logic      	Snx_enb,
   output logic      	Pnx_enb,
   
   /** SQRT*/
   output logic      	sqrt_enb_Rnx,
   output logic      	sqrt_enb_Qnx,
   output logic      	sqrt_enb_aluOP
);

/** SYSTEM*/
logic sys_enb_counter;
logic sys_ovf, sys_ovf_counter, sys_ovf_decreaser;

count_t counter;
count_t decreaser;
	
logic booth_loadX, booth_loadY;
logic sqrt_loadX, sqrt_loadY;

logic sys_start_booth, sys_start_div, sys_start_sqrt;
/** BOOTH*/
logic booth_enb_counter;
logic booth_ready;

/** DIV*/
logic div_ready;
/** SQRT*/
logic sqrt_ready;
logic sqrt_enb_decreaser;
	
/********************************************************************************************************/
/** BOOTH*/
/********************************************************************************************************/
fsm_control_booth booth_control
(
   .clk(clk),
   .rst(rst),
   .start(sys_start_booth),
   .ovf(sys_ovf),
   
   .load(load),
   
   .loadX(booth_loadX),
   .loadY(booth_loadY),
   
   .counter_enb(booth_enb_counter),
   .Anx_enb(Anx_enb),
   .Snx_enb(Snx_enb),
   .Pnx_enb(Pnx_enb),
   .ready(booth_ready)
);
/********************************************************************************************************/
/** SQRT*/
/********************************************************************************************************/
fsm_control_sqrt sqrt_control
(
	.clk(clk),
	.rst(rst),
	.start(sys_start_sqrt),
	.ovf(sys_ovf),
	
	.load(load),
   
    .loadX(sqrt_loadX),
    .loadY(sqrt_loadY),
   
	.enb_counter(sqrt_enb_decreaser),
	.enb_Rnx(sqrt_enb_Rnx),
	.enb_Qnx(sqrt_enb_Qnx),
	.enb_aluOP(sqrt_enb_aluOP),
	.ready(sqrt_ready)
);

counter_ovf counter_ovf
(
	.clk(clk),
	.rst(rst),
	.enb(sys_enb_counter),
	.count(counter),
	.ovf(sys_ovf_counter)
);

decrease_ovf
decrease_ovf_jx
(
	.clk(clk),
	.rst(rst),
	.enb(sys_enb_counter),
	.count(decreaser),
	.ovf(sys_ovf_decreaser)
);


assign sys_start_booth = (op_sel == MULTI) ? (start):('b0);
assign sys_start_sqrt = (op_sel == SQRT) ? (start):('b0);
assign sys_ovf = (op_sel == SQRT) ? (sys_ovf_decreaser):(sys_ovf_counter);
assign count = (op_sel == SQRT)?(decreaser):(counter);


assign sys_enb_counter = (booth_enb_counter || sqrt_enb_decreaser);


mux_2_1 
#(
	.DW(1)
)
mux_loadX
(
	.i_a(booth_loadX),
	.i_b(sqrt_loadX),
	.i_sel((selectr_e'(op_sel) == MULTI) ? (OP_A) : (OP_B)),
	.o_sltd(loadX)
);
	
mux_2_1 
#(
	.DW(1)
)
mux_loadY
(
	.i_a(booth_loadY),
	.i_b(sqrt_loadY),
	.i_sel((selectr_e'(op_sel) == MULTI) ? (OP_A) : (OP_B)),
	.o_sltd(loadY)
);

mux_4_1 
#(
	.DW(1)
)
mux_ready
(
	.i_a(booth_ready),
	.i_b(div_ready),
	.i_c(sqrt_ready),
	.i_d(),
	.i_sel(selectr_e'(op_sel)),
	.o_sltd(ready)
);

endmodule 