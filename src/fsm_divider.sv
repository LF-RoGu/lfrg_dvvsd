module fsm_divider
import p02_pkg::*, fsm_pkg::*;
(
  input                		 clk,
  input        	      	 rst,
  input    logic            start,
  input    logic            counter_ovf,
  input    logic            rem_sign,
  input    logic            divisor_load_flag,
  input    logic            quotient_load_flag,
  output   logic            counter_enb,
  output   logic            divisor_shift,
  output   logic            divisor_dir,
  output   logic            divisor_load,
  output   logic            quotient_shift,
  output   logic            quotient_dir,
  output   logic            quotient_load,
  output   logic            quotient_val,
  output   logic            remainder_write,
  output   alu_op_sel_str   alu_op,
  output   logic            mux_rem_sel,
  output   logic            ready
);

/* current state and next state */ 
fsm_divider_state_e  current_st;	
fsm_divider_state_e  nxt_state;

always_comb begin: cto_comb_in
   case(current_st)
   START_FSM_DIV: begin
     if(start == 1'b1)
        nxt_state = INIT_DIV;
     else
        nxt_state = START_FSM_DIV;
   end
   INIT_DIV: begin /* start of division */
        nxt_state = IDLE_DIV;
   end
   IDLE_DIV: begin
    if(divisor_load_flag == 1'b1)
        nxt_state = REMAINDER_CALC;
    else
        nxt_state = IDLE_DIV;
   end
   REMAINDER_CALC: begin /* substract divisor register from the remainder */
      nxt_state = TEST_REMAINDER;
   end
   TEST_REMAINDER: begin  /* verify remainder sign*/
      if(rem_sign == 1'b0) //positive remainder
        nxt_state = POSITIVE_REMAINDER;
      else                 //negative remainder
        nxt_state = NEGATIVE_REMAINDER;
   end
   POSITIVE_REMAINDER: begin /* shift quotient reg to the left, new bit 1 */
      nxt_state = SHIFT_DIVISOR;
   end
   NEGATIVE_REMAINDER: begin /* add divisor to remainder and place in remainder */
      nxt_state = SHIFT_DIVISOR;
   end
   SHIFT_DIVISOR: begin /* shift divisor register to the right 1 bit */
      nxt_state = ITERATION_VER;
   end
   ITERATION_VER: begin /* verify iteration is less than DW/2: 8 */
    if(counter_ovf == 1'b1)
          nxt_state = END_DIV;
    else
          nxt_state = REMAINDER_CALC;
   end
   END_DIV: begin /* end of division */
     if(start == 1'b1)
        nxt_state = START_FSM_DIV;
     else
        nxt_state = END_DIV;
   end
   default: begin
     nxt_state = START_FSM_DIV;
   end
   endcase
end

// Registros or flop array
always_ff@(posedge clk or negedge rst) begin // Circuito Secuenicial en un proceso always.
   if (!rst)
      current_st <= START_FSM_DIV;
   else
      current_st <= nxt_state;
end 

always_comb begin: cto
   case(current_st)
   START_FSM_DIV: begin
      alu_op = NO_OP;  
      remainder_write = 1'b1; //don't write
      divisor_shift = 1'b0;   //don't shift divisor reg
      divisor_dir = 1'b0;     //right
      quotient_shift = 1'b0;  //don't shift quotient reg
      quotient_dir = 1'b0;    //right 
      mux_rem_sel = 1'b0;   // dividend data
      counter_enb = 1'b0;
      quotient_val = 1'b0;
      ready = 1'b0;
      if(start == 1'b1) begin
        quotient_load = 1'b0;   //load initial values
        divisor_load = 1'b1;    //load initial values
      end
      else begin
        quotient_load = 1'b0;   //don't load initial values
        divisor_load = 1'b0;    //don't load initial values
      end
   end
   INIT_DIV: begin /* start of division */
      alu_op = NO_OP;
      divisor_shift = 1'b0;   //don't shift divisor reg
      divisor_dir = 1'b0;     //right
      divisor_load = 1'b0;    //load initial values
      quotient_shift = 1'b0;  //don't shift quotient reg
      quotient_dir = 1'b0;    //right 
      quotient_load = 1'b0;   //load initial values
      mux_rem_sel = 1'b0;   // dividend data
      remainder_write = 1'b0; // write dividend data
      counter_enb = 1'b0;
      quotient_val = 1'b0;
      ready = 1'b0;
   end
   IDLE_DIV: begin
      alu_op = NO_OP;
      divisor_shift = 1'b0;   //don't shift divisor reg
      divisor_dir = 1'b0;     //right
      divisor_load = 1'b0;    //load initial values
      quotient_shift = 1'b0;  //don't shift quotient reg
      quotient_dir = 1'b0;    //right 
      quotient_load = 1'b0;   //load initial values
      mux_rem_sel = 1'b0;   // dividend data
      remainder_write = 1'b0; // write dividend data
      counter_enb = 1'b0;
      quotient_val = 1'b0;
      ready = 1'b0;
   end
   REMAINDER_CALC: begin /* substract divisor register from the remainder */
      alu_op = SUBSTRACTION;
      divisor_shift = 1'b0;   //don't shift divisor reg
      divisor_dir = 1'b0;     //right
      divisor_load = 1'b0;    //don't load initial values
      quotient_shift = 1'b0;  //don't shift quotient reg
      quotient_dir = 1'b1;    //left
      quotient_load = 1'b0;   //don't load initial values
      mux_rem_sel = 1'b1;   // alu result
      remainder_write = 1'b1; //write calc in remainder register (alu_result)
      counter_enb = 1'b0;
      quotient_val = 1'b0;
      ready = 1'b0;
   end
   TEST_REMAINDER: begin  /* verify remainder sign*/
      alu_op = NO_OP;
      remainder_write = 1'b0;
      divisor_shift = 1'b0;
      divisor_dir = 1'b0;     //right
      divisor_load = 1'b0;    //load initial values
      quotient_shift = 1'b0;  //don't shift quotient reg
      quotient_dir = 1'b1;    //left
      quotient_load = 1'b0;   //load initial values
      mux_rem_sel = 1'b1;   // alu result
      counter_enb = 1'b0;
      quotient_val = 1'b0;
      ready = 1'b0;
   end
   POSITIVE_REMAINDER: begin /* shift quotient reg to the left, new bit 1 */
      alu_op = NO_OP;
      remainder_write = 1'b0;
      divisor_shift = 1'b0;
      divisor_dir = 1'b0;     //right
      divisor_load = 1'b0;    //load initial values
      quotient_shift = 1'b1;  //don't shift quotient reg
      quotient_dir = 1'b1;    //left 
      quotient_load = 1'b0;   //load initial values
      mux_rem_sel = 1'b1;   // alu result
      counter_enb = 1'b0;
      quotient_val = 1'b1;
      ready = 1'b0;
   end
   NEGATIVE_REMAINDER: begin /* add divisor to remainder and place in remainder */
      alu_op = ADDITION;
      remainder_write = 1'b1;
      divisor_shift = 1'b0;
      divisor_dir = 1'b0;     //right
      divisor_load = 1'b0;    //load initial values
      quotient_shift = 1'b1;  //don't shift quotient reg
      quotient_dir = 1'b1;    //left 
      quotient_load = 1'b0;   //load initial values
      mux_rem_sel = 1'b1;   // alu result
      counter_enb = 1'b0;
      quotient_val = 1'b0;
      ready = 1'b0;
   end
   SHIFT_DIVISOR: begin /* shift divisor register to the right 1 bit */
      alu_op = NO_OP;
      remainder_write = 1'b0;
      divisor_shift = 1'b1;
      divisor_dir = 1'b0;     //right
      divisor_load = 1'b0;    //load initial values
      quotient_shift = 1'b0;  //don't shift quotient reg
      quotient_dir = 1'b1;    //left
      quotient_load = 1'b0;   //load initial values
      mux_rem_sel = 1'b1;   // alu result
      counter_enb = 1'b0;
      quotient_val = 1'b0;
      ready = 1'b0;
   end
   ITERATION_VER: begin /* verify iteration is less than DW/2: 8 */
      alu_op = AND;
      remainder_write = 1'b0;
      divisor_shift = 1'b0;
      divisor_dir = 1'b0;     //right
      divisor_load = 1'b0;    //load initial values
      quotient_shift = 1'b0;  //don't shift quotient reg
      quotient_dir = 1'b1;    //left 
      quotient_load = 1'b0;   //load initial values
      mux_rem_sel = 1'b1;   // alu result
      quotient_val = 1'b0;
      ready = 1'b0;
      if(counter_ovf == 1'b1)
          counter_enb = 1'b0;
      else
          counter_enb = 1'b1;
   end
   END_DIV: begin /* end of division */
     alu_op = NO_OP;
     remainder_write = 1'b0;
     divisor_shift = 1'b0;
     divisor_dir = 1'b0;     //right
     divisor_load = 1'b0;    //load initial values
     quotient_shift = 1'b0;  //don't shift quotient reg
     quotient_dir = 1'b1;    //left 
     quotient_load = 1'b0;   //load initial values
     mux_rem_sel = 1'b1;   // alu result
     counter_enb = 1'b0;
     quotient_val = 1'b0;
     ready = 1'b1;
   end
   default: begin
      alu_op = NO_OP;
      remainder_write = 1'b0; //don't write
      divisor_shift = 1'b0;   //don't shift divisor reg
      divisor_dir = 1'b0;     //right
      quotient_shift = 1'b0;  //don't shift quotient reg
      quotient_dir = 1'b0;    //right 
      mux_rem_sel = 1'b0;   // dividend data
      quotient_load = 1'b0;   //don't load initial values
      divisor_load = 1'b0;    //don't load initial values
      counter_enb = 1'b0;
      quotient_val = 1'b0;
      ready = 1'b0;
   end
   endcase
end
endmodule
