/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: sequential_multiplier
    Description: sequential_multiplier source file
    Last modification: 14/02/2021
*/

import p02_pkg::*;
module booth
(
	input            	clk,
	input            	rst,
	input  logic        start,
	input  data_n_t 	multiplicand,
	input  data_n_t  	multiplier,
	output data_2n_t 	product,
	output logic     	ready,
	output logic        sign
);

/* Wire declarations */
data_n_t multiplier_c2_w;
data_n_t multiplicand_c2_w;
logic mcand_sign_w;
logic mplier_sign_w;

data_2n1_t  register_Anx_D, register_Anx_Q;
data_2n1_t  register_Snx_D, register_Snx_Q;
data_2n1_t  register_Pnx_D, register_Pnx_Q;
data_2n1_t  alu_res;


data_2n1_t  mux_Pnx_i_a, mux_Pnx_i_bt, mux_Pnx_i_b, mux_Pnx_i_o; selectr_e mux_Pnx_sel;


logic ovf_w;
logic counter_enb_w;
logic ready_w;
logic Pnx_enb;
logic Anx_enb;
logic Snx_enb;

data_2n1_t mux_alu_dataB;
data_n_t mux_aluOP_dataA, mux_aluOP_dataB, mux_aluOP_o;
selectr_e mux_aluOP_sel;

comp2bin
#(
	.DW(p02_pkg::N)
)   multiplier_binary_2_c2
(	
	.i_data(multiplier),
	.o_data(multiplier_c2_w),
	.sign(mplier_sign_w)
);

comp2bin
#(
	.DW(p02_pkg::N)
)   multiplicand_binary_2_c2
(	
	.i_data(multiplicand),
	.o_data(multiplicand_c2_w),
	.sign(mcand_sign_w)
);


assign register_Anx_D = {multiplicand_c2_w, (p02_pkg::N)'('b0)};
pipo_register
#(
	.DW(2*p02_pkg::N+1)
)   
register_Anx
(
	.clk(clk),
	.rst(rst),
	.enb(start || Anx_enb),
	.i_data(register_Anx_D),
	.o_data(register_Anx_Q)
);

assign register_Snx_D = (multiplicand_c2_w > multiplier_c2_w) ? ({multiplicand_c2_w, (p02_pkg::N+1)'('b0)}):({multiplicand_c2_w, (p02_pkg::N+1)'('b0)});//multiplicand_c2_w + multiplier_c2_w;
pipo_register
#(
	.DW(2*p02_pkg::N+1)
)   
register_Snx
(
	.clk(clk),
	.rst(rst),
	.enb(start || Snx_enb),
	.i_data(register_Snx_D),
	.o_data(register_Snx_Q)
);

assign mux_Pnx_i_a = {((p02_pkg::N)-1)'('b0), multiplier_c2_w, 1'b0};
assign mux_Pnx_i_bt = alu_res[(2*p02_pkg::N)];
assign mux_Pnx_i_b =  {alu_res[(2*p02_pkg::N)], alu_res[(2*p02_pkg::N):1]};
assign mux_Pnx_sel = (start == 'd1) ? (OP_A):(OP_B);
mux_2_1
#(
	.DW(2*p02_pkg::N+1)
)
mux_Pnx
(
	.i_a(mux_Pnx_i_a),
	.i_b(mux_Pnx_i_b), 
	.i_sel(mux_Pnx_sel),
	.o_sltd(mux_Pnx_i_o)
);
	
assign register_Pnx_D = mux_Pnx_i_o;
pipo_register
#(
	.DW(2*p02_pkg::N+1)
)
register_Pnx
(
	.clk(clk),
	.rst(rst),
	.enb(start || Pnx_enb),
	.i_data(register_Pnx_D),
	.o_data(register_Pnx_Q)
);


assign mux_aluOP_dataA = (register_Pnx_Q[1:0] == 'd2) ? (data_n_t'(SUBSTRACTION)):(data_n_t'(ADDITION));
assign mux_aluOP_dataB = NO_OP;
assign mux_aluOP_sel = (register_Pnx_Q[1] == register_Pnx_Q[0]) ? (OP_B):(OP_A);
mux_2_1 
mux_aluOP
(
	.i_a(mux_aluOP_dataA),
	.i_b(mux_aluOP_dataB), 
	.i_sel(mux_aluOP_sel), //Assign con un & para checar si son iguales o no
	.o_sltd(mux_aluOP_o)
);

mux_2_1
#(
	.DW(2*p02_pkg::N+1)
)
mux_dataB
(
	.i_a(register_Snx_Q),
	.i_b(register_Anx_Q), 
	.i_sel(mux_aluOP_sel),
	.o_sltd(mux_alu_dataB)
);

arithmetic_logic_unit
#(
	.DW(2*(p02_pkg::N)+1)
)
adder
(
	.A(register_Pnx_Q),
	.B(mux_alu_dataB),
	.op_sel(alu_op_sel_str'(mux_aluOP_o)),
	.result(alu_res)
);

fsm_control_booth control
(
   .clk(clk),
   .rst(rst),
   .start(start),
   .ovf(ovf_w),
   .counter_enb(counter_enb_w),
   .Pnx_enb(Pnx_enb),
   .Anx_enb(Anx_enb),
   .Snx_enb(Snx_enb),
   .ready(ready_w)
);
	
counter_ovf counter
(
	.clk(clk),
	.rst(rst),
	.enb(counter_enb_w),
	.count(),
	.ovf(ovf_w)
);

assign ready = ready_w;
assign sign = mplier_sign_w ^ mcand_sign_w; 

//assign product = (sign)?(~({1'b1 ,register_Pnx_Q[2*(p02_pkg::N-1):1]}) + 1'b1):( register_Pnx_Q[2*(p02_pkg::N):1] );
assign product = register_Pnx_Q[2*(p02_pkg::N):1];

endmodule