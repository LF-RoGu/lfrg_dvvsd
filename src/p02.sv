/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: sequential_multiplier
    Description: sequential_multiplier source file
    Last modification: 30/03/2021
*/

import p02_pkg::*;
`define PLL_SYS
module p02
(
	input               clk,
	input               rst,
	input  logic        start,

	input  op_selectr_e op,

	input  data_n_t     data,
	input  logic		load,
	
	output logic 		loadX,
	output logic		loadY,
	
	output data2display_t 	data2display_hundreds_product,
	output data2display_t 	data2display_tens_product,
	output data2display_t 	data2display_units_product,
	
	output data2display_t 	data2display_hundreds_remainder,
	output data2display_t 	data2display_tens_remainder,
	output data2display_t 	data2display_units_remainder,
	output logic        ready,
	output logic		error
);

/*
 * PLL
 */
logic start_w, rst_w, load_w, op_w;
logic outclk;
`ifndef PLL_SYS
/*
 * PLL
 */

logic locked_pll;
pll_50_2M pll_2M_inst
(
	.refclk   (clk),   //  refclk.clk
	.rst      (!rst),      //   reset.reset
	.outclk_0 (outclk), // outclk0.clk
	.locked   (locked_pll)    //  locked.export
);

 `else
/*
 * CLK_DIVIDER
 */
clk_divider sm_clk_divider
(
	.clk_FPGA(clk),
	.rst(rst),
	.clk(outclk)
);
`endif

/*
 * DBCR
 */
dbcr_top dbcr_start
(
	.clk(clk),
	.rst_n(rst),
	.Din(start),
	.one_shot(start_w)
);
	
dbcr_top dbcr_load
(
	.clk(clk),
	.rst_n(rst),
	.Din(load),
	.one_shot(load_w)
);


data_2n_t    result_w, remainder_w;

mdr_top mdr_top
(
	.clk(clk),
	.rst(rst),
	.start(start),

	.op_sel(op),

	.data(data),
	.load(load),
	
	.loadX(loadX),
	.loadY(loadY),
	
	.result(result_w),
	.remainder(remainder_w),
	.ready(ready),
	.error(error)
);
	
/*
 * BIN2BCD
 */	
 
bin2bcd bin2bcd_product
(
	.binary_data(binary_data_t'(result_w)),
	.data2display_units(data2display_units_product),
	.data2display_tens(data2display_tens_product),
	.data2display_hundreds(data2display_hundreds_product),
	.sign()
);

bin2bcd bin2bcd_remainder
(
	.binary_data(binary_data_t'(remainder_w)),
	.data2display_units(data2display_units_remainder),
	.data2display_tens(data2display_tens_remainder),
	.data2display_hundreds(data2display_hundreds_remainder),
	.sign()
);
endmodule 