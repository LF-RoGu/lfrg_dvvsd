/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: decrease_ovf
    Description: decrease_ovf source file
    Last modification: 14/02/2021
*/

import p02_pkg::*;
module decrease_ovf
(
    input  logic   clk,
    input  logic   rst,
    input  logic   enb,
    output logic   ovf,
    output count_t count
);

count_t count_r, count_nxt;
logic ovf_st;

always_comb begin
    if(count < 'd1) begin
        count_nxt = 2*(p02_pkg::N);
        ovf_st = 1'b1;
    end
    else begin
        count_nxt  = count_r - 'd2;
        ovf_st = 1'b0;
    end
end
// Sequential process
always_ff@(posedge clk, negedge rst)begin: counter
    if (!rst)
        count_r <= 2*(p02_pkg::N);
    else if (enb)
        count_r <= count_nxt;
end:counter

assign count = count_r; 
assign ovf = ovf_st; 

endmodule
