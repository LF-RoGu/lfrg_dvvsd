/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: square_root
    Description: square_root source file
    Last modification: 17/03/2021
*/

import p02_pkg::*;

module square_root
(
	input            	clk,
	input            	rst,
	input  logic        start,
	input  data_n_t 	D,
	output logic     	ready
);

data_n_t sqrt_Rnx_D, sqrt_Rnx_Q;
data_n_t sqrt_Qnx_D, sqrt_Qnx_Q;
data_n_t sqrt_Dnx_D, sqrt_Dnx_Q;
	
data_n_t sqrt_alu_dataA, sqrt_alu_dataB;

data_n_t sqrt_dataA, sqrt_dataB;

data_n_t alu_res_w;
selectr_e alu_op_w;
data_n_t sqrt_C;

data_n_t counter_jx;
logic ovf_w, enb_counter_w;
	
logic enb_Rnx_w, check_Rnx_w, enb_Qnx_w, enb_aluOP_w;

data_n_t mux_Rnx_i_a, mux_Rnx_i_b, mux_Rnx_o; selectr_e mux_Rnx_i_sel;
data_n_t mux_Qnx_i_a, mux_Qnx_i_b, mux_Qnx_o; selectr_e mux_Qnx_i_sel;
data_n_t mux_dataC_i_a, mux_dataC_i_b, mux_dataC_o; selectr_e mux_dataC_i_sel;
data_n_t mux_aluA_i_a, mux_aluA_i_b, mux_aluA_o; selectr_e mux_aluA_i_sel;
data_n_t mux_aluB_i_a, mux_aluB_i_b, mux_aluB_o; selectr_e mux_aluB_i_sel;
data_n_t mux_aluOP_i_a, mux_aluOP_i_b, mux_aluOP_o; selectr_e mux_aluOP_i_sel;
	
	
fsm_control_sqrt
control_system
(
	.clk(clk),
	.rst(rst),
	.start(start),
	.ovf(ovf_w),
	.enb_counter(enb_counter_w),
	.enb_Rnx(enb_Rnx_w),
	.enb_Qnx(enb_Qnx_w),
	.enb_aluOP(enb_aluOP_w),
	.ready(ready)
);

decrease_ovf
decrease_ovf_jx
(
	.clk(clk),
	.rst(rst),
	.enb(enb_counter_w),
	.count(counter_jx),
	.ovf(ovf_w)
);

/** Register D*/
assign sqrt_Dnx_D = D;


assign mux_Rnx_i_a = data_n_t'(alu_res_w);
assign mux_Rnx_i_b = data_n_t'('b0);
assign mux_Rnx_i_sel = (enb_Rnx_w == 1'b1) ? (OP_A):(OP_B);
assign mux_Rnx_o = data_n_t'(sqrt_Rnx_D);
mux_2_1 	
mux_Rnx
(
	.i_a( mux_Rnx_i_a ),
	.i_b( mux_Rnx_i_b ),
	.i_sel( mux_Rnx_i_sel ),
	.o_sltd( mux_Rnx_o )
);


assign mux_dataC_i_a = data_n_t'((sqrt_Qnx_Q << 'd2) | 'd1);
assign mux_dataC_i_b = data_n_t'((sqrt_Qnx_Q << 'd2) | 'd3);
assign mux_dataC_i_sel = (sqrt_Rnx_Q < 'd127) ? (OP_A):(OP_B);
assign mux_dataC_o = data_n_t'(sqrt_C);
mux_2_1 	
mux_dataC
(
	.i_a( mux_dataC_i_a ),
	.i_b( mux_dataC_i_b ),
	.i_sel( mux_dataC_i_sel ),
	.o_sltd( mux_dataC_o )
);

assign sqrt_dataA = (sqrt_Rnx_Q << 'd2);
assign sqrt_dataB = (sqrt_Dnx_Q >> counter_jx) & 'd3;

assign mux_aluA_i_a = data_n_t'(sqrt_dataA | sqrt_dataB);
assign mux_aluA_i_b = data_n_t'(sqrt_Rnx_Q);
assign mux_aluA_i_sel = (ready == 'd0) ? (OP_A):(OP_B);
assign mux_aluA_o = data_n_t'(sqrt_alu_dataA);
mux_2_1 	
mux_alu_dataA
(
	.i_a( mux_aluA_i_a ),
	.i_b( mux_aluA_i_b ),
	.i_sel( mux_aluA_i_sel ),
	.o_sltd( mux_aluA_o )
);
	
assign mux_aluB_i_a = data_n_t'(mux_dataC_o);
assign mux_aluB_i_b = data_n_t'('b0);
assign mux_aluB_i_sel = (ready == 'd0) ? (OP_A):(OP_B);
assign mux_aluB_o = data_n_t'(sqrt_alu_dataB);
mux_2_1 	
mux_alu_dataB
(
	.i_a( mux_aluB_i_a ),
	.i_b( mux_aluB_i_b ),
	.i_sel( mux_aluB_i_sel ),
	.o_sltd( mux_aluB_o )
);
//enb_Rnx_w
assign mux_aluOP_i_a = data_n_t'((sqrt_Rnx_Q < 'd127) ? (SUBSTRACTION):(ADDITION));
assign mux_aluOP_i_b = data_n_t'(ADDITION);
assign mux_aluOP_i_sel = (enb_aluOP_w == 'd1) ? (OP_A):(OP_B);
assign mux_aluOP_o = data_n_t'(alu_op_w);
mux_2_1 
mux_alu_op
(
	.i_a( mux_aluOP_i_a ),
	.i_b( mux_aluOP_i_b ),
	.i_sel( mux_aluOP_i_sel ),
	.o_sltd( mux_aluOP_o )
);
/** MODULO EXTERNO **/
arithmetic_logic_unit
#(
	.DW(2*(p02_pkg::N) + 2) 
)
adder
(
	.A({'b0, mux_aluA_o}),
	.B({'b0, mux_aluB_o}),
	.op_sel( alu_op_sel_str'(mux_aluOP_o) ),
	.result(alu_res_w)
);
/** MODULO EXTERNO **/

//assign mux_Qnx_i_a = data_n_t'((sqrt_Qnx_Q << 'd1) | 'd1);
assign mux_Qnx_i_a = data_n_t'( (counter_jx < 2*(p02_pkg::N)) ? ((sqrt_Qnx_Q << 'd1) | 'd1):('b0) );
assign mux_Qnx_i_b = data_n_t'((sqrt_Qnx_Q << 'd1) | 'd0) ;
assign mux_Qnx_i_sel = (mux_Rnx_o < 'd127) ? (OP_A):(OP_B);
assign mux_Qnx_o = data_n_t'(sqrt_Qnx_D);
mux_2_1 
mux_Qnx
(
	.i_a( mux_Qnx_i_a ),
	.i_b( mux_Qnx_i_b ),
	.i_sel( mux_Qnx_i_sel ),
	.o_sltd( mux_Qnx_o )
);
/** MODULO EXTERNO **/
pipo_register
register_D
(
	.clk(clk),
	.rst(rst),
	.enb(start),
	.i_data(sqrt_Dnx_D),
	.o_data(sqrt_Dnx_Q)
);
/** MODULO EXTERNO **/

/** MODULO EXTERNO **/

pipo_register
register_Rnx
(
	.clk(clk),
	.rst(rst),
	.enb(enb_Rnx_w || start),
	.i_data(( ready && (sqrt_Rnx_Q > 'd128) ) ? (sqrt_Rnx_Q + ((sqrt_Qnx_Q >> 1'b1) | 1'b1) ):(mux_Rnx_o)),
	.o_data(sqrt_Rnx_Q)
);
/** MODULO EXTERNO **/

/** MODULO EXTERNO **/
pipo_register
register_Qnx
(
	.clk(clk),
	.rst(rst),
	.enb(enb_Qnx_w || start),
	.i_data(mux_Qnx_o),
	.o_data(sqrt_Qnx_Q)
);
/** MODULO EXTERNO **/

endmodule 