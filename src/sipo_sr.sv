 /*
	Coded by: César Villarreal
				 Luis Fernando Rodriguez
	Date: September 22, 2020
	Description: PIPO register to hold the information and release after the one_shot signal is received
 */
 
 module sipo_register
 #(
	parameter DW = 4
 ) 
 (
	input 	clk,
	input 	rst,
	input   logic dir,
	input   logic enable,
	input	logic data_in,			
	output	logic [DW-1:0] data_out
 );
 
logic [DW-1:0] rgstr_r;
logic [DW-1:0] rgstr_next;

always_comb begin
		if(dir == 1'b0)
			rgstr_next = {data_in,rgstr_r[DW-1:1]}; //shift-right
		else
			rgstr_next = {rgstr_r[DW-2:0], data_in}; //shift-left
end
 
always_ff@(posedge clk or negedge rst) begin: rgstr_label
    if(!rst)
        rgstr_r  <= '0;
    else if (enable)
        rgstr_r  <= rgstr_next;
	else
		rgstr_r  <= rgstr_r;
end:rgstr_label

assign data_out  = rgstr_r;

endmodule