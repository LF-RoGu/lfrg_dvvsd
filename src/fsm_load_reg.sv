/* 
    Author: César Villarreal @cv4497
				Luis Fernando Rodriguez @LF-RoGu
    Title: 
    Description: FSM control
    Last modification: 12/09/2020

    TODO: 
*/

module fsm_load_reg
import fsm_pkg::*;
(
  input                		    clk,
  input        	      		    rst,
  input    logic              load,
  input    logic              ovf,
  output   logic              counter_enb,
  output   logic              reg_enb,
  output   logic              load_flag
);

/* wires */ 
fsm_load_state_e  current_st;	
fsm_load_state_e  nxt_state;

// Circuito combinacional entrada, define ssiguiente estado
always_comb begin: cto_comb_in
   case(current_st)
   INIT_FSM: begin
     nxt_state = START_REG;
   end
   START_REG: begin
    if(load == 1'b1)
        nxt_state = LOAD;
    else
        nxt_state = START_REG;
   end
   LOAD: begin
    if(ovf == 1'b1)
        nxt_state = FINISH_REG;
    else
        nxt_state = LOAD;
   end
   FINISH_REG: begin
     nxt_state = START_REG;
   end
   default: begin
     nxt_state = START_REG;
   end
   endcase
end

// Registros or flop array
always_ff@(posedge clk or negedge rst) begin // Circuito Secuenicial en un proceso always.
   if (!rst)
      current_st <= START_REG;
   else
      current_st <= nxt_state;
end 

always_comb begin: cto
   case(current_st)
   INIT_FSM: begin
     counter_enb = 1'b0;
     reg_enb = 1'b0; 
     load_flag = 1'b0;
   end
   START_REG: begin
     counter_enb = 1'b0;
     reg_enb = 1'b0; 
     load_flag = 1'b0;
   end
   LOAD: begin
     counter_enb = 1'b1;
     reg_enb = 1'b1; 
     load_flag = 1'b0;
   end
   FINISH_REG: begin
     counter_enb = 1'b0;
     reg_enb = 1'b0; 
     load_flag = 1'b1;
   end
   default: begin
     counter_enb = 1'b0;
     reg_enb = 1'b0; 
     load_flag = 1'b0;
   end
   endcase
end
endmodule
