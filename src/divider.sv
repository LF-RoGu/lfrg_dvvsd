

module seq_divider
import fsm_pkg::*, p02_pkg::*;
(
   input                      clk,
   input                      rst,
   input     logic            start,
   input     data_n_t dividend,  
   input     data_n_t  divisor,
   input     data_2n1_t       alu_result,
   output    data_2n1_t       alu_a,
   output    data_2n1_t       alu_b,
   output    alu_op_sel_str   alu_op,   
   output    data_2n1_t   quotient,
   output    data_2n1_t  remainder,
   output    logic            ready
);

data_2n_t divisor_data_out_w;   //16-bit
data_2n1_t quotient_data_out_w;   //16-bit
data_2n1_t remainder_data_out_w;   //16-bit
data_2n_t data_mux_remainder_w;
logic quotient_val_w;

logic ovf_counter_fsm_w;
logic enb_fsm_counter_w;

logic divisor_shift_w;
logic divisor_dir_w;
logic divisor_load_w;
logic quotient_shift_w;
logic quotient_dir_w;
logic quotient_load_w;
logic remainder_write_w;
alu_op_sel_str alu_op_w;

logic load_end_divisor_w;
logic load_end_quotient_w;
logic mux_rem_sel_w;
logic ready_w;

sipo_reg_load #(
    .DW((p02_pkg::N)*2) //32-BIT
)   divisor_reg
(
    .clk(clk),
    .rst(rst),
    .dir(divisor_dir_w),
    .load(divisor_load_w),
    .enable(divisor_shift_w),
    .load_flag(load_end_divisor_w),
    .data_in({divisor,(p02_pkg::N)'(10'd0)}),
    .data_out(divisor_data_out_w)
);

sipo_reg_load #(
    .DW((p02_pkg::N)) //16-BIT
)   quotient_reg
(
    .clk(clk),
    .rst(rst),
    .dir(quotient_dir_w),
    .load(quotient_load_w),
    .enable(quotient_shift_w),
    .load_flag(load_end_quotient_w),
    .data_in({9'd0,quotient_val_w}),
    .data_out(quotient_data_out_w)
);

pipo_register #(
    .DW((p02_pkg::N)*2) //32-BIT
)    reimainder_reg
(
    .clk(clk),
    .rst(rst),
    .enb(remainder_write_w),
    .i_data(data_mux_remainder_w),
    .o_data(remainder_data_out_w)
);

mux_2_1
#(
    .DW((p02_pkg::N)*2)  //32-BIT
)   mux_remainder
(
    .i_a({(p02_pkg::N)'(10'd0), dividend}),
	.i_b(alu_result),
	.i_sel(selectr_e'(mux_rem_sel_w)),
	.o_sltd(data_mux_remainder_w)
);


fsm_divider div_control_fsm
(
    .clk(clk),
    .rst(rst),
    .start(start),
    .counter_ovf(ovf_counter_fsm_w),
    .rem_sign(remainder_data_out_w[((p02_pkg::N)*2)-1]),
    .divisor_load_flag(load_end_divisor_w),
    .quotient_load_flag(load_end_quotient_w),
    .counter_enb(enb_fsm_counter_w),
    .divisor_shift(divisor_shift_w),
    .divisor_dir(divisor_dir_w),
    .divisor_load(divisor_load_w),
    .quotient_shift(quotient_shift_w),
    .quotient_dir(quotient_dir_w),
    .quotient_load(quotient_load_w),
    .quotient_val(quotient_val_w),
    .remainder_write(remainder_write_w),
    .alu_op(alu_op_w),
    .mux_rem_sel(mux_rem_sel_w),
    .ready(ready_w)
);

bin_counter #(
    .DW((p02_pkg::N)+1),
    .OVF_VAL((p02_pkg::N)+1)
)   it_count
(
    .clk(clk),
    .rst(rst),
    .enb(enb_fsm_counter_w),
    .ovf(ovf_counter_fsm_w),
    .count() //N.C.
);

assign alu_a = data_2n1_t'(remainder_data_out_w);
assign alu_b = data_2n1_t'(divisor_data_out_w);
assign alu_op = alu_op_w;
assign quotient = quotient_data_out_w;
assign remainder = remainder_data_out_w;
assign ready = ready_w;

endmodule
