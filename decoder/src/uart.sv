import data_pkg::*;

module uart
(
	input bit clk,
	input logic rst,
	input logic transmit,			//button start tx
	
	input  data_n_t data_tx, //parallel data from uart2uart
	output data_n_t data_rx, //parallel data from uart2uart
	
	output rx_interrupt,
	output parity_error
);

logic serial_tx2rx;
	
logic cts;
logic parity_bit;
	
logic o_clk_tx;
clk_divider
#(
	.FREQUENCY(uartTxBR),
	.REFERENCE_CLOCK(uartRefClk)
)
uartTx_clk_divider
(
	.clk_FPGA(clk),
	.rst(rst),
	.clk(o_clk_tx)
);

uart_tx
uart_tx_top
(
	.clk(o_clk_tx),
	.rst(rst),
	.data(data_tx),
	
	.transmit(transmit),
	.cts(cts),
	
	.serial_tx(serial_tx2rx)
);

logic o_clk_rx;
clk_divider
#(
	.FREQUENCY(uartRxBR),
	.REFERENCE_CLOCK(uartRefClk)
)
uartRx_clk_divider
(
	.clk_FPGA(clk),
	.rst(rst),
	.clk(o_clk_rx)
);
uart_rx
uart_rx_top
(
	.clk(o_clk_tx),
	.clk_t(clk),
	.rst(rst),
	
	.serial_rx(serial_tx2rx),

	.cts(cts),
	
	.rts(rx_interrupt),
	
	.pairty_error(parity_error),
	.data(data_rx)
);


endmodule 
