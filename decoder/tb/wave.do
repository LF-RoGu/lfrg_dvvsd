onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /uart_tb/uut/uartTx_clk_divider/clk_FPGA
add wave -noupdate /uart_tb/uut/uartTx_clk_divider/clk
add wave -noupdate /uart_tb/rst
add wave -noupdate /uart_tb/transmit
add wave -noupdate -radix binary /uart_tb/data_rx
add wave -noupdate /uart_tb/data_tx
add wave -noupdate -divider {UART TX}
add wave -noupdate /uart_tb/uut/uartTx_clk_divider/clk
add wave -noupdate /uart_tb/uut/uart_tx_top/cts
add wave -noupdate -group uart_tx_bin_counter /uart_tb/uut/uart_tx_top/bin_counter_uartTX/clk
add wave -noupdate -group uart_tx_bin_counter /uart_tb/uut/uart_tx_top/bin_counter_uartTX/rst
add wave -noupdate -group uart_tx_bin_counter /uart_tb/uut/uart_tx_top/bin_counter_uartTX/enb
add wave -noupdate -group uart_tx_bin_counter /uart_tb/uut/uart_tx_top/bin_counter_uartTX/ovf
add wave -noupdate -group uart_tx_bin_counter /uart_tb/uut/uart_tx_top/bin_counter_uartTX/count
add wave -noupdate -group uart_tx_bin_counter /uart_tb/uut/uart_tx_top/bin_counter_uartTX/count_r
add wave -noupdate -group uart_tx_bin_counter /uart_tb/uut/uart_tx_top/bin_counter_uartTX/count_nxt
add wave -noupdate -group uart_tx_bin_counter /uart_tb/uut/uart_tx_top/bin_counter_uartTX/ovf_st
add wave -noupdate -group uart_tx_fsm_cntrl /uart_tb/uut/uart_tx_top/uart_tx_fsm/clk
add wave -noupdate -group uart_tx_fsm_cntrl /uart_tb/uut/uart_tx_top/uart_tx_fsm/rst
add wave -noupdate -group uart_tx_fsm_cntrl /uart_tb/uut/uart_tx_top/uart_tx_fsm/transmit
add wave -noupdate -group uart_tx_fsm_cntrl /uart_tb/uut/uart_tx_top/uart_tx_fsm/cts
add wave -noupdate -group uart_tx_fsm_cntrl /uart_tb/uut/uart_tx_top/uart_tx_fsm/ovf
add wave -noupdate -group uart_tx_fsm_cntrl /uart_tb/uut/uart_tx_top/uart_tx_fsm/l_s
add wave -noupdate -group uart_tx_fsm_cntrl /uart_tb/uut/uart_tx_top/uart_tx_fsm/cntr_enb
add wave -noupdate -group uart_tx_fsm_cntrl /uart_tb/uut/uart_tx_top/uart_tx_fsm/reg_enb
add wave -noupdate -group uart_tx_fsm_cntrl /uart_tb/uut/uart_tx_top/uart_tx_fsm/mux_sel
add wave -noupdate -group uart_tx_fsm_cntrl /uart_tb/uut/uart_tx_top/uart_tx_fsm/tx_active
add wave -noupdate -group uart_tx_fsm_cntrl /uart_tb/uut/uart_tx_top/uart_tx_fsm/current_st
add wave -noupdate -group uart_tx_fsm_cntrl /uart_tb/uut/uart_tx_top/uart_tx_fsm/nxt_state
add wave -noupdate -group uart_tx_piso_reg /uart_tb/uut/uart_tx_top/piso_register_lsb_uartTX/clk
add wave -noupdate -group uart_tx_piso_reg /uart_tb/uut/uart_tx_top/piso_register_lsb_uartTX/rst
add wave -noupdate -group uart_tx_piso_reg /uart_tb/uut/uart_tx_top/piso_register_lsb_uartTX/enb
add wave -noupdate -group uart_tx_piso_reg /uart_tb/uut/uart_tx_top/piso_register_lsb_uartTX/l_s
add wave -noupdate -group uart_tx_piso_reg /uart_tb/uut/uart_tx_top/piso_register_lsb_uartTX/i_data
add wave -noupdate -group uart_tx_piso_reg /uart_tb/uut/uart_tx_top/piso_register_lsb_uartTX/o_data
add wave -noupdate -group uart_tx_piso_reg /uart_tb/uut/uart_tx_top/piso_register_lsb_uartTX/rgstr_r
add wave -noupdate -group uart_tx_piso_reg /uart_tb/uut/uart_tx_top/piso_register_lsb_uartTX/rgstr_next
add wave -noupdate -divider {UART RX}
add wave -noupdate /uart_tb/uut/uartRx_clk_divider/clk
add wave -noupdate /uart_tb/uut/uart_rx_top/cts
add wave -noupdate /uart_tb/uut/uart_rx_top/rts
add wave -noupdate -group uart_rx_bin_counter /uart_tb/uut/uart_rx_top/bin_counter_uartRX/clk
add wave -noupdate -group uart_rx_bin_counter /uart_tb/uut/uart_rx_top/bin_counter_uartRX/rst
add wave -noupdate -group uart_rx_bin_counter /uart_tb/uut/uart_rx_top/bin_counter_uartRX/enb
add wave -noupdate -group uart_rx_bin_counter /uart_tb/uut/uart_rx_top/bin_counter_uartRX/ovf
add wave -noupdate -group uart_rx_bin_counter -radix unsigned /uart_tb/uut/uart_rx_top/bin_counter_uartRX/count
add wave -noupdate -group uart_rx_bin_counter -radix unsigned /uart_tb/uut/uart_rx_top/bin_counter_uartRX/count_r
add wave -noupdate -group uart_rx_bin_counter -radix unsigned /uart_tb/uut/uart_rx_top/bin_counter_uartRX/cnt_nxt
add wave -noupdate -group uart_rx_bin_counter -radix unsigned /uart_tb/uut/uart_rx_top/bin_counter_uartRX/ovf_t
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/clk
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/rst
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/ovf
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/data_in
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/start_cntr_ovf
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/rts
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/enable
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/bin_cntr_enb
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/start_cntr_enable
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/current_st
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/nxt_state
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/data_cntr_ovf
add wave -noupdate -group uart_rx_fsm_cntrl /uart_tb/uut/uart_rx_top/fsm_cntrl_rx_top/data_cntr_enable
add wave -noupdate -group uart_rx_sipo_reg /uart_tb/uut/uart_rx_top/sipo_register_msb_uartRX/clk
add wave -noupdate -group uart_rx_sipo_reg /uart_tb/uut/uart_rx_top/sipo_register_msb_uartRX/rst
add wave -noupdate -group uart_rx_sipo_reg /uart_tb/uut/uart_rx_top/sipo_register_msb_uartRX/enb
add wave -noupdate -group uart_rx_sipo_reg /uart_tb/uut/uart_rx_top/sipo_register_msb_uartRX/data_in
add wave -noupdate -group uart_rx_sipo_reg /uart_tb/uut/uart_rx_top/sipo_register_msb_uartRX/data_out
add wave -noupdate -group uart_rx_start_counter /uart_tb/uut/uart_rx_top/start_counter_uartRx/clk
add wave -noupdate -group uart_rx_start_counter /uart_tb/uut/uart_rx_top/start_counter_uartRx/rst
add wave -noupdate -group uart_rx_start_counter /uart_tb/uut/uart_rx_top/start_counter_uartRx/enb
add wave -noupdate -group uart_rx_start_counter /uart_tb/uut/uart_rx_top/start_counter_uartRx/ovf
add wave -noupdate -group uart_rx_start_counter -radix decimal /uart_tb/uut/uart_rx_top/start_counter_uartRx/count
add wave -noupdate -group uart_rx_start_counter -radix decimal /uart_tb/uut/uart_rx_top/start_counter_uartRx/count_r
add wave -noupdate -group uart_rx_start_counter -radix decimal /uart_tb/uut/uart_rx_top/start_counter_uartRx/cnt_nxt
add wave -noupdate -group uart_rx_start_counter /uart_tb/uut/uart_rx_top/start_counter_uartRx/ovf_t
add wave -noupdate -group uart_rx_data_counter /uart_tb/uut/uart_rx_top/data_counter_uartRx/clk
add wave -noupdate -group uart_rx_data_counter /uart_tb/uut/uart_rx_top/data_counter_uartRx/rst
add wave -noupdate -group uart_rx_data_counter /uart_tb/uut/uart_rx_top/data_counter_uartRx/enb
add wave -noupdate -group uart_rx_data_counter /uart_tb/uut/uart_rx_top/data_counter_uartRx/ovf
add wave -noupdate -group uart_rx_data_counter /uart_tb/uut/uart_rx_top/data_counter_uartRx/count
add wave -noupdate -group uart_rx_data_counter /uart_tb/uut/uart_rx_top/data_counter_uartRx/cnt_nxt
add wave -noupdate -divider {SERIAL DATA}
add wave -noupdate /uart_tb/uut/uart_tx_top/serial_tx
add wave -noupdate /uart_tb/uut/uart_rx_top/serial_rx
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {549393 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 409
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {787500 ps}
