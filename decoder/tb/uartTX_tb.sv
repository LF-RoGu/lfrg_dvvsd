`timescale 1ns / 1ps
module uartTX_tb();
import data_pkg::*;

localparam PERIOD = 2;

logic clk;
logic rst;
logic transmit;
data_n_t data_rx;
logic data_tx;

uart_tx_top uut
(
    .clk(clk),
    .rst(rst),
    .transmit(transmit),
	
	.data_rx(data_rx),
    .data_tx(data_tx)
);

initial begin
	clk = 1'd0;
	rst = 1'd1;
	transmit = 1'b0;
	
	#PERIOD rst = 1'b1;
	#PERIOD rst = 1'b0;
	#PERIOD rst = 1'b1;
	
	#PERIOD; #PERIOD; #PERIOD
	
	data2send('d156);
	//data2send('d55);
	//data2send('d25);
	//data2send('d2);
	//data2send('d47);
end

always begin
    #(PERIOD/2) clk<= ~clk;
end

task data2send(data_n_t data);
	#PERIOD data_rx = data;
	#PERIOD transmit = 1'b0;
	#PERIOD transmit = 1'b1; #PERIOD #PERIOD transmit = 1'b1; #PERIOD #PERIOD transmit = 1'b1; #PERIOD
	#PERIOD transmit = 1'b0;
	
	delay(50);
	
endtask

//delay
task delay(int dly = 1);
    repeat(dly)
        @(posedge clk);
endtask
endmodule
