onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /uartTX_tb/uut/clk
add wave -noupdate /uartTX_tb/uut/rst
add wave -noupdate /uartTX_tb/uut/transmit
add wave -noupdate -divider {UART TX}
add wave -noupdate /uartTX_tb/uut/uart_tx_top/clk
add wave -noupdate /uartTX_tb/uut/data_rx
add wave -noupdate /uartTX_tb/uut/data_tx
add wave -noupdate -group uart_tx_bin_counter /uartTX_tb/uut/uart_tx_top/bin_counter_uartTX/clk
add wave -noupdate -group uart_tx_bin_counter /uartTX_tb/uut/uart_tx_top/bin_counter_uartTX/rst
add wave -noupdate -group uart_tx_bin_counter /uartTX_tb/uut/uart_tx_top/bin_counter_uartTX/enb
add wave -noupdate -group uart_tx_bin_counter /uartTX_tb/uut/uart_tx_top/bin_counter_uartTX/ovf
add wave -noupdate -group uart_tx_bin_counter /uartTX_tb/uut/uart_tx_top/bin_counter_uartTX/count
add wave -noupdate -expand -group uart_tx_fsm_cntrl /uartTX_tb/uut/uart_tx_top/uart_tx_fsm/transmit
add wave -noupdate -expand -group uart_tx_fsm_cntrl /uartTX_tb/uut/uart_tx_top/uart_tx_fsm/ovf
add wave -noupdate -expand -group uart_tx_fsm_cntrl /uartTX_tb/uut/uart_tx_top/uart_tx_fsm/l_s
add wave -noupdate -expand -group uart_tx_fsm_cntrl /uartTX_tb/uut/uart_tx_top/uart_tx_fsm/cntr_enb
add wave -noupdate -expand -group uart_tx_fsm_cntrl /uartTX_tb/uut/uart_tx_top/uart_tx_fsm/reg_enb
add wave -noupdate -expand -group uart_tx_fsm_cntrl /uartTX_tb/uut/uart_tx_top/uart_tx_fsm/mux_sel
add wave -noupdate -expand -group uart_tx_fsm_cntrl /uartTX_tb/uut/uart_tx_top/uart_tx_fsm/cts
add wave -noupdate -expand -group uart_tx_fsm_cntrl /uartTX_tb/uut/uart_tx_top/uart_tx_fsm/tx_active
add wave -noupdate -expand -group uart_tx_fsm_cntrl /uartTX_tb/uut/uart_tx_top/uart_tx_fsm/current_st
add wave -noupdate -expand -group uart_tx_fsm_cntrl /uartTX_tb/uut/uart_tx_top/uart_tx_fsm/nxt_state
add wave -noupdate -expand -group uart_tx_pipo_reg /uartTX_tb/uut/uart_tx_top/pipo_register_uartTX/enb
add wave -noupdate -expand -group uart_tx_pipo_reg /uartTX_tb/uut/uart_tx_top/pipo_register_uartTX/i_data
add wave -noupdate -expand -group uart_tx_pipo_reg /uartTX_tb/uut/uart_tx_top/pipo_register_uartTX/o_data
add wave -noupdate -expand -group uart_tx_piso_reg /uartTX_tb/uut/uart_tx_top/piso_register_lsb_uartTX/enb
add wave -noupdate -expand -group uart_tx_piso_reg /uartTX_tb/uut/uart_tx_top/piso_register_lsb_uartTX/l_s
add wave -noupdate -expand -group uart_tx_piso_reg /uartTX_tb/uut/uart_tx_top/piso_register_lsb_uartTX/i_data
add wave -noupdate -expand -group uart_tx_piso_reg /uartTX_tb/uut/uart_tx_top/piso_register_lsb_uartTX/o_data
add wave -noupdate -expand -group uart_tx_piso_reg /uartTX_tb/uut/uart_tx_top/piso_register_lsb_uartTX/rgstr_r
add wave -noupdate -expand -group uart_tx_piso_reg /uartTX_tb/uut/uart_tx_top/piso_register_lsb_uartTX/rgstr_next
add wave -noupdate -divider {SERIAL DATA}
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {50555 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 409
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {3150 ns}
