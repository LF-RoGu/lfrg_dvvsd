onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group {reg A} /tb_examen/uut_dut/register_firA/clk
add wave -noupdate -expand -group {reg A} /tb_examen/uut_dut/register_firA/rst
add wave -noupdate -expand -group {reg A} /tb_examen/uut_dut/register_firA/enb
add wave -noupdate -expand -group {reg A} /tb_examen/uut_dut/register_firA/i_data
add wave -noupdate -expand -group {reg A} /tb_examen/uut_dut/register_firA/o_data
add wave -noupdate -expand -group {reg A} /tb_examen/uut_dut/register_firA/rgstr_r
add wave -noupdate -expand -group {reg B} /tb_examen/uut_dut/register_firB/clk
add wave -noupdate -expand -group {reg B} /tb_examen/uut_dut/register_firB/rst
add wave -noupdate -expand -group {reg B} /tb_examen/uut_dut/register_firB/enb
add wave -noupdate -expand -group {reg B} /tb_examen/uut_dut/register_firB/i_data
add wave -noupdate -expand -group {reg B} /tb_examen/uut_dut/register_firB/o_data
add wave -noupdate -expand -group {reg B} /tb_examen/uut_dut/register_firB/rgstr_r
add wave -noupdate -expand -group {reg C} /tb_examen/uut_dut/register_firC/clk
add wave -noupdate -expand -group {reg C} /tb_examen/uut_dut/register_firC/rst
add wave -noupdate -expand -group {reg C} /tb_examen/uut_dut/register_firC/enb
add wave -noupdate -expand -group {reg C} /tb_examen/uut_dut/register_firC/i_data
add wave -noupdate -expand -group {reg C} /tb_examen/uut_dut/register_firC/o_data
add wave -noupdate -expand -group {reg C} /tb_examen/uut_dut/register_firC/rgstr_r
add wave -noupdate -expand -group {reg D} /tb_examen/uut_dut/register_firD/clk
add wave -noupdate -expand -group {reg D} /tb_examen/uut_dut/register_firD/rst
add wave -noupdate -expand -group {reg D} /tb_examen/uut_dut/register_firD/enb
add wave -noupdate -expand -group {reg D} /tb_examen/uut_dut/register_firD/i_data
add wave -noupdate -expand -group {reg D} /tb_examen/uut_dut/register_firD/o_data
add wave -noupdate -expand -group {reg D} /tb_examen/uut_dut/register_firD/rgstr_r
add wave -noupdate /tb_examen/uut_dut/y
add wave -noupdate -expand -group counter /tb_examen/uut_dut/counter/clk
add wave -noupdate -expand -group counter /tb_examen/uut_dut/counter/rst
add wave -noupdate -expand -group counter /tb_examen/uut_dut/counter/enb
add wave -noupdate -expand -group counter /tb_examen/uut_dut/counter/ovf
add wave -noupdate -expand -group counter /tb_examen/uut_dut/counter/count
add wave -noupdate -expand -group counter /tb_examen/uut_dut/counter/count_r
add wave -noupdate -expand -group counter /tb_examen/uut_dut/counter/count_nxt
add wave -noupdate -expand -group counter /tb_examen/uut_dut/counter/ovf_st
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {35974 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 349
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {1535 ps} {95090 ps}
