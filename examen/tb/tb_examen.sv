`timescale 1ns / 1ps

import fir_pkg::*;
module tb_examen();

localparam PERIOD = 2;

logic       clk;
logic       rst;
logic       start;
logic       ready;
	
logic 		x;
logic x_data[fir_pkg::N] = {0,0,0,0,0,0,0,1,0,0,0,0,0,0};
logic [(fir_pkg::N)+1] 		y;
fir uut_dut
(
	.clk(clk),
	.rst(rst),
	.start(start),
	.x(x),
	.ready(ready),
	.y(y)
);

initial begin
	clk = 0;
	#PERIOD rst = 1;
	#PERIOD rst = 0;	
	#PERIOD rst = 1;
	
	x = 0;

	#PERIOD start = 0;
	#PERIOD start = 1;
	#PERIOD start = 0;
	#PERIOD x = 0;
	#PERIOD x = 0;
	#PERIOD x = 0;
	#PERIOD x = 0;
	#PERIOD x = 0;
	#PERIOD x = 0;
	#PERIOD x = 0;
	#PERIOD x = 1;
	#PERIOD x = 0;
	#PERIOD x = 0;
	#PERIOD x = 0;
	#PERIOD x = 0;
	#PERIOD x = 0;
	#PERIOD x = 0;
	#50;

	$stop;
end

always begin
    #(PERIOD/2) clk<= ~clk;
end

endmodule
