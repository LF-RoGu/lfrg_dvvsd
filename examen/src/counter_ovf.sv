/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: counter_ovf
    Description: counter_ovf source file
    Last modification: 14/02/2021
*/

import fir_pkg::*;
module counter_ovf
(
    input  logic   clk,
    input  logic   rst,
    input  logic   enb,
    output logic   ovf,
    output count_t count
);

count_t count_r, count_nxt;
logic ovf_st;

always_comb begin
    if(count == MAX_COUNT) begin
        count_nxt = 1'b0;
        ovf_st = 1'b1;
    end
    else begin
        count_nxt  = count_r + 1'b1;
        ovf_st = 1'b0;
    end
end
// Sequential process
always_ff@(posedge clk, negedge rst)begin: counter
    if (!rst)
        count_r <='0;
    else if (enb)
        count_r <= count_nxt;
end:counter

assign count = count_r; 
assign ovf = ovf_st; 

endmodule
