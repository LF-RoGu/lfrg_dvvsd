/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: op_sel
    Description: op_sel source file
    Last modification: 14/02/2021
*/

import fir_pkg::*;
module op_sel
(	
	input						clk,
	input						rst,
	input   logic				enb,
	output  op_selectr_e	   	o_sltd
);

logic [2:0] op_r, op_nxt = 3'b001;
	
always_comb begin
	if( 3'b100 > op_r )
		op_nxt = 3'b001;
	else
		begin
			op_nxt = op_r << 'd1;
			case(op_r)
			3'b001: begin
				o_sltd = BOOTH;
			end 
			3'b010: begin
				o_sltd = DIV;
			end 
			3'b100: begin
				o_sltd = SQRT;
			end
			default: begin
				o_sltd = BOOTH;
			end 
			endcase 
		end 
end
// Sequential process
always_ff@(posedge clk, negedge rst)begin
    if (!rst)
        op_r <= 3'b001;
    else if (enb)
        op_r <= op_nxt;
end

endmodule
