/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: arithmetic_logic_unit
    Description: arithmetic_logic_unit source file
    Last modification: 14/02/2021
*/

import fir_pkg::*;
module arithmetic_logic_unit
#(
	parameter DW = fir_pkg::N
)
(
	/** INPUT*/
	input logic [DW-1:0] A,
	input logic [DW-1:0] B,
	input alu_op_sel_str op_sel,
	/** OUTPUT*/
	output logic [DW-1:0] result
);
	 
always_comb begin
	case(op_sel)
		NO_OP: begin
			result = A + 'd0;
		end 
		ADDITION: begin
			result = A + B;
		end 
		SUBSTRACTION: begin
			result = A + (~B + 1'b1);
		end 
		default: begin
			result = '0;
		end 
	endcase 
end 

endmodule 