/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: fsm_control_sqrt
    Description: fsm_control_sqrt source file
    Last modification: 18/02/2021
*/

import fir_pkg::*;
module fsm_control
(
   input        	clk,
   input        	rst,
   input  logic 	start,
   input  logic 	ovf,
   output logic		enb_counter,
   output logic 	enb_pipo,
   output logic 	ready
);

control_st  current_st;	
control_st  nxt_state;

always_comb begin 
   case(current_st)
   IDLE_ST: begin
      if(start == 1'b1)
         nxt_state = START_ST;
      else
         nxt_state = IDLE_ST;          
   end
   START_ST: begin
       nxt_state = BUSY_ST;
   end
   BUSY_ST: begin
      if(ovf == 1'b1)
         nxt_state = IDLE_ST;
      else
         nxt_state = BUSY_ST;    
   end
   endcase
end

always_ff@(posedge clk or negedge rst) begin 
   if (!rst)
      current_st <= IDLE_ST;
   else
      current_st <= nxt_state;
end 

always_comb begin 
   case(current_st)
   IDLE_ST: begin
	   enb_pipo = 1'b0;
	   enb_counter = 1'b0;
	   ready = 1'b1;
   end
   START_ST: begin
	   enb_pipo = 1'b1;
	   enb_counter = 1'b1;
	   ready = 1'b0;
   end
   BUSY_ST: begin
	   enb_pipo = 1'b1;
	   enb_counter = 1'b1;
	   ready = 1'b0;
   end
   endcase
end

endmodule 