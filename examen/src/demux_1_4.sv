/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: mux_2_1
    Description: mux_2_1 source file
    Last modification: 14/02/2021
*/

import fir_pkg::*;
module demux_1_4
#(
	parameter DW = fir_pkg::N
)
(	
	input   logic [DW-1:0]   	i_data,
	input   selectr_e   		i_sel,
	output  logic [DW-1:0]   	o_data_a,
	output  logic [DW-1:0]   	o_data_b,
	output  logic [DW-1:0]   	o_data_c,
	output  logic [DW-1:0]   	o_data_d
);

always_comb begin
    case(i_sel)
    OP_A: begin
        o_data_a = i_data;   
    end
    OP_B: begin
        o_data_b = i_data;   
    end
    OP_C: begin
        o_data_c = i_data;   
    end
    OP_D: begin
        o_data_d = i_data;   
    end     
    endcase
end

endmodule
