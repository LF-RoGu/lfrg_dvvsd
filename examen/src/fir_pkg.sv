/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: booth_multiplier_pkg
    Description: booth_multiplier_pkg package file
    Last modification: 14/02/2021
*/

 `ifndef FIR_PKG_SV
	`define FIR_PKG_SV
package fir_pkg;
	/** Parameters*/
	localparam N = 14; 

	/** Data Types*/
	/*
	 * OP data type
	 */
	typedef enum logic [2:0]
	{
		BOOTH,
		DIV,
		SQRT
	}op_selectr_e;
	/*
	 * TOP MODULE data type
	 */
	typedef logic [N-1:0] data_n_t; 
	typedef logic [(2*N):0] data_2n_t; 
	typedef logic [(2*N)+1:0] data_2n1_t; 

	/*
	 * ALU data type
	 */
	typedef enum logic [1:0]
	{
		NO_OP,
		ADDITION,
		SUBSTRACTION
	}alu_op_sel_str;

	typedef logic [N-1:0] alu_dt; //alu_data_type

	localparam N_SEL = 4;
	typedef logic [N-1:0]   dtwidth_t;
	typedef enum logic [N_SEL-1:0]
	{
		OP_A,
		OP_B,
		OP_C,
		OP_D
	}selectr_e;
	
	/*
	 * FSM_CONTROL data_type
	 */
	typedef enum logic[2:0]
	{
	    IDLE_ST,
	    START_ST,
	    BUSY_ST
	} control_st;
	
	/*
	 * COUNTER_OVF data_type
	 */
    localparam MAX_COUNT = N-1;

    typedef logic [N-1:0] count_t; 


endpackage
 `endif