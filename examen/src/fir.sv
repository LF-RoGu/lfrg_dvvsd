/* 
    Authors: 
             Luis Fernando Rodriguez @LF-RoGu
    Title: fir
    Description: fir source file
    Last modification: 14/02/2021
*/

import fir_pkg::*;
module fir
(
	input            	clk,
	input            	rst,
	input  logic        start ,
	input  logic		x,
	output logic		ready,
	output logic  		y
);


logic xt;
logic firA, firB, firC, firD;
logic gainA, gainB, gainC, gainD, gainE;
logic sumA, sumB, sumC, sumD;
	
count_t count_w;
logic ovf_w;
logic enb_counter;
logic enb_pipo;

assign xt = (~x) + 1'b1;

/** Preferible meter mux*/
assign gainA = xt*(1);
pipo_register
#(
	.DW(1)
) 
register_firA
(
	.clk(clk),
	.rst(rst),
	.enb(enb_pipo),
	.i_data(xt),
	.o_data(firA)
);
/** Preferible meter mux*/
assign gainB = firA*(-1);
pipo_register 
#(
	.DW(1)
)
register_firB
(
	.clk(clk),
	.rst(rst),
	.enb(enb_pipo),
	.i_data(firA),
	.o_data(firB)
);
/** Preferible meter mux*/
assign gainC = firB*(1);
pipo_register 
#(
	.DW(1)
)
register_firC
(
	.clk(clk),
	.rst(rst),
	.enb(enb_pipo),
	.i_data(firB),
	.o_data(firC)
);
/** Preferible meter mux*/
assign gainD = firC*(-1);
pipo_register  
#(
	.DW(1)
)
register_firD
(
	.clk(clk),
	.rst(rst),
	.enb(enb_pipo),
	.i_data(firC),
	.o_data(firD)
);
/** Preferible meter mux*/
assign gainE = firD*(1);

/*
 * Propuesta
 * PIPO -> MUX(ENTRADA Bn / ADDER) -> ADDER -> retro PIPO
 */
assign sumA = gainA + gainB;
assign sumB = sumA + gainC;
assign sumC = sumB + gainD;
assign sumD = sumC + gainE;
	

assign y = sumD;

fsm_control
control
(
	.clk(clk),
	.rst(rst),
	.start(start),
	.enb_counter(enb_counter),
	.enb_pipo(enb_pipo),
	.ovf(ovf_w),
	.ready(ready)
);

counter_ovf
counter
(
	.clk(clk),
	.rst(rst),
	.enb(enb_counter),
	.count(count_w),
	.ovf(ovf_w)
);

endmodule