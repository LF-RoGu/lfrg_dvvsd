# ***PRÁCTICA #1: MULTIPLICADOR SECUENCIAL***

**Desarrolladores:**
- César Villarreal ie707560 @cv4497
- Luis F. Rodríguez ie705694 @LF-RoGu

**Descripción del repositorio:** 
- Implementación de multiplicador secuencial

**Índice**
1. [src](https://gitlab.com/cv4497/ie707560_ie705694_dvvsd_p2021/-/tree/master/P1/src): archivos de código fuente 
2. [tb](https://gitlab.com/cv4497/ie707560_ie705694_dvvsd_p2021/-/tree/master/P1/tb): archivos de simulación
3. [docs](https://gitlab.com/cv4497/ie707560_ie705694_dvvsd_p2021/-/tree/master/P1/docs): documentos de la práctica
4. [scripts](https://gitlab.com/cv4497/ie707560_ie705694_dvvsd_p2021/-/tree/master/P1/scripts): herramientas para verificación

**Tarjeta de Desarrollo**
- [DE-10 Standard](https://www.intel.com/content/dam/altera-www/global/en_US/portal/dsn/42/doc-us-dsnbk-42-5505271707235-de10-standard-user-manual-sm.pdf): tarjeta de desarrollo.

**FPGA**
- [5CSXFC6D6F31C6N SoC](https://www.mouser.mx/datasheet/2/612/cv_51002-1709862.pdf): modelo del FPGA.
- [Cyclone V](https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/hb/cyclone-v/cv_5v1.pdf): familia del FPGA.

**Documentos**
- [Micro-arquitectura](https://gitlab.com/cv4497/ie707560_ie705694_dvvsd_p2021/-/blob/master/P1/docs/P1_microarquitectura.pdf)
- [Requerimientos](https://gitlab.com/cv4497/ie707560_ie705694_dvvsd_p2021/-/blob/master/P1/docs/Practica%201.pdf)

![arquitectura](docs/P1_microarquitectura.png)
