/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: sequential_multiplier_pkg
    Description: sequential_multiplier_pkg package file
    Last modification: 15/02/2021
*/
import sequential_multiplier_pkg::*;
module SM_FPGA
(
	/** INPUT*/
	input            	clk,
	input            	rst,
	input  logic        start,
	input  data_n_t 	multiplicand,
	input  data_n_t  	multiplier,
	/** OUTPUT*/
	output  data_n_t 	o_multiplicand,
	output  data_n_t  	o_multiplier,
	output data2display_t 	data2display_hundreds,
	output data2display_t 	data2display_tens,
	output data2display_t 	data2display_units,
	output logic     	ready,
	output logic        sign
);

logic start_w, rst_w;
logic outclk;

`ifndef PLL_SYS
/*
 * PLL
 */

logic locked_pll;
pll_50_2M pll_2M_inst
(
	.refclk   (clk),   //  refclk.clk
	.rst      (!rst),      //   reset.reset
	.outclk_0 (outclk), // outclk0.clk
	.locked   (locked_pll)    //  locked.export
);

 `else
/*
 * CLK_DIVIDER
 */
clk_divider sm_clk_divider
(
	.clk_FPGA(clk),
	.rst(rst),
	.clk(outclk)
);
`endif

data_2n_t 	product_w;
logic		sign_w;

/*
 * DBCR
 */
dbcr_top dbcr_start
(
	.clk(outclk),
	.rst_n(rst),
	.Din(!start),
	.one_shot(start_w)
);
	
/*
 * Sequential_Multiplier
 */
sequential_multiplier SM_FPGA
(
	.clk(outclk),
	.rst(rst),
	.start(start_w),
	.multiplicand(multiplicand),
	.multiplier(multiplier),
	.product(product_w),
	.ready(ready),
	.sign(sign_w)
);
/*
 * BIN2BCD
 */	
 
bin2bcd bin2bcd_multiplicand
(
	.binary_data(product_w),
	.data2display_units(data2display_units),
	.data2display_tens(data2display_tens),
	.data2display_hundreds(data2display_hundreds),
	.sign(sign)
	);
	
assign o_multiplicand = multiplicand;
assign o_multiplier = multiplier;
/** Send product to leds*/
endmodule 