/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: control
    Description: control source file
    Last modification: 14/02/2021
*/

import sequential_multiplier_pkg::*;

module control
(
	input   		  clk,
	input  		      rst,
	input  logic      start,
	output logic      prod_enb,
	output logic      mcand_enb,
	output logic      mux_sel,
	output logic 	  ready
);

logic ovf_w;
logic counter_enb_w;
logic ready_w;
logic prod_enb_w;
logic mcand_enb_w;
logic mux_sel_w;

counter_ovf counter
(
	.clk(clk),
	.rst(rst),
	.enb(counter_enb_w),
	.count(),
	.ovf(ovf_w)
);

fsm_control fsm_contr
(
   .clk(clk),
   .rst(rst),
   .start(start),
   .ovf(ovf_w),
   .counter_enb(counter_enb_w),
   .prod_enb(prod_enb_w),
   .mcand_enb(mcand_enb_w),
   .mux_sel(mux_sel_w),
   .ready(ready_w)
);

assign prod_enb = prod_enb_w;
assign mcand_enb = mcand_enb_w;
assign mux_sel = mux_sel_w;
assign ready = ready_w;

endmodule 
