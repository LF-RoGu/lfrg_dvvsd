/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: clk_divider
    Description: clk_divider package file
    Last modification: 14/02/2021
*/

module clk_divider
import sequential_multiplier_pkg::*;
#(
    parameter FREQUENCY=2_000_000,            //input frequency
    parameter REFERENCE_CLOCK=50_000_000 //50 MHz clock
)(
	input   clk_FPGA,
    input   rst,
    output  clk
);

/* clock divider counters */
counter_t count_r, cnt_nxt;
logic clk_r;

/* Combinational process */
always_comb begin
  cnt_nxt  = count_r + 1'b1 ;

end

/* Sequential process */
always_ff@(posedge clk_FPGA, negedge rst)begin: clk_div
    if (!rst)
	 begin
        count_r	<=  '0;
		  clk_r <= 1'b0;
	 end
    else
		 begin
			if(count_r >= (REFERENCE_CLOCK/(FREQUENCY)))
					count_r <= '0;
			else
					count_r	<=  cnt_nxt;
					
			if(count_r < ((REFERENCE_CLOCK/(FREQUENCY*2))))
					clk_r <= 1'b1;
			else
					clk_r <= 1'b0;
		 end
		  
end:clk_div

assign clk = clk_r;

endmodule