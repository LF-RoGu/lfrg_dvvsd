/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: arithmetic_logic_unit
    Description: arithmetic_logic_unit source file
    Last modification: 14/02/2021
*/

import sequential_multiplier_pkg::*;
module arithmetic_logic_unit
(
	/** INPUT*/
	input alu_dt A,
	input alu_dt B,
	input alu_op_sel_str op_sel,
	/** OUTPUT*/
	output alu_dt result
);
	 
always_comb begin
	case(op_sel)
		NO_OP: begin
			result = '0;
		end 
		ADDITION: begin
			result = A + B;
		end 
		SUBSTRACTION: begin
			result = A + (~B + 1'b1);
		end 
		AND: begin
			result = A & B;
		end 
		OR: begin
			result = A | B;
		end 
		default: begin
			result = '0;
		end 
	endcase 
end 

endmodule 