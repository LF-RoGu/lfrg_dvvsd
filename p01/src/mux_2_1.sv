/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: mux_2_1
    Description: mux_2_1 source file
    Last modification: 14/02/2021
*/

import sequential_multiplier_pkg::*;
module mux_2_1
#(
	parameter DW = sequential_multiplier_pkg::N
)
(	
	input   logic [DW-1:0]   	i_a,
	input   logic [DW-1:0]   	i_b,
	input   selectr_e   		i_sel,
	output  logic [DW-1:0]   	o_sltd
);

always_comb begin
    case(i_sel)
    OP_A: begin
        o_sltd = i_a;   
    end
    OP_B: begin
        o_sltd = i_b;   
    end
    default: begin
        o_sltd = i_a;  
    end      
    endcase
end

endmodule
