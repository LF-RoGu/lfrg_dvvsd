/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: sequential_multiplier_pkg
    Description: sequential_multiplier_pkg package file
    Last modification: 14/02/2021
*/

 `ifndef P01_PKG_SV
	`define P01_PKG_SV
package sequential_multiplier_pkg;
	/** Parameters*/
	localparam N = 4; 
	// TRUE and FALSE definitions
	localparam bit TRUE     =1'b1;
	localparam bit FALSE    =1'b0;

	/** Data Types*/
	/*
	 * TOP MODULE data type
	 */
	typedef logic [N-1:0] data_n_t; 
	typedef logic [(2*N)-1:0] data_2n_t; 

	/*
	 * ALU data type
	 */
	typedef enum logic [3:0]
	{
		NO_OP,
		ADDITION,
		SUBSTRACTION,
		AND,
		OR
	}alu_op_sel_str;

	typedef logic [N-1:0] alu_dt; //alu_data_type

	localparam N_SEL = 2;
	typedef logic [N-1:0]   dtwidth_t;
	typedef enum logic [N_SEL-1:0]
	{
		OP_A,
		OP_B
	}selectr_e;
	
	/*
	 * FSM_CONTROL data_type
	 */
	typedef enum logic[2:0]
	{
	    IDLE_ST,
	    START_ST,
	    BUSY_ST
	} control_st;
	
	/*
	 * COUNTER_OVF data_type
	 */
    localparam MAX_COUNT = N-1;
	/*
	 * CLK_DIVIDER data_type
	 */
	localparam N_count = 27;  //number of counters
    typedef logic [N_count-1:0] counter_t;
	/*
	 * DBCR_CNTR data_type
	 */
	 typedef enum logic [1:0]{
		 BAJO = 2'b00,
		 DLY1 = 2'b01,
		 ALTO = 2'b10,
		 DLY2 = 2'b11
	 } fsm_dbcr_state_e;
    typedef logic [N-1:0] count_t; 
	/*
	 * BIN2BCD data_type
	 */
	localparam PARAM_DW_BCD = 4;
	localparam PARAM_DW_SEGMENT_DISPLAY = 7;
	 
	 /** Data Types*/
	 typedef logic [2*N - 1:0] binary_data_t;
	 typedef logic [PARAM_DW_BCD - 1:0] bcd_data_t;
	 typedef logic [PARAM_DW_SEGMENT_DISPLAY - 1:0] data2display_t;

endpackage
 `endif