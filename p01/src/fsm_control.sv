/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: fsm_control
    Description: fsm_control source file
    Last modification: 14/02/2021
*/

import sequential_multiplier_pkg::*;
module fsm_control
(
   input        clk,
   input        rst,
   input  logic start,
   input  logic ovf,
   output logic counter_enb,
   output logic prod_enb,
   output logic mcand_enb,
   output logic mux_sel,
   output logic ready
);

control_st  current_st;	
control_st  nxt_state;

always_comb begin 
   case(current_st)
   IDLE_ST: begin
      if(start == 1'b1)
         nxt_state = START_ST;
      else
         nxt_state = IDLE_ST;          
   end
   START_ST: begin
       nxt_state = BUSY_ST;
   end
   BUSY_ST: begin
      if(ovf == 1'b1)
         nxt_state = IDLE_ST;
      else
         nxt_state = BUSY_ST;    
   end
   endcase
end

always_ff@(posedge clk or negedge rst) begin 
   if (!rst)
      current_st <= IDLE_ST;
   else
      current_st <= nxt_state;
end 

always_comb begin 
   case(current_st)
   IDLE_ST: begin
       ready = 1'b1;
       counter_enb = 1'b0;
       prod_enb = 1'b0;
       mcand_enb = 1'b0;
       mux_sel = 1'b1;
   end
   START_ST: begin
       ready = 1'b0;
       counter_enb = 1'b0;
       prod_enb = 1'b1;
       mcand_enb = 1'b1;
       mux_sel = 1'b1;
   end
   BUSY_ST: begin
       ready = 1'b0;
       counter_enb = 1'b1;
       prod_enb = 1'b1;
       mcand_enb = 1'b0;
       mux_sel = 1'b0;
   end
   endcase
end

	
endmodule 