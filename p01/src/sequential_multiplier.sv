/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: sequential_multiplier
    Description: sequential_multiplier source file
    Last modification: 14/02/2021
*/

import sequential_multiplier_pkg::*;
module sequential_multiplier
(
	input            	clk,
	input            	rst,
	input  logic        start,
	input  data_n_t 	multiplicand,
	input  data_n_t  	multiplier,
	output data_2n_t 	product,
	output logic     	ready,
	output logic        sign
);

/* Wire declarations */
data_n_t  adder_res_w;
data_n_t  adder_result_w;
data_n_t  mux_adder_stld_w;
data_n_t  mcand_q_reg_w;
logic mcand_enb_w;
data_2n_t prod_q_reg_w;
logic mux_prod_sel_w;
data_2n_t mux_prod_stld_w;
logic prod_enb_w;
logic ready_w;

data_n_t multiplier_c2_w;
data_n_t multiplicand_c2_w;
logic mcand_sign_w;
logic mplier_sign_w;

data_2n_t prod_c2_w;

comp2bin
#(
	.DW(sequential_multiplier_pkg::N)
)   multiplier_binary_2_c2
(	
	.i_data(multiplier),
	.o_data(multiplier_c2_w),
	.sign(mplier_sign_w)
);

comp2bin
#(
	.DW(sequential_multiplier_pkg::N)
)   multiplicand_binary_2_c2
(	
	.i_data(multiplicand),
	.o_data(multiplicand_c2_w),
	.sign(mcand_sign_w)
);

pipo_register
#(
	.DW(sequential_multiplier_pkg::N)
)   mutiplicand_register
(
	.clk(clk),
	.rst(rst),
	.enb(mcand_enb_w),
	.i_data(multiplicand_c2_w),
	.o_data(mcand_q_reg_w)
);

mux_2_1
#(
	.DW(sequential_multiplier_pkg::N) //N
)	mux_adder
(
	.i_a(sequential_multiplier_pkg::N'(4'd0)),
	.i_b(mcand_q_reg_w),
	.i_sel(selectr_e'(prod_q_reg_w[0])),
	.o_sltd(mux_adder_stld_w)
);

arithmetic_logic_unit adder
(
	.A(prod_q_reg_w[(2*sequential_multiplier_pkg::N)-1:sequential_multiplier_pkg::N]),
	.B(mux_adder_stld_w),
	.op_sel(alu_op_sel_str'(ADDITION)),
	.result(adder_result_w)
);

mux_2_1 
#(
	.DW(2*sequential_multiplier_pkg::N) //2N
)	mux_prod
(
	.i_a({sequential_multiplier_pkg::N'(4'd0),{adder_result_w, prod_q_reg_w[sequential_multiplier_pkg::N-1:1]}}), //SHIFTED DATA
	.i_b({sequential_multiplier_pkg::N'(4'd0),multiplier_c2_w}),
	.i_sel(selectr_e'(mux_prod_sel_w)),
	.o_sltd(mux_prod_stld_w)
);

pipo_register
#(
	.DW(2*sequential_multiplier_pkg::N) //2N
)
Product_Register
(
	.clk(clk),
	.rst(rst),
	.enb(prod_enb_w),
	.i_data(mux_prod_stld_w),
	.o_data(prod_q_reg_w)
);


control system_control
(
	.clk(clk),
	.rst(rst),
	.start(start),
	.prod_enb(prod_enb_w),
	.mcand_enb(mcand_enb_w),
	.mux_sel(mux_prod_sel_w),
	.ready(ready_w)
);

assign ready = ready_w;
assign sign = mplier_sign_w ^ mcand_sign_w; 
assign product = (sign == 1'b1) ? (~prod_q_reg_w+1'b1):prod_q_reg_w;


endmodule