 /*
  Authors: Cesar Villarreal @cv4497
  Luis Fernando Rodriguez @LF-RoGu
  Title: mux_2_1_tb
  Description: mux_2_1_tb test bench file
  Last modification: 08/02/2021
  */
 /** scale*/
 `timescale 1ns / 1ps
/** Init module for tb*/
module mux_2_1_tb();

	import p01_pkg::*;

	/** LocalParam*/
	localparam PERIOD = 2;

	/** Signal Declaration*/
	logic clk;
	dtwidth_t data_a, data_b, data_out;

	selectr_e selectr;

	/** Instace of unit under test*/
	mux_2_1 uut_dut
	(
		.i_a(data_a),
		.i_b(data_b),
		.i_sel(selectr),
		.o_sltd(data_out)
	);
	/** hilos de ejecucion, podemos tener hasta #n hilos de ejecucion*/
	initial begin
		integer index_a, index_b, index_op;
		
		selectr = OP_A;
		
		data_a = 20;
		data_b = 10;

		for(index_op = 0; index_op < 2; index_op++)
		begin
			case (index_op)
				1: begin
					selectr = OP_A;
					for(index_a = 0; index_a < 127; index_a++)
					begin
						data_a = $random();
						for(index_b = 0; index_b < 127; index_b++)
						begin
							data_b = $random();
						end
					end
				end
				2: begin
					selectr = OP_B;
					for(index_a = 0; index_a < 127; index_a++)
					begin
						data_a = $random();
						for(index_b = 0; index_b < 127; index_b++)
						begin
							data_b = $random();
						end
					end
				end
			endcase
		end
	end

	always begin
		#(PERIOD/2) clk <= ~clk;
	end
endmodule 