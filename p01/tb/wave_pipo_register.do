onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider TB_SIGNALS
add wave -noupdate /pipo_register_tb/clk
add wave -noupdate /pipo_register_tb/rst
add wave -noupdate /pipo_register_tb/enable_w
add wave -noupdate -radix decimal /pipo_register_tb/i_data
add wave -noupdate /pipo_register_tb/o_data
add wave -noupdate /pipo_register_tb/#ublk#264489170#34/index
add wave -noupdate -divider PIPO_SIGNALS
add wave -noupdate /pipo_register_tb/uut_dut/clk
add wave -noupdate /pipo_register_tb/uut_dut/rst
add wave -noupdate /pipo_register_tb/uut_dut/enb
add wave -noupdate -radix decimal /pipo_register_tb/uut_dut/i_data
add wave -noupdate -radix decimal /pipo_register_tb/uut_dut/rgstr_r
add wave -noupdate -radix decimal /pipo_register_tb/uut_dut/o_data
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {14000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 258
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {8405 ps} {59463 ps}
