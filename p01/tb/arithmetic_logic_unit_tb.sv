 /*
  Authors: Cesar Villarreal @cv4497
  Luis Fernando Rodriguez @LF-RoGu
  Title: arithmetic_logic_unit_tb
  Description: arithmetic_logic_unit_tb test bench file
  Last modification: 08/02/2021
  */
 /** scale*/
 `timescale 1ns / 1ps
/** Init module for tb*/
module arithmetic_logic_unit_tb();

	import p01_pkg::*;

	/** LocalParam*/
	localparam PERIOD = 2;

	/** Signal Declaration*/

	logic clk;

	alu_dt data_a;
	alu_dt data_b;
	alu_op_sel_str alu_op_sel;
	alu_dt data_res;
	/** Instace of unit under test*/
	arithmetic_logic_unit uut_dut
	(
		.A(data_a),
		.B(data_b),
		.op_sel(alu_op_sel),
		.result(data_res)
	);
	/** hilos de ejecucion, podemos tener hasta #n hilos de ejecucion*/
	initial begin
		integer index_a, index_b, index_alu_op;

		for(index_alu_op = 0; index_alu_op < 6; index_alu_op++)
		begin
			case(index_alu_op)
				0: begin
					alu_op_sel = NO_OP;
					for(index_a = 0; index_a < 63; index_a++)
					begin
						for(index_b = 0; index_b < 63; index_b++)
						begin
							data_a = index_a;
							data_b = index_b;

							$display("Product:  %d",data_res);

							#1;
						end
						#1;
					end
				end
				1: begin
					alu_op_sel = ADDITION;
					for(index_a = 0; index_a < 63; index_a++)
					begin
						for(index_b = 0; index_b < 63; index_b++)
						begin
							data_a = index_a;
							data_b = index_b;

							$display("Product:  %d",data_res);

							#1;
						end
						#1;
					end
				end
				2: begin
					alu_op_sel = SUBSTRACTION;
					for(index_a = 0; index_a < 63; index_a++)
					begin
						for(index_b = 0; index_b < 63; index_b++)
						begin
							data_a = index_a;
							data_b = index_b;

							$display("Product:  %d",data_res);

							#1;
						end
						#1;
					end
				end
				3: begin
					alu_op_sel = AND;
					for(index_a = 0; index_a < 63; index_a++)
					begin
						for(index_b = 0; index_b < 63; index_b++)
						begin
							data_a = index_a;
							data_b = index_b;

							$display("Product:  %d",data_res);

							#1;
						end
						#1;
					end
				end
				4: begin
					alu_op_sel = OR;
					for(index_a = 0; index_a < 63; index_a++)
					begin
						for(index_b = 0; index_b < 63; index_b++)
						begin
							data_a = index_a;
							data_b = index_b;

							$display("Product:  %d",data_res);

							#1;
						end
						#1;
					end
				end
				default: begin
				end
			endcase
		end
	end

	always begin
		#(PERIOD/2)clk <= ~clk;
	end
endmodule 