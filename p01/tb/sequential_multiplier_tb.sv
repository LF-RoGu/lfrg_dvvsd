 /*
  Authors: Cesar Villarreal @cv4497
  Luis Fernando Rodriguez @LF-RoGu
  Title: sequential_multplier_tb
  Description: sequential_multplier_tb test bench file
  Last modification: 14/02/2021
  */
 /** scale*/
 `timescale 1ns / 1ps
/** Init module for tb*/
module sequential_multiplier_tb();

	import sequential_multiplier_pkg::*;

	/** LocalParam*/
	localparam PERIOD = 2;
	localparam CYCLES = 2**(sequential_multiplier_pkg::N);
	localparam WAIT_EVENT = 2*(sequential_multiplier_pkg::N);

	/** Signal Declaration*/

	logic clk, rst, start;
	data_n_t multiplicand, multiplier;
	data_2n_t product;
	logic ready;
	/** Instace of unit under test*/
	sequential_multiplier uut_dut
	(
		.clk(clk),
		.rst(rst),
		.start(start),
		.multiplicand(multiplicand),
		.multiplier(multiplier),
		.product(product),
		.ready(ready)
	);
	
	/** hilos de ejecucion, podemos tener hasta #n hilos de ejecucion*/
	initial begin
		integer index_a, index_b = 0;
		
		#PERIOD clk = 0;
		#PERIOD rst = 1; #PERIOD rst = ~rst; #PERIOD rst = ~rst;
		#PERIOD start = 0;
		multiplicand = 0;
		multiplier = 0;
		
		for (index_a = 1; index_a < CYCLES; index_a++) begin
			multiplicand = index_a;
			for (index_b = 1; index_b < CYCLES; index_b += 1) begin
				multiplier = index_b;
				#PERIOD start = 1'b0; #PERIOD start = 1'b1; #PERIOD start = 1'b0;
				#WAIT_EVENT;
			end 
		end 
		$stop;
	end

	always begin
		#(PERIOD/2)clk <= ~clk;
	end
endmodule 