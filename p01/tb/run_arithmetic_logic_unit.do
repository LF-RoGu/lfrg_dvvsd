if [file exists work] {vdel -all}
vlib work
vlog -f files_arithmetic_logic_unit.f
onbreak {resume}
set NoQuitOnFinish 1
vsim -voptargs=+acc work.arithmetic_logic_unit_tb
do wave_arithmetic_logic_unit.do
run 1100ns
