onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider ALU
add wave -noupdate -radix decimal /arithmetic_logic_unit_tb/data_a
add wave -noupdate -radix decimal /arithmetic_logic_unit_tb/data_b
add wave -noupdate /arithmetic_logic_unit_tb/alu_op_sel
add wave -noupdate -radix decimal /arithmetic_logic_unit_tb/data_res
add wave -noupdate -divider {Cycle Counter}
add wave -noupdate /arithmetic_logic_unit_tb/#ublk#74361458#35/index_alu_op
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {8001955 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 361
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {421155 ns}
