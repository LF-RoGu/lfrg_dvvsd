if [file exists work] {vdel -all}
vlib work
vlog -f files_mux_2_1.f
onbreak {resume}
set NoQuitOnFinish 1
vsim -voptargs=+acc work.mux_2_1_tb
do wave_mux_2_1.do
run 1100ns
