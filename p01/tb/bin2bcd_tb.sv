/*
 * CODER: Luis Fernando Rodriguez Gtz
 * DATE: 25/Jan/2021
 * PROJECT: basic_counter_tb
 */
 /** scale*/
 `timescale 1ns / 1ps
 /** Init module for tb*/
 module bin2bcd_tb();
	 
 import p01_pkg::*;
	 
 /** LocalParam*/
 
 /** Signal Declaration*/	 
 binary_data_t binary_data;
 data2display_t data2display_hundreds;
 data2display_t data2display_tens;
 data2display_t data2display_units;
 logic sign;
 /** Instace of unit under test*/
 bin2bcd uut_dut
 (
	 /** INPUT*/
	 .binary_data( binary_data ),
	 /** OUTPUT*/
	 .data2display_hundreds( data2display_hundreds ),
	 .data2display_tens( data2display_tens ),
	 .data2display_units( data2display_units ),
	 .sign( sign )
	 );
 /** hilos de ejecucion, podemos tener hasta #n hilos de ejecucion*/
 initial begin
	 integer index;
	 for(index = -128; index < 127; index++)
		 begin
			 binary_data = index;
			 #1;
		 end 
	 $stop;
 end 
	 
 endmodule 