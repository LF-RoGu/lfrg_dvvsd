onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix decimal /bin2bcd_tb/binary_data
add wave -noupdate -divider BCD
add wave -noupdate -radix decimal /bin2bcd_tb/uut_dut/bin2bcd_decoder_top/bcd_data_hundreds
add wave -noupdate -radix decimal /bin2bcd_tb/uut_dut/bin2bcd_decoder_top/bcd_data_tens
add wave -noupdate -radix unsigned /bin2bcd_tb/uut_dut/bin2bcd_decoder_top/bcd_data_units
add wave -noupdate /bin2bcd_tb/uut_dut/bin2bcd_decoder_top/sign
add wave -noupdate -divider Segments
add wave -noupdate -radix binary /bin2bcd_tb/data2display_hundreds
add wave -noupdate -radix binary /bin2bcd_tb/data2display_tens
add wave -noupdate -radix binary /bin2bcd_tb/data2display_units
add wave -noupdate /bin2bcd_tb/sign
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {27967 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 361
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {34672 ps}
