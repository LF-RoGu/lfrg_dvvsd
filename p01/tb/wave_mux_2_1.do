onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider Data
add wave -noupdate -radix decimal /mux_2_1_tb/data_a
add wave -noupdate -radix decimal /mux_2_1_tb/data_b
add wave -noupdate -radix decimal /mux_2_1_tb/data_out
add wave -noupdate -divider Selector
add wave -noupdate /mux_2_1_tb/selectr
add wave -noupdate -divider Counters
add wave -noupdate /mux_2_1_tb/#ublk#42285634#33/index_a
add wave -noupdate /mux_2_1_tb/#ublk#42285634#33/index_b
add wave -noupdate /mux_2_1_tb/#ublk#42285634#33/index_op
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {1155 ns}
