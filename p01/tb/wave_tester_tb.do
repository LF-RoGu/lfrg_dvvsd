onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tester_tb/clk
add wave -noupdate /tester_tb/rst
add wave -noupdate /tester_tb/start
add wave -noupdate -radix binary /tester_tb/multiplicand
add wave -noupdate -radix unsigned /tester_tb/multiplier
add wave -noupdate -radix decimal /tester_tb/product
add wave -noupdate /tester_tb/ready
add wave -noupdate /tester_tb/sign
add wave -noupdate /tester_tb/multiplier_binary_2_c2/i_data
add wave -noupdate /tester_tb/multiplier_binary_2_c2/o_data
add wave -noupdate /tester_tb/multiplicand_binary_2_c2/i_data
add wave -noupdate /tester_tb/multiplicand_binary_2_c2/o_data
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1099196 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {22168 ps} {23168 ps}
