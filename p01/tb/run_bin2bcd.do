if [file exists work] {vdel -all}
vlib work
vlog -f files_bin2bcd.f
onbreak {resume}
set NoQuitOnFinish 1
vsim -voptargs=+acc work.bin2bcd_tb
do wave_bin2bcd.do
run 1100ns
