onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -group Control -label clk /tester_tb/system_control/clk
add wave -noupdate -group Control -label rst /tester_tb/system_control/rst
add wave -noupdate -group Control -color Gold -label start /tester_tb/system_control/start
add wave -noupdate -group Control -color Gold -label prod_enb /tester_tb/system_control/prod_enb
add wave -noupdate -group Control -color Gold -label mcand_enb /tester_tb/system_control/mcand_enb
add wave -noupdate -group Control -color Gold -label mux_sel /tester_tb/system_control/mux_sel
add wave -noupdate -group Control -color Gold -label ready /tester_tb/system_control/ready
add wave -noupdate -group Control -group {FSM Control} -label start /tester_tb/system_control/fsm_contr/start
add wave -noupdate -group Control -group {FSM Control} -label ovf /tester_tb/system_control/fsm_contr/ovf
add wave -noupdate -group Control -group {FSM Control} -label counter_enb /tester_tb/system_control/fsm_contr/counter_enb
add wave -noupdate -group Control -group {FSM Control} -label prod_enb /tester_tb/system_control/fsm_contr/prod_enb
add wave -noupdate -group Control -group {FSM Control} -label mcand_enb /tester_tb/system_control/fsm_contr/mcand_enb
add wave -noupdate -group Control -group {FSM Control} -label mux_sel /tester_tb/system_control/fsm_contr/mux_sel
add wave -noupdate -group Control -group {FSM Control} -label ready /tester_tb/system_control/fsm_contr/ready
add wave -noupdate -group Control -label current_st /tester_tb/system_control/fsm_contr/current_st
add wave -noupdate -group Control -group Counter -label enb /tester_tb/system_control/counter/enb
add wave -noupdate -group Control -group Counter -label ovf /tester_tb/system_control/counter/ovf
add wave -noupdate -group Control -group Counter -label count -radix unsigned /tester_tb/system_control/counter/count
add wave -noupdate -expand -group {Product Register} -label clk /tester_tb/Product_Register/clk
add wave -noupdate -expand -group {Product Register} -label rst /tester_tb/Product_Register/rst
add wave -noupdate -expand -group {Product Register} -label enb /tester_tb/Product_Register/enb
add wave -noupdate -expand -group {Product Register} -label i_data /tester_tb/Product_Register/i_data
add wave -noupdate -expand -group {Product Register} -label o_data -radix binary /tester_tb/Product_Register/o_data
add wave -noupdate -expand -group adder -label A /tester_tb/adder/A
add wave -noupdate -expand -group adder -label B /tester_tb/adder/B
add wave -noupdate -expand -group adder -label op_sel /tester_tb/adder/op_sel
add wave -noupdate -expand -group adder -label result /tester_tb/adder/result
add wave -noupdate -expand -group {Mux adder} -label i_a /tester_tb/mux_adder/i_a
add wave -noupdate -expand -group {Mux adder} -label i_b /tester_tb/mux_adder/i_b
add wave -noupdate -expand -group {Mux adder} -label i_sel /tester_tb/mux_adder/i_sel
add wave -noupdate -expand -group {Mux adder} -label o_sltd /tester_tb/mux_adder/o_sltd
add wave -noupdate -expand -group {mux product} /tester_tb/mux_prod/i_a
add wave -noupdate -expand -group {mux product} /tester_tb/mux_prod/i_b
add wave -noupdate -expand -group {mux product} /tester_tb/mux_prod/i_sel
add wave -noupdate -expand -group {mux product} /tester_tb/mux_prod/o_sltd
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {19346 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 279
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {7626 ps} {40628 ps}
