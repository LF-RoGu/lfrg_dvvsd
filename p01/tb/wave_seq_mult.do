onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /sequential_multiplier_tb/uut_dut/clk
add wave -noupdate /sequential_multiplier_tb/uut_dut/rst
add wave -noupdate /sequential_multiplier_tb/uut_dut/start
add wave -noupdate -radix unsigned /sequential_multiplier_tb/uut_dut/multiplicand
add wave -noupdate -radix unsigned /sequential_multiplier_tb/uut_dut/multiplier
add wave -noupdate -radix unsigned /sequential_multiplier_tb/uut_dut/product
add wave -noupdate /sequential_multiplier_tb/uut_dut/ready
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {553852 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 279
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {453054 ps} {551544 ps}
