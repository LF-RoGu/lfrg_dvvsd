/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: pipo_register_tb
    Description: pipo_register_tb test bench file
    Last modification: 08/02/2021
*/
 /** scale*/
 `timescale 1ns / 1ps
 /** Init module for tb*/
 module pipo_register_tb();
	 
 import p01_pkg::*;
	 
 /** LocalParam*/
 localparam PERIOD = 2;
 
 /** Signal Declaration*/	
 logic clk;
 logic rst;
 pipo_dt i_data, o_data;
 logic enable_w;

 /** Instace of unit under test*/
 pipo_register uut_dut
 (
	 .clk(clk),
	 .rst(rst),
	 .enb(enable_w),
	 .i_data(i_data),
	 .o_data(i_data)
 );
 /** hilos de ejecucion, podemos tener hasta #n hilos de ejecucion*/
 initial begin
	 integer index;
	 
	 /** Set variables to start*/
	 #PERIOD clk = 0;
	 #PERIOD rst = 1;
	 #PERIOD rst = ~rst;
	 #PERIOD rst = ~rst;
	 
	 #PERIOD enable_w = 0;
	 
	 for(index = 0; index < 127; index++)
		 begin
			 i_data = index;
			 #PERIOD enable_w = ~enable_w;
			 #1;
		 end 
	 $stop;
 end 
 
 always begin
	 #(PERIOD/2) clk <= ~clk;
 end 
	 
 endmodule 