if [file exists work] {vdel -all}
vlib work
vlog -f files_pipo_register.f
onbreak {resume}
set NoQuitOnFinish 1
vsim -voptargs=+acc work.pipo_register_tb
do wave_pipo_register.do
run 1100ns
