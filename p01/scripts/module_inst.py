import os
import glob

file_name = "fsm_booth.sv"

os.chdir("..")
os.chdir("src")

f = open(file_name, "r")
inputs = []
outputs = []
parameters = []

for line in f:
    if("input" in line):
        newLine = line.replace("input","")
        newLine = newLine.replace("logic","")
        newLine = newLine.replace(",","")
        newLine = newLine.replace(" ","")
        newLine = newLine.replace("[","")
        newLine = newLine.replace("DW-1","")
        newLine = newLine.replace("]","")
        newLine = newLine.replace("1","")
        newLine = newLine.replace("0","")
        newLine = newLine.replace(":","")
        inputs.append(newLine)
f.close()

f = open(file_name, "r")
for line in f:
    if("output" in line):
        newLine = line.replace("output","")
        newLine = newLine.replace("logic","")
        newLine = newLine.replace(",","")
        newLine = newLine.replace(" ","")
        newLine = newLine.replace("[","")
        newLine = newLine.replace("]","")
        newLine = newLine.replace(":","")
        newLine = newLine.replace("DW-1","")
        outputs.append(newLine)

f.close()

f = open(file_name, "r")
for line in f:
    if("parameter" in line):
        parameters.append(line)
        print(newLine)
    
f.close()

f = open("inst.txt", "w")
f.write("module " + "fsm_booth" + '\n')

if (parameters == []): 
    #no parameters
    print("NO INPUT PARAMETERS")
else:
    f.write("#("+'\n')
    for i in parameters:
        f.write(i)
        if(i == parameters[len(parameters)-1]):
            f.write("")
        else:
            f.write(","+'\n')

f.write("("+'\n')

for i in inputs:
    print(i)
    f.write("   .")
    f.write(i.replace("\n",""))
    f.write("()")
    f.write(","+'\n')

for i in outputs:
    print(i)
    f.write("   .")
    f.write(i.replace("\n",""))
    f.write("()")
    if(i == outputs[len(outputs)-1]):
        f.write("")
    else:
        f.write(","+'\n')


f.write("\n"+");")
f.close()