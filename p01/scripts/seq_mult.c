#include <stdio.h>   /* printf */
#include <string.h>  /* strcat */
#include <stdlib.h>  /* strtol */
#include <math.h>

#define N_BITS 10

const char *integer_to_binary(int x, int n_bits)
{
    static char b[9];
    b[0] = '\0';
    int bits_needed;
    int z;

    bits_needed = pow(2,x)/pow(2,1);

    bits_needed = round(bits_needed);
    
    for (z = pow(2,N_BITS)/2; z > 0; z >>= 1)
    {
        strcat(b, ((x & z) == z) ? "1" : "0");
    }

    return b;
}

int strbin_to_dec(const char * str) {
    unsigned int result = 0;
    for (int i = strlen(str) - 1, j = 0; i >= 0; i--, j++) {
        char k = str[i] - '0'; // we assume ASCII coding
        k <<= j;
        result += k;
    }
    return result;
}

int sequential_multiplier(unsigned int multiplicand,unsigned int multiplier)
{
    char* product;
    unsigned int product_hp, product_lp;
    unsigned int i;

    product_hp = 0;             // initialize product high part with zeroes
    product_lp = multiplier;    // initialize product low part with multiplier

    printf("multiplicand = 0b%s = %i\n\r", integer_to_binary(multiplicand, N_BITS),multiplicand);
    printf("multiplier   = 0b%s = %i \n\r", integer_to_binary(multiplier, N_BITS),multiplier);
    printf("product_hp = 0b%s\n\r", integer_to_binary(product_hp, N_BITS));
    printf("product_lp = 0b%s\n\r", integer_to_binary(product_lp, N_BITS));
    printf("Iteration    	      Step	             Product		  Multiplicand\n\r");


    for(i = 0; i <= N_BITS; i++)
    {
        printf(" %i          ", i);
        if(i > 0)
        {
            
            /* VERIFY IF LSB OF PRODUCT IS 1 */
            if(product_lp & 0b1)
            {
                product_hp = product_hp + multiplicand;
                printf(" Prod = Prod (HP) + Mcand ");
            }
            else
            {
                printf("          NO OP           ");
            }

            /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

            printf("      ");
            printf("%s", integer_to_binary(product_hp, 2*N_BITS));
            printf(" %s", integer_to_binary(product_lp, 2*N_BITS));
            printf("        ");
            printf("     %s", integer_to_binary(multiplicand, 2*N_BITS));
            printf("\n\r");
            
            /* SHIFT RIGHT PRODUCT REGISTER */
            if(product_hp & 0b1)              
            {
                product_hp = product_hp >> 0b1;                              
                product_lp = product_lp >> 0b1;
                product_lp = product_lp + pow(2,N_BITS-1); //last bit masking
            }
            else
            {
                product_hp = product_hp >> 0b1;
                product_lp = product_lp >> 0b1;
            }
            /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

            printf(" %i          ", i);
            printf(" Shift product ");
            printf("                 ");
            printf("%s", integer_to_binary(product_hp, 2*N_BITS));
            printf(" %s", integer_to_binary(product_lp, 2*N_BITS));
            printf("        ");
            printf("     %s", integer_to_binary(multiplicand, 2*N_BITS));
            printf("\n\r");
        }
        else
        {
            printf("   INITIAL VALUES ");
            printf("              ");
            printf("%s", integer_to_binary(product_hp, 2*N_BITS));
            printf(" %s", integer_to_binary(product_lp, 2*N_BITS));
            printf("        ");
            printf("     %s", integer_to_binary(multiplicand, 2*N_BITS));
            printf("\n\r");
        }
    }
 
    /* CONCATENATION OF PRODUCT HP AND LP */
    product = (char*)malloc(sizeof(char)*1000);
    memset(product,'\0',sizeof(product));
    strcat(product, integer_to_binary(product_hp, 2*N_BITS));
    strcat(product, integer_to_binary(product_lp, 2*N_BITS));
    return strbin_to_dec(product);
    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
}

int main(void)
{
    unsigned int multiplicand, multiplier;
    unsigned int product_result;
    multiplicand = 2;
    multiplier = 4;
    product_result = sequential_multiplier(multiplicand,multiplier);
    printf("product = %i", product_result);
    return 0;
}