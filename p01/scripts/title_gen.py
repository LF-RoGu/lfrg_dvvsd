from re import search
import glob
import os
from datetime import date
today = date.today();

os.chdir("..")
os.chdir("src")

all_files = []
for file in glob.glob("*.sv"):
    all_files.append(file)
    print("generated title for %s" % file)
    
    file_name = file
    f = open(file_name, "r")

    if(search('pkg',file_name)):
        file_title = file_name[0:len(file_name)-3]
        file_des = file_name[0:len(file_name)-3] + " package file"
    else:
        file_title = file_name[0:len(file_name)-3]
        file_des = file_name[0:len(file_name)-3] + " source file"
        
    header = ['/* \n',
              '    Authors: Cesar Villarreal @cv4497\n',
              '             Luis Fernando Rodriguez @LF-RoGu\n',
              '    Title: ', file_title,'\n',
              '    Description: ', file_des,'\n',
              '    Last modification: ', today.strftime("%d/%m/%Y"), '\n',
              '*/\n','\n']

    lines = []
    new_line = '..'
    while new_line != '':
        new_line = f.readline()
        lines.append(new_line)
    f.close()
        
    i = 0
    ifdef_line = 0
    for line in lines:
        i += 1
        if(search('pkg',file_name)):
            if(search('`ifndef',line)):
                ifdef_line = i
        else:
            if(search('import',line)):
                ifdef_line = i

    file_object = open(file_name, 'w')

    for line in header:
        file_object.write(line)
        
    i=0
    for line in lines:
        i += 1
        if(i >= ifdef_line):
            file_object.write(line)
        
    file_object.close()



