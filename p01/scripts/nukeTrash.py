import os
import glob

os.chdir("..")
os.chdir("tb")

if(os.path.exists('work') == 1):
    os.chdir("work")
    for file in glob.glob("*"):
        os.remove(file)
    os.chdir("..")
    os.rmdir("work")
    print('removed work directory \n')


for file in glob.glob("*"):
    os.remove(file)
    print('removed '+ file)

for file in glob.glob("*.bak"):
    os.remove(file)
    print('removed '+ file)

for file in glob.glob("*.wlf"):
    os.remove(file)
    print('removed '+ file)

for file in glob.glob("*.vstf"):
    os.remove(file)
    print('removed '+ file)

for file in glob.glob("transcript"):
    os.remove(file)
    print('removed '+ file)

os.chdir("..")

for file in glob.glob("transcript"):
    os.remove(file)
    print('removed '+ file)
