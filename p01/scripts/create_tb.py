import os
import glob

os.chdir("..")
os.chdir("tb")

if(os.path.exists('work') == 1):
    os.chdir("work")
    for file in glob.glob("*"):
        os.remove(file)
    os.chdir("..")
    os.rmdir("work")
    print('removed work directory \n')


for file in glob.glob("*.wlf"):
    os.remove(file)
    print('removed '+ file)

for file in glob.glob("*.vstf"):
    os.remove(file)
    print('removed '+ file)

for file in glob.glob("transcript"):
    os.remove(file)
    print('removed '+ file)
    
tb_files = []
for file in glob.glob("*.sv"):
    tb_files.append(file.replace(".sv",""))

for file in glob.glob("*.do"):
    os.remove(file)
    print('removed '+ file)

for file in tb_files:
    file_name = 'run_'+file.replace("_tb","")+'.do'
    print('created '+file_name)
    f = open(file_name, "w")
    f.write("if [file exists work] {vdel -all}" + '\n')
    f.write("vlib work" + '\n')
    f.write("vlog -f files.f"+ '\n')
    f.write("onbreak {resume}" + '\n')
    f.write("set NoQuitOnFinish 1" + '\n')
    f.write("vsim -voptargs=+acc work." + file + '\n')
    f.write("do wave_" + file + '.do\n')
    f.write("run 500000ps" + '\n')
    f.close()

for file in tb_files:
    file_name = 'wave_'+file.replace("_tb","")+'.do'
    f = open(file_name, "w")
    f.write("onerror {resume}" + "\n")
    f.write("quietly WaveActivateNextPane {} 0" + "\n")
    f.write("WaveRestoreCursors {{Cursor 1} {583 ps} 0}" + "\n")
    f.write("quietly wave cursor active 1" + "\n")
    f.write("configure wave -namecolwidth 150" + "\n")
    f.write("configure wave -valuecolwidth 100" + "\n")
    f.write("configure wave -justifyvalue left" + "\n")
    f.write("configure wave -signalnamewidth 0" + "\n")
    f.write("configure wave -snapdistance 10" + "\n")
    f.write("configure wave -datasetprefix 0" + "\n")
    f.write("configure wave -rowmargin 4" + "\n")
    f.write("configure wave -childrowmargin 2" + "\n")
    f.write("configure wave -gridoffset 0" + "\n")
    f.write("configure wave -gridperiod 1" + "\n")
    f.write("configure wave -griddelta 40" + "\n")
    f.write("configure wave -timeline 0" + "\n")
    f.write("configure wave -timelineunits ps" + "\n")
    f.write("update" + "\n")
    f.write("WaveRestoreZoom {0 ps} {289 ps}" + "\n")
    print('created '+file_name)
    f.close()


