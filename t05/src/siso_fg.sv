
module siso_fg
#(
    parameter DW = 8
)
(
    input        clk,
    input        rst,
    input  logic enable,
    input  logic D,
    output logic Q
);

genvar i;

logic [DW-1:0] serial_outputs;

flip_flop_d
first_ffd
(
    .clk(clk),
    .rst(rst),
    .enable(enable),
    .D(D),
    .Q(serial_outputs[0])
);

generate
    for(i = 1; i < DW-1; i=i+1) begin: siso
            flip_flop_d
            ffd
            (
                .clk(clk),
                .rst(rst),
                .enable(enable),
                .D(serial_outputs[i-1]),
                .Q(serial_outputs[i])
            );
    end
endgenerate

flip_flop_d
last_ffd
(
    .clk(clk),
    .rst(rst),
    .enable(enable),
    .D(serial_outputs[DW-2]),
    .Q(serial_outputs[DW-1])
);

assign Q = serial_outputs[DW-1];

endmodule