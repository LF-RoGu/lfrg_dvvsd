
// Engineer:        Cesar Villarreal
// 
// Create Date:     November 10th, 2020
// Design Name: 
// Module Name:     fifo_pkg
// Project Name:    FIFO
// Target Devices:  DE-10 Standard
// Description:     This is package of the fifo implementation
//
//////////////////////////////////////////////////////////////////////////////////

`ifndef FIFO_PKG_SV
    `define FIFO_PKG_SV
package fifo_pkg;

// TRUE and FALSE definitions
localparam bit TRUE     =1'b1;
localparam bit FALSE    =1'b0;

localparam  W_DATA = 10; //data width of fifo's data
localparam  N_ELEMENTS = 5 //number of elements in fifo
;
localparam  W_ADDR =  N_ELEMENTS+1;  //number of bits in FIFO 
localparam  W_DEPTH = 1<<W_ADDR;

typedef enum logic [1:0]
{
    BAJO = 2'b00, 
    DLY1 = 2'b01, 
    ALTO = 2'b10, 
    DLY2 = 2'b11
} fsm_dbcr_state_e;

typedef logic [W_DATA-1:0] data_t;
typedef logic [W_ADDR-1:0] gray_count_t;
typedef logic [W_ADDR-1:0] addr_t;
typedef logic [W_ADDR:0]   ptr_t;
typedef logic [W_ADDR-1:0] adder_t;
    
endpackage
`endif 

