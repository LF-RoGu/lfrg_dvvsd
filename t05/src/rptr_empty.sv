
module rptr_empty
import fifo_pkg::*;
(
    input               clk,
    input               rst,
    input  logic        inc,
    input  ptr_t        wptr,
    output addr_t       raddr,
    output ptr_t        rptr,
    output logic        empty_flag
);

ptr_t rptr_graycount_w;
addr_t raddr_graycount_w;
ptr_t rgraynext_w;

logic not_full_or_empty_w;

logic empty_val;

gray_counter gray_count
(
    .clk(clk),
    .rst(rst),
    .inc(inc),
    .graynext(rgraynext_w),
    .not_full_or_empty(~empty_flag),
    .addr(raddr_graycount_w),
    .ptr(rptr_graycount_w)
);

always_ff@(posedge clk or negedge rst) begin
    if(!rst)
        empty_flag <= 1'b1;
    else
        empty_flag <= empty_val;
end

assign empty_val = (rgraynext_w == wptr)?1'b1:1'b0;
assign raddr = raddr_graycount_w;
assign rptr = rptr_graycount_w;

endmodule
