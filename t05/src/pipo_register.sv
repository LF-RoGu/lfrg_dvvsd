
// Engineer:        César Villarreal
//                  Luis Fernando Rodriguez
// 
// Create Date:     November 10th, 2020
// Design Name: 
// Module Name:     pipo register
// Project Name:    FIFO
// Target Devices:  DE-10 Standard
// Description:     This is module for the fifo implementation
//
//////////////////////////////////////////////////////////////////////////////////

 
module pipo_register
#(
	parameter DW = 5
) 
(
	//INPUT
	input               	 clk,
	input               	 rst,
	input  logic             enable,
	input  logic [DW - 1:0]	 D,
	output logic [DW - 1:0]	 Q
);

logic [DW-1:0] rgstr_r;
logic [DW-1:0] rgstr_next;

always_ff@(posedge clk or negedge rst) 
begin: rgstr_label
    if(!rst)
        rgstr_r  <= '0;
	else if (enable)
		rgstr_r  <= D;
	else
		rgstr_r  <= rgstr_r;
end:rgstr_label

assign Q = rgstr_r;

endmodule
