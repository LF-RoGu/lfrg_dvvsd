
// Engineer:        César Villarreal
//                  Luis Fernando Rodriguez
// 
// Create Date:     November 13th, 2020
// Design Name: 
// Module Name:     fifo
// Project Name:    FIFO
// Target Devices:  DE-10 Standard
// Description:     This is module for the fifo implementation
//
//////////////////////////////////////////////////////////////////////////////////

module FIFO
import fifo_pkg::*;
(
    input                wclk,
    input                rclk,
    input                rst,
    input  logic         push,
    input  logic         pop,
    input  data_t        DataInput,
    output data_t        DataOutput,
    output logic         full,
    output logic         empty
);
/*******************************************************/

logic full_flag;
logic empty_flag;
logic winc;
logic rinc;
logic [sdp_dc_ram_pkg::W_ADDR-1:0] wdata;
logic disable_we;


/*******************************************************/
ptr_t owptr_iffd1wr_w;
ptr_t offd1wr_iffd2wr_w;
ptr_t offd2wr_irptr_w;

ptr_t orptr_iffd1rd_w;
ptr_t offd1rd_iffd2rd_w;
ptr_t offd2rd_iwptr_w;

addr_t waddr;
addr_t raddr;

logic disable_rd;
logic mem_written;
logic push_rptr;
logic start_rptr;

sdp_dc_ram_if mem_if(); 

sdp_dc_ram  ram(
    .clk_a(wclk),
    .clk_b(rclk),
    .mem_if(mem_if.mem)
);

wptr_full wptr_full_and_inc
(
    .clk(wclk),
    .rst(rst),
    .inc(push),
    .full_flag(full_flag),
    .rptr(offd2rd_iwptr_w),
    .waddr(waddr),
    .wptr(owptr_iffd1wr_w)
);


/* 2FF SYNCHRONIZER FRM RCLK TO WCLK */
pipo_register 
#(
    .DW(W_ADDR+1)
)   sync_to_read_2ff_1
(
    .clk(rclk),
    .rst(rst),
    .enable(winc),
    .D(owptr_iffd1wr_w),
    .Q(offd1wr_iffd2wr_w)
);

pipo_register 
#(
    .DW(W_ADDR+1)
)   sync_to_read_2ff_2
(
    .clk(rclk),
    .rst(rst),
    .enable(winc),
    .D(offd1wr_iffd2wr_w),
    .Q(offd2wr_irptr_w)
);
/**************************************/

/* 2FF SYNCHRONIZER FRM WCLK TO RCLK */
pipo_register
#(
    .DW(W_ADDR+1)
)   sync_to_write_2ff_2
(
    .clk(wclk),
    .rst(rst),
    .enable(rinc),
    .D(offd1rd_iffd2rd_w),
    .Q(offd2rd_iwptr_w)
);

pipo_register
#(
    .DW(W_ADDR+1)
)   sync_to_write_2ff_1
(
    .clk(wclk),
    .rst(rst),
    .enable(rinc),
    .D(orptr_iffd1rd_w),
    .Q(offd1rd_iffd2rd_w)
);
/**************************************/

rptr_empty rptr_empty_and_inc
(
    .clk(rclk),
    .rst(rst),
    .inc(rinc),
    .wptr(offd2wr_irptr_w),
    .raddr(raddr),
    .rptr(orptr_iffd1rd_w),
    .empty_flag(empty_flag)
);

always_ff@(posedge rclk or negedge rst) begin
    if(!rst) begin
        mem_if.cln.rd_addr_b <= '0;
        disable_rd <= 1'b0;
        rinc <= 1'b0;
    end
    else if(pop) begin
        disable_rd <= 1'b1;
        rinc <= 1'b1;
        mem_if.cln.rd_addr_b <= raddr;
    end
    else if(disable_rd == 1'b1) begin
        disable_rd <= 1'b0;
        rinc <= 1'b0;
    end
end

always_ff@(posedge wclk  or negedge rst) begin
    if(!rst) begin
        mem_if.cln.we_a <= 1'b0;
        mem_if.cln.wr_addr_a <= '0;
        mem_if.cln.data_a <= wdata;
        winc <= 1'b0;
    end
    else if(push) begin
        mem_if.cln.we_a <= 1'b1;
        mem_if.cln.wr_addr_a <= waddr;
        mem_if.cln.data_a <= wdata;
        disable_we <= 1'b1;
        winc <= 1'b1;
    end
    else if(disable_we == 1'b1) begin
        disable_we <= 1'b0;
        winc <= 1'b0;
        mem_if.cln.we_a <= 1'b0;
    end
end

assign full = full_flag;
assign empty = empty_flag;
assign wdata = DataInput;
assign DataOutput = mem_if.cln.rd_data_a;

endmodule


/*
module fifo
import fifo_pkg::*;
(
    .clk(),
    .reset(),
    .DataInput(),
    .push(),
    .pop(),
    .DataOutput(),
    .full(),
    .empty()
);
*/