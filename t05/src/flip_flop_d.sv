/*
	Coded by: César Villarreal
			  Luis Fernando Rodriguez
	Date:     September 22, 2020
	Description: 
 */
 
module flip_flop_d
(
	//INPUT
	input                   clk,
	input                   rst,
	input  logic            enable,
	input  logic 			D,
	output logic   			Q
);

logic rgstr_r;

always_ff@(posedge clk or negedge rst) 
begin: rgstr_label
    if(!rst)
        rgstr_r  <= '0;
	else if (enable)
		rgstr_r  <= D;
	else
		rgstr_r  <= rgstr_r;
end:rgstr_label

assign Q = rgstr_r;

endmodule
