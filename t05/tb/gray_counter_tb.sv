
timeunit 1ps; //It specifies the time unit that all the delay will take in the simulation.
timeprecision 1ps;// It specifies the resolution in the simulation.

module gray_counter_tb;
import fifo_pkg::*;

// Parameter Declarations
parameter Word_Length = 16;
parameter Depth_Of_FIFO = 8;
localparam PERIOD = 2;

// Input Ports
bit          clk;
bit          rst;
logic        inc;
logic        not_full_or_empty;
addr_t addr;
ptr_t ptr;
ptr_t graynext;

gray_counter uut
(
    .clk(clk), 
    .rst(rst),
    .inc(inc),
    .not_full_or_empty(not_full_or_empty),
    .graynext(graynext),
    .addr(addr),
    .ptr(ptr)
);
	
/******************** Stimulus *************************/

initial begin 
    not_full_or_empty = 1'b1;
    inc = 1'b0;
	#0 rst = 1'b0;
	#3 rst = 1'b1;
	#0 inc  = 1'b0;

    repeat (16) begin
	#PERIOD inc  = 1'b1;
    #PERIOD inc  = 1'b0;
    end
    #20;
end 

always begin
    #(PERIOD/2) clk<= ~clk;
end

endmodule

 