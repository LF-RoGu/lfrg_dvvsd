onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label wclk /FIFO_TB/wclk
add wave -noupdate -label rclk /FIFO_TB/rclk
add wave -noupdate -label rst /FIFO_TB/rst
add wave -noupdate -label full_flag /FIFO_TB/full
add wave -noupdate -label empty_flag /FIFO_TB/empty
add wave -noupdate -color Gold -label push /FIFO_TB/push
add wave -noupdate -color Gold -label pop /FIFO_TB/pop
add wave -noupdate -expand -group {Memory Interface} -label we_a /FIFO_TB/DUV/mem_if/we_a
add wave -noupdate -expand -group {Memory Interface} -label data_a -radix unsigned /FIFO_TB/DUV/mem_if/data_a
add wave -noupdate -expand -group {Memory Interface} -label rd_data_a -radix unsigned /FIFO_TB/DUV/mem_if/rd_data_a
add wave -noupdate -expand -group {Memory Interface} -label wr_addr_a -radix unsigned /FIFO_TB/DUV/mem_if/wr_addr_a
add wave -noupdate -expand -group {Memory Interface} -label rd_addr_b -radix unsigned /FIFO_TB/DUV/mem_if/rd_addr_b
add wave -noupdate -expand -group {WPTR FULL AND INC} -label wclk /FIFO_TB/DUV/wptr_full_and_inc/clk
add wave -noupdate -expand -group {WPTR FULL AND INC} -label rst /FIFO_TB/DUV/wptr_full_and_inc/rst
add wave -noupdate -expand -group {WPTR FULL AND INC} -label inc /FIFO_TB/DUV/wptr_full_and_inc/inc
add wave -noupdate -expand -group {WPTR FULL AND INC} -label rptr /FIFO_TB/DUV/wptr_full_and_inc/rptr
add wave -noupdate -expand -group {WPTR FULL AND INC} -label waddr -radix unsigned /FIFO_TB/DUV/wptr_full_and_inc/waddr
add wave -noupdate -expand -group {WPTR FULL AND INC} -label wptr /FIFO_TB/DUV/wptr_full_and_inc/wptr
add wave -noupdate -expand -group {WPTR FULL AND INC} -label full_flag /FIFO_TB/DUV/wptr_full_and_inc/full_flag
add wave -noupdate -group {2FF: WCLK->RCLK} -group FF1 -label rclk /FIFO_TB/DUV/sync_to_read_2ff_1/clk
add wave -noupdate -group {2FF: WCLK->RCLK} -group FF1 -label rst /FIFO_TB/DUV/sync_to_read_2ff_1/rst
add wave -noupdate -group {2FF: WCLK->RCLK} -group FF1 -label enable /FIFO_TB/DUV/sync_to_read_2ff_1/enable
add wave -noupdate -group {2FF: WCLK->RCLK} -group FF1 -label D /FIFO_TB/DUV/sync_to_read_2ff_1/D
add wave -noupdate -group {2FF: WCLK->RCLK} -group FF1 -label Q /FIFO_TB/DUV/sync_to_read_2ff_1/Q
add wave -noupdate -group {2FF: WCLK->RCLK} -group FF2 -label rclk /FIFO_TB/DUV/sync_to_read_2ff_2/clk
add wave -noupdate -group {2FF: WCLK->RCLK} -group FF2 -label rst /FIFO_TB/DUV/sync_to_read_2ff_2/rst
add wave -noupdate -group {2FF: WCLK->RCLK} -group FF2 -label enable /FIFO_TB/DUV/sync_to_read_2ff_2/enable
add wave -noupdate -group {2FF: WCLK->RCLK} -group FF2 -label D /FIFO_TB/DUV/sync_to_read_2ff_2/D
add wave -noupdate -group {2FF: WCLK->RCLK} -group FF2 -label Q /FIFO_TB/DUV/sync_to_read_2ff_2/Q
add wave -noupdate -group {2FF: RCLK->WCLK} -group FF1 -label wclk /FIFO_TB/DUV/sync_to_write_2ff_1/clk
add wave -noupdate -group {2FF: RCLK->WCLK} -group FF1 -label rst /FIFO_TB/DUV/sync_to_write_2ff_1/rst
add wave -noupdate -group {2FF: RCLK->WCLK} -group FF1 -label enable /FIFO_TB/DUV/sync_to_write_2ff_1/enable
add wave -noupdate -group {2FF: RCLK->WCLK} -group FF1 -label D /FIFO_TB/DUV/sync_to_write_2ff_1/D
add wave -noupdate -group {2FF: RCLK->WCLK} -group FF1 -label Q /FIFO_TB/DUV/sync_to_write_2ff_1/Q
add wave -noupdate -group {2FF: RCLK->WCLK} -group FF2 -label wclk /FIFO_TB/DUV/sync_to_write_2ff_2/clk
add wave -noupdate -group {2FF: RCLK->WCLK} -group FF2 -label rst /FIFO_TB/DUV/sync_to_write_2ff_2/rst
add wave -noupdate -group {2FF: RCLK->WCLK} -group FF2 -label enable /FIFO_TB/DUV/sync_to_write_2ff_2/enable
add wave -noupdate -group {2FF: RCLK->WCLK} -group FF2 -label D /FIFO_TB/DUV/sync_to_write_2ff_2/D
add wave -noupdate -group {2FF: RCLK->WCLK} -group FF2 -label Q /FIFO_TB/DUV/sync_to_write_2ff_2/Q
add wave -noupdate -expand -group {RPTR EMPTY AND INC} -label rclk /FIFO_TB/DUV/rptr_empty_and_inc/clk
add wave -noupdate -expand -group {RPTR EMPTY AND INC} -label rst /FIFO_TB/DUV/rptr_empty_and_inc/rst
add wave -noupdate -expand -group {RPTR EMPTY AND INC} -label inc /FIFO_TB/DUV/rptr_empty_and_inc/inc
add wave -noupdate -expand -group {RPTR EMPTY AND INC} -label wptr /FIFO_TB/DUV/rptr_empty_and_inc/wptr
add wave -noupdate -expand -group {RPTR EMPTY AND INC} -label raddr /FIFO_TB/DUV/rptr_empty_and_inc/raddr
add wave -noupdate -expand -group {RPTR EMPTY AND INC} -label rptr /FIFO_TB/DUV/rptr_empty_and_inc/rptr
add wave -noupdate -expand -group {RPTR EMPTY AND INC} -label empty_flag /FIFO_TB/DUV/rptr_empty_and_inc/empty_flag
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {421833 ps} 0} {{Cursor 2} {15407 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 311
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {117170 ps}
