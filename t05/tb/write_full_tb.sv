
timeunit 1ps; //It specifies the time unit that all the delay will take in the simulation.
timeprecision 1ps;// It specifies the resolution in the simulation.

module write_full_tb;
import fifo_pkg::*;

// Parameter Declarations
parameter Word_Length = 16;
parameter Depth_Of_FIFO = 8;
localparam PERIOD = 2;

// Input Ports
bit          clk;
bit          rst;
logic        inc;
logic        full_flag;
gray_count_t rptr;
gray_count_t waddr;
gray_count_t wptr;

wptr_full uut
(
    .clk(clk),
    .rst(rst),
    .inc(inc),
    .rptr(rptr),
    .waddr(waddr),
    .wptr(wptr),
    .full_flag(full_flag)
);
	
/******************** Stimulus *************************/

initial begin 
    #0 rptr = 4'b1100;
    #0 inc = 1'b0;
	#0 rst = 1'b0;
	#3 rst = 1'b1;
	#0 inc  = 1'b0;

    repeat (16) begin
	#PERIOD inc  = 1'b1;
    #PERIOD inc  = 1'b0;
    end
    #20;
end 

always begin
    #(PERIOD/2) clk<= ~clk;
end

endmodule

 