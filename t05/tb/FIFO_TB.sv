
timeunit 1ps; //It specifies the time unit that all the delay will take in the simulation.
timeprecision 1ps;// It specifies the resolution in the simulation.

module FIFO_TB;
import fifo_pkg::*;
localparam WFRQ = 60000;
localparam RFRQ = 40000;
localparam PERIOD = 2;
localparam WPERIOD = 1666;  //f = 60000; t = 1/f*100000000;
localparam RPERIOD = 2500;

// Input Ports
bit clk;
bit rst;
logic pop;
logic push;
logic [W_DATA-1:0] DataInput;

// Output Ports
logic full;
logic empty;
logic [W_DATA-1:0] DataOutput;

logic push_oneshot;
logic pop_oneshot;
logic ff1_push_Q_w;
logic ff2_push_Q_w;

clk_divider
#(
    .FREQUENCY(WFRQ),
    .REFERENCE_CLOCK(50_000_000)
)   wclk_div
(
	.clk_FGPA(clk),
    .rst(rst),
    .clk(wclk)
);

clk_divider
#(
    .FREQUENCY(RFRQ),
    .REFERENCE_CLOCK(50_000_000)
)   rclk_div
(
	.clk_FGPA(clk),
    .rst(rst),
    .clk(rclk)
);

/********************* Device Under Verification **************/
FIFO
DUV
(
	// Input Ports
	.wclk(wclk),
	.rclk(rclk),
	.rst(rst),
	.pop(pop),
	.push(push),
	.DataInput(DataInput),
	// Output Ports
	.full(full),
	.empty(empty),
	.DataOutput(DataOutput)
);

/**************************************************************************/

/*----------------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------------------*/
initial begin 
    #0 clk = 1'b0;
	#0 rst = 1'b0;
    #0 pop = 1'b0;
    #0 push = 1'b0;
    #0 DataInput = 4'd5;
	#3 rst = 1'b1;
    #PERIOD rst = 1'b0;
    #PERIOD rst = 1'b1;
	
    repeat (32) begin
    #1666 push = 1'b1;
    #1666 push = 1'b0;
    #1666 DataInput = DataInput +1'b1;
    end


    repeat (3) begin
    #2500 pop = 1'b1;
    #2500 pop = 1'b0;
    end

    #20;
end 


/*--------------------------------------------------------------------*/
always begin
    #(PERIOD/2) clk<= ~clk;
end


endmodule
 
 
/*************************************************************/
/*************************************************************/

 