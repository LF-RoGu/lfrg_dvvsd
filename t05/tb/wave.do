onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label clk /pointers_tb/clk
add wave -noupdate -label wclk /pointers_tb/wclk
add wave -noupdate -label rclk /pointers_tb/rclk
add wave -noupdate -label rst /pointers_tb/rst
add wave -noupdate -label full_flag /pointers_tb/full_flag
add wave -noupdate -label empty_flag /pointers_tb/empty_flag
add wave -noupdate /pointers_tb/push
add wave -noupdate /pointers_tb/pop
add wave -noupdate /pointers_tb/rinc
add wave -noupdate -expand -group {Memory Interface} -label we_a /pointers_tb/mem_if/we_a
add wave -noupdate -expand -group {Memory Interface} -label data_a -radix decimal /pointers_tb/mem_if/data_a
add wave -noupdate -expand -group {Memory Interface} -label rd_data_a -radix decimal /pointers_tb/mem_if/rd_data_a
add wave -noupdate -expand -group {Memory Interface} -label wr_addr_a /pointers_tb/mem_if/wr_addr_a
add wave -noupdate -expand -group {Memory Interface} -label rd_addr_b -radix decimal /pointers_tb/mem_if/rd_addr_b
add wave -noupdate -group {WPTR FULL AND INC} -label wclk /pointers_tb/wptr_full_and_inc/clk
add wave -noupdate -group {WPTR FULL AND INC} -label rst /pointers_tb/wptr_full_and_inc/rst
add wave -noupdate -group {WPTR FULL AND INC} -label inc /pointers_tb/wptr_full_and_inc/inc
add wave -noupdate -group {WPTR FULL AND INC} -label rptr /pointers_tb/wptr_full_and_inc/rptr
add wave -noupdate -group {WPTR FULL AND INC} -label waddr /pointers_tb/wptr_full_and_inc/waddr
add wave -noupdate -group {WPTR FULL AND INC} -label wptr /pointers_tb/wptr_full_and_inc/wptr
add wave -noupdate -group {WPTR FULL AND INC} -label full_flag /pointers_tb/wptr_full_and_inc/full_flag
add wave -noupdate -group {2FF: WCLK->RCLK} -group FF1 -label rclk /pointers_tb/sync_to_read_2ff_1/clk
add wave -noupdate -group {2FF: WCLK->RCLK} -group FF1 -label rst /pointers_tb/sync_to_read_2ff_1/rst
add wave -noupdate -group {2FF: WCLK->RCLK} -group FF1 -label enable /pointers_tb/sync_to_read_2ff_1/enable
add wave -noupdate -group {2FF: WCLK->RCLK} -group FF1 -label D /pointers_tb/sync_to_read_2ff_1/D
add wave -noupdate -group {2FF: WCLK->RCLK} -group FF1 -label Q /pointers_tb/sync_to_read_2ff_1/Q
add wave -noupdate -group {2FF: WCLK->RCLK} -group FF2 -label rclk /pointers_tb/sync_to_read_2ff_2/clk
add wave -noupdate -group {2FF: WCLK->RCLK} -group FF2 -label rst /pointers_tb/sync_to_read_2ff_2/rst
add wave -noupdate -group {2FF: WCLK->RCLK} -group FF2 -label enable /pointers_tb/sync_to_read_2ff_2/enable
add wave -noupdate -group {2FF: WCLK->RCLK} -group FF2 -label D /pointers_tb/sync_to_read_2ff_2/D
add wave -noupdate -group {2FF: WCLK->RCLK} -group FF2 -label Q /pointers_tb/sync_to_read_2ff_2/Q
add wave -noupdate -group {2FF: RCLK->WCLK} -group FF1 -label wclk /pointers_tb/sync_to_write_2ff_1/clk
add wave -noupdate -group {2FF: RCLK->WCLK} -group FF1 -label rst /pointers_tb/sync_to_write_2ff_1/rst
add wave -noupdate -group {2FF: RCLK->WCLK} -group FF1 -label enable /pointers_tb/sync_to_write_2ff_1/enable
add wave -noupdate -group {2FF: RCLK->WCLK} -group FF1 -label D /pointers_tb/sync_to_write_2ff_1/D
add wave -noupdate -group {2FF: RCLK->WCLK} -group FF1 -label Q /pointers_tb/sync_to_write_2ff_1/Q
add wave -noupdate -group {2FF: RCLK->WCLK} -group FF2 -label wclk /pointers_tb/sync_to_write_2ff_2/clk
add wave -noupdate -group {2FF: RCLK->WCLK} -group FF2 -label rst /pointers_tb/sync_to_write_2ff_2/rst
add wave -noupdate -group {2FF: RCLK->WCLK} -group FF2 -label enable /pointers_tb/sync_to_write_2ff_2/enable
add wave -noupdate -group {2FF: RCLK->WCLK} -group FF2 -label D /pointers_tb/sync_to_write_2ff_2/D
add wave -noupdate -group {2FF: RCLK->WCLK} -group FF2 -label Q /pointers_tb/sync_to_write_2ff_2/Q
add wave -noupdate -expand -group {RPTR EMPTY AND INC} -label rclk /pointers_tb/rptr_empty_and_inc/clk
add wave -noupdate -expand -group {RPTR EMPTY AND INC} -label rst /pointers_tb/rptr_empty_and_inc/rst
add wave -noupdate -expand -group {RPTR EMPTY AND INC} -label inc /pointers_tb/rptr_empty_and_inc/inc
add wave -noupdate -expand -group {RPTR EMPTY AND INC} -label wptr /pointers_tb/rptr_empty_and_inc/wptr
add wave -noupdate -expand -group {RPTR EMPTY AND INC} -label raddr /pointers_tb/rptr_empty_and_inc/raddr
add wave -noupdate -expand -group {RPTR EMPTY AND INC} -label rptr /pointers_tb/rptr_empty_and_inc/rptr
add wave -noupdate -expand -group {RPTR EMPTY AND INC} -label empty_flag /pointers_tb/rptr_empty_and_inc/empty_flag
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {469107 ps} 0} {{Cursor 2} {101119 ps} 0}
quietly wave cursor active 2
configure wave -namecolwidth 311
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {464349 ps} {501877 ps}
