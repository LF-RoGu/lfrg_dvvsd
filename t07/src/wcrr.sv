/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: rr
    Description: rr source file
    Last modification: 21/04/2021
*/

import data_pkg::*;

module wcrr
(
	input logic clk,
	input logic rst,
	input logic ack,
	input req_vector_t request_vector,
	output logic valid,
	output req_vector_t grant
);

req_vector_t i_pipo_rv, o_pipo_rv;
req_vector_t i_ppc_pipo_rv, o_ppc_pipo_rv;
req_vector_t i_ppc_rv, o_ppc_rv;
	
req_vector_t mask_logic_vector;
	
assign i_pipo_rv = request_vector;
	
pipo_register pipo_module
(
	.clk(clk),
	.rst(rst),
	.enb( (i_pipo_rv != o_pipo_rv) ? (1'b1):(1'b0) ),
	.i_data(i_pipo_rv),
	.o_data(o_pipo_rv)
);
	
mask_logic mask_logic_module
(
	.clk(clk),
	.rst(rst),
	.grant_vector(o_pipo_rv),
	.ppc_vector(o_ppc_pipo_rv),
	.mask_logic_vector(mask_logic_vector)
);
	
mux_2_1 mux_2_1_module
(
	.i_a(o_pipo_rv),
	.i_b(mask_logic_vector),
	.i_sel( ( (o_ppc_pipo_rv == 'b0) ) ? (OP_A):(OP_B) ),
	.o_sltd(i_ppc_rv)
);
	
ppc ppc_module
(
	.i_data(i_ppc_rv),
	.o_data(o_ppc_rv)
);

one_hot thermometer2oneshot
(
	.i_data(o_ppc_rv),
	.o_data(grant)
);

assign i_ppc_pipo_rv = o_ppc_rv;	
pipo_register ppc_pipo_module
(
	.clk(clk),
	.rst(rst),
	.enb( ((ack == 1'b1)) ? (1'b1):(1'b0) ),
	.i_data(i_ppc_pipo_rv),
	.o_data(o_ppc_pipo_rv)
);

endmodule 