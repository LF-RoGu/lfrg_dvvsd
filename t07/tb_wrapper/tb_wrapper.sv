module tb_wrapper
//FIXME[DEV]: import your own package
import data_pkg::*, tb_wrapper_pkg::*;
(
	input          			clk,
	input          			rst,
	tb_wrapper_if.wrapper  	itf
);

//FIXME[DEV]:define two variable using your RTL datatype
req_vector_t grant;
logic 		 valid;

//Instance your own MDR and cast to the specific datatype to RTL
wcrr dut(
    .clk   	    		( itf.clk                    ),
    .rst        		( rst                        ),
    .ack				( itf.ack 					 ),
    .request_vector		( req_vector_t'(itf.request_vector) ),
    .valid				( valid					 ),
    .grant				( grant )
);

//Cast using this testbench data_t type
assign itf.grant    = data_t'( grant     );
assign itf.valid = valid;
 
          
endmodule
