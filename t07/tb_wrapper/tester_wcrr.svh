// Coder: DSc Abisai Ramirez Perez
// Date:  2020, 28th May
// Disclaimer: This code is not intended to be shared. It is 
// intended to be used for ITESO students at DVVSD Class 2020

class tester_wcrr ;

localparam DW = 10;


`ifdef INVERTED_PULSES
   localparam FALSE  = 1'b1;
   localparam TRUE   = 1'b0;
`else
   localparam TRUE   = 1'b1;
   localparam FALSE  = 1'b0;
`endif

/*
 * Variables
 */
typedef logic signed [DW:0]       data_t;
localparam INC       = 1;
localparam PERIOD = 4; // Minimum value 4
data_t  index;
/** open_file variable*/
integer text_id         ;
/** */
localparam LIMINF =-(2**(DW-1));
localparam LIMSUP = (2**(DW-1))-1;
	
virtual tb_wrapper_if itf   ;


function new(virtual tb_wrapper_if.tstr t);
    itf = t;
endfunction

task automatic init();
   itf.ack     = FALSE;
endtask

task automatic open_file();
    text_id = $fopen("report.txt", "w");
    //$display("File open %0d", text_id);
endtask

//***    Inject data for request vector   ***//
task automatic inject_request_vector();
    $display("Request Vector", $time);
	//task for ack
	
	
endtask 

//***    Inject data with a certain delay   ***//
task automatic inject_all_data_md();
	/** 
	 * LIMINF	-> Set the minimum amount of cycles to test
	 * LIMSUP	-> Set the top amount of cycles to test
	 * INC		-> Set the amount of increments to take
	 */
    for (index =LIMINF; index<LIMSUP ; index=index+INC ) begin

    end
endtask

endclass
