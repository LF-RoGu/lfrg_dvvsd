`timescale 1ns / 1ps
`include "tester_wcrr.svh"

//`include "tb_mdr_pkg.sv"

module tb_wcrr();

import tb_wrapper_pkg::*;
integer text_id;
logic clk;
logic rst;

// Definition of tester
tester_wcrr  t     ;

// Instance of interface
tb_wrapper_if   itf(
.clk(clk)
) ;

tb_wrapper uut(
.clk(clk),
.rst(rst),
.itf (itf.wrapper)
);

initial begin
                t   = new(itf);
                t.init();
                clk = 'd0; 
                rst = 'd1; 
  #(2*PERIOD)   
  		rst = 'd0; 
  #(2*PERIOD)   rst = 'd1; 
                fork
                    t.open_file();
                    t.inject_request_vector();
                    //t.review_output();
                join                
                //t.close_file();
                $stop();
end

always begin
    #(PERIOD/2) clk =!clk; 
end

endmodule
