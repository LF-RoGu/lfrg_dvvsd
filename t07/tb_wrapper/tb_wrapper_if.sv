interface tb_wrapper_if(
input clk
);
import tb_wrapper_pkg::*;

logic ack;
req_vector_t      request_vector;
logic valid;
req_vector_t      grant;

modport tstr (
   input clk,
   output request_vector,
   output ack,
   input valid,
   input grant
);

modport  wrapper(
   input clk,
   input request_vector,
   input ack,
   output valid,
   output grant
);
endinterface
