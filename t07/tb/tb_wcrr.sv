`timescale 1ns / 1ps
module tb_wcrr();
import data_pkg::*;

localparam PERIOD = 2;

logic clk;
logic rst;
logic ack;
	
req_vector_t request_vector;
req_vector_t grant;

wcrr uut
(
    .clk(clk),
    .rst(rst),
    .ack(ack),
    .request_vector(request_vector),
    .valid(),
    .grant(grant)
);

initial begin
	clk = 1'd0;
	rst = 1'd1;
	
	#PERIOD rst = 1'b1;
	#PERIOD rst = 1'b0;
	#PERIOD rst = 1'b1;
	#PERIOD ack = 'b0;
	
	
	#PERIOD request_vector = 'b0;
	
	#PERIOD; #PERIOD; #PERIOD
	
	data2send('d156);
	data2send('d55);
	data2send('d25);
	data2send('d2);
	data2send('d47);
end

always begin
    #(PERIOD/2) clk<= ~clk;
end

task data2send(data_n_t data);
	#PERIOD request_vector = data;
	#PERIOD ack = 1'b0; #PERIOD ack = 1'b1; #PERIOD ack = 1'b0;
	delay(1);
	#PERIOD ack = 1'b0; #PERIOD ack = 1'b1; #PERIOD ack = 1'b0;
	delay(10);
endtask

//delay
task delay(int dly = 1);
    repeat(dly)
        @(posedge clk);
endtask
endmodule
