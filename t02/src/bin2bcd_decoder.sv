/*
 * CODER: Luis Fernando Rodriguez Gtz
 * DATE: 27/Jan/2021
 * PROJECT: bin2bcd_decoder
 */
 
 import bin2bcd_pkg::*;
 
 module bin2bcd_decoder
	 (
		 /** INPUT*/
		 input binary_data_t binary_data,
		 /** OUTPUT*/
		 output bcd_data_t bcd_data_hundreds,
		 output bcd_data_t bcd_data_tens,
		 output bcd_data_t bcd_data_units,
		 output logic sign
		 );
	 
 binary_data_t binary_data_complement2;
	 
 /** Obtain the sign, if the bit is: 
  * 1 -> negative 
  * 0 -> positive*/
 assign sign = (binary_data[7] == 'd1) ? (1'b0) : (1'b1);
	 
 /** Apply the operation for complement 2*/
 assign binary_data_complement2 = (binary_data[7] == 'd1) ? (~binary_data + 1'b1) : (binary_data);
	 
 /** */
 assign bcd_data_hundreds = ( (binary_data_complement2 * 'd10) >> 'd10 );
 /** */
 assign bcd_data_tens = ( ( binary_data_complement2 - (bcd_data_hundreds * 'd100) ) * 'd102 ) >> 'd10;
 /** */
 assign bcd_data_units = ( binary_data_complement2 - ( bcd_data_hundreds * 'd100 ) - ( bcd_data_tens * 'd10 ) );
 
 endmodule 