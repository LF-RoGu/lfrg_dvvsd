/*
 * CODER: Luis Fernando Rodriguez Gtz
 * DATE: 27/Jan/2021
 * PROJECT: bin2bcd
 */
 
 `ifndef BIN2BCD_PKG_SV
 	`define BIN2BCD_PKG_SV
 package bin2bcd_pkg;
	 /** Parameters*/
	 localparam PARAM_DW = 8;
	 localparam PARAM_DW_BCD = 4;
	 localparam PARAM_DW_SEGMENT_DISPLAY = 7;
	 
	 /** Data Types*/
	 typedef logic [PARAM_DW - 1:0] binary_data_t;
	 typedef logic [PARAM_DW_BCD - 1:0] bcd_data_t;
	 typedef logic [PARAM_DW_SEGMENT_DISPLAY - 1:0] data2display_t;
 
 endpackage
 `endif