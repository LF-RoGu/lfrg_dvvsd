/*
 * CODER: Luis Fernando Rodriguez Gtz
 * DATE: 27/Jan/2021
 * PROJECT: bin2bcd
 */
 
 import bin2bcd_pkg::*;
 
 module bin2bcd
	 (
		 /** INPUT*/
		 input binary_data_t binary_data,
		 /** OUTPUT*/
		 output data2display_t data2display_hundreds,
		 output data2display_t data2display_tens,
		 output data2display_t data2display_units,
		 output logic sign
		 );
	 
 bcd_data_t data2display_hundreds_w, data2display_tens_w, data2display_units_w;
	 
	 
 bin2bcd_decoder bin2bcd_decoder_top
 (
	 .binary_data(binary_data),
	 .bcd_data_hundreds(data2display_hundreds_w),
	 .bcd_data_tens(data2display_tens_w),
	 .bcd_data_units(data2display_units_w),
	 .sign(sign)
	 );
	 
 bin2bcd_segments bin2bcd_segments_hundreds_top
 (
	 .bcd_data(data2display_hundreds_w),
	 .data2display(data2display_hundreds)
	 );
 bin2bcd_segments bin2bcd_segments_tens_top
 (
	 .bcd_data(data2display_tens_w),
	 .data2display(data2display_tens)
	 );
 bin2bcd_segments bin2bcd_segments_units_top
 (
	 .bcd_data(data2display_units_w),
	 .data2display(data2display_units)
	 );
	 
 endmodule 