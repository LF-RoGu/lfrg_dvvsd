/*
 * CODER: Luis Fernando Rodriguez Gtz
 * DATE: 27/Jan/2021
 * PROJECT: bin2bcd
 */
 
  import bin2bcd_pkg::*;
 
 module bin2bcd_segments
	 (
		 /** INPUT*/
		 input bcd_data_t bcd_data,
		 /** OUTPUT*/
		 output data2display_t data2display
		 );
	 
 /** Cambiar esto a un switch case*/
 always_comb begin
 
 	case(bcd_data)		 //   gfedcba
	 	4'b0000: begin
		 	data2display = 7'b1000000; /** 0*/
	 	end 
	 	4'b0001: begin
		 	data2display = 7'b1111001; /** 1*/
	 	end 
	 	4'b0010: begin
		 	data2display = 7'b0100100; /** 2*/
	 	end 
	 	4'b0011: begin
		 	data2display = 7'b0110000; /** 3*/
	 	end 
	 	4'b0100: begin
		 	data2display = 7'b0011001; /** 4*/
	 	end 
	 	4'b0101: begin
		 	data2display = 7'b0010010; /** 5*/
	 	end 
	 	4'b0110: begin
		 	data2display = 7'b0000010; /** 6*/
	 	end 
	 	4'b0111: begin
		 	data2display = 7'b1111000; /** 7*/
	 	end 
	 	4'b1000: begin
		 	data2display = 7'b0000000; /** 8*/
	 	end 
	 	4'b1001: begin
		 	data2display = 7'b0011000; /** 9*/
	 	end 
	 	default: begin
		 	data2display = 7'b1000000; /** default*/
	 	end 
 	endcase 
 end 
 endmodule